<?php 

require_once "func.php";

if (isset($_GET["link"])){
	$oGet = new cGet($_GET["link"]);
}

if (isset($_POST["data"])){
	$oPost = new cPost($_POST["data"]);
}



//==============================================================================
class cGet{

//==============================================================================
public function __construct($sLink){
	global $oConf;
	$bFound = 1;
	if ($sLink){
		$oUser = doSQL("SELECT", "CMSSession",
			"oUser",
			array(array("sSessionID", "=", $sLink))
		);
	if (isset($oUser[0]["oUser"])){
			$oUser = json_decode($oUser[0]["oUser"], 1);
			$sAction = $oUser["sAction"];
		} else {
			$bFound = 0;
		}
	} else {
		$bFound = 0;
	}
	if (!$bFound){
		$aURL = explode("&", base64_decode($sLink));
		if ($aURL[1]){
			$sAction = explode("=", $aURL[0])[1];
		}
		if (isset($sAction)){
			$bFound = 1;
		}
	}
	if (!$bFound){
		//not found
		header("Location: " . $oConf["oSystem"]["sWebRoot"]);
		return;
	}
	//found
	$sLocation = $oConf["oSystem"]["sWebRoot"];
	switch ($sAction){
		case "gridexport":
			$sSession = explode("=", $aURL[2])[1];
			$sFormName = explode("=", $aURL[3])[1];
			$iTabNo = intval(explode("=", $aURL[4])[1]);
			$oGrid = new cGrid();
			$oGrid->export($sFormName, $iTabNo, $sSession);
		break;
		
		case "passwordreset":
			if ((isset($oUser["sDevice"])) && ($oUser["sDevice"] == "app")){
				$sLocation .= "/app";
			}
			$sRequest = base64_encode("passwordreset|" . 	$sLink);
			header("Location: " . $sLocation . "?a=" . $sRequest);
		break;
		
		case "registeractivate":
			$sRequest = base64_encode("registeractivate|" . $sLink);
			header("Location: " . $sLocation . "?a=" . $sRequest);
		break;
		
		case "adduseractivate":
			$sRequest = base64_encode("adduseractivate|" . $sLink);
			header("Location: " . $sLocation . "?a=" . $sRequest);
		break;
		
	}
}



}



//==============================================================================
class cPost{

//==============================================================================
public function __construct($sData){
	$sData =  base64_decode($sData);
	$sData =  rawurldecode($sData);
	$oData = json_decode($sData, 1);
	$oUser = doSQL("SELECT", "CMSSession",
		"iUserID, oUser",
		array(array("sSessionID", "=", $oData["sSessionID"]))
	);
	$iUserID = 0;
	if (isset ($oUser[0])){
		$iUserID = $oUser[0]["iUserID"]; 
		$oUser = json_decode($oUser[0]["oUser"], 1);
	}
	doSQL("INSERT", "CMSLog",
		"iUserID, sAction, sPage, sParameters, dDateTime, sIPAddress",
		0,
		array($iUserID, $oData["sAction"],
		"", $sData,
		getTimeStamp(), getVar("conf","oUser","sFromIP"))
	);
	$oData["oUser"] = $oUser;
	
	switch ($oData["sAction"]){
		case "activate":
			$oInit = new cInit();
			$oData = $oInit->registerActivate($oData);
		break;
		
		case "createpostsavedraft":
			$oCreatePost = new cCreatePost();
			$oData = $oCreatePost->savedraft($oData);
		break;
		
		case "createpostpostnow":
			$oCreatePost = new cCreatePost();
			$oData = $oCreatePost->postnow($oData);
		break;
		
		case "docs":
			$oInit = new cInit();
			$oData = $oInit->docs($oData);
		break;
		
		case "forgotpassword":
			$oLogin = new cLogin();
			$oData = $oLogin->forgotPassword($oData);
		break;
		
		case "grid":
			$oGrid = new cGrid();
			$oData = $oGrid->data($oData);
		break;
		
		case "homechangepassword":
			$oInit = new cInit();
			$oData = $oInit->changepassword($oData);
			break;
		
		case "init":
			$oInit = new cInit();
			$oData = $oInit->init($oData);
		break;
		
		case "login":
			$oLogin = new cLogin();
			$oData = $oLogin->login($oData);
		break;
		
		case "logout":
			$oLogin = new cLogin();
			$oData = $oLogin->logout($oData);
		break;
		
		case "logocrop":
			$oInit = new cInit();
			$oData = $oInit->logoCrop($oData);
		break;

		case "mypostsactiveduplicatetodrafts":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->activeduplicatetodrafts($oData);
		break;
		
		case "mypostsactiveduplicatetodrafts1":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->activeduplicatetodrafts($oData);
		break;
		
		case "mypostsactivemarkascomplete":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->activemarkascomplete($oData);
		break;

		case "mypostsactivemarkasdraft":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->activemarkasdraft($oData);
			break;
			
		case "mypostsactivesave":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->activesave($oData);
		break;
			
		case "mypostsdraftsdelete":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->draftsdelete($oData);
		break;
		
		case "mypostsdraftsduplicate":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->draftsduplicate($oData);
		break;
		
		case "mypostsdraftspostnow":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->draftspostnow($oData);
		break;
		
		case "mypostsdraftssave":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->draftssave($oData);
		break;
			
		case "mypostshistorydelete":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->historydelete($oData);
		break;
		
		case "mypostshistorydelete1":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->historydelete($oData);
		break;
		
		case "mypostshistoryduplicatetodrafts":
			$oMyPosts = new cMyPosts();
			$oData = $oMyPosts->historyduplicatetodrafts($oData);
		break;
		
		case "profile":
			$oInit = new cInit();
			$oData = $oInit->profile($oData);
		break;
		
		case "profilesave":
			$oInit = new cInit();
			$oData = $oInit->profilesave($oData);
		break;
		
		case "register":
			$oInit = new cInit();
			$oData = $oInit->register($oData);
		break;
				
		case "resetpassword":
			$oLogin = new cLogin();
			$oData = $oLogin->resetPassword($oData);
		break;
				
		case "usersaddsave":
			$oUsers = new cUsers();
			$oData = $oUsers->addsave($oData);
		break;
				
		case "usersdelete":
			$oUsers = new cUsers();
			$oData = $oUsers->delete($oData);
		break;
				
		case "userseditsave":
			$oUsers = new cUsers();
			$oData = $oUsers->editsave($oData);
		break;
				
	}
	echo base64_encode(json_encode($oData, 1));
}
	
	
}




//==============================================================================
class cInit{

//==============================================================================
public function api($sFolder = "", $oParams = array(), $sToken = "", 
	$sMethod = ""){
error_log("xxxxxxxxxxx API PARAMETERS: " . $sFolder);	
error_log(json_encode($oParams,1));
	global $oConf;
	if ($oConf["oSystem"]["iIsSSL"]){
		$sURL = "https";
	} else {
		$sURL = "http";
	}
	$sURL .= "://" .$_SERVER["HTTP_HOST"] . ":" .
		$oConf["oSystem"]["iAPIPort"] . "/" . $oConf["oSystem"]["sAPIFolder"] . 
		"/" . $sFolder;
	
error_log("url ".$sURL);
	$sJSON = json_encode($oParams);
  $oCURL = curl_init($sURL);
  if ($sToken){
  	$aHeader = array(
			"Content-Type: application/json",
  		"Access-Control-Allow-Origin: *",
			"Authorization: Basic " . base64_encode("user:" . $sToken)
		);
		curl_setopt($oCURL,CURLOPT_HTTPHEADER, $aHeader);
  }
  if ($sMethod){
  	curl_setopt($oCURL, CURLOPT_CUSTOMREQUEST, $sMethod);
  }
	curl_setopt($oCURL,CURLOPT_POST, count($oParams));
	curl_setopt($oCURL,CURLOPT_POSTFIELDS, $sJSON);
	curl_setopt($oCURL,CURLOPT_RETURNTRANSFER, 1);
	$oJSON = json_decode(curl_exec($oCURL), 1);
	
error_log("xxxxxxxxxxx API RESPONSE");	
error_log(json_encode($oJSON,1));	
	curl_close($oCURL);
	return $oJSON;
}



//==============================================================================
public function changepassword($oData){
		$sCurrent = $oData["aValues"][1]["sValue"];
		$sNew = $oData["aValues"][2]["sValue"];
		$oParams = array(
			"old_password" => $sCurrent,
			"new_password" => $sNew
		);
	$oUser = doSQL("SELECT", "CMSSession",
		"oUser",
		array(array("sSessionID", "=", $oData["sSessionID"]))
	);
	if (!isset($oUser[0])){
		return $oData;
	}
	$oUser = json_decode($oUser[0]["oUser"], 1);
	
	$sToken = $oUser["sToken"];
error_log($sToken);	
	$oResult = $this->api("users/changePassword", $oParams, $sToken);
	if ($oResult["response_code"] != -1){
		$oData["sError"] = $oResult["message"];
	} else {
		//success
		$oData["sMessage"] = "oPasswordChangeSuccess";
	}
	$oData["resp"] = $oResult;
	$oData["us"] = $oUser;
	return $oData;
}

	
	
//==============================================================================
public function docs($oData){
	$iObjNo = $oData["aValues"][1]["iObjNo"];
	if ($oData["aValues"][0]["sValue"] == "terms"){
		$sDoc = "sDocsTermsOfUse";
	}
	if ($oData["aValues"][0]["sValue"] == "privacy"){
		$sDoc = "sDocsPrivacyPolicy";
	}
	$oData = doSQL("SELECT", "CMSApp",
		"sValue",
		array(array("sParameter", "=", $sDoc, "AND"),
		array("sCategory", "=", "oLoggedOut"))
	);
	$oData = array(
		"sText" => $oData[0]["sValue"],
		"iObjNo" => $iObjNo
	);
	return $oData;
}

	
	
//==============================================================================
public function init($oData){
	$iTimeClient = $oData["iTimeClient"];
	$oData = $oData["aValues"];
	$sSessionID = "";
	if (isset($oData["sSessionID"])){
		$aSession = doSQL("SELECT", "CMSSession",
			"iSessionID, iUserID, oUser, sSessionID",
			array(array("sSessionID", "=", $oData["sSessionID"]))
		);
		if (isset($aSession[0])){
			$sSessionID = $aSession[0]["sSessionID"];
			$oData = json_decode($aSession[0]["oUser"], 1);
			$oData["iTimeClient"] = $iTimeClient;
			$oData["iTimeServer"] = getTimeStamp(4);
			doSQL("UPDATE", "CMSSession",
				"oUser",
				array(array("iSessionID", "=", $aSession[0]["iSessionID"])),
				array(json_encode($oData, 1))
			);
		}
	}
	if (!$sSessionID){
		if (isset($oData["sSessionID"])){
			unset($oData["sSessionID"]);
		}
		$sSessionID = substr(str_replace(array("+", "="), "",
			base64_encode(sha1(rand()) . sha1(rand()))), 0, 128);
		doSQL("INSERT", "CMSSession",
			"sSessionID, iTimeClient, iTimeServer, iUserID, oUser",
			0,
			array($sSessionID, $oData["iTimeClient"], 
				getTimeStamp(4),
			"0", json_encode($oData, 1))
		);
		$oData["sSessionID"] = $sSessionID;
	} else {
		if (intval($aSession[0]["iUserID"]) > 0){
			$oData = $this->loggedIn(json_decode($aSession[0]["oUser"], 1));
		} else {
			$oData = $this->loggedOut();
		}
	}
	return $oData;
}

	

//==============================================================================
public function loggedIn($oUser){
	if (!isset($oUser["sToken"])){
		$oUser = doSQL("SELECT", "CMSSession",
			"oUser",
			array(array("sSessionID", "=", $oUser["session"]))
		);
		$oUser = json_decode($oUser[0]["oUser"], 1);
	}
	$oData=array(
		"sAction" => "loggedin",
		"oInit" => array(),
		"oError" => array(),
		"aPages" => array(),
	);
	$oApp = doSQL("SELECT", "CMSApp",
		"sCategory, sSubCategory, sParameter, sValue",
		array(array("sCategory", "=", "oLoggedIn", "OR"),
		array("sCategory", "=", "oGeneral"))
	);
	$iSize = sizeof($oApp);
	for ($iI = 0; $iI < $iSize; $iI++){
		$oRecord = $oApp[$iI];
		$sType = substr($oRecord["sParameter"], 0, 1);
		switch ($sType){
			case "a":
			case "o":
				$oValue = json_decode($oRecord["sValue"]);
				break;
			case "s":
				$oValue = $oRecord["sValue"];
				break;
			case "i":
				$oValue = intval($oRecord["sValue"]);
				break;
		}
		if ((($oRecord["sCategory"] == "oLoggedIn")
			|| ($oRecord["sCategory"] == "oGeneral"))
			&& ($oRecord["sSubCategory"] == "oInit")){
			$oData["oInit"][$oRecord["sParameter"]] = $oValue;
		}
		if (($oRecord["sCategory"] == "oLoggedIn")
			&& ($oRecord["sSubCategory"] == "oError")){
			$oData["oError"][$oRecord["sParameter"]] = $oValue;
		}
	}
	$oData["oUser"] = $oUser;
	$aAddresses = doSQL("SELECT", "organisation_places",
		"id, address_line1, address_line2, region_id, main",
		array(array("organisation_id", "=", $oUser["iOrganisationID"], "AND"),
		array("deleted_at", "IS", "null"))
	);
	$oData["aRegions"] = doSQL("SELECT", "regions",
		"id AS iID, parent_id AS iParentID, name AS sValue, " .
		"region_type AS iRegionType",
		0, 0,
		"ORDER BY name"
	);
	$oData["oUser"]["aAddresses"] = $aAddresses;
//	$oData["oUser"]["oGeo"] = $this->geoIP();
	$aLog = doSQL("SELECT", "CMSLog",
		"dDateTime, sIPAddress",
		array(array("iUserID", "=", $oUser["iUserID"], "AND"),
		array("sAction", "=", "login")),
		0,
		"ORDER BY iLogID DESC LIMIT 2"
	);
	if (isset($aLog[1])){
		$aLog = $aLog[1];
	} else {
		$aLog = 0;
	}
	if ($aLog){
//		$oData["oUser"]["oLastLogin"]["oGeo"] = getGeoIP($aLog["sIPAddress"]);
		$oData["oUser"]["oLastLogin"]["sDate"] = $aLog["dDateTime"];
	} else {
		$oData["oUser"]["oLastLogin"]["oGeo"] = array(
			"sCountry" => "",
			"sCity" => ""
		);
		$oData["oUser"]["oLastLogin"]["sDateTime"] = "First Login";
	}
	$oData["oInit"]["aUpload"] = array(
		"sSession" => ini_get("session.upload_progress.name"),
		"iSpeed" => 10
	);
	$aPosts = doSQL("SELECT", "posts",
		"need_type",
		array(array("organisation_id", "=", $oUser["iOrganisationID"], "AND"),
		array("deleted_at", "IS", "null"))
	);
	$iSize = sizeof($aPosts);
	$aTypes = array(0, 0, 0);
	for ($iI = 0; $iI < $iSize; $iI++){
		switch ($aPosts[$iI]["need_type"]){
			case "0":
				$aTypes[0]++;
				break;
			case "1":
				$aTypes[1]++;
				break;
			case "2":
				$aTypes[2]++;
				break;
		}
	}
	$aCommitments = doSQL("SELECT", "commitments,posts" .
		"|A.post_id=B.id",
		"A.id, A.post_id, B.title, B.need_type",
		array(array("B.organisation_id", "=", $oUser["iOrganisationID"], "AND"),
		array("B.deleted_at", "IS", "null"))
	);
	$iNumDaysMember = doSQL("SELECT", "organisations",
		"created_at",
		array(array("id", "=", $oUser["iOrganisationID"]))
	);
	$oDate1 = new DateTime("now");
	$oDate2 = new DateTime($iNumDaysMember[0]["created_at"]);
	$oInterval = $oDate1->diff($oDate2);
	$iNumDaysMember = $oInterval->days;
	$oData["oDashboard"]= array(
		"aPostTypes" => $aTypes,
		"aCommitments" => $aCommitments,
		"iNumDaysMember" => $iNumDaysMember
	);
	return $oData;
}



//==============================================================================
public function loggedOut(){
	global $oConf;
	$oData = array(
			"sAction" => "init",
			"oInit" => array(),
			"oError" => array()
	);
	$oApp = doSQL("SELECT", "CMSApp",
			"sCategory, sSubCategory, sParameter, sValue",
			array(array("sCategory", "=", "oLoggedOut", "OR"),
					array("sCategory", "=", "oGeneral"))
			);
	$iSize = sizeof($oApp);
	for ($iI = 0; $iI < $iSize; $iI++){
		$oRecord = $oApp[$iI];
		$sType = substr($oRecord["sParameter"], 0, 1);
		switch ($sType){
			case "a":
			case "o":
				$oValue = json_decode($oRecord["sValue"]);
				break;
			case "s":
				$oValue = $oRecord["sValue"];
				break;
			case "i":
				$oValue = intval($oRecord["sValue"]);
				break;
		}
		if ((($oRecord["sCategory"] == "oLoggedOut")
				|| ($oRecord["sCategory"] == "oGeneral"))
				&& ($oRecord["sSubCategory"] == "oInit")){
					$oData["oInit"][$oRecord["sParameter"]] = $oValue;
		}
		if (($oRecord["sCategory"] == "oLoggedOut")
				&& ($oRecord["sSubCategory"] == "oError")){
					$oData["oError"][$oRecord["sParameter"]] = $oValue;
		}
	}
	$oData["aRegions"] = doSQL("SELECT", "regions",
			"id AS iID, parent_id AS iParentID, name AS sValue,
	region_type AS iRegionType",
			0, 0,
			"ORDER BY name"
			);
	$oData["oUser"] = $oConf["oUser"];
	return $oData;
}



//==============================================================================
public function logoCrop($oData){
	$sFilename = $oData["aValues"][0]["sLogo"];
	$oData["iObjNo"] = $oData["aValues"][1]["iObjNo"];
	$oMime = getimagesize("tmp/" . $sFilename);
	$oFile = pathinfo("tmp/" . $sFilename);
	$sExt = strtolower($oFile["extension"]);
	$sBase = $oFile["filename"];
	$oData["oFile"]  = $oFile;
	$oData["oMime"]  = $oMime;
	$iW = $oMime[0];
	$iH = $oMime[1];
	if (!$iW){
		return $oData;
	}
	$iSquare = 300;
	if ($iW > $iH){
		$iWN = $iSquare;
		$iScale = $iWN / $iW; 
		$iHN = intval($iH * $iScale);
	} else {
		$iHN = $iSquare;
		$iScale = $iHN / $iH; 
		$iWN = intval($iW * $iScale);
	}
	$oData["iWN"] = $iWN;
	$oData["iHN"] = $iHN;
	switch ($oMime["mime"]){
		case "image/jpeg":
			$oSrc = imagecreatefromjpeg("tmp/" . $sFilename);
		break;

		case "image/png":
			$oSrc = imagecreatefrompng("tmp/" . $sFilename);
		break;
	}
	$oDst = imagecreatetruecolor($iWN, $iHN);
	imagecopyresampled($oDst, $oSrc, 0, 0, 0, 0, $iWN, $iHN, $iW, $iH);
	$oDst1 = imagecreatetruecolor(
			$iSquare, $iSquare);
	$sWhite = imagecolorallocate($oDst1, 255, 255, 255);
	imagefill($oDst1, 0, 0, $sWhite);
	if ($iW >= $iH){
		$iTop = ($iSquare - $iHN) / 2;
		$iLeft = 0;
	} else {
		$iLeft = ($iSquare - $iWN) / 2;
		$iTop = 0;
	}
	imagecopy($oDst1, $oDst, $iLeft, $iTop, 0, 0, $iWN, $iHN);
	$sNewFilename = "tmp/" . $oFile["filename"] . "-R." . $sExt;
	switch ($oMime["mime"]){
		case "image/jpeg":
			imagejpeg($oDst1, $sNewFilename);
		break;
	
		case "image/png":
			imagepng($oDst1, $sNewFilename);
		break;
	}
	$oData["sNewFilename"] = $sNewFilename;
	return $oData;
}



//==============================================================================
public function profile($oData){
	$sPicBox = "picbox";
	$oUser = doSQL("SELECT", "CMSSession",
		"oUser",
		array(array("sSessionID", "=", $oData["sSessionID"]))
	);
	$oUser = json_decode($oUser[0]["oUser"], 1);
	$iUserID = $oUser["iUserID"];
	$iOrganisationID = $oUser["iOrganisationID"];
	$oUser = doSQL("SELECT", "users,organisations" . 
		"|A.organisation_id=B.id",
		"A.name, A.surname, A.email, B.name as sOrganisationName, " .
		"B.category as iOrganisationCategoryID, organisation_type, B.description, " .
		"B.contact_number, B.contact_email, B.website_link, B.facebook_link," . 
		"B.twitter_handle, B.instagram_link, B.logo, B.contact_person",
		array(array("A.id", "=", $iUserID))
	);
	$aHours = doSQL("SELECT", "organisation_hours",
		"id, days_of_week, opening_time, closing_time",
		array(array("organisation_id", "=", $iOrganisationID, "AND"),
		array("deleted_at", "IS", "null"))
	);
	$aPlaces = doSQL("SELECT", "organisation_places,regions" . 
		"|A.region_id=B.id",
		"A.id, A.address_line1, A.address_line2, A.region_id, " .
		"B.parent_id, A.main",
		array(array("A.organisation_id", "=", $iOrganisationID, "AND"),
		array("A.deleted_at", "IS", "null"))
	);
	$oLogo = 0;
	if ($oUser[0]["logo"]){
		global $oConf;
		$sDir = $oConf["oFiles"]["sDirectoryData"] . "/" . 
			$oConf["oFiles"]["sCompanyLogoFolder"];
		$oLogo = $sDir . "/" .$oUser[0]["logo"];
		if (file_exists($oLogo)){
			$oInfo = getimagesize($oLogo);
			$oLogo = array("sB64" => base64_encode(file_get_contents($oLogo)),
				"oInfo" => $oInfo);
		}
	}
	$oData["oData"] = array(
		"oLogo" => $oLogo,
		"oUser" => $oUser[0],
		"aPlaces" => json_encode($aPlaces, 1),
		"aHours" => json_encode($aHours, 1),
		"sPicBox" => $sPicBox);

	return $oData;
	doSQL("SELECT", "xxxx");
}



//==============================================================================
public function profilesave($oData){
	$oUser = doSQL("SELECT", "CMSSession",
		"oUser",
		array(array("sSessionID", "=", $oData["sSessionID"]))
	);
	$iTabNo = $oData["aValues"][1]["iTabNo"];
	switch ($iTabNo){
		case 1:
			$sName = $oData["aValues"][2]["sValue"];
			$sSurname = $oData["aValues"][3]["sValue"];
			$sEmail = $oData["aValues"][4]["sValue"];
			if (isset($oUser[0])){
				$iUserID = json_decode($oUser[0]["oUser"], 1)["iUserID"];
				doSQL("UPDATE", "users",
					"name, surname, email",
					array(array("id", "=", $iUserID)),
					array($sName, $sSurname, $sEmail)
				);
				$oData["sMessage"] = "oRecordEdited";
			}
		break;
		
		case 2:
			$sName = $oData["aValues"][2]["sValue"];
			$iCategory = intval($oData["aValues"][3]["sValue"]);
			$iType = intval($oData["aValues"][4]["sValue"]);
			$sDesc = $oData["aValues"][5]["sValue"];
			if (isset($oUser[0])){
				$iOrganisationID = 
					json_decode($oUser[0]["oUser"], 1)["iOrganisationID"];
				doSQL("UPDATE", "organisations",
					"name, category, organisation_type, description",
					array(array("id", "=", $iOrganisationID)),
					array($sName, $iCategory, $iType, $sDesc)
				);
				$oData["sMessage"] = "oRecordEdited";
			}
		break;
		
		case 3:
			$sPerson = $oData["aValues"][2]["sValue"];
			$sEmail = $oData["aValues"][3]["sValue"];
			$sNumber = $oData["aValues"][4]["sValue"];
			if (isset($oUser[0])){
				$iOrganisationID = 
					json_decode($oUser[0]["oUser"], 1)["iOrganisationID"];
				doSQL("UPDATE", "organisations",
					"contact_person, contact_email, contact_number",
					array(array("id", "=", $iOrganisationID)),
					array($sPerson, $sEmail, $sNumber)
				);
				$oData["sMessage"] = "oRecordEdited";
			}
		break;
		
		case 4:
			$iI = 2;
			$aPlaces = array();
			while (isset($oData["aValues"][$iI])){
				$oValue = $oData["aValues"][$iI]["sValue"];
				if ($oValue[4] != -1){
					$iRegion = $oValue[4];
				} else {
					$iRegion = $oValue[3];
				}
				if ($oValue[7] == 1){
					$iMain = 1;
				} else {
					$iMain = 0;
				}
				$sFields = "region_id, latitude, longitude, main, " .
				"address_line1, address_line2";
				$aValues = array($iRegion, 0 , 0, $iMain, 
					$oData["aValues"][$iI]["sValue"][5],
					$oData["aValues"][$iI]["sValue"][6]
				);
				if (isset($oUser[0])){
					$iOrganisationID = 
						json_decode($oUser[0]["oUser"], 1)["iOrganisationID"];
				}
				if ($oData["aValues"][$iI]["sValue"][0]){
					doSQL("UPDATE", "organisation_places",
						$sFields,
						array(array("id", "=", $iOrganisationID)),
						$aValues);
				} else {
					$aValues[] = $iOrganisationID;
					doSQL("INSERT","organisation_places",
						$sFields . ", organisation_id",
						0,
						$aValues
					);
				}
				$iI++;
			}
			$oData["sMessage"] = "oRecordEdited";
			$oData["aAddresses"] = doSQL("SELECT", "organisation_places",
				"id, address_line1, address_line2, region_id, main",
				array(array("organisation_id", "=", $iOrganisationID, "AND"),
				array("deleted_at", "IS", "null"))
			);
		break;
		
		case 5:
			$sWebsite = $oData["aValues"][2]["sValue"];
			$sFacebook = $oData["aValues"][3]["sValue"];
			$sTwitter = $oData["aValues"][4]["sValue"];
			$sInstagram = $oData["aValues"][5]["sValue"];
			if (isset($oUser[0])){
				$iOrganisationID = 
					json_decode($oUser[0]["oUser"], 1)["iOrganisationID"];
				doSQL("UPDATE", "organisations",
					"website_link, facebook_link, twitter_handle, instagram_link",
					array(array("id", "=", $iOrganisationID)),
					array($sWebsite, $sFacebook, $sTwitter, $sInstagram)
				);
				$oData["sMessage"] = "oRecordEdited";
			}
		break;
		
	}
	return $oData;
}



//==============================================================================
public function registerActivate($oData){
	$sLink = $oData["aValues"][0]["sLink"];
	$oUser = doSQL("SELECT", "CMSSession",
		"oUser",
		array(array("sSessionID", "=", $sLink))
	);
	if (isset($oUser[0])){
		$oUser = json_decode($oUser[0]["oUser"], 1);
		$sEmailAddress = $oUser["sEmailAddress"];
		doSQL("UPDATE", "CMSSession",
			"sSessionID",
			array(array("sSessionID", "=", $sLink)),
			array("")
		);
		doSQL("UPDATE", "users",
			"is_active, link",
			array(array("email", "=", $sEmailAddress)),
			array(1, "")
		);
		$oData = array("sMessage" => "oRegisterActivateSuccess");
	} else {
		$oData = array("sMessage" => "oRegisterActivateFail");
	}
	$oData["sLink"]  = $sLink;
	return $oData;
}



//==============================================================================
public function register($oData){
	$iNumPages = sizeof($oData["aValues"]);
	$aCount = array();
	$aErrors = array();
	$bPageError = 0;
	for ($iI = 1; $iI < $iNumPages; $iI++){
		$iNumValues = sizeof($oData["aValues"][$iI]);
		if (!$iNumValues){
			if ($iI != 4){
				$bPageError = 1;
			}
		}
		$aCount[$iI] = $iNumValues;
		for ($iJ = 0; $iJ < $iNumValues; $iJ++){
			$aValue = $oData["aValues"][$iI][$iJ];
			if ((!$aValue["sValue"]) && ($aValue["bRequired"] == 1) && 
				($aValue["sType"] != "select-one")){
				$aErrors[] = "Required " . $aValue["sText"];
			}
			if (($aValue["sValue"] == -1) && ($aValue["bRequired"] == 1) && 
				($aValue["sType"] == "select-one")){
				$aErrors[] = "Required " . $aValue["sText"];
			}
			if (($aValue["sType"] == "email") && ($aValue["sValue"])){
				if (!filter_var($aValue["sValue"], FILTER_VALIDATE_EMAIL)){
					$aErrors[] = "Invalid " . $aValue["sText"];
				}
			}
		}
	}
	if ($bPageError){
		$aErrors[] = "Missing page information";
	}
	$bMainAddress = 0;
	$iNumAddresses = sizeof($oData["aValues"][0]["aAddresses"]);
	if (!$iNumAddresses){
		$aErrors[] = "Provide at least one address";
	}
	for ($iI = 0; $iI < $iNumAddresses; $iI++){
		$aAddress = $oData["aValues"][0]["aAddresses"][$iI];
		if ($aAddress[7]){
			$bMainAddress = 1;
		}
		if ($aAddress[3] == -1){
			$aErrors[] = "Invalid Address Number " . ($iI + 1);
		}
	}
	if (!$bMainAddress){
		$aErrors[] = "Choose a main address";
	}
	
	$aPlaces = array();
	for ($iI = 0; $iI < $iNumAddresses; $iI++){
		$aAddress = $oData["aValues"][0]["aAddresses"][$iI];
			if ($aAddress[7]){
			$bMainAddress = true;
		} else {
			$bMainAddress = false;
		}
		if ($aAddress[4]){
			$iRegion = $aAddress[4];
		} else {
			$iRegion = $aAddress[3];
		}
		$aPlaces[] = array(
			"address_line_1" => $aAddress[5],
			"address_line_2" => $aAddress[6],
			"main_address" => $bMainAddress,
			"latitude" => 0,
			"longitude" => 0,
			"region_id" => $iRegion
		);
	}
	
	$aDays = explode(",", ",Sunday,Monday,Tuesday, Wednesday,Thursday," .
		"Friday,Saturday");
	$aHoursOfWeek = array();
	$iNumHours = sizeof($oData["aValues"][0]["aWeekHours"]);
	for ($iI = 0; $iI < $iNumHours; $iI++){
		$aHour = $oData["aValues"][0]["aWeekHours"][$iI];
		$aHoursOfWeek[] = array(
			"days_of_week" => $aDays[$aHour[0]] . "-" . $aDays[$aHour[1]],	
			"opening_time" => $aHour[2],	
			"closing_time" => $aHour[3],	
		);
	}
	
	//errors, abort
	if (sizeof($aErrors)){
		$oData["aPlaces"] = $aPlaces;
		$oData["aHours"] = $aHoursOfWeek;
		$oData["aErrors"] = $aErrors;
		return $oData;
	}
	//continue
	$aValues = $oData["aValues"];
	$oParamsUser = array(
		"name" => $aValues[1][0]["sValue"],
		"surname" => $aValues[1][1]["sValue"],
		"password" => $aValues[1][3]["sValue"],
		"platform" => "organisation",
		"email" => $aValues[1][2]["sValue"]
	);
	$oParamsOrg = array(
		"user" => $oParamsUser,
		"name" => $aValues[2][0]["sValue"],
		"description" => $aValues[2][4]["sValue"],
		"npo_registration_number" => "",
		"organisation_type" => intval($aValues[2][3]["sValue"]),
		"category" => intval($aValues[2][2]["sValue"]),
		"logo" => $aValues[0]["sLogo"],
		"contact_person" => $aValues[3][0]["sValue"],
		"contact_email" => $aValues[3][1]["sValue"],
		"contact_number" => "0123456789",//$aValues[3][2]["sValue"],
		"places" => $aPlaces,
		"hours" => $aHoursOfWeek
	);
	if ($aValues[5][0]["sValue"]){
		$oParamsOrg["website_link"] = $aValues[5][0]["sValue"];
		if (substr($oParamsOrg["website_link"], 0, 4) != "http"){
			$oParamsOrg["website_link"] = "http://" . $oParamsOrg["website_link"];
		}
	}
	if ($aValues[5][1]["sValue"]){
		$oParamsOrg["facebook_link"] = $aValues[5][1]["sValue"];
		if (substr($oParamsOrg["facebook_link"], 0, 4) != "http"){
			$oParamsOrg["facebook_link"] = "http://" . $oParamsOrg["facebook_link"];
		}
	}
	if ($aValues[5][2]["sValue"]){
		$oParamsOrg["twitter_handle"] = $aValues[5][2]["sValue"];
	}
	if ($aValues[5][3]["sValue"]){
		$oParamsOrg["instagram_link"] = $aValues[5][3]["sValue"];
		if (substr($oParamsOrg["instagram_link"], 0, 4) != "http"){
			$oParamsOrg["instagram_link"] = "http://" . $oParamsOrg["instagram_link"];
		}
	}
	$oData["aErrors"] = ["Done"];
	$oResponse = $this->api("organisations", $oParamsOrg);
	$oData["oResponse"] = $oResponse;
	if ($oResponse["response_code"] == -1){
		global $oConf;
		$sLogo = $oData["aValues"][0]["sLogo"];
		$sDir = $oConf["oFiles"]["sCompanyLogoFolder"] ."/";
		rename("tmp/" . $sLogo, $sDir . $sLogo);
		
		$sMessage = doSQL("SELECT", "CMSApp",
			"sValue",
			array(array("sCategory", "=", "oSystem", "AND"),
			array("sSubCategory", "=", "oSystem", "AND"),
			array("sParameter", "=", "oEmailRegisterActivate"))
		);
		$oMessage = json_decode($sMessage[0]["sValue"], 1);
		$sMessage = $oMessage["sMessage"];
		$sLink = md5($aValues[1][2]["sValue"] . getTimeStamp());
		$sURL = $oConf["oSystem"]["sWebURL"] . "/?link=" . $sLink;
		$sMessage .= $sURL;
		sendEmail($aValues[1][2]["sValue"], $oMessage["sSubject"], $sMessage);
		$oUser = array(
			"sAction" => "registeractivate",
			"sEmailAddress" => $aValues[1][2]["sValue"]
		);
		doSQL("INSERT", "CMSSession",
			"sSessionID, oUser, iTimeServer",
			0,
			array($sLink, json_encode($oUser, 1), getTimeStamp(4))
		);
		
	}
	return $oData;
}
	
	
	
}




//==============================================================================
class cCreatePost{

//==============================================================================
public function postnow($oData){
	
	return $oData;
}



//==============================================================================
public function savedraft($oData){
	$iType =  $oData["aValues"][0]["iType"];
	$sTitle = $oData["aValues"][1]["sValue"];
	$sDescription = $oData["aValues"][2]["sValue"];
	$sDateStart = $oData["aValues"][3]["sValue"];
	$sDateEnd = $oData["aValues"][4]["sValue"];
	$iLocation = 0;
	$sVolunteerStartDate = "";
	$sVolunteerEndDate = "";
	$sVolunteerStartTime = "";
	$sVolunteerEndTime = "";
	$sVolunteerMinTime = "";
	$sVolunteerMaxTime = "";
	switch ($iType){
		case 1:
			$iLocation = intval($oData["aValues"][5]["sValue"]);
		break;
		
		case 2:
			$sVolunteerStartDate = $oData["aValues"][6]["sValue"];
			$sVolunteerEndDate = $oData["aValues"][7]["sValue"];
			$sVolunteerStartTime = $oData["aValues"][8]["sValue"];
			$sVolunteerEndTime = $oData["aValues"][9]["sValue"];
			$sVolunteerMinTime = $oData["aValues"][10]["sValue"];
			$sVolunteerMaxTime = $oData["aValues"][11]["sValue"];
			$iLocation = intval($oData["aValues"][5]["sValue"]);
		break;
		
	}
	
	if (!$sVolunteerMinTime){
		$sVolunteerMinTime = "00:00";
	}
	if (!$sVolunteerMaxTime){
		$sVolunteerMaxTime = "00:00";
	}
	if (!$sVolunteerStartTime){
		$sVolunteerStartTime = "00:00";
	}
	if (!$sVolunteerEndTime){
		$sVolunteerEndTime = "00:00";
	}
	$iVolunteerMinTime = explode(":", $sVolunteerMinTime);
	$iVolunteerMinTime = intval($iVolunteerMinTime[0]) * 60 +
		intval($iVolunteerMinTime[1]);
	if (!$sVolunteerMaxTime){
		$sVolunteerMaxTime = "00:00";
	}
	$iVolunteerMaxTime = explode(":", $sVolunteerMaxTime);
	$iVolunteerMaxTime = intval($iVolunteerMaxTime[0]) * 60 +
		intval($iVolunteerMaxTime[1]);
	
	$bHasEndDate = true;
	if (!$sDateEnd){
		$bHasEndDate = false;
	}
	$bHasVolunteerEndDate = true;
	if (!$sVolunteerEndDate){
		$bHasVolunteerEndDate = false;
	}
	$sTime = "T00:00:00Z00:00";
	$sDateStart .= $sTime;
	if ($sDateEnd){
		$sDateEnd .= $sTime;
	}
	if ($sVolunteerStartDate){
		$sVolunteerStartDate .= $sTime;
	}
	if ($sVolunteerEndDate){
		$sVolunteerEndDate .= $sTime;
	}
	
	$oUser = doSQL("SELECT", "CMSSession",
		"oUser",
		array(array("sSessionID", "=", $oData["sSessionID"]))
	);
	if (!isset($oUser[0])){
		return $oData;
	}
	$oUser = json_decode($oUser[0]["oUser"], 1);
	$sToken = $oUser["sToken"];
	$iOrganisationID = $oUser["iOrganisationID"];
	
	
	$oData["tk"] = $sToken;
	
		
	$oParams = array(
		"organisation_id" => $iOrganisationID,
		"title" => $sTitle,
		"description" => $sDescription,
		"need_type" => $iType,
		"start_date" => $sDateStart,
		"has_end_date" => $bHasEndDate,
		//		"end_date" => $sDateEnd,
		"volunteers_needed" => 1,
		"volunteers_min_time" => $iVolunteerMinTime,
		"volunteers_max_time" => $iVolunteerMaxTime,
		"volunteers_start_date" => $sVolunteerStartDate,
		"has_volunteer_end_date" => $bHasVolunteerEndDate,
		//		"volunteers_end_date" => $sVolunteerEndDate,
		"volunteers_start_time" => $sVolunteerStartTime,
		//		"volunteers_end_time" => $sVolunteerEndTime,
		"volunteer_notes" =>  "",
		"has_lat_long" => false,
		"latitude" => 0,
		"longitude" => 0,
		"post_status" => 1,
		"address_id" => $iLocation
	);
	if ($bHasEndDate){
		$oParams["end_date"] = $sDateEnd;
	}
	if ($bHasVolunteerEndDate){
		$oParams["volunteers_end_date"] = $sVolunteerEndDate;
	}
	
	$oData["pr"] = $oParams;
	
	$oInit = new cInit();
	$oResponse = $oInit->api("posts", $oParams, $sToken);
	if ($oResponse["response_code"] == -1){
		$oData["sMessage"] = "oRecordAdded";
	} else {
		$oData["sMessage"] = "";
		$oData["sError"] = "oRecordAddError";
	}
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "mypostsdrafts", "iGridNo" =>0));
	$oData["aData2"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	
	return $oData;
}



}


class cLogin{

//==============================================================================
public function forgotPassword($oData){
	global $oConf;
	$sEmail = $oData["aValues"][0]["sValue"];
	//check that user is active first
	$oUser1 = doSQL("SELECT", "users",
		"id, is_active",
		array(array("email", "=", $sEmail))
	);
	if (isset($oUser1[0]) && ($oUser1[0]["is_active"] == 1)){	
		$sMessage = doSQL("SELECT", "CMSApp",
			"sValue",
			array(array("sCategory", "=", "oSystem", "AND"),
			array("sSubCategory", "=", "oSystem", "AND"),
			array("sParameter", "=", "oEmailPasswordReset"))
		);
		$oMessage = json_decode($sMessage[0]["sValue"], 1);
		$sMessage = $oMessage["sMessage"];
		$sLink = md5($sEmail . getTimeStamp());
		$sURL = $oConf["oSystem"]["sWebURL"] . "/?link=" . $sLink;
		$sMessage .= $sURL;
		sendEmail($sEmail, $oMessage["sSubject"], $sMessage);
		$oUser = array(
			"sAction" => "passwordreset",
			"sEmailAddress" => $sEmail
		);
		if ((isset($oPost["device"])) && ($oPost["device"] == "app")){
			$oUser["sDevice"] = "app";
		}
		doSQL("INSERT", "CMSSession",
			"sSessionID, oUser, iTimeServer",
			0,
			array($sLink, json_encode($oUser, 1), getTimeStamp(4))
		);
		doSQL("UPDATE", "users",
			"link",
			array(array("email", "=", $sEmail)),
			array($sLink)
		);
		$oData = array("sMessage" => "oPasswordResetLink");
	} else {
		//not active, cannot continue
error_log("not active, cannot continue");
		$oData = array("sMessage" => "oAccountNotActive");
	}
	return $oData;
}



public function login($oData){
	$sEmail = $oData["aValues"][0]["sValue"];
	$sPassword = $oData["aValues"][1]["sValue"];
	$sSessionID = $oData["sSessionID"];
	$oParams = array(
		"email" => $sEmail,
		"password" => $sPassword
	);
	$aErrors = [];
	if (!filter_var($sEmail, FILTER_VALIDATE_EMAIL)){
		$aErrors[] = "sInvalidEmailAddress";
	}
	if (strlen($sPassword) < 6){
		$aErrors[] = "sPasswordTooShort";
	}
	$oInit = new cInit();
	$oResult = $oInit->api("users/login", $oParams);
	$iOrganisationID = 0;
	if (isset($oResult["data"])){
		if (isset($oResult["data"]["user"])){
			$iOrganisationID = $oResult["data"]["user"]["organisation_id"];
		}
	}
	if (!$iOrganisationID){
		$oResult["response_code"] = -2;
		$oResult["message"] = "Your login details were incorrect.";
	}
	if ($oResult["response_code"] == -1){
		//success
		$iOrganisationID = $oResult["data"]["user"]["organisation_id"];
		$aOrganisation = doSQL("SELECT", "organisations",
			"name, logo",
			array(array("id", "=", $iOrganisationID))
		);
		/*
		$sAvatar = doSQL("SELECT", "users",
			"avatar",
			array(array("id", "=", $oResult["data"]["user"]["id"]))
		);
		*/
		
		global $oConf;
		$sDir = $oConf["oFiles"]["sDirectoryData"] . "/" .
			$oConf["oFiles"]["sCompanyLogoFolder"];
		$oLogo = $sDir . "/" .$aOrganisation[0]["logo"];
		if (file_exists($oLogo)){
			$oInfo = getimagesize($oLogo);
			$oLogo = array("sB64" => base64_encode(file_get_contents($oLogo)),
				"oInfo" => $oInfo);
		}
		
		
		
		$oUser = array(
			"iUserID" => $oResult["data"]["user"]["id"],
			"sFirstName" => $oResult["data"]["user"]["name"],
			"sSurname" => $oResult["data"]["user"]["surname"],
			"sEmailAddress" => $oResult["data"]["user"]["email"],
			"iUserType" => $oResult["data"]["user"]["user_type"],
			"sToken" => $oResult["data"]["token"],
			"iOrganisationID" => $iOrganisationID,
			"sOrganisationName" => $aOrganisation[0]["name"],
			"oLogo" => $oLogo
		);	
		if (!$oLogo["oInfo"]){
			$sAvatar = doSQL("SELECT", "CMSApp",
				"sValue",
				array(array("sCategory", "=", "oSystem", "AND"),
				array("sParameter", "=", "sAvatar"))
			);
			$oUser["oLogo"] = array("sB64" =>$sAvatar[0]["sValue"],
				"oInfo" => array("mime-type" => "image/png"));
		}
		doSQL("INSERT", "CMSLog",
			"iUserID, sAction, sPage, sParameters, dDateTime, sIPAddress",
			0,
			array($oResult["data"]["user"]["id"], "login",
			"", "",
			getTimeStamp(), getVar("conf","oUser","sFromIP"))
		);
		$oData = $oInit->loggedIn($oUser);
	} else {
		$oUser = array(
			"iUserID" => "0",
			"sToken" => ""
		);
		$oData = $oInit->loggedOut();
		$oData["sError"] = $oResult["message"];
	}
	doSQL("UPDATE", "CMSSession",
		"iUserID, oUser",
		array(array("sSessionID", "=", $sSessionID)),
		array($oUser["iUserID"], json_encode($oUser, 1))
	);
	$oData["curl"] = $oResult;
	return $oData;
}



//==============================================================================
public function logout($oData){
	$oUser = doSQL("SELECT", "CMSSession",
		"oUser",
		array(array("sSessionID", "=", $oData["sSessionID"]))
	);
	$oUser = json_decode($oUser[0]["oUser"], 1);
	$sToken = $oUser["sToken"];
	$oInit = new cInit();
	$oInit->api("users/session", array(), $sToken, "DELETE");
	doSQL("UPDATE", "CMSSession",
		"iUserID, oUser",
		array(array("sSessionID", "=", $oData["sSessionID"])),
		array(0, "")
	);
	$oData = $oInit->loggedOut();
	return $oData;
}



public function resetPassword($oData){
	$sPassword = $oData["aValues"][0]["sValue"];
	$sLink = $oData["aValues"][2]["sValue"];
	$oResponseUser = array();
	$oUser = doSQL("SELECT", "CMSSession",
		"oUser",
		array(array("sSessionID", "=", $sLink))
	);
	$sHash = password_hash($sPassword, PASSWORD_DEFAULT);
	if (isset($oUser[0])){
		$oUser = json_decode($oUser[0]["oUser"], 1);
		doSQL("UPDATE", "CMSSession",
			"sSessionID",
			array(array("sSessionID", "=", $sLink)),
			array("")
		);
		$oParamsUser = array(
			"link" => $sLink,
			"password" => $sPassword,
		);
		$oInit = new cInit();
		$oResponseUser = $oInit->api("users/changePasswordNotAuth", $oParamsUser);
		$oData = array("sMessage" => "oResetPasswordSuccess",
			"api" => $oResponseUser);
	} else {
		$oData = array("sMessage" => "oResetPasswordFail",
			"api" => $oResponseUser);
	}
	return $oData;
}



}



class cMyPosts{

//==============================================================================
public function activeduplicatetodrafts($oData){
	$iPostID = intval($oData["aValues"][0]["iPostID"]);
	$sNow = getTimeStamp();
	$iNewID = doSQL("MULTI", "CREATE TEMPORARY TABLE tmp SELECT * FROM posts " . 
		"WHERE id = '" . $iPostID . "'; " .
		"UPDATE tmp SET id = NULL; " .
		"INSERT INTO posts SELECT * FROM tmp");
	
	doSQL("UPDATE", "posts",
		"created_at, updated_at, post_status",
		array(array("id", "=", $iNewID)),
		array($sNow, $sNow, 0)
	);
	$oData["sMessage"] = "oRecordDuplicated";
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "mypostsdrafts", "iGridNo" =>0));
	$oData["aData2"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



//==============================================================================
public function activemarkascomplete($oData){
	$iPostID = intval($oData["aValues"][0]["iPostID"]);
	$sNow = getTimeStamp();
	doSQL("UPDATE", "posts",
		"updated_at, post_status",
		array(array("id", "=", $iPostID)),
		array($sNow, 3)
	);
	$oData["sMessage"] = "oRecordMoved";
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "mypostsactive", "iGridNo" =>0));
	$oData["aData1"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	$oValues = array(array("sName" => "mypostshistory", "iGridNo" =>0));
	$oData["aData3"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



//==============================================================================
public function activemarkasdraft($oData){
	$iPostID = intval($oData["aValues"][0]["iPostID"]);
	$sNow = getTimeStamp();
	doSQL("UPDATE", "posts",
		"updated_at, post_status",
		array(array("id", "=", $iPostID)),
		array($sNow, 0)
	);
	$oData["sMessage"] = "oRecordMoved";
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "mypostsactive", "iGridNo" =>0));
	$oData["aData1"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	$oValues = array(array("sName" => "mypostsdrafts", "iGridNo" =>0));
	$oData["aData2"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



//==============================================================================
public function activesave($oData){
	$iPostID = intval($oData["aValues"][0]["iPostID"]);
	$sTitle = $oData["aValues"][3]["sValue"];
	$sDescription = $oData["aValues"][4]["sValue"];
	$iLocation = intval($oData["aValues"][5]["sValue"]);
	$sStartDate = $oData["aValues"][6]["sValue"];
	$sEndDate = $oData["aValues"][7]["sValue"];
	$sNow = getTimeStamp();
	doSQL("UPDATE", "posts",
		"updated_at, title, description, start_date, end_date",
		array(array("id", "=", $iPostID)),
		array($sNow, $sTitle, $sDescription, $sStartDate, $sEndDate)
	);
	$oData["sMessage"] = "oRecordEdited";
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "mypostsactive", "iGridNo" =>0));
	$oData["aData1"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



//==============================================================================
public function draftsdelete($oData){
	$iPostID = intval($oData["aValues"][0]["iPostID"]);
	$sNow = getTimeStamp();
	doSQL("UPDATE", "posts",
		"deleted_at",
		array(array("id", "=", $iPostID)),
		array($sNow)
	);
	$oData["sMessage"] = "oRecordDeleted";
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "mypostsdrafts", "iGridNo" =>0));
	$oData["aData2"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



//==============================================================================
public function draftspostnow($oData){
	$iPostID = intval($oData["aValues"][0]["iPostID"]);
	$sNow = getTimeStamp();
	doSQL("UPDATE", "posts",
		"updated_at, post_status",
		array(array("id", "=", $iPostID)),
		array($sNow, 1)
	);
	$oData["sMessage"] = "oRecordMoved";
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "mypostsactive", "iGridNo" =>0));
	$oData["aData1"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	$oValues = array(array("sName" => "mypostsdrafts", "iGridNo" =>0));
	$oData["aData2"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



//==============================================================================
public function draftsduplicate($oData){
	$iPostID = intval($oData["aValues"][0]["iPostID"]);
	$sNow = getTimeStamp();
	$iNewID = doSQL("MULTI", "CREATE TEMPORARY TABLE tmp SELECT * FROM posts " . 
		"WHERE id = '" . $iPostID . "'; " .
		"UPDATE tmp SET id = NULL; " .
		"INSERT INTO posts SELECT * FROM tmp");
	$oData["sMessage"] = "oRecordDuplicated";
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "mypostsdrafts", "iGridNo" =>0));
	$oData["aData2"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



//==============================================================================
public function draftssave($oData){
	$iPostID = intval($oData["aValues"][0]["iPostID"]);
	$sTitle = $oData["aValues"][3]["sValue"];
	$sDescription = $oData["aValues"][4]["sValue"];
	$iLocation = intval($oData["aValues"][5]["sValue"]);
	$sStartDate = $oData["aValues"][6]["sValue"];
	$sEndDate = $oData["aValues"][7]["sValue"];
	$sNow = getTimeStamp();
	doSQL("UPDATE", "posts",
		"updated_at, title, description, start_date, end_date",
		array(array("id", "=", $iPostID)),
		array($sNow, $sTitle, $sDescription, $sStartDate, $sEndDate)
	);
	$oData["sMessage"] = "oRecordEdited";
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "mypostsdrafts", "iGridNo" =>0));
	$oData["aData2"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



//==============================================================================
public function historydelete($oData){
	$iPostID = intval($oData["aValues"][0]["iPostID"]);
	$sNow = getTimeStamp();
	doSQL("UPDATE", "posts",
		"deleted_at",
		array(array("id", "=", $iPostID)),
		array($sNow)
	);
	$oData["sMessage"] = "oRecordDeleted";
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "mypostshistory", "iGridNo" =>0));
	$oData["aData3"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



//==============================================================================
public function historyduplicatetodrafts($oData){
	$iPostID = intval($oData["aValues"][0]["iPostID"]);
	$sNow = getTimeStamp();
	$iNewID = doSQL("MULTI", "CREATE TEMPORARY TABLE tmp SELECT * FROM posts " . 
		"WHERE id = '" . $iPostID . "'; " .
		"UPDATE tmp SET id = NULL; " .
		"INSERT INTO posts SELECT * FROM tmp"
	);
	doSQL("UPDATE", "posts",
		"created_at, updated_at, post_status",
		array(array("id", "=", $iNewID)),
		array($sNow, $sNow, 0)
	);
	$oData["sMessage"] = "oRecordDuplicated";
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "mypostsdrafts", "iGridNo" =>0));
	$oData["aData2"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



}// end myposts


class cGrid{

//==============================================================================
private function dataEnum($aData, $iColumnNo, $sEnum){
	$oEnums = doSQL("SELECT", "CMSApp",
		"sValue",
		array(array("sParameter", "=", "oEnum"))
	);
	$oEnums = json_decode($oEnums[0]["sValue"], 1);
	$iSize = sizeof($aData);
	$oTypes = $oEnums[$sEnum];
	$iSize1 = sizeof($oTypes);
	for ($iI = 0; $iI < $iSize; $iI++){
		$iJ = 0;
		$iFound = 0;
		while ((!$iFound) && ($iJ < $iSize1)){
			if ($aData[$iI][$iColumnNo] == $oTypes[$iJ]["iID"]){
				$aData[$iI][$iColumnNo] = $oTypes[$iJ]["sValue"];
				$iFound = 1;
			} else {
				$iJ++;
			}
		}
	}
	return $aData;
}



//==============================================================================
public function data($oData){
	$sName = $oData["aValues"][0]["sName"];
	$iGridNo = intval($oData["aValues"][0]["iGridNo"]);
	$oUser = doSQL("SELECT", "CMSSession",
		"oUser",
		array(array("sSessionID", "=", $oData["sSessionID"]))
	);
	$iOrganisationID = json_decode($oUser[0]["oUser"], 1)["iOrganisationID"];
	switch ($sName){
		case "mypostsactive":
			$aPosts = doSQL("SELECT", "posts",
				"id, title, id, need_type, " .
				"CONCAT(SUBSTR(start_date, 1, 10), ' - ', SUBSTR(end_date, 1, 10)), " .
				"address_id, description, need_type, updated_at",
				array(array("post_status", "=", "1", "AND"),
				array("organisation_id", "=", $iOrganisationID, "AND"),
				array("deleted_at", "IS", "null")),
				0, "", 0, 1
			);
			$aCommitments = doSQL("SELECT", "commitments",
				"id, post_id",
				0,
				0, "", 0, 1
			);
			$iSizeP = sizeof($aPosts);
			$iSizeC = sizeof($aPosts);
			for ($iI = 0; $iI < $iSizeP; $iI++){
				$aPosts[$iI][2] = 0;
				$iPos = strpos($aPosts[$iI][4], " - 0000-00-00");
				if ($iPos){
					$aPosts[$iI][4] = substr($aPosts[$iI][4], 0, $iPos);
				}
				for ($iJ = 0; $iJ < $iSizeC; $iJ++){
					if ($aCommitments[$iJ][1] == $aPosts[$iI][0]){
						$aPosts[$iI][2]++;
					}
				}
			}
			$aData = $this->dataEnum($aPosts, 3, "sNeedType");
		break;
		
		case "mypostsdrafts":
			$aPosts = doSQL("SELECT", "posts",
				"id, title, id, need_type, " . 
				"CONCAT(SUBSTR(start_date, 1, 10), ' - ', SUBSTR(end_date, 1, 10)), " .
				"address_id, description, need_type, updated_at",
				array(array("post_status", "=", "0", "AND"),
				array("organisation_id", "=", $iOrganisationID, "AND"),
				array("deleted_at", "IS", "null")),
				0, "", 0, 1
			);
			$aCommitments = doSQL("SELECT", "commitments",
				"id, post_id",
				0,
				0, "", 0, 1
			);
			$iSizeP = sizeof($aPosts);
			$iSizeC = sizeof($aPosts);
			for ($iI = 0; $iI < $iSizeP; $iI++){
				$aPosts[$iI][2] = 0;
				$iPos = strpos($aPosts[$iI][4], " - 0000-00-00");
				if ($iPos){
					$aPosts[$iI][4] = substr($aPosts[$iI][4], 0, $iPos);
				}
				for ($iJ = 0; $iJ < $iSizeC; $iJ++){
					if ($aCommitments[$iJ][1] == $aPosts[$iI][0]){
						$aPosts[$iI][2]++;
					}
				}
			}
			$aData = $this->dataEnum($aPosts, 3, "sNeedType");
		break;
		
		case "mypostshistory":
			$aPosts = doSQL("SELECT", "posts",
				"id, title, id, need_type, " . 
				"CONCAT(SUBSTR(start_date, 1, 10), ' - ', SUBSTR(end_date, 1, 10)), " .
				"address_id, description, need_type, updated_at",
				array(array("post_status", "=", "3", "AND"),
				array("organisation_id", "=", $iOrganisationID, "AND"),
				array("deleted_at", "IS", "null")),
				0, "", 0, 1
			);
			$aCommitments = doSQL("SELECT", "commitments",
				"id, post_id",
				0,
				0, "", 0, 1
			);
			$iSizeP = sizeof($aPosts);
			$iSizeC = sizeof($aPosts);
			for ($iI = 0; $iI < $iSizeP; $iI++){
				$aPosts[$iI][2] = 0;
				$iPos = strpos($aPosts[$iI][4], " - 0000-00-00");
				if ($iPos){
					$aPosts[$iI][4] = substr($aPosts[$iI][4], 0, $iPos);
				}
				for ($iJ = 0; $iJ < $iSizeC; $iJ++){
					if ($aCommitments[$iJ][1] == $aPosts[$iI][0]){
						$aPosts[$iI][2]++;
					}
				}
			}
			$aData = $this->dataEnum($aPosts, 3, "sNeedType");
		break;
		
		case "users":
			$oData = array("aData" => doSQL("SELECT", "users",
			"id, name, surname, email, user_type, user_type",
			array(array("user_type", "<>", "0", "AND"),
			array("organisation_id", "=", $iOrganisationID, "AND"),
			array("deleted_at", "IS", "null")),
			0, "", 0, 1
			));
			$aData = $this->dataEnum($oData["aData"], 4, "sUserType");
		break;
		
		
	}
	$oData = array("iGridNo" => $iGridNo, "aData" => $aData);
	return $oData;
}



}



class cUsers{

//==============================================================================
public function addsave($oData){
	$sEmailAddress = $oData["aValues"][0]["sValue"];
	$sFirstName = $oData["aValues"][1]["sValue"];
	$sSurname = $oData["aValues"][2]["sValue"];
	$iRole = $oData["aValues"][3]["sValue"];
	$sPassword = "aaaaaa";
	global $oConf;
	$oParamsUser = array(
		"email" => $sEmailAddress,
		"name" => $sFirstName,
		"surname" => $sSurname,
		"password" => $sPassword,
		"platform" => "organisation",
	);
	$oUser = doSQL("SELECT", "CMSSession",
		"oUser",
		array(array("sSessionID", "=", $oData["sSessionID"]))
	);
	$iOrganisationID = json_decode($oUser[0]["oUser"], 1)["iOrganisationID"];
	$oInit = new cInit();
	$oResponseUser = $oInit->api("users", $oParamsUser);
	if ($oResponseUser["response_code"] == -1){
		$iUserID = $oResponseUser["data"]["user"]["id"];
		doSQL("UPDATE", "users",
			"user_type, organisation_id",
			array(array("id", "=", $iUserID)),
			array($iRole, $iOrganisationID)
		);
		$oData["sMessage"] = "";
		$oData["sError"] = "oUserAdded";
	} else {
		$oData["sMessage"] = "";
		$oData["sError"] = $oResponseUser["message"];
	}
	$oGrid = new cGrid();
	$oValues = array(array("sName" => "users", "iGridNo" =>0));
	$oData["aData"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	$sMessage = doSQL("SELECT", "CMSApp",
		"sValue",
		array(array("sCategory", "=", "oSystem", "AND"),
		array("sSubCategory", "=", "oSystem", "AND"),
		array("sParameter", "=", "oEmailUserAdded"))
	);
	$oMessage = json_decode($sMessage[0]["sValue"], 1);
	$sMessage = $oMessage["sMessage"];
	$sLink = md5($sEmailAddress . getTimeStamp());
	$sURL = $oConf["oSystem"]["sWebURL"] . "/?link=" . $sLink;
	$sMessage .= $sURL;
	sendEmail($sEmailAddress, $oMessage["sSubject"], $sMessage);
	$oUser = array(
		"sAction" => "adduseractivate",
		"sEmailAddress" => $sEmailAddress
	);
	doSQL("INSERT", "CMSSession",
		"sSessionID, oUser, iTimeServer",
		0,
		array($sLink, json_encode($oUser, 1), getTimeStamp(4))
	);
	doSQL("UPDATE", "users",
		"link",
		array(array("email", "=", $sEmailAddress)),
		array($sLink)
	);
	return $oData;
}



//==============================================================================
public function editsave($oData){
	$sEmailAddress = $oData["aValues"][0]["sValue"];
	$sFirstName = $oData["aValues"][1]["sValue"];
	$sSurname = $oData["aValues"][2]["sValue"];
	$iRole = $oData["aValues"][3]["sValue"];
	$iUserID = intval($oData["aValues"][4]["sValue"]);
	doSQL("UPDATE", "users",
		"updated_at, name, surname, email, user_type",
		array(array("id", "=", $iUserID)),
		array(getTimeStamp(), $sFirstName, $sSurname, $sEmailAddress, $iRole)
	);
	$oGrid = new cGrid();
	$oData["sMessage"] = "oRecordEdited";
	$oValues = array(array("sName" => "users", "iGridNo" =>0));
	$oData["aData"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



//==============================================================================
public function delete($oData){
	$iUserID = intval($oData["aValues"][0]["iUserID"]);
	doSQL("UPDATE", "users",
		"deleted_at",
		array(array("id", "=", $iUserID)),
		array(getTimeStamp())
	);
	$oGrid = new cGrid();
	$oData["sMessage"] = "oRecordDeleted";
	$oValues = array(array("sName" => "users", "iGridNo" =>0));
	$oData["aData"] = $oGrid->data(array("aValues" => $oValues,
		"sSessionID" => $oData["sSessionID"]));
	return $oData;
}



}// end cUsers




?>