<?php
$oConf = array();


//=== GLOBAL SETTINGS HERE, for administrator to modify ========================

$oConf["oFiles"]["sDirectoryWebRoot"] = "helpout/h2srv";
$oConf["oFiles"]["sDirectoryWeb"] = "/data/samba/www";
$oConf["oFiles"]["sDirectoryData"] = "/data/samba/www/helpout/h2srv/datau/www.helpout";
$oConf["oFiles"]["sDirectoryPHPIDS"] = "/data/samba/www/tools/phpids/lib/";
$oConf["oFiles"]["sCompanyLogoFolder"] = "companylogos";
$oConf["oSystem"]["sWebRoot"] = "helpout";
$oConf["oSystem"]["iIsSSL"] = 0;
$oConf["oSystem"]["sTimeZone"] = "Africa/Johannesburg";
$oConf["oSystem"]["iPHPIDSTriggerScore"] = 10;
$oConf["oSystem"]["iAPIPort"] = 9999;
$oConf["oSystem"]["sAPIFolder"] = "api/v1";
$oConf["oSystem"]["sPHPLocation"] = "/usr/local/bin/php";

//=== NO MORE CONFIGURABLE SETTINGS BELOW THIS LINE ============================

require_once $oConf["oFiles"]["sDirectoryData"] . "/database.php";
if ($oConf["oSystem"]["iIsSSL"]==1){
	$sHTTP = "https://";
} else {
	$sHTTP = "http://";
}
if (isset($_SERVER["SERVER_ADDR"])){
	$oConf["oSystem"]["sServerIP"] = $_SERVER["SERVER_ADDR"];
} else {
	$oConf["oSystem"]["sServerIP"] = "0.0.0.0";
}
if (isset($_SERVER["HTTP_HOST"])){
	$oConf["oSystem"]["sWebURL"] = $sHTTP . $_SERVER["HTTP_HOST"] . "/" .
		$oConf["oFiles"]["sDirectoryWebRoot"];
	$oConf["oSystem"]["sWebRoot"] = $sHTTP . $_SERVER["HTTP_HOST"] . "/" .
		$oConf["oSystem"]["sWebRoot"];
} else {
	$oConf["oSystem"]["sWebURL"] = "";
	$oConf["oSystem"]["sWebRoot"] = "";
}
$oConf["oUser"] = array();
$oConf["oUser"]["iUserID"] = 0;
if (isset($_SERVER["REMOTE_ADDR"])){
	$oConf["oUser"]["sFromIP"] = $_SERVER["REMOTE_ADDR"];
} else {
	$oConf["oUser"]["sFromIP"] = "0.0.0.0";
}
if (isset($_SERVER["HTTP_USER_AGENT"])){
	$oConf["oUser"]["sUserAgent"] = $_SERVER["HTTP_USER_AGENT"];
} else {
	$oConf["oUser"]["sUserAgent"] = "NO_USER_AGENT";
}
ini_set("date.timezone",$oConf["oSystem"]["sTimeZone"]);
?>