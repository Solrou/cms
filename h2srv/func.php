<?php 
require_once "config.php";



//==============================================================================
function deleteDir($sDirPath){
	if (!is_dir($sDirPath)){
		throw new InvalidArgumentException("$sDirPath must be a directory");
	}
	if (substr($sDirPath, strlen($sDirPath) - 1, 1) != "/"){
		$sDirPath .= "/";
	}
	$aFiles = glob($sDirPath . "*", GLOB_MARK);
	foreach ($aFiles as $aFile) {
		if (is_dir($aFile)){
			deleteDir($aFile);
		} else {
			unlink($aFile);
		}
	}
	rmdir($sDirPath);
}



//==============================================================================
function doSQL($sCmd, $sTables, $sFields = "", $aWhere = array(),
	$aValues = array(), $sExtra = "", $iUser = 0, $iOnlyIndexed = 0,
	$iDontLog = 0, $iTruncateLogLength = 5000){
	$sPrefix="";
	switch ($iUser){
		case 0:
			//administrator on website table
			$sPrefix = getVar("conf","oDatabase","sTablePrefix");
			if ($sPrefix){
				$sPrefix .= "_";
			}
			$aLink = mysqli_connect(
				getVar("conf", "oDatabase", "sHost"),
				getVar("conf", "oDatabase", "sUsername"),
				getVar("conf", "oDatabase", "sPassword"),
				getVar("conf", "oDatabase", "sName")
			);
		break;
		case 1:
			//user on user table
			$aLink = mysqli_connect(
				getVar("session", "oUser", "sDatabaseHost"),
				getVar("session", "oUser", "sDatabaseName")."_" .
				getZeroID(getVar("session", "oUser", "iUserID")),
				getVar("session", "oUser", "sDatabasePassword"),
				getVar("session", "oUser", "sDatabaseName")
			);
		break;
		case 2:
			//root on user tables
			$aLink = mysqli_connect(
				getVar("web", "aConf","oDatabase","sUserDatabaseHost"),
				"root",
				getVar("conf", "oDatabase", "sPassword"),
				getVar("web", "aConf", "oDatabase", "sUserDatabaseName")
			);
		break;
		case 3:
			//root on mysql tables
			$aLink = mysqli_connect(
				getVar("conf", "oDatabase", "sHost"),
				"root",
				getVar("conf", "oDatabase", "sPassword"),
				"mysql"
			);
		break;
	}
	$sWhere = "";
	if ($aWhere){
		$iSize = sizeof($aWhere);
		for ($iI = 0; $iI < $iSize; $iI++){
			$aWhere1 = $aWhere[$iI];
			if ($aWhere1[1] == "IN"){
				$sOneValue = ($aWhere1[2]);
			} else {
				$sOneValue = "'" . mysqli_real_escape_string
					($aLink, $aWhere1[2]) . "'";
				if ($sOneValue == "'null'"){
					$sOneValue = "null";
				}
			}
			$sWhere .= $aWhere1[0] . " " . $aWhere1[1] . " " . $sOneValue . " ";
			if (isset($aWhere1[3])){
				$sWhere .= $aWhere1[3] . " ";
			}
		}
	}
	switch ($sCmd){
		case "RAW":
		case "MULTI":
			$sQuery = $sTables;
		break;
		case "SELECT":
			$aTables = explode(",", $sTables);
			$iSize = sizeof($aTables);
			if ($iSize > 1){
				$aJoinOn = explode("|", $sTables);
				$aTables = explode(",", $aJoinOn[0]);
				$iSize = sizeof($aTables);
				$aJoinOn = explode(",", $aJoinOn[1]);
				$sTables = "";
				$sJoinOn = "";
				$iChr = 65;
				for ($iI = 0; $iI < $iSize; $iI++){
					$sTables .= $sPrefix . $aTables[$iI] . " " .
						chr($iChr + $iI) . $sJoinOn;
					if ($iI < $iSize - 1){
						$sTables .= " INNER JOIN ";
						$sJoinOn = " ON " . $aJoinOn[$iI];
					} else {
						$sJoinOn = "";
					}
				}
			} else {
				$sTables = $sPrefix . $sTables;
			}
			$sQuery = "SELECT " . $sFields . " FROM " . $sTables;
			if ($sWhere){
				$sQuery .= " WHERE " . $sWhere;
			}
		break;
		case "INSERT":
			$aFields = explode(", ", $sFields);
			$iSize = sizeof($aFields);
			$sFields = "";
			$sValues = "";
			for ($iI=0; $iI < $iSize; $iI++){
				$sFields .= $aFields[$iI];
				$sOneValue = "'" . mysqli_real_escape_string
					($aLink, $aValues[$iI]) . "'";
				if ($sOneValue == "'null'"){
					$sOneValue = "null";
				}
				$sValues .= $sOneValue;
				if (isset($aFields[$iI + 1])){
					$sFields .= ", ";
					$sValues .= ", ";
				}
			}
			$sQuery = "INSERT INTO " . $sPrefix . $sTables . 
				" (" . $sFields . ") VALUES (" . $sValues . ")";
		break;
		case "UPDATE":
			$aFields = explode(", ", $sFields);
			$iSize = sizeof($aFields);
			$sFields = "";
			for ($iI=0; $iI < $iSize; $iI++){
				if ($aValues[$iI] != "null"){
					$sFields .= $aFields[$iI] . " = '" .
						mysqli_real_escape_string($aLink, $aValues[$iI]) . "'";
				} else {
					if ($aValues[$iI] === 0){
						$sFields .= $aFields[$iI] . " = '0'";
					} else {
						$sFields .= $aFields[$iI] . " = NULL";
					}
				}
				if (isset($aFields[$iI + 1])){
					$sFields .= ", ";
				}
			}
			$sQuery = "UPDATE " . $sPrefix . $sTables . " SET " . $sFields;
			if ($sWhere){
				$sQuery .= " WHERE " . $sWhere;
			}
		break;
		case "DELETE":
			$sQuery = "DELETE FROM " . $sPrefix . $sTables;
			if ($sWhere){
				$sQuery .= " WHERE " . $sWhere;
			}
		break;
	}
	
	if ($sExtra){
		$sQuery .= " " . $sExtra;
	}
	if (!$iDontLog){
		$sLogFilename = getVar("conf", "oFiles", "sDirectoryData") . "/var/" .
			getTimeStamp(3) . ".sql";
		$aFileHandle = fopen($sLogFilename, "a+");
		$sQueryCut = substr(str_replace
			("\t", "", $sQuery), 0, $iTruncateLogLength);
		fwrite($aFileHandle, date("Y-m-d H:i:s", time()) . " " .
			getVar("conf", "oUser", "sFromIP") . " " . $iUser . "\n" .
			$sQueryCut . "\n");
	}
	if ($aLink){
		mysqli_query($aLink, "set names utf8");
		if ($sCmd == "MULTI"){
			$sQuery .= "; SELECT LAST_INSERT_ID()";
			$aResult = @mysqli_multi_query($aLink, $sQuery);
			while (mysqli_more_results($aLink)) {
				mysqli_use_result($aLink);
				mysqli_next_result($aLink);
			}
			$aResult = mysqli_store_result($aLink);
			$aResult = mysqli_fetch_row($aResult)[0];
		} else {
			$aResult = @mysqli_query($aLink, $sQuery);
		}
	}
	$sError = "";
	if (mysqli_error($aLink)){
		$sError = mysqli_error($aLink);
	}
	if (!$iDontLog){
		if (strlen($sError)){
			fwrite($aFileHandle, mysqli_error($aLink) . "\n");
		}
		fclose($aFileHandle);
	}
	$aOutput = 1;
	if ($aLink){
		switch ($sCmd){
			case "SELECT":
			case "RAW":
				$iI = 0;
				$aFields = array();
				if ($aResult){
					$aFieldsInfo = mysqli_fetch_fields($aResult);
					foreach ($aFieldsInfo as $aFieldInfo){
						$aFields[] = $aFieldInfo->name;
					}
				}
				$iNumFields = sizeof($aFields);
				$aOutput = array();
				$iI = 0;
				while ($aRow = @mysqli_fetch_array($aResult, MYSQLI_BOTH)){
					for ($iJ = 0; $iJ < $iNumFields; $iJ++){
						$sValue = rawurldecode($aRow[$iJ]);
						if (!$iOnlyIndexed){
							$aOutput[$iI][$aFields[$iJ]] = $sValue;
						} else {
							$aOutput[$iI][$iJ] = $sValue;
						}
					}
					$iI++;
				}
			break;
			case "INSERT":
				$aOutput = mysqli_insert_id($aLink);
			break;
			break;
			break;
			case "DELETE":
			case "UPDATE":
				$aOutput = 1;
			break;
			case "MULTI":
				$aOutput = $aResult;
			break;
		}
		if ($sError){
			$aOutput = 0;
		}
	} else {
		$aOutput = 0;
	}
	@mysqli_free_result($aResult);
	mysqli_close($aLink);
	return $aOutput;
}



//==============================================================================
function getTimeStamp($sISO = 1, $iExtraMinutes = 0){
	$iTime = time();
	if ($iExtraMinutes){
		$iTime += (60 * $iExtraMinutes);
	}
	if (!$sISO) {
		$sTime = date("YmdHis", $iTime);
	}
	if ($sISO == 1){
		$sTime = date("Y-m-d H:i:s", $iTime);
	}
	if ($sISO == 2){
		$sTime = date("Ymd", $iTime) . "T" . date("His", $iTime) . "Z";
	}
	if ($sISO == 3){
		$sTime = date("YmdH", $iTime);
	}
	if ($sISO == 4){
		$sTime = $iTime;
	}
	if ($sISO == 5){
		$sTime = date("Y-m-d", $iTime) . "T" . date("H:i:s", $iTime) . "Z";
	}
	return $sTime;
}



//===============================================================================
function getVar($sMethod, $sVar, $sVar2 = "", $sVar3 = ""){
	global $oConf;
	$sAnswer = "";
	if ($sMethod == "conf"){
		$sAnswer = $oConf[$sVar][$sVar2];
	}
	return $sAnswer;
}



//==============================================================================
function removeAccent($sInString){
	return strtr(utf8_decode($sInString), 
		"������������������������������",
		"aaaaaaaceeeeiiiionoooooouuuuyy");
}



//==============================================================================
function sendEmail($sEmailAddressTo, $sSubject, $sMessage,
	$iWaitSeconds = 0, $iEmailScriptID = 1, $iUserID = 1, $iDontSend  = 0,
	$iEmailSentID = 0){
	global $oConf;
	if ((!$iDontSend) && ($sEmailAddressTo)){	
	$sMessage1 = setBase32Encode($sMessage);
		error_log($oConf["oSystem"]["sPHPLocation"] . " " . 
			$oConf["oFiles"]["sDirectoryWeb"] .
			"/" . $oConf["oFiles"]["sDirectoryWebRoot"] .
			"/sendemail.php " . $sEmailAddressTo . " '" . 
			$sSubject . "' '" . $sMessage1 . "' '" . 
			$iWaitSeconds . "' 1>/dev/null 2>&1 &");
			
error_log("XXXXXXX EMAILSEND " . $sMessage);
	}
	return array("sEmailAddressTo" => $sEmailAddressTo, 
		"sMessage" => $sMessage);
}



//==============================================================================
function setBase32Encode($sInString){
	if (!$sInString){
		return "";
	}
	$sOutString = "";
	$sCompBits = "";
	$BASE32_TABLE = array(
		"00000" => 0x61, "00001" => 0x62, "00010" => 0x63, "00011" => 0x64,
		"00100" => 0x65, "00101" => 0x66, "00110" => 0x67, "00111" => 0x68,
		"01000" => 0x69, "01001" => 0x6a, "01010" => 0x6b, "01011" => 0x6c,
		"01100" => 0x6d, "01101" => 0x6e, "01110" => 0x6f, "01111" => 0x70,
		"10000" => 0x71, "10001" => 0x72, "10010" => 0x73, "10011" => 0x74,
		"10100" => 0x75, "10101" => 0x76, "10110" => 0x77, "10111" => 0x78,
		"11000" => 0x79, "11001" => 0x7a, "11010" => 0x32, "11011" => 0x33,
		"11100" => 0x34, "11101" => 0x35, "11110" => 0x36, "11111" => 0x37,
	);
	for ($iCount = 0; $iCount <strlen($sInString); $iCount++) {
		$sCompBits .= str_pad(decbin(ord
			(substr($sInString, $iCount, 1))), 8, "0", STR_PAD_LEFT);
	}
	if((strlen($sCompBits) % 5) != 0) {
		$sCompBits=str_pad($sCompBits, strlen
			($sCompBits) + (5 - (strlen($sCompBits) % 5)), "0", STR_PAD_RIGHT);
	}
	$aFiveBitsArray = explode("\n", rtrim(chunk_split($sCompBits, 5, "\n")));
	foreach ($aFiveBitsArray as $sFiveBitsString){
		$sOutString .= chr($BASE32_TABLE[$sFiveBitsString]);
	}
	return $sOutString;
}



//==============================================================================
function setBase32Decode($sInString) {
	$aRandomChars = array("0", "1", "8", "9", "A", "B", "C", "D", "E", "F");
	$sInString = str_replace($aRandomChars, "", $sInString);
	$iInputCheck = null;
	$sDeCompBits = null;
	$BASE32_TABLE = array(
		0x61 => "00000", 0x62 => "00001", 0x63 => "00010", 0x64 => "00011",
		0x65 => "00100", 0x66 => "00101", 0x67 => "00110", 0x68 => "00111",
		0x69 => "01000", 0x6a => "01001", 0x6b => "01010", 0x6c => "01011",
		0x6d => "01100", 0x6e => "01101", 0x6f => "01110", 0x70 => "01111",
		0x71 => "10000", 0x72 => "10001", 0x73 => "10010", 0x74 => "10011",
		0x75 => "10100", 0x76 => "10101", 0x77 => "10110", 0x78 => "10111",
		0x79 => "11000", 0x7a => "11001", 0x32 => "11010", 0x33 => "11011",
		0x34 => "11100", 0x35 => "11101", 0x36 => "11110", 0x37 => "11111",
	);
	$iInputCheck = strlen($sInString) % 8;
	if(($iInputCheck == 1) || ($iInputCheck == 3) || ($iInputCheck == 6)){
		return "strlen";
	}
	for ($iCount = 0; $iCount < strlen($sInString); $iCount++){
		$sInChar = ord(substr($sInString, $iCount, 1));
		if (isset($BASE32_TABLE[$sInChar])){
			$sDeCompBits .= $BASE32_TABLE[$sInChar];
		} else {
			return 0;
		}
	}
	$iPadding = strlen($sDeCompBits) % 8;
	$sPaddingContent = substr
		($sDeCompBits, (strlen($sDeCompBits) - $iPadding));
	if(substr_count($sPaddingContent, "1") > 0){
		return 0;
	}
	$aDeArr = array();
	for($iCount = 0; $iCount < (int)(strlen($sDeCompBits) / 8); $iCount++){
		$aDeArr[$iCount] = chr(bindec(substr($sDeCompBits, $iCount*8, 8)));
	}
	$sOutString = join("", $aDeArr);
	return $sOutString;
}



//==============================================================================
function stripNonPrint($sStringUserInput){
	$sString=preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $sStringUserInput);
	$sString=str_replace("&","+",$sString);
	$sString=str_replace("<br>","",$sString);
	$sString=str_replace("<","",$sString);
	$sString=str_replace(">","",$sString);

	return $sString;
}




?>