<?php
if (!isset($_POST["iUploadNum"])){
	return;
}
$aFile = "aFile_" . $_POST["iUploadNum"];
$sFilename = $_FILES[$aFile]["name"];
$sFileTemp = $_FILES[$aFile]["tmp_name"];
$fileType = $_FILES[$aFile]["type"];
$fileSize = $_FILES[$aFile]["size"];
$fileErrorMsg = $_FILES[$aFile]["error"];
if (!$sFileTemp) { 
	echo "Browse for a file first."; 
	exit(); 
} 
if(move_uploaded_file($sFileTemp, "../tmp/" . $sFilename)){
	$sExt = strtolower(pathinfo($sFilename, PATHINFO_EXTENSION));
	$sMD5 = md5_file("../tmp/" . $sFilename);
	$sNewFilename = $sMD5. "-" . time() . "." . $sExt;
	rename("../tmp/" . $sFilename, "../tmp/" . $sNewFilename);
	echo $sNewFilename; 
} else { 
	echo ""; 
} 
?>