//VARIABLES  ===================================================================
var V ={
	sSrvURL: "h2srv/",
	aAA:[],
	oDiv:{},
	oHouse:{
		sIconLogo:"&#xE05B;",
		sText1:"Helping Out is a mobile platform that enables non-profit organisations to receive help by advertising their needs free of charge to individuals who are willing to volunteer their time or donate to a good cause.",
		aSocials:[{sIcon:"&#xE073",sLink:"http://fb"},{sIcon:"&#xE07C",sLink:"http://fb"},{sIcon:"&#xE083",sLink:"mailto:helpouting@helpingout.com"}],
		sIconStep:"&#xE080",
		sText21:"Are you an organisation that",
		sText22:"NEEDS HELP?",
		aIcons2:[
		{sIcon:"&#xE081",sText1:"REGISTER",sText2:"Register your organisation online"},
		{sIcon:"&#xE082",sText1:"LIST NEEDS",sText2:"List things your organisation needs help with on the website"},
		{sIcon:"&#xE083",sText1:"GET HELP",sText2:"Receive commitments from individuals via your organisation's mail box"}
		],
		sButtonRegister:"REGISTER NOW",
		sText31:"Are you an individual looking to",
		sText32:"HELP OUT?",
		aIcons3:[
		{sIcon:"&#xE084",sText1:"DOWNLOAD",sText2:"Download the app on your phone"},
		{sIcon:"&#xE085",sText1:"FIND NEEDS",sText2:"Find and commit to needs close to your heart and connect with organisations"},
		{sIcon:"&#xE086",sText1:"HELP OUT",sText2:"Help out with needs you found and make the world a better place"}
		],
		aAppLinks:["https://play.google.com","https://www.apple.com"],
		aSocials:[
		{sIcon:"&#xE073",sName:"socialfacebook",sURL:"https://www.facebook.com/helpingoutza/"},
		{sIcon:"&#xE07C",sName:"socialtwitter",sURL:"https://twitter.com/@Helpingout_ZA"},
		{sIcon:"&#xE083",sName:"socialemail",sURL:"mailto:helpout@4imobile.co.za"}
		],
		sDocs:"View our |Terms & Conditions| and/or |Privacy Policy",
		aMenusLeft:["HOME", "CREATE POST", "MY POSTS", "USERS", "PROFILE"],
		aMenusTop:["Login", "Register"],
		aTabsRegister:["USER LOGIN", "BASIC INFO", "CONTACT INFO", "ADDRESS", "LINKS"],
		aTabsMyPosts:["ACTIVE", "DRAFTS", "HISTORY", "POST", "RESPONSES", "EDIT", "POST", "RESPONSES", "EDIT", "POST", "RESPONSES", "EDIT", "BACK"],
		aTabsUsers:["ADD USER", "EDIT", "ADD", "BACK"],
		aFooterButtonsReg:["CANCEL", "NEXT STEP", "SAVE & CREATE"],
		oUserInfo:{sCompany:"SOLO Dogs", sName:"Sol Rousseau", sLogo:"data/0delmeavatar.jpg"},
	},
	oDB:{
		oWords:{sLoginTitlebar:"Login", sButtonCancel:"CANCEL", 
			oDate:{sDays:"Sun|Mon|Tue|Wed|Thu|Fri|Sat",sMonths:"January|February|March|April|May|June|July|August|September|October|November|December",sDaysLong:"Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday"},
			sButtonOK:"OK"},
		oError:{
			oInput:{
				empty:"Required field is empty.",
				email:"Invalid email address.",
				password:"Invalid password.",
			},
		},
		oDashboard:{
			sLastLogin:"Your last login was on ", sReport:"report suspicious activity.", 
			sLastLoginReport:" <br>If you think this was someone else, please<br>",
			sYourPostTypes:"YOUR POST TYPES", sResponses:"RESPONSES", 
			sToYourPost:"to your post<br>",
			aPostTypes:["Time", "Funds", "Items"], sDays:"DAYS", 
			sMemberFor:"You have been a member of<br>", sTitle:"Helping Out", sFor:" for",
		},
		oEnum:{
			aSelect:[{iID:-1,sValue:"--- Select ---"}],
			oColor:{sGold:"#FEC111", sGreen:"#72BF44", sRed:"#F05455"},
			sUserType:[{iID:1,sValue:"Contributor"},{iID:2,sValue:"Administrator"}],
			sOrganisationType:[{iID:0,sValue:"NGO"},{iID:1,sValue:"CBO"},{iID:2,sValue:"FBO"},{iID:3,sValue:"Section 21"},{iID:4,sValue:"Trust"},{iID:5,sValue:"Other"}],
			sNeedType:[{iID:0,sValue:"Time"},{iID:1,sValue:"Funds"},{iID:2,sValue:"Items"}],
			sPostPriority:[{iID:0,sValue:"Normal"},{iID:1,sValue:"High"}],
			sPostStatus:[{iID:0,sValue:"Inactive"},{iID:1,sValue:"Active"},{iID:2,sValue:"Deleted"},{iID:3,sValue:"Archived"}],
			sOrganisationCategory:[{iID:0,sValue:"Animal Welfare"},{iID:1,sValue:"Education"},{iID:2,sValue:"Environment"},{iID:3,sValue:"Health"},{iID:4,sValue:"Safety"},{iID:5,sValue:"Social Services"}],
			sReadStatus:[{iID:0,sValue:"Unread"},{iID:1,sValue:"Read"}],
			sInboxType:[{iID:0,sValue:"Email"},{iID:1,sValue:"Push"}],
			sPushType:[{iID:0,sValue:"None"},{iID:1,sValue:"Post"},{iID:2,sValue:"Success Sory"},{iID:3,sValue:"Message"}],
			sRegionType:[{iID:0,sValue:"Country"},{iID:1,sValue:"Province"},{iID:2,sValue:"Suburb"},{iID:3,sValue:"Town"}]
		},		
		oForm:{
			aAddress:[
				{sType:"select", sText:"Province", bRequired:1},
				{sType:"select", sText:"Region"},
				{sType:"select", sText:"Town", bRequired:1},
				{sType:"select", sText:"Suburb (If applicable)"},
				{sType:"input", sText:"Address 1"},
				{sType:"input", sText:"Address 2"},
				{sType:"radio", sText:"Main Address", sName:"mainaddress"},
				{sType:"button", sText:"ADD", sClassName:"address"},
				{sType:"button", sText:"REMOVE", sClassName:"address"},
				{sType:"line", sClassName:"address"},
			],
			aLogin:[
				{sType:"email", sText:"Email", sClassName:"popup"},
				{sType:"password", sText:"Password", sClassName:"popup"},
				{sType:"button", sText:"LOGIN", sClassName:"centered act", bDefault:1, sID:"login", bCanDisable:1},
				{sType:"href", sText:"Forgot Password", sClassName:"centered act"},
				{sType:"memo", sText:"Not registered yet?", sClassName:"notregisteredyet"},
				{sType:"href", sText:"Register Now", sClassName:"registernow act"},
			],
			aLoginForgotPassword:[
				{sType:"memo", sText:"Use this form to reset your password. You will receive a password reset link via email when you continue.", sClassName:"popup"},
				{sType:"email", sText:"Email", sClassName:"popup"},
				{sType:"button", sText:"SEND RESET LINK", sClassName:"centered act", bDefault:1, sID:"sendresetlink"},
			],
			aLoginRegisterActivate:[
				{sType:"memo", sText:"Welcome and thanks for helping out!<br><br>After activation, you may login with the details that you have provided.", sClassName:"popup"},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"button", sText:"ACTIVATE", sClassName:"centered act", bDefault:1, sID:"registeractivate"},
			],
			aLoginResetPassword:[
				{sType:"memo", sText:"Enter your new password twice.", sClassName:"popup"},
				{sType:"password", sText:"New Password", sClassName:"popup"},
				{sType:"password", sText:"Repeat New Password", sClassName:"popup"},
				{sType:"button", sText:"RESET PASSWORD", sClassName:"centered act", bDefault:1, sID:"resetpassword", bCanDisable:1},
			],
			aHomeChangePassword:[
			{sType:"memo", sText:"Enter your new password twice.", sClassName:"popup"},
			{sType:"password", sText:"Current Password", sClassName:"popup"},
			{sType:"password", sText:"New Password", sClassName:"popup"},
			{sType:"password", sText:"Repeat New Password", sClassName:"popup"},
			{sType:"button", sText:"CHANGE PASSWORD", sClassName:"centered", sID:"changepassword", bCanDisable:1},
		],
			aUserBox:[
				{sType:"button", sText:"PROFILE", sClassName:"menuuserbox act"},
				{sType:"button", sText:"CHANGE PASSWORD", sClassName:"menuuserbox act"},
				{sType:"button", sText:"LOGOUT", sClassName:"menuuserbox act"},				
			],	
			aUserBoxSmall:[
				{sType:"button", sText:"CREATE POST", sClassName:"menuuserbox act"},
				{sType:"button", sText:"MY POSTS", sClassName:"menuuserbox act"},
				{sType:"button", sText:"USERS", sClassName:"menuuserbox act"},				
				{sType:"button", sText:"PROFILE", sClassName:"menuuserbox act"},
				{sType:"button", sText:"CHANGE PASSWORD", sClassName:"menuuserbox act"},
				{sType:"button", sText:"LOGOUT", sClassName:"menuuserbox act"},				
			],	
			aReg1:[
				{sType:"line", sClassName:"registerpage"},
				{sType:"input", sText:"Name", bRequired:1},
				{sType:"input", sText:"Surname", bRequired:1},
				{sType:"email", sText:"Email", bRequired:1},
				{sType:"password", sText:"Password", bRequired:1, bNotProfile:1},
				{sType:"password", sText:"Confirm Password", bRequired:1, bNotProfile:1},
				{sType:"line", sClassName:"registerpage"},
			],
			aReg2:[
				{sType:"line", sClassName:"registerpage"},
				{sType:"input", sText:"Organisation Name", bRequired:1},
				{sType:"upload", sText:"Upload Company Logo", sAction:"uploaddone", bRequired:1, sExtensions:"jpg, png"},
				{sType:"imagebox", sText:"Company Logo", bNoDisplay:1},
				{sType:"select", sText:"Organisation Category", sEnum:"sOrganisationCategory", bRequired:1},
				{sType:"select", sText:"Organisation Type", sEnum:"sOrganisationType"},
				{sType:"textarea", sText:"Description / Statement", iRows:4},
				{sType:"line", sClassName:"registerpage"},
			],
			aReg3:[
				{sType:"line", sClassName:"registerpage"},
				{sType:"input", sText:"Contact Person", bRequired:1},
				{sType:"email", sText:"Email", bRequired:1},
				{sType:"telephone", sText:"Contact Number", bRequired:1},
				{sType:"weekhours", sText:"Business Hours"},
				{sType:"line", sClassName:"registerpage"},
			],
			aReg4:[
				{sType:"line", sClassName:"registerpage"},
				{sType:"address"},
			],
			aReg5:[
				{sType:"line", sClassName:"registerpage"},
				{sType:"input", sText:"Website", bURL:1},
				{sType:"input", sText:"Facebook", bURL:1},
				{sType:"input", sText:"Twitter Handle"},
				{sType:"input", sText:"Instagram", bURL:1},
				{sType:"line", sClassName:"registerpage"},
			],
			aMyPosts1:[
				{sType:"grid",iTabNo:1,sName:"mypostsactive",sFormName:"MyPosts",aPos:[0,10,0,0],oSort:{iColumn:4,iDirection:1},iRowOverWidth:70,
					aColumns:[{sName:"Title",iWidth:25},{sName:"Responses",iWidth:12,bNumerical:1},{sName:"Type",iWidth:13},{sName:"Date",iWidth:20},{sName:"",bNoSort:1,
					oEle:{sType:"button",sText:"DUPLICATE TO DRAFTS", sAction:"activeduplicatetodrafts"}}]}
			],
			aMyPosts2:[
				{sType:"grid",iTabNo:2,sName:"mypostsdrafts",sFormName:"MyPosts",aPos:[0,10,0,0],oSort:{iColumn:4,iDirection:1},iRowOverWidth:70,
					aColumns:[{sName:"Title",iWidth:25},{sName:"Responses",iWidth:12,bNumerical:1},{sName:"Type",iWidth:13},{sName:"Date",iWidth:20},{sName:"",bNoSort:1,
					oEle:{sType:"button",sText:"DUPLICATE",sAction:"draftsduplicate"}}]}
			],
			aMyPosts3:[
				{sType:"grid",iTabNo:3,sName:"mypostshistory",sFormName:"MyPosts",aPos:[0,10,0,0],oSort:{iColumn:4,iDirection:1},iRowOverWidth:70,
					aColumns:[{sName:"Title",iWidth:25},{sName:"Responses",iWidth:12,bNumerical:1},{sName:"Type",iWidth:13},{sName:"Date",iWidth:20},{sName:"",bNoSort:1,
					oEle:{sType:"button",sText:"DELETE",sAction:"historydelete"}}]}
			],
			aMyPosts4:[
				{sType:"output", sClassName:"mypoststitle"},
				{sType:"output", sClassName:"mypoststype"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Drop Off Location"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Post Date"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Post Start Date"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Post End Date"},
				{sType:"output", sClassName:"myposts"},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"button",sText:"EDIT", sClassName:"footerbutton"},
				{sType:"button",sText:"MARK AS COMPLETE", sClassName:"footerbutton"},
				{sType:"button",sText:"MARK AS DRAFT", sClassName:"footerbutton"},
				{sType:"button",sText:"DUPLICATE TO DRAFTS", sClassName:"footerbutton"}
			],
			aMyPosts5:[
				{sType:"grid",iTabNo:5,sName:"mypostsedit",sFormName:"MyPosts",oSort:{iColumn:1,iDirection:1},bNoPopulate:1,bNoEdit:1,
					aColumns:[{sName:"Title",iWidth:50},{sName:"Email",iWidth:25},{sName:"Date"}], bNoPopulate:1}
			],
			aMyPosts6:[
				{sType:"memo", sText:""},
				{sType:"input", sText:"Title", bRequired:1, iMaxChars:25},
				{sType:"textarea", sText:"Description", bRequired:1, iRows:4},
				{sType:"select",sText:"Drop Off Location", bRequired:1},
				{sType:"date",sText:"Post Start Date", bRequired:1},
				{sType:"date",sText:"Post End Date"},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"button",sText:"SAVE", sClassName:"footerbutton"},
				{sType:"button",sText:"CANCEL", sClassName:"footerbutton"},
			],
			aMyPosts7:[
				{sType:"output", sClassName:"mypoststitle"},
				{sType:"output", sClassName:"mypoststype"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Drop Off Location"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Post Date"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Post Start Date"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Post End Date"},
				{sType:"output", sClassName:"myposts"},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"button",sText:"EDIT", sClassName:"footerbutton"},
				{sType:"button",sText:"DELETE", sClassName:"footerbutton"},
				{sType:"button",sText:"POST NOW", sClassName:"footerbutton"},
			],
			aMyPosts8:[
				{sType:"input", sText:"Tab22"},
				{sType:"line", sClassName:"registerpage"},
			],
			aMyPosts9:[
				{sType:"input", sText:"Title", bRequired:1, iMaxChars:25},
				{sType:"textarea", sText:"Description", bRequired:1, iRows:4},
				{sType:"select",sText:"Drop Off Location", bRequired:1},
				{sType:"date",sText:"Post Start Date", bRequired:1},
				{sType:"date",sText:"Post End Date"},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"button",sText:"SAVE", sClassName:"footerbutton"},
				{sType:"button",sText:"CANCEL", sClassName:"footerbutton"},
			],
			aMyPosts10:[
				{sType:"output", sClassName:"mypoststitle"},
				{sType:"output", sClassName:"mypoststype"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Drop Off Location"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Post Date"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Post Start Date"},
				{sType:"output", sClassName:"myposts"},
				{sType:"memo", sClassName:"mypostsedit", sText:"Post End Date"},
				{sType:"output", sClassName:"myposts"},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"button",sText:"DELETE", sClassName:"footerbutton"},
				{sType:"button",sText:"DUPLICATE TO DRAFTS", sClassName:"footerbutton"},
			],
			aMyPosts11:[
				{sType:"input", sText:"Tab32"},
				{sType:"line", sClassName:"registerpage"},
			],
			aMyPosts12:[
				{sType:"input", sText:"Title", bRequired:1, iMaxChars:25},
				{sType:"textarea", sText:"Description", bRequired:1, iRows:4},
				{sType:"select",sText:"Drop Off Location", bRequired:1},
				{sType:"date",sText:"Post Start Date", bRequired:1},
				{sType:"date",sText:"Post End Date"},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"button",sText:"SAVE", sClassName:"footerbutton"},
				{sType:"button",sText:"CANCEL", sClassName:"footerbutton"},
			],
			aUsers1:[
				{sType:"grid",iTabNo:1,sName:"users",sFormName:"Users",aPos:[2,10,0,0],oSort:{iColumn:3,iDirection:1},
					aColumns:[{sName:"Name",iWidth:20},{sName:"Surname",iWidth:20},{sName:"Email",iWidth:35},{sName:"Role"}]}
			],
			aUsers2:[
				{sType:"memo", sText:""},
				{sType:"email", sText:"Email", bRequired:1},
				{sType:"input", sText:"Name", bRequired:1},
				{sType:"input", sText:"Surname", bRequired:1},
				{sType:"select", sText:"Role", bRequired:1},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"button", sText:"SAVE", sClassName:"createpost"},
				{sType:"button", sText:"DELETE", sClassName:"createpost"},
				{sType:"button", sText:"CLOSE", sClassName:"createpost"}
			],
			aUsers3:[
				{sType:"memo", sText:""},
				{sType:"email", sText:"Email", bRequired:1},
				{sType:"input", sText:"Name", bRequired:1},
				{sType:"input", sText:"Surname", bRequired:1},
				{sType:"select", sText:"Role", bRequired:1},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"button", sText:"SAVE", sClassName:"createpost"},
				{sType:"button", sText:"CLOSE", sClassName:"createpost"}
			],
			aHome1:[
				{sType:"box", sClassName:"home lastlogin"},
				{sType:"box", sClassName:"home yourposttypes"},
				{sType:"box", sClassName:"home member"},
				{sType:"box", sClassName:"home reponses red"},
				{sType:"box", sClassName:"home responses green"},
				{sType:"box", sClassName:"home responses gold"},
			],
			aCreatePost1:[
				{sType:"memo", sText:""},
				{sType:"input", sText:"Title", bRequired:1, iMaxChars:25},
				{sType:"textarea", sText:"Description", bRequired:1, iRows:4},
				{sType:"date", sText:"Post Start Date", bRequired:1, sClassName:"floatleft"},
				{sType:"date", sText:"Post End Date", bNoValue:1, sClassName:"floatleft"},
				{sType:"memo", sText:"TYPE", sClassName:"createposttype"},
				{sType:"button", sText:"ITEMS", sClassName:"createpost"},
				{sType:"button", sText:"TIME", sClassName:"createpost"},
				{sType:"button", sText:"FUNDS", sClassName:"createpost"},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"box", sClassName:"createpostinfo"},
				{sType:"box", sClassName:"createpostinfo"},
				{sType:"box", sClassName:"createpostinfo"},
				{sType:"button", sText:"CANCEL", sClassName:"createpost"},
				{sType:"button", sText:"SAVE DRAFT", sClassName:"createpost"},
				{sType:"button", sText:"POST NOW", sClassName:"createpost"}
			],
			aCreatePostItems1:[
				{sType:"select", sText:"Drop Off Location", bRequired:1},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"line", sClassName:"registerpage"},
			],
			aCreatePostTime1:[
				{sType:"date",sText:"Volunteer Start Date", bRequired:1, sClassName:"floatleft"},
				{sType:"date",sText:"Volunteer End Date", bNoValue:1, sClassName:"floatleft"},
				{sType:"time",sText:"Volunteer Start Time", bRequired:1, sClassName:"floatleft"},
				{sType:"time",sText:"Volunteer End Time", sClassName:"floatleft"},
				{sType:"time",sText:"Minimum Volunteer Time", bRequired:1, sClassName:"floatleft"},
				{sType:"time",sText:"Maximum Volunteer Time", sClassName:"floatleft"},
				{sType:"select",sText:"Location", bRequired:1},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"line", sClassName:"registerpage"},
			],
			aCreatePostFunds1:[
				{sType:"memo", sClassName:"createposttype", sText:"<b>Banking Details Policy</b><br><br>It is our policy not to display banking details on this platform.  Please add the link or detail of where a user can find your banking details in the description field.  Any post which displays banking details will be removed."},
				{sType:"spacer", sClassName:"popupspacer"},
				{sType:"line", sClassName:"registerpage"},
			],
		},		
	},
	oGen:{
		iLastKey:0,
		divActive:0,
		aBreadcrumbs:[],
		iLoaderWidth:0,
		iLoaderDir:3,
		iMenuNo:0,
		oMouse:{},
		iPageTop:0,
		bResizing:0,
		iScrollTop:0,
		iHeaderHeight:0,
		iWidthScrollbar:0,
		oZIndex:{iIndex:5, divLast:0},
		sIDButtonDefault:"",
		sIDButtonDisabled:"",
		bUserBoxShow:0,
		iWidthWindow:0,
		iAAStart:1, iAAMax:1,
		bJSLoaded:0,
		iDisplayScreenSize:0,
		aDisplayScreenSizes:[
			{iFontSize:10, iHeightFooter:20, iHeightLogo:48,	aHeightHeaders:[48, 70, 68], iWidthLogo:60,	 iWidthLeft:0,	  iWidthMain:160},
			{iFontSize:10, iHeightFooter:30, iHeightLogo:48,	aHeightHeaders:[48, 70, 68], iWidthLogo:60,  iWidthLeft:0,  	iWidthMain:360},
			{iFontSize:11, iHeightFooter:40, iHeightLogo:123, aHeightHeaders:[48, 75, 68], iWidthLogo:100, iWidthLeft:100,	iWidthMain:640},
			{iFontSize:12, iHeightFooter:40, iHeightLogo:128,	aHeightHeaders:[48, 80, 69], iWidthLogo:160, iWidthLeft:160,	iWidthMain:1024},
			{iFontSize:13, iHeightFooter:40, iHeightLogo:140,	aHeightHeaders:[48, 80, 71], iWidthLogo:200, iWidthLeft:200,	iWidthMain:1440},
		],
	}
};



//APP CLASS  ===================================================================
var A = {
		
		
		
action:function(oObj, sAction){
//	console.log(oObj.sName + " initaction: " + sAction);
	return function(){
		console.log(oObj.sName + " action: " + sAction);
		oObj.action(sAction);
	}
},



ajaxA:function(oAjax){
	if (V.oDiv.loader){
		V.oDiv.loader.style.display = "block";
		B.loader();
	}
	oAjax.oXHR = new XMLHttpRequest();
	if ("withCredentials" in oAjax.oXHR){
		oAjax.oXHR.open("POST", V.sSrvURL, true);
	} else if (typeof XDomainRequest != "undefined"){
		oAjax.oXHR = new XDomainRequest();
		oAjax.oXHR.open("POST", V.sSrvURL);
	} else {
		return;
	}
	oAjax.oXHR.setRequestHeader("Content-type", 
		"application/x-www-form-urlencoded; charset=utf-8");
	oAjax.oXHR.onload = function(){
		if (V.oDiv.loader){
			V.oDiv.loader.style.display = "none";
			V.oDiv.loader.style.width = "0";
		}
		var oData = A.json(atob(oAjax.oXHR.responseText));
		if (oAjax.fCallback){
			oAjax.fCallback(oData);
		}
		V.aAA[oAjax.iAjaxNo] = 0;
	};
	var sData = A.json(1, 
		{sAction:oAjax.sAction, aValues:oAjax.aValues, 
		iTimeClient:parseInt(((new Date()).getTime())/1000),
		sSessionID:A.session("sSessionID")});
	sData = encodeURIComponent(sData);
	oAjax.oXHR.send("data=" + btoa(sData));	
},



ajax:function(sAction, aValues, fCallback){
	var iObjNo = A.object();
	V.aAA[iObjNo] = {sAction:sAction, aValues:aValues, fCallback:fCallback};
	V.aAA[iObjNo].sType =  "ajax";
	V.aAA[iObjNo].iStatus =  0;
	V.aAA[iObjNo].iObjNo =  iObjNo;
},



attribute:function(divX, sKey, sValue){
	if (!divX){
		return;
	}
	if ((sValue) || (sValue == "") || (sValue === 0)){
		var oAttr = document.createAttribute(sKey);
		oAttr.value = sValue;
		divX.setAttributeNode(oAttr);
	} else {
		sValue = divX.attributes[sKey];
		if (sValue){
			sValue = sValue.value;
		} else {
			sValue = "";
		}
	}
	return sValue;
},



dg:function(sID){
	return document.getElementById(sID);
},



gcn:function(sClassName, bAll){
	var iI = 0;
	var aReturn = document.getElementsByClassName(sClassName);
	if (!aReturn.length){
		aReturn = [V.oDiv.dummy];
//		console.log("V.oDiv.dummy = "+sClassName)
	}
	if (bAll){
		var iSize = aReturn.length;
		var aRet1 = [];
		for (iI = 0; iI < iSize; iI++){
			var oRec = aReturn[iI];
			if ((oRec) && (oRec.className.indexOf(sClassName) != -1)){
				aRet1.push(oRec);
			}
		}
		return aRet1;
	} else {
		return aReturn[0];
	}
},


history:function(sHash){
	var sHashNow = window.location.href.split("#")[1];
	if (sHash){
		var sTitle = document.title.split(" - ")[0];
		if (sHashNow != sHash){
			window.history.pushState({"html":"", "pageTitle":""}, 
				"", ".#" + sHash);
		}
	}
	if (!sHashNow){
		sHashNow = "";
	}
	return sHashNow;
},



json:function(sKey, sValue){
	if (sValue){
		return JSON.stringify(sValue);
	} else {
		return JSON.parse(sKey);
	}
},



object:function(){
	var iI = V.oGen.iAAStart;
	while ((V.aAA[iI] !== undefined) && (V.aAA[iI] !== 0)){
		iI++;
	}
	if (iI > V.oGen.iAAMax){
		V.oGen.iAAMax = iI;
	}
	V.aAA[iI] = {};
	return iI;
},



session:function(sKey, sValue){
	var sAppName = document.title.replace(/ /g, "");
	var oSession = sessionStorage.getItem(sAppName);
	if (oSession){
		oSession = A.json(oSession);
	} else {
		//create
		oSession = {iTimeClient:0, iTimeServer:0, sURLHash:""};
		sessionStorage.setItem(sAppName, A.json(0, oSession));
	}
	if ((sValue) || (sValue === 0)){
		oSession = A.json(sessionStorage.getItem(sAppName));
		oSession[sKey] = sValue;
		sessionStorage.setItem(sAppName, A.json(0, oSession));
		return sValue;
	} else {
		return oSession[sKey];
	}
},



url: function(sParam){
	var sURL = window.location.href;
	sParam = sParam.replace(/[\[\]]/g, "\\$&");
	var oRegex = new RegExp("[?&]" + sParam + "(=([^&#]*)|&|#|$)");
	var aResult = oRegex.exec(sURL);
	if (!aResult) {
		return null;
	}
	if (!aResult[2]) {
		return "";
	}
  return decodeURIComponent(aResult[2].replace(/\+/g, " "));
},



valid:function(oEle){
	var bValid = 1;
	switch (oEle.sType){
		case "email":
			var sReg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if ((oEle.sValue) && (!sReg.test(oEle.sValue))){
				bValid = 0;
			}
		break;
		
		case "password":
			if (oEle.sValue.length < 6){
				bValid = 0;
			}
		break;
			
	}
	return bValid;
},



value:function(sID, sValue){
	var iI = 0, oReturn = {}, divX = 0, iIndex = 0, divOption = 0,
		sOption = "", bFound = 0 , sText = "", aValue = [], aNumeric = [],
		iObjNo = 0, bDone = 0;
	iIndex = (parseInt(sValue) * 1);
	divX = A.dg(sID);
	if (!divX){
		return {};
	}
	var sType = A.attribute(divX, "type");
	if ((!sValue) && (isNaN(iIndex))){
		//get
		switch (sType){
			case "input":
			case "textarea":
			case "date":
			case "time":
			case "email":
			case "password":
			case "telephone":
				oReturn = {sValue:divX.value, sType:sType, sID:sID};
			break;
			
			case "radio":
				sValue = divX.checked;
				if (sValue){
					sValue = 1;
				} else {
					sValue = 0;
				}
				oReturn = {sValue:sValue, sType:sType, sID:sID};
			break;
			
			case "select-one":
				bFound = 0;
				iI = 0;
				divOption = 0;
				sText = "";
				while ((!bFound) && (divOption = divX.options[iI])){
					sOption = divOption.text;
					if (divOption.value == divX.value){
						bFound = 1;
						sText = divOption.text;
					} else {
						iI++;
					}
				}
				oReturn = {sValue:divX.value, sType:sType, sID:sID, sText:sText};
			break;
			
			case "address":
				aValue = [0];
				aNumeric = [1, 2, 3, 4, 7];
				for (iI = 1; iI < 8; iI++){
					sValue = A.value(sID + "_" + iI);
					if (aNumeric.indexOf(iI) != -1){
						aValue.push(parseInt(sValue.sValue));
					} else {
						aValue.push(sValue.sValue);	
					}
				}
				oReturn = {sValue:aValue, sType:sType, sID:sID};
			break;
			
			case "upload":
				oReturn = {sValue:A.attribute(divX, "uploaded"), 
					sType:sType, sID:sID};
			break;
			
			case "weekhours":
				aValue = [];
				/*
				aValue[0] = parseInt(A.value(sID + "-dayfrom").sValue);
				aValue[1] = parseInt(A.value(sID + "-dayto").sValue);
				aValue[2] = A.dg(sID + "-timefrom").value;
				aValue[3] = A.dg(sID + "-timeto").value;
				oReturn = {sValue:aValue, sType:sType, sID:sID};
				*/
			break;
	
		}
	} else {
		//set
		if (typeof sValue.sValue == "undefined"){
			sValue = {sValue:sValue};
		}
		switch (sType){
			case "input":
			case "textarea":
			case "email":
			case "date":
			case "time":
			case "password":
			case "telephone":
				divX.value = sValue.sValue;
			break;
			
			case "radio":
				if (sValue.sValue){
					divX.checked = true;
				} else {
					divX.checked = false;
				}
			break;
			
			case "select-one":
				var sTypeOf = typeof sValue.sValue;
				switch (sTypeOf){
					case "object":
						divX.innerHTML = "";
						var iIndex = 0;
						var divOption = E.div(divX, "eleselectoption", "", "option");
						divOption.value = -1;
						divOption.text = V.oDB.oEnum.aSelect[0].sValue;
						sValue.sValue.forEach(function(oRec){
							var divOption = E.div(divX, "eleselectoption", "", "option");
							if (oRec.iID){
								divOption.value = oRec.iID;
							} else {
								divOption.value = iIndex;
							}
							divOption.text = oRec.sValue;
							iIndex++;
						});
					break;
					
					case "number":
						divX.value = sValue.sValue;
					break;
					
				}
				
			break;
			
			case "address":
				iObjNo = parseInt(sID.split("_")[3]);
				for (iI = 1; iI < 8; iI++){
					bDone = 0;
					switch (iI){
						case 1:
							A.dg(sID + "_" + iI).value = sValue.sValue[iI];
							V.aAA[iObjNo].province();
							bDone = 1;
						break;
						
						case 2:
							A.value(sID + "_" + iI, sValue.sValue[iI]);
							V.aAA[iObjNo].region();
							bDone = 1;
						break;
						
						case 3:
							A.value(sID + "_" + iI, sValue.sValue[iI]);
							V.aAA[iObjNo].town();
							bDone = 1;
						break;
						
					}
					if (!bDone){
						A.value(sID + "_" + iI, sValue.sValue[iI]);
					}
				}
			break;
			
			case "weekhours":
				sValue = sValue.sValue;
				A.value(sID + "-dayfrom", sValue[0]);
				A.value(sID + "-dayto", sValue[1]);
				A.value(sID + "-timefrom", sValue[2]);
				A.value(sID + "-timeto", sValue[3]);
			break;
			
		}
	}
	return oReturn;
},



zindex:function(divX){
	if (divX){
		if (V.oGen.oZIndex.divLast != divX){
			divX.style.zIndex = V.oGen.oZIndex.iIndex;
			V.oGen.oZIndex.iIndex++;	
		}
		V.oGen.oZIndex.divLast = divX;
	}
}
};



//BODY CLASS  ==================================================================
var B = {
	sName:"BODY",
	sGet:"",
	oOb:{iLogin:0, iRegister:0, iHome:0, iCreatePost:0, iMyPosts:0,
		iUsers:0, iProfile:0, iHouse:0},
	
	
actions:function(){
	var aDivAct = A.gcn("act", 1);
	var iI = 0;
	aDivAct.forEach(function(oRec){
		var sClassName  = oRec.className;
		if (sClassName == "maindummy"){
			delete oRec;
		} else {
			var sClass = "";
			var aClassName = sClassName.split(" ");
			aClassName.forEach(function(sName){
				if (sName != "act"){
					sClass += sName + " ";
				}
			});
			sClass = sClass.slice(0, -1);
			var sType = A.attribute(oRec, "type");
			var sText = "";
			switch (sType){
				case "menu":
					sText = oRec.childNodes[0].textContent;
				break;
				
				case "icon":
					sText = A.attribute(oRec, "name");
				break;
				
				case "href":
					sText = oRec.childNodes[0].textContent;
				break;
				
				case "image":
					sText = A.attribute(oRec, "name");
				break;
				
				case "span":
					sText = A.attribute(oRec, "name");
				break;
				
				case "button":
					sText = oRec.childNodes[0].textContent;
				break;
				
				case "userbox":
					sText = A.attribute(oRec, "name");
				break;
				
			}
			sText = sText.replace(/ /g, "").toLowerCase().substr(0, 35);
			oRec.className = sClass;
			sClass = sClass.replace(/ /g, ".");
			var sID = oRec.id;
			if (!sID){
				sID = "";
			}
			oRec.onclick = A.action(B, sClass + "-" + sText + "-" + sID);
		}
		iI++;
	});
},



action:function(sAction){
	var sSession = "", aAction = [], sElement = "", divX = 0, aPageTop =[],
		iIndex = 0, aDivMenus = [], aID = [], aBreadcrumbs = [];
	aAction = sAction.split("-");
	sElement = aAction[0].split(".")[0];
	switch (sElement){
		case "login":
			V.aAA[B.oOb.iLogin].action(sAction);
		break;
		
		case "mainlogo1":
			V.oGen.iMenuNo = 0;
			B.reset()
		break;
			
		case "menuleft":
			iIndex = parseInt(aAction[2].split("_")[1]);
			aPageTop = [1, 1, 0, 0, 2];
			V.oGen.iPageTop = aPageTop[iIndex];
			divX = A.gcn("menuleft active");
			if ((divX) && (divX.className != "maindummy")){
				divX.className = "menuleft";
			}
			aDivMenus = A.gcn("menuleft", 1);
			aDivMenus.forEach(function(divX){
				aID = divX.id.split("_");
				if (aID[1] == iIndex){
					divX.className = "menuleft active";
				} else {
					divX.className = "menuleft";
				}
			});
			aBreadcrumbs = [];
			switch (iIndex){
				case 0:
					A.history("home");
					if (!B.oOb.iHome){
						B.oOb.iHome = A.object();
						V.aAA[B.oOb.iHome] = new HO_HOME();
						V.aAA[B.oOb.iHome].iObjNo = B.oOb.iHome;
						V.aAA[B.oOb.iHome].init();		
					} else {
						B.action("elebutton.tabbutton-active-tabbutton-" + 
							B.oOb.iHome + "_" + 
							V.aAA[B.oOb.iHome].iTabNo);
					}
				break;
				
				case 1:
					aBreadcrumbs.push("Create Post");
					if (!B.oOb.iCreatePost){
						B.oOb.iCreatePost = A.object();
						V.aAA[B.oOb.iCreatePost] = new HO_CREATEPOST();
						V.aAA[B.oOb.iCreatePost].iObjNo = B.oOb.iCreatePost;
						V.aAA[B.oOb.iCreatePost].init();		
					} else {
						B.action("elebutton.tabbutton-active-tabbutton-" + 
							B.oOb.iCreatePost + "_" + 
							V.aAA[B.oOb.iCreatePost].iTabNo);
					}
				break;
				
				case 2:
					aBreadcrumbs.push("My Posts");
					if (!B.oOb.iMyPosts){
						B.oOb.iMyPosts = A.object();
						V.aAA[B.oOb.iMyPosts] = new HO_MYPOSTS();
						V.aAA[B.oOb.iMyPosts].iObjNo = B.oOb.iMyPosts;
						V.aAA[B.oOb.iMyPosts].init();		
					} else {
						B.action("elebutton.tabbutton-active-tabbutton-" + 
							B.oOb.iMyPosts + "_" + 
							V.aAA[B.oOb.iMyPosts].iTabNo);
					}
				break;
				
				case 3:
					aBreadcrumbs.push("Users");
					if (!B.oOb.iUsers){
						B.oOb.iUsers = A.object();
						V.aAA[B.oOb.iUsers] = new HO_USERS();
						V.aAA[B.oOb.iUsers].iObjNo = B.oOb.iUsers;
						V.aAA[B.oOb.iUsers].init();		
					} else {
						B.action("elebutton.tabbutton-active-tabbutton-" + 
							B.oOb.iUsers + "_" + 
							V.aAA[B.oOb.iUsers].iTabNo);
					}
				break;
				
				case 4:
					aBreadcrumbs.push("Profile");
					if (!B.oOb.iProfile){
						B.oOb.iProfile = A.object();
						V.aAA[B.oOb.iProfile] = new HO_PROFILE();
						V.aAA[B.oOb.iProfile].iObjNo = B.oOb.iProfile;
						V.aAA[B.oOb.iProfile].init();		
					} else {
						B.action("elebutton.tabbutton-active-tabbutton-" + 
							B.oOb.iProfile + "_" + V.aAA[B.oOb.iProfile].iTabNo);
					}
				break;
			}
			B.resize();
			E.breadcrumbs(aBreadcrumbs);
		break;

		case "menutop":
			switch (aAction[1]){
				case "login":
					if (!B.oOb.iLogin){
						B.oOb.iLogin = A.object();
						V.aAA[B.oOb.iLogin] = new HO_LOGIN();
						V.aAA[B.oOb.iLogin].iObjNo = B.oOb.iLogin;
					}
					V.aAA[B.oOb.iLogin].init();		
				break;
				
				case "register":
					if (!B.oOb.iRegister){
						B.oOb.iRegister = A.object();
						V.aAA[B.oOb.iRegister] = new HO_REGISTER();
						V.aAA[B.oOb.iRegister].iObjNo = B.oOb.iRegister;
						V.oGen.iPageTop = 2;
						V.oGen.iMenuNo = 2;
						B.reset();
						V.aAA[B.oOb.iRegister].init();
					} else {
						if (V.oGen.iMenuNo != 2){
							B.action("elebutton.regbutton-userlogin-regbutton-" + 
								B.oOb.iRegister + "_" + 
								V.aAA[B.oOb.iRegister].iStepNow);
						}
					}
				break;
				
				case "house":
					if (!B.oOb.iHouse){
						B.oOb.iHouse = A.object();
						V.aAA[B.oOb.iHouse] = new HO_HOUSE();
						V.aAA[B.oOb.iHouse].iObjNo = B.oOb.iHouse;
					}
					V.aAA[B.oOb.iHouse].init();
				break;
				
			}
		break;
		
		case "menuuserboxover":
			if (B.oOb.iHome){
				V.aAA[B.oOb.iHome].userbox();
			}
		break;
			
		case "elehref":
			switch (aAction[1]){
				case "forgotpassword":
					V.aAA[B.oOb.iLogin].passwordforgot();
				break;
				
				case "registernow":
					E.action("popupclosex");
					B.action("menutop-register");
				break;
				
			}			
		break;

		case "eleiconover":
			switch (aAction[1]){
				case "popupclosex":
					E.action("popupclosex");
				break;
				
				case "pageclose":
					var iObjNo = parseInt(aAction[2].split("_")[1]);
					var that = V.aAA[iObjNo].oEle.that;
					if (that){
						that.editclose(iObjNo);
					}
				break;
				
				case "logo":
					var iUser = A.session("iUser");
					if (iUser){
						B.action("menuleft-home-menuleft_0");			
					} else {
						B.action("menutop-house");
					}
				break;
				
				case "socialfacebook":
					var sURL = V.oHouse.aSocials[0].sURL;
					window.open(sURL, "_parent");
				break;
				case "socialtwitter":
					var sURL = V.oHouse.aSocials[1].sURL;
					window.open(sURL, "_parent");
				break;
				case "socialemail":
					var sURL = V.oHouse.aSocials[2].sURL;
					window.open(sURL, "_parent");
				break;
			}			
		break;

		case "eleimage":
			switch (aAction[1]){
				case "android":
					var sURL = V.oHouse.aAppLinks[0];
					window.open(sURL, "_parent");
				break;
				case "apple":
					var sURL = V.oHouse.aAppLinks[0];
					window.open(sURL, "_parent");
				break;
			}			
		break;
		
		case "elebutton":
			switch (aAction[1]){
			
			case "activate":
				V.aAA[B.oOb.iLogin].activate();
			break;
			
				case "changepassword":
					V.aAA[B.oOb.iHome].changepassword();
				break;
				
				case "login":
					V.aAA[B.oOb.iLogin].go();
				break;
				
				case "logout":
					V.aAA[B.oOb.iHome].logout();
				break;
				
				case "ok":
					if (aAction[2] == "okpopup"){
						E.action("popupclose");
					}
				break;
				
				case "resetpassword":
					V.aAA[B.oOb.iLogin].passwordresetGo();
				break;
				
				case "sendresetlink":
					V.aAA[B.oOb.iLogin].sendresetlink();
				break;
				
				case "profile":
					V.aAA[B.oOb.iHome].profile();
				break;
				
				case "createpost":
					V.aAA[B.oOb.iHome].userbox();
					B.action("menuleft--_1");
				break;
				
				case "myposts":
					V.aAA[B.oOb.iHome].userbox();
					B.action("menuleft--_2");
				break;
				
				case "upload":
					var iObjNo = parseInt(aAction[2].split("_")[1]);
					E.uploadStart(iObjNo);
				break;
				
				case "users":
					V.aAA[B.oOb.iHome].userbox();
					B.action("menuleft--_3");
				break;
				
				case "registernow":
					B.action("menutop-register");
				break;
				
				case "nextstep":
					V.aAA[B.oOb.iRegister].next();
				break;
				
				case "cancel":
					if (aAction[2] == "regfooter"){
						B.action("menutop-house");
					}
				break;
				
				
				
			}
			if (!aAction[3]){
				return;
			}
//			aAction = aAction[2].split("_");
			switch (aAction[2]){
			
				case "regbutton":
					V.aAA[B.oOb.iRegister].step(aAction);
				break;
				
				case "tabbutton":
					var aObj = aAction[3].split("_");
					sSession = "iTabNo_" + V.aAA[parseInt(aObj[0])].sName;
					A.session(sSession, parseInt(aObj[1]));
					V.aAA[parseInt(aObj[0])].tab(parseInt(aObj[1]))
//					B.tab(aAction);
				break;
				
				
				case "footerbutton":
					V.aAA[B.oOb.iRegister].footer(sAction);
				break;
				
			}
			
		break;
	
		case "footerdocslinks":
			V.aAA[B.oOb.iHouse].docs(aAction[1]);
		break;
	


	}
},



height:function(){
	var divBody = document.body;
	var divHTML = document.documentElement;
	var iH = Math.max(divBody.scrollHeight,divBody.offsetHeight, 
		divHTML.clientHeight, divHTML.scrollHeight, divHTML.offsetHeight);
	return iH;
},



initAnimation:function(){
	if (!window.requestAnimationFrame){
		window.requestAnimationFrame = (function(){
			return window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.oRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
		function(fCallback, oElement){
			window.setTimeout(fCallback, 1000/60);
		};})();}
	if (!window.cancelAnimationFrame){
		window.cancelAnimationFrame = (function(){
			return window.webkitcancelAnimationFrame ||
			window.mozcancelAnimationFrame ||
			window.ocancelAnimationFrame ||
			window.mscancelAnimationFrame ||
			function(fCallback, oElement){
				window.setTimeout(fCallback, 1000/60);
		};})();}
},



initEvents:function(){
	screen.orientation.addEventListener("change", function(){
		B.resizeA();
	});
	window.onresize = B.resizeA;
	V.oDiv.body.onkeyup = B.onKeyPress;
	V.oDiv.body.onmousemove = B.onMouseMove;
	window.onhashchange = B.onHashChange;
	window.onbeforeunload = B.onBeforeUnload;
},


initScrollbar:function(){
	var divOuter = document.createElement("div");
  divOuter.style.visibility = "hidden";
  divOuter.style.width = "100px";
  divOuter.style.msOverflowStyle = "scrollbar";
  document.body.appendChild(divOuter);
  var iWidthNoScroll = divOuter.offsetWidth;
  divOuter.style.overflow = "scroll";
  var divInner = document.createElement("div");
  divInner.style.width = "100%";
  divOuter.appendChild(divInner);        
  var iWidthWithScroll = divInner.offsetWidth;
  divOuter.parentNode.removeChild(divOuter);
  return iWidthNoScroll - iWidthWithScroll;
},



initA:function(oData){
	if (!oData){
		oData = V.oDB.oData;
	}
	if (!oData){
		alert("No data :(");
		return;
	}
	var iUser = 0, sAppName = "", oSession = {}, iMenuNo = 0;
	if (oData.sSessionID){
		//handshake
		A.session("sSessionID", oData.sSessionID);
		sAppName = document.title.replace(/ /g, "");
		oSession = A.json(sessionStorage.getItem(sAppName));
		A.ajax("init", oSession, B.initA);
		return;
	}
	//init
	V.oDB.oData = oData;
	var aBreadcrumbs = [];
	if (oData.oUser.iUserID){
		iUser = 1;
	}
	A.session("iUser", iUser);
	var iMenuNo = V.oGen.iMenuNo;
	var sURLHash = A.session("sURLHash");
	if (!sURLHash){
		A.history("house");
		A.session("sURLHash", "house");
	}
	if (sURLHash){
		switch (sURLHash){
			case "house":
				iMenuNo = 0;
			break;
			
			case "register":
				iMenuNo = 2;
			break;
			
		}		
	}
	if (iUser){
		iMenuNo = 1;
		V.oGen.iMenuNo = iMenuNo;
		B.reset();
		B.oOb.iHome = A.object();
		V.aAA[B.oOb.iHome] = new HO_HOME();
		V.aAA[B.oOb.iHome].iObjNo = B.oOb.iHome;
		V.aAA[B.oOb.iHome].init();		
	}
	V.oGen.iMenuNo = iMenuNo;
	if (V.oGen.iMenuNo == 2){
		B.reset();
		B.action("menutop-register");
	}
	if (V.oGen.iMenuNo == 0){
		B.action("menutop-house");
	}
	if (B.sGet){
		B.oOb.iLogin = A.object();
		V.aAA[B.oOb.iLogin] = new HO_LOGIN();
		try {
			var sGet = atob(B.sGet);
			var aGet = sGet.split("|");
			switch (aGet[0]){
				case "passwordreset":
					V.aAA[B.oOb.iLogin].passwordreset(aGet[1]);
				break;
				case "registeractivate":
					V.aAA[B.oOb.iLogin].registeractivate(aGet[1]);
				break;
				case "adduseractivate":
					V.aAA[B.oOb.iLogin].adduserActivate(aGet[1]);
				break;
			}
		} catch(e){
			return;
		} finally {}
	}
},



init:function(){
	V.oDiv.body = document.getElementsByTagName("body")[0];
	V.oGen.iWidthScrollbar = B.initScrollbar();
	B.initAnimation();
	B.initEvents();
	var sGet = A.url("a");
	if (sGet){
		B.sGet = sGet;
	}
	window.setInterval(B.loopMain, 250);
	var sAppName = document.title.replace(/ /g, "");
	var oSession = A.json(sessionStorage.getItem(sAppName));
//	B.reset();
	A.ajax("init", oSession, B.initA);
},



loader: function(){
	if ((V.oDiv.loader) && (V.oDiv.loader.style.display == "block")){
		requestAnimationFrame(B.loader);
		V.oGen.iLoaderWidth += V.oGen.iLoaderDir;
		var iAbs = Math.abs(V.oGen.iLoaderDir);
		if ((V.oGen.iLoaderWidth > 47) || 
			(V.oGen.iLoaderWidth < 3)){
			V.oGen.iLoaderDir *= -1;
		}
		V.oDiv.loader.style.width = V.oGen.iLoaderWidth + "%";
	}
},



loopMain: function(){
	//ajax
	var iI = V.oGen.iAAStart;
	var iFound = 0;
	while ((!iFound) && (iI <= V.oGen.iAAMax)){
		var oRecord = V.aAA[iI];
		if ((oRecord) && (oRecord.sType)){
			if (oRecord.iStatus == 1){
				V.aAA[iI] = 0;
			} else {
				if (!oRecord.iStatus){
					iFound = iI;
					V.aAA[iI].iStatus = 2;
				}
			}
		}
		iI++;
	}
	if (iFound){
		V.aAA[iFound].iAjaxNo = iFound;
		A.ajaxA(V.aAA[iFound]);
	}
	//timed messaged
	if (V.sMessageTimed){
		V.oDiv.messagetimed.style.padding = "0 3em 0 3em";
		V.oDiv.messagetimed.innerHTML = V.sMessageTimed;
		V.sMessageTimed = "";
		window.setTimeout(function(){
			V.oDiv.messagetimed.style.padding = "0";
			V.oDiv.messagetimed.innerHTML = "";
		}, 1500);
	}
},



onBeforeUnload: function(oEvent){
	var aHash = window.location.href.split("#");
	if (aHash[1]){
		A.session("sURLHash", aHash[1]);
	}
	
},



onHashChange:function(oEvent){
	var iMenuNo = V.oGen.iMenuNo;
	var sURLHash = location.hash.slice(1);
	var bFound = 0;
	switch (sURLHash){
		case "house":
			bFound = 1;
			iMenuNo = 0;
		break;
		
		case "register":
			bFound = 1;
			iMenuNo = 2;
		break;
		
	}
	if (!bFound){
		iMenuNo = 1;
	}
	V.oGen.iMenuNo = iMenuNo;
	A.history(sURLHash);
	A.session("sURLHash", sURLHash);
	B.initA();
},



onKeyPress:function(oEvent){
	V.oGen.iLastKey = oEvent.keyCode || oEvent.which;
	V.oGen.divActive = document.activeElement;
	if ((V.oGen.sIDButtonDefault) && (V.oGen.iLastKey == 13)){
		var divButton = A.dg(V.oGen.sIDButtonDefault);
		if (divButton){
			divButton.click();
		}
	}
},



onMouseMove:function(oEvent){
	V.oGen.oMouse.divParent = oEvent.target;
	V.oGen.oMouse.sClassName = oEvent.target.className;
	V.oGen.oMouse.sID = oEvent.target.id;
	V.oGen.oMouse.aPos = 
		[oEvent.screenX, oEvent.screenY, oEvent.offsetX, oEvent.offsetY];
	if (V.oGen.bUserBoxShow){
		if (V.oGen.oMouse.sClassName){
			if (V.oGen.oMouse.sClassName.indexOf("menuuser") == -1){
				if (B.oOb.iHome){
					V.aAA[B.oOb.iHome].userbox();
				}
			}
		}
	}
},



reset2:function(){
	var iI = 0, oEle = {}, divParent = 0, divX = 0, oAll = {}, iWidth = 0,
		divButton = 0, sClassName = "";
	oAll = V.oHouse;
	divParent = A.gcn("maintop one");
	iI = 0; 
	oAll.aMenusTop.forEach(function(oRec){
		divX = E.menu({divParent:divParent, sClassName:"top", 
			iIndex:iI, aMenus:oAll.aMenusTop});
		if (iI == 1){
			divX.className = "menutop active";
		}
		iI++;
	});
	
	V.oDiv.logo = E.icon({divParent:V.oDiv.header, sClassName:"logo1",
		bAction:1, sName:"logo", sText:oAll.sIconLogo});
	E.div(V.oDiv.footer, "mainheaderleft");
	E.div(V.oDiv.footer, "mainheaderwork", "registerfooter");
	return [];
},



reset1:function(){
	var iI = 0, oEle = {}, divParent = 0, divChild = 0, oAll = {}, divMenu = 0;
	oAll = V.oHouse;
	V.oDiv.logo = E.icon({divParent:V.oDiv.header, sClassName:"logo1",
		bAction:1, sName:"logo", sText:oAll.sIconLogo});
	
	var divMenu = A.gcn("mainheaderleftmenu");
	E.div(divMenu,"menuleftspacer");
	iI = 0;
	oAll.aMenusLeft.forEach(function(oRec){
		if ((!iI) || ((iI + 1) == oAll.aMenusLeft.length)){
			
		} else {
			divParent = E.div(divMenu, "menuleft act", "menuleft_" + iI);
			A.attribute(divParent, "type", "menu");
			A.attribute(divParent, "name", "menuleft");
			divChild = E.div(divParent, "menulefttext");
			divChild.textContent = oAll.aMenusLeft[iI];
		}
		iI++;
	});
	
	divParent = A.gcn("maintop one");
	divParent = E.div(divParent, "menutopbox1");
	divParent = E.div(divParent, "menuuserbox");
	E.div(V.oDiv.body, "menuuserboxdropdown");
	var divChild = E.div(divParent, "menuusertext");
	divChild.textContent = oAll.oUserInfo.sCompany;
	divChild = E.div(divParent, "menuuserlogo");
	divChild = E.div(divChild, "img100", "", "img");
	divChild.src = oAll.oUserInfo.sLogo;
	E.div(divParent, "menuuserspacer");
	divChild = E.div(divParent, "menuusertext");
	divChild.textContent = oAll.oUserInfo.sName;	
	divParent = E.div(divParent.parentNode, "menuuserboxover act");
	A.attribute(divParent, "type", "userbox");
	A.attribute(divParent, "name", "userbox");
	V.oDiv.message = E.div(V.oDiv.body, "messagetimed");
	divParent = E.div(V.oDiv.body, "mainpage1");
	V.oDiv.mainpage = divParent;	
	E.div(divParent, "mainleft1");
	V.oDiv.work = E.div(divParent, "mainwork1");
	var divTabButtonBox = E.div(V.oDiv.footer, "mainheadertabs1");
	E.div(divTabButtonBox, "mainfootertabs1");
	var divTabButtonsBox = E.div(divTabButtonBox, "tabbuttonsbox");
	divParent = E.div(divTabButtonsBox, "tabbuttonsboxin");
	
	
	return [];
},



reset0:function(){
	var iI = 0;
	var oEle = {};
	var oAll = V.oHouse;
	var divParent = A.gcn("maintop one");
	iI = 0; 
	oAll.aMenusTop.forEach(function(oRec){
		E.menu({divParent:divParent, sClassName:"top", 
			iIndex:iI, aMenus:oAll.aMenusTop});
		iI++;
	});
		
	var divHouse = E.div(V.oDiv.work, "house1");
	E.icon({divParent:divHouse, sText:oAll.sIconLogo, sClassName:"house1",
		sName:"logo"});
	var divCenter = E.div(divHouse, "house1center");
	var divBox = E.div(divCenter, "house1box");
	var divLine = E.div(divBox, "house1boxline");
	var divImg = E.div({divParent:divLine, sClassName:"img100", sType:"img"});
	divImg.src = "data/houseline.png"
	var divText = E.div(divBox, "house1boxtext");
	divText.innerHTML = oAll.sText1;
	var divLine = E.div(divBox, "house1boxline");
	var divImg = E.div({divParent:divLine, sClassName:"img100", sType:"img"});
	divImg.src = "data/houseline.png"	
	var divTable = E.div({divParent:divCenter, sType:"table",
		sClassName:"house1centericons"});
	var divTR = E.div({divParent:divTable, sType:"tr"});
	var iSize = oAll.aSocials.length;
	var iI = 0;
	for (iI = 0; iI < iSize; iI++){
		var divTD = E.div({divParent:divTR, sType:"td"}); 
		var divXX = E.icon({divParent:divTD, sText:oAll.aSocials[iI].sIcon, 
			sClassName:"house1social", bAction:1, sName:oAll.aSocials[iI].sName});
	}

	divHouse = E.div(V.oDiv.work, "house2");
	var divCenter = E.div(divHouse, "house2centerphones");
	var divPhones = E.div(divCenter, "house2phones");
	var divImg = E.div({divParent:divPhones, sClassName:"img100", sType:"img"});
	divImg.src = "data/housephones.png"		
		
	divHouse = E.div(V.oDiv.work, "house3");
	var divText = E.div(divHouse, "house3text1");
	divText.textContent = oAll.sText21;
	var divText = E.div(divHouse, "house3text2");
	divText.textContent = oAll.sText22;
	E.button({divParent:divHouse, sClassName:"whitebutton act", 
		sText:oAll.sButtonRegister});
	divHouse = E.div(V.oDiv.work, "house4");
	var divCenter = E.div(divHouse, "house4center");
	for (iI = 0; iI < 3; iI++){
		var oRec = oAll.aIcons2[iI];
		var divBox = E.div(divHouse, "house4iconsbox")
		E.icon({divParent:divBox, sClassName:"house41", sText:oAll.sIconStep});
		var divCount = E.div(divBox, "house4count");
		divCount.textContent = (iI + 1);
		E.icon({divParent:divBox, sClassName:"house42", sText:oRec.sIcon});
		var divText = E.div(divBox, "house4text1");
		divText.textContent = oRec.sText1;
		var divText = E.div(divBox, "house4text2");
		divText.textContent = oRec.sText2;
	}
	divHouse = E.div(V.oDiv.work, "house5");
	var divText = E.div(divHouse, "house3text1");
	divText.textContent = oAll.sText31;
	var divText = E.div(divHouse, "house3text2");
	divText.textContent = oAll.sText32;
	var divBox = E.div(divHouse, "house5center");
	E.image({divParent:divBox, sName:"android", 
		sClassName:"eleimage house5 android act", sSrc:"data/houseandroid.png"});
	E.image({divParent:divBox, sName:"apple", 
		sClassName:"eleimage house5 apple act", sSrc:"data/houseapple.png"});
	divHouse = E.div(V.oDiv.work, "house6");
	var divCenter = E.div(divHouse, "house4center");
	for (iI = 0; iI < 3; iI++){
		var oRec = oAll.aIcons3[iI];
		var divBox = E.div(divHouse, "house4iconsbox")
		E.icon({divParent:divBox, sClassName:"house41", sText:oAll.sIconStep});
		var divCount = E.div(divBox, "house4count");
		divCount.textContent = (iI + 1);
		E.icon({divParent:divBox, sClassName:"house62", sText:oRec.sIcon});
		var divText = E.div(divBox, "house4text1");
		divText.textContent = oRec.sText1;
		var divText = E.div(divBox, "house4text2");
		divText.textContent = oRec.sText2;
	}
	var divDocs = E.div(V.oDiv.footer, "footerdocs");
	var aText = oAll.sDocs.split("|");
	E.text({divParent:divDocs, sText:aText[0]});
	E.span({divParent:divDocs, sClassName:"footerdocslinks act", sName:"terms",
		sText:aText[1]});
	E.text({divParent:divDocs, sText:aText[2]});
	E.span({divParent:divDocs, sClassName:"footerdocslinks act", sName:"privacy",
		sText:aText[3]});
	return [];
},



reset:function(){
	var iI = 0, divX = 0, divY = 0, aTabs = [];
	V.oDiv.body.innerHTML = "";
	V.oDiv.cloak = E.div(V.oDiv.body, "maincloak");
	V.oDiv.dummy = E.div(V.oDiv.cloak, "maindummy");
	V.oDiv.header = E.div(V.oDiv.body, "mainheader");
	divX = E.div(V.oDiv.header, "mainheaderleft");
	divX = E.div(divX, "maintopleftone");
	E.div(divX, "mainheaderleftmenu");
	
	divX = E.div(V.oDiv.header, "mainheaderwork");
	divY = E.div(divX, "maintop one");
	V.oDiv.breadcrumbs = E.div(divY, "breadcrumbs");
	divY = E.div(divY, "mainloaderfull");
	V.oDiv.loader = E.div(divY, "mainloader");
	E.div(divX, "maintop two");
	E.div(divX, "maintop three");
	V.oDiv.mainpage = E.div(V.oDiv.body, "mainpage");
	divX = E.div(V.oDiv.mainpage, "mainfull");
	E.div(divX, "mainleft");
	V.oDiv.work = E.div(divX, "mainwork");
	E.div(V.oDiv.mainpage, "maincentered");
	V.oDiv.footer = E.div(V.oDiv.body, "mainfooter");
	
	switch (V.oGen.iMenuNo){
		case 0:
			V.oDiv.work = A.gcn("maincentered");
			if (V.aAA[B.oOb.iRegister]){
				V.aAA[B.oOb.iRegister].kill();
			}
			B.reset0();
		break;
		
		case 1:
			B.reset1();
		break;
	
		case 2:
			if (V.aAA[B.oOb.iHouse]){
				V.aAA[B.oOb.iHouse].kill();
			}
			B.reset2();
		break;
		
	}
	B.resize();
	B.actions();
},



resizeA:function(){
	if (V.oGen.bResizing){
		return;
	}
	window.setTimeout(function(){
		B.resize();
	}, 50);
	V.oGen.bResizing = 1;
},



resize:function(){
	var iMode = 0, iTop = 0, iI = 0, aHeights =[], oSize ={}, iW = 0, 
		divX = 0, iWidthLeft = 0;
	iW = window.innerWidth - V.oGen.iWidthScrollbar;
	V.oGen.aDisplayScreenSizes.forEach(function(oRec){
		if ((iW + 30) >= oRec.iWidthMain){
			iMode = iI;
		}
		iI++;
	});
	oSize = V.oGen.aDisplayScreenSizes[iMode];
	V.oGen.iDisplayScreenSize = iMode;
	V.oDiv.body.style.fontSize = oSize.iFontSize + "px";
	V.oDiv.work = A.gcn("mainwork");
	aHeights = [oSize.aHeightHeaders[0], oSize.aHeightHeaders[1], 
		oSize.aHeightHeaders[2]];
	switch (V.oGen.iMenuNo){
		case 0:
			aHeights[1] = 0;
			aHeights[2] = 0;
			iWidthLeft = 0;
			V.oDiv.work = A.gcn("maincentered");
		break;
		
		case 1:
			iWidthLeft = oSize.iWidthLeft;
		break;

		case 2:
			aHeights[2] = 0;
			iWidthLeft = oSize.iWidthLeft;
		break;
	}
	A.gcn("maintop one", 1).forEach(function(divBox){
		divBox.style.height = oSize.aHeightHeaders[0] + "px";
	});
	V.oGen.iHeaderHeight = oSize.aHeightHeaders[0];
	var aBoxes = A.gcn("mainheaderleft", 1);
	aBoxes.forEach(function(divBox){
		divBox.style.width = iWidthLeft + "px";
	});
	A.gcn("mainleft").style.width = iWidthLeft + "px";	
	A.gcn("maintopleftone").style.height = aHeights[0] + "px";
	if (V.oGen.iPageTop == 0){
		iTop = aHeights[0] + aHeights[1] + aHeights[2];
		A.gcn("maintop two").style.display = "block";
		A.gcn("maintop three").style.display = "block";
	}
	if (V.oGen.iPageTop == 1){
		iTop = aHeights[0];
		A.gcn("maintop two").style.display = "none";
		A.gcn("maintop three").style.display = "none";				
	}
	if (V.oGen.iPageTop == 2){
		iTop = aHeights[0] + aHeights[1];
		A.gcn("maintop two").style.display = "block";
		A.gcn("maintop three").style.display = "none";
	}
	A.gcn("maintop two").style.height = aHeights[1] + "px";
	A.gcn("maintop three").style.height = aHeights[2] + "px";
	A.gcn("mainpage").style.marginTop = iTop + "px";
	A.gcn("maincentered").style.width = oSize.iWidthMain + "px";
	A.gcn("mainfooter").style.height = oSize.iHeightFooter + "px";
	A.gcn("messagetimed").style.top = (aHeights[0] + 1) + "px";
	var divX = A.gcn("logo1");
	if (divX.className != "maindummy"){
		divX.style.width = oSize.iWidthLogo + "px";
		divX.style.height = (oSize.iHeightLogo + 2) + "px";
	}
	A.gcn("menuleftspacer").style.height = (oSize.iHeightLogo + 3) + "px";
	var aBoxes = A.gcn("gridbox", 1);
	aBoxes.forEach(function(divBox){
		divBox.style.width = (V.oDiv.work.clientWidth - 40) + "px";
	});
	var aBoxes = A.gcn("gridcolumnsbox", 1);
	aBoxes.forEach(function(divBox){
		divBox.style.width = (V.oDiv.work.clientWidth - 40) + "px";
	});
	V.oGen.bResizing = 0;	
},



scrolltop:function(){
	var divDoc = document.documentElement;
	var iTop = (window.pageYOffset || 
		divDoc.scrollTop)  - (divDoc.clientTop || 0);	
	return iTop;
},



tab:function(aAction){
	var aDivWorkTab = [], sClassName = "", aID = [], divGridHeader = 0,
		divButton = 0, aDivTabButtons = [], sText = "", aBreadcrumbs = [],
		iObjNo = 0, iTabNo = 0, aObj = [], sSession = "";
	aDivWorkTab = A.gcn("tabwork", 1);
	aDivWorkTab.forEach(function(oRec){
		sClassName = "tabbutton";
		aID = oRec.id.split("-");
		aObj = aID[1].split("_");
		iObjNo = parseInt(aObj[0]);
		iTabNo = parseInt(aObj[1]);		
		divButton = A.dg("tabbutton-" + aID[1]);
		divGridHeader = A.dg("gridcolumnsbox-" + aID[1]);
console.log((aID[1] +"=="+ aAction[3]))
		if (aID[1] == aAction[3]){
			V.aAA[iObjNo].iTabNo = iTabNo;
			oRec.style.display = "block";
			if (divGridHeader){
				divGridHeader.style.display = "block";
			}
			if (divButton){
				divButton.className = "elebutton " + sClassName + " active";
				sText = divButton.childNodes[0].textContent;
				sText = sText.charAt(0).toUpperCase() + sText.slice(1).toLowerCase();
//				aBreadcrumbs = V.oGen.aBreadcrumbs;
//				aBreadcrumbs.push(sText);
			}
		} else {
			oRec.style.display = "none";
			if (divGridHeader){
				divGridHeader.style.display = "none";
			}
			if (divButton){
				divButton.className = "elebutton " + sClassName;
			}
		}
	});
	aAction = aAction[3].split("_");
	V.aAA[parseInt(aAction[0])].iTabNo = parseInt(aAction[1]);
	aDivTabButtons = A.gcn("tabbuttonsbox", 1);
	aDivTabButtons.forEach(function(oRec){
		aID = oRec.id.split("-");
		if (aID[1] != aAction[0]){
			oRec.style.display = "none";
		} else {
			oRec.style.display = "block";
		}
	});
},



};



// ELEMENTS CLASS ==============================================================
var E = {
	sName:"ELES",

actionA:function(sAction){
	return function(){
		E.action(sAction);
	}
},



action:function(sAction){
	var divParent = 0, divX = 0, sID = "";
	var aAction = sAction.split("_");
	switch (aAction[0]){
		case "buttondisable":
			var divButton = A.dg(V.oGen.sIDButtonDisabled);
			divButton.parentNode.childNodes[1].style.display = "block";
		break;
		
		case "buttonenable":
			var divButton = A.dg(V.oGen.sIDButtonDisabled);
			divButton.parentNode.childNodes[1].style.display = "none";
		break;

		case "maxchars":
			sID = aAction[1] + "_" + aAction[2] + "_" + aAction[3];
			sValue = A.value(sID).sValue;
			var divMaxChars = A.dg(sID + "_chars");
			var aMax = divMaxChars.textContent.split("/");
			var iMax = parseInt(aMax[1]);
			var iLength = sValue.length;
			if (iLength > iMax){
				iLength = iMax;
			}
			var sMax = " (" + iLength + "/" + aMax[1];
			divMaxChars.textContent = sMax;
			A.value(sID, sValue.substr(0, iMax));
		break;
		
		case "popupclose":
			divX = A.gcn("elepopupover");
			if ((divX) && (divX.className != "maindummy")){
				if (divX.style.display == "block"){
					divX.style.display = "none";
				} else {
					V.oDiv.cloak.innerHTML = "";
					V.oDiv.cloak.style.display = "none";
					V.oDiv.popup = 0;
					V.oDiv.popupover = 0;
					window.scrollTo(0, V.oGen.iScrollTop);
				}
			} else {
				V.oDiv.cloak.innerHTML = "";
				V.oDiv.cloak.style.display = "none";
				V.oDiv.popup = 0;
				window.scrollTo(0, V.oGen.iScrollTop);
			}
		break;
		
		case "popupclosex":
			V.oDiv.cloak.innerHTML = "";
			V.oDiv.cloak.style.display = "none";
			V.oDiv.popup = 0;
			V.oDiv.popupover = 0;
			window.scrollTo(0, V.oGen.iScrollTop);
		break;
		
		case "popupclosebutton":
			V.oDiv.cloak.innerHTML = "";
			V.oDiv.cloak.style.display = "none";
			V.oDiv.popup = 0;
			window.scrollTo(0, V.oGen.iScrollTop);
		break;

		case "tabbuttons":
			var iObjNo = parseInt(aAction[1]);
			var aTabNos = aAction[2].split("-");
			var sIDPre = "tabbutton-" + iObjNo + "_";
			var bFound = 1;
			var iI = 1;
			while (A.dg(sIDPre + iI)){
				var divButton = A.dg(sIDPre + iI);
				if (aTabNos.indexOf("" + iI) != -1){
					divButton.style.display = "block";
				} else {
					divButton.style.display = "none";
				}
				iI++;
			}
		break;

		case "tabbuttonswidth":
			var iObjNo = parseInt(aAction[1]);
			var sIDPre = "tabbutton-" + iObjNo + "_";
			var bFound = 1;
			var iI = 1;
			var iWidth = 0;
			var sw = "";
			while (A.dg(sIDPre + iI)){
				var divButton = A.dg(sIDPre + iI);
					if (divButton.style.display == "block"){
					sw += iI + ","+divButton.clientWidth+ " ";
					iWidth += (divButton.clientWidth + 45);
				}
				iI++;
			}
			if (iWidth > 160){
				divButton.parentNode.style.width = iWidth + "px";
			}
console.log("tabbuttonswid " + sIDPre + ", " +iWidth + "px, " + iI+", "+sw)
		break;
		
		/*		
		case "tabbutton":
			var aDivWorkTab = A.gcn("tabwork", 1);
			aDivWorkTab.forEach(function(oRec){
				var aID = oRec.id.split("_");
				if (aID[1] == aAction[1]){
					oRec.style.display = "block";
				} else {
					oRec.style.display = "none";
				}
			});
		break;
		
		*/
	}
},



address:function(oEle){
	var iI = 0, iSize = 0, oAd = {}, sID = "";
	var iObjNo = A.object();
	V.aAA[iObjNo] = new HO_ADDRESS();
	V.aAA[iObjNo].iObjNo = iObjNo;
	V.aAA[iObjNo].oEle = oEle;
	V.aAA[iObjNo].aValues = V.aAA[iObjNo].aDefaultValues;
	oEle.that.aAddresses.push(iObjNo);
	iSize = oEle.that.aAddresses.length;
	for (iI = 0; iI < iSize; iI++){
		oAd = V.aAA[oEle.that.aAddresses[iI]];
		sID = oAd.oEle.sID + "_" + oAd.iObjNo;
		iObjNo = oEle.that.aAddresses[iI];
		var iID = V.aAA[iObjNo].aValues[0];
		var aValues = A.value(sID).sValue;
		if (!aValues){
			aValues = [];
		}
		V.aAA[iObjNo].aValues = aValues;
		V.aAA[iObjNo].aValues[0] = iID;
		if (!oEle.bNoInit){
			V.aAA[iObjNo].init(iI);
		}
	}
	return iObjNo;
},



box:function(oEle){
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "elebox " + sClassName, oEle.sID);
	return divX;
},



breadcrumbs: function(aBreadcrumbs){
	V.oDiv.breadcrumbs.innerHTML = "";
	var iUser = A.session("iUser");
	if (!iUser){
		return;
	}
	if (aBreadcrumbs){
		V.oGen.aBreadcrumbs = aBreadcrumbs;
		V.oGen.aBreadcrumbs.unshift("Dashboard");
	}
	var iSize = V.oGen.aBreadcrumbs.length;
	var divSpan = 0;
	var divSpan1 = 0;
	for (var iI = 0; iI < iSize; iI++){
		divSpan = E.div({divParent:V.oDiv.breadcrumbs, 
			sClassName:"ele-breadcrumb", sID:"breadcrumb_" + iI, sType:"span"});
		divSpan.textContent = V.oGen.aBreadcrumbs[iI];
		if (iI != (iSize - 1)){
			divSpan1 = E.div({divParent:V.oDiv.breadcrumbs, 
				sClassName:"ele-breadcrumb1", sType:"span"});
			divSpan1.textContent = " > ";
			A.action.call(divSpan, "click", E.action, [E, "breadcrumb_" + iI]);
		} else {
			divSpan.className = "ele-breadcrumblast";
		}
	}
},



button:function(oEle){
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var sID = oEle.sID
	if (!sID){
		sID = ""; 
	}
	var divParent = E.div(oEle.divParent, "elebuttonbox");
	var divX = E.div(divParent, "elebutton " + sClassName, sID);
	divX.innerHTML = oEle.sText;
	sClassName = sClassName.replace(/ act/g, "");
	if (oEle.bCanDisable){
		var divOver = E.div(divParent, "elebutton disabled " + sClassName);
	}
	if (oEle.bDefault){
		V.oGen.sIDButtonDefault = oEle.sID;
	}
	A.attribute(divX, "type", "button");
	return divX;
},



date:function(oEle){
	var sText = oEle.sText;
	if (!sText){
		sText = "";
	}
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput" + sClassName);
	divX.style.width = "170px";
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox");
	var divX = E.div(divBox, "eleinputinput", oEle.sID, "input");
	A.attribute(divX, "type", "date");
	return divX;
},



div:function(oEle, sClassName, sID, sType){
	if (sClassName){
		if (!sID){
			sID = "";
		}
		if (!sType){
			sType = "";
		}
		oEle = {divParent:oEle, sClassName:sClassName, sID:sID, sType:sType};
	}	
	var sNewType = "div";
	if (oEle.sType){
		sNewType = oEle.sType;
	}
	var divAType = document.createElement(sNewType);
	if (oEle.sClassName){
		divAType.className = oEle.sClassName;
	}
	if (oEle.sID){
		divAType.setAttribute("id", oEle.sID);
	}
	if (oEle.divParent){
		oEle.divParent.appendChild(divAType);
	}
	return divAType;
},



email:function(oEle){
	var divX = E.input(oEle);
	A.attribute(divX, "type", "email");
	return divX;
},



error:function(oEle){
	var sError = "";
	var iI = 0;
	if (oEle.aInput){
		oEle.aInput.forEach(function(oRec){
			sError += V.oDB.oError.oInput[oRec] + "<br>";
			iI++;
		});
	}
	if (iI){
		oEle.sMessage = sError;
		E.popup(oEle);
	}
},



form:function(oForm){
	var iI = 0, iObjNo = 0, sIDPost, divX = 0;
	if (!oForm.iIndex){
		oForm.iIndex = "";
	}
	var aForm = V.oDB.oForm[oForm.sFormName + oForm.iIndex];
	iObjNo = 0;
	if (!oForm.bNooObject){
		if (!oForm.iObjNo){
			iObjNo = A.object();
		} else {
			iObjNo = oForm.iObjNo;
		}
		V.aAA[iObjNo] = new HO_FORM();
		V.aAA[iObjNo].oForm = oForm;
	}
	if (!aForm){
		return iObjNo;
	}
	aForm.forEach(function(oRec){
		oRec.divParent = oForm.divParent;
		sIDPost = "";
		if (oForm.sIDPost){
			sIDPost = "-" + oForm.sIDPost;
		}
		if (!oRec.iIndex){
			oRec.iIndex = 0;
		}
		oRec.sID = oForm.sIDPre + "-" + iObjNo + "_" + 
			oForm.iIndex+ "_"+ iI + sIDPost;
		oRec.that = oForm.that;
		var bShow = 1;
		if (oRec.bNotProfile){
			 if (oForm.that.sName == "PROFILE"){
				 bShow = 0;
			 }
		}
		if (bShow){
			divX = E[(oRec.sType)](oRec);
		}
		if (oRec.sType == "grid"){
			V.aAA[iObjNo].iGridNo = divX;
		}
		iI++;
	});
	B.actions();
	return iObjNo;
},



grid:function(oEle){
	var divHeader = A.gcn("maintop three");
	divHeader = E.div(divHeader, "gridcolumnsbox", 
		"gridcolumnsbox-" + oEle.that.iObjNo + "_" + oEle.iTabNo);
	var divBox = E.div(oEle.divParent, "gridbox");
	var iObjNo = A.object();
	V.aAA[iObjNo] = new HO_GRID();
	V.aAA[iObjNo].iObjNo = iObjNo;
	V.aAA[iObjNo].oDiv.header = divHeader;
	V.aAA[iObjNo].oDiv.box = divBox;
	V.aAA[iObjNo].oEle = oEle;
	V.aAA[iObjNo].init();
	return iObjNo;
},



href:function(oEle){
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "elehref " + sClassName, oEle.sID);
	divX.innerHTML = oEle.sText;
	A.attribute(divX, "type", "href");
	if (oEle.sName){
		A.attribute(divX, "name", oEle.sName);
	} else {
		A.attribute(divX, "name", "");
	}
	return divX;
},



icon:function(oEle){
	var sID = "";
	if (oEle.sID){
		sID = oEle.sID;
	}
	var divX = E.div(oEle.divParent, "eleicon " + oEle.sClassName);
	divX.innerHTML = oEle.sText;
	var sClassName = "eleiconover";
	if (oEle.bAction){
		sClassName += " act";
	}
	divX = E.div(divX, sClassName, sID);
	if (oEle.sName){
		A.attribute(divX, "name", oEle.sName);
	} else {
		A.attribute(divX, "name", "");
	}
	A.attribute(divX, "type", "icon");
	return divX;
},



imagebox:function(oEle){
	var sText = oEle.sText;
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleimagebox " + sClassName, oEle.sID);
	if (!oEle.aSize){
		oEle.aSize = [300, 300];
	}
	divBox.style.width = oEle.aSize[0] + "px";
	divBox.style.height = oEle.aSize[1] + "px";
	if (oEle.bNoDisplay){
		divBox.parentNode.style.display = "none";
	}
	return divX;
},



image:function(oEle){
	var divX = E.div(oEle.divParent, oEle.sClassName);
	var divImg = E.div({divParent:divX, sClassName:"img100", sType:"img"});
	divImg.src = oEle.sSrc;
	if (oEle.sName){
		A.attribute(divX, "name", oEle.sName);
	} else {
		A.attribute(divX, "name", "");
	}
	A.attribute(divX, "type", "image");
},



input:function(oEle){
	var sText = oEle.sText;
	if (!sText){
		sText = "";
	}
	if (oEle.bRequired){
		sText += " *";
	}
	if (oEle.iMaxChars){
		sText += "<span id = '" + oEle.sID + "_chars" + "'> (0 / " + oEle.iMaxChars + ")";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput " + sClassName);
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox " + sClassName);
	if (oEle.bBoxOnly){
		if (oEle.sID){
			divBox.id = oEle.sID;
		}
		return divBox;
	} else { 
		var divX = E.div(divBox, "eleinputinput", oEle.sID, "input");
	}
	A.attribute(divX, "type", "input");
	if (oEle.iMaxChars){
		divX.maxLength = oEle.iMaxChars;
		divX.onkeyup = E.actionA("maxchars_" + oEle.sID);
		divX.onblur = E.actionA("maxchars_" + oEle.sID);
	}

	return divX;
},



line:function(oEle){
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleline " + sClassName, oEle.sID);
	return divX;
},



memo:function(oEle){
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "elememo " + sClassName, oEle.sID);
	divX.innerHTML = oEle.sText;
	return divX;
},



menu:function(oEle){
	var sClassName = "menu" + oEle.sClassName;
	var divParent = E.div(oEle.divParent, sClassName + " act");
	var divX = E.div(divParent, sClassName + "text");
	divX.textContent = oEle.aMenus[oEle.iIndex];
	if (oEle.iIndex < oEle.aMenus.length - 1){
		E.div(oEle.divParent, sClassName +"spacer");
	}
	A.attribute(divParent, "type", "menu");
	return divParent;
},



message:function(sMessage){
	V.oGen.iScrollTop = B.scrolltop();		
	V.oDiv.message.style.top = 
		(V.oGen.iHeaderHeight + V.oGen.iScrollTop + 1) + "px";
	V.oDiv.message.style.display = "block";
	V.oDiv.message.innerHTML = sMessage;
	A.zindex(V.oDiv.message);
	window.setTimeout(function(){
		V.oDiv.message.innerHTML = "";
		V.oDiv.message.style.display = "none";
	}, 2000);
},



output:function(oEle){
	var divX = E.div(oEle.divParent, oEle.sClassName, oEle.sID);
	return divX;
},



pageover:function(oEle){
	var divPage = E.div(oEle.divParent, "pageover", "pageover-" + oEle.iIndex);
	V.oGen.iPageTop = 1;
	B.resize();
	E.icon({divParent:divPage, sText:"&#xE06E", sClassName:"pageoverclose", 
		sName:"pageoverclose-" + oEle.iIndex, bAction:1});
	B.actions();
	return divPage;
},



page:function(oEle){
	var divX = 0, iObjNo = 0, sID = "";
	if (!(V.oDiv.work)){
		return;
	}
	if (oEle.iObjNo){
		iObjNo = oEle.iObjNo;
	} else {
		iObjNo = oEle.that.iObjNo;
	}
	sID = "tabwork-" + iObjNo + "_" + oEle.iIndex;
	divX = E.div(V.oDiv.work, "tabwork", sID);
	if (oEle.bCloseX){
		E.icon({divParent:divX, sText:"&#xE06E", 
			sClassName:"pageclose", sName:"pageclose", bAction:1,
			sID:"pageclose_" + iObjNo});	
	}
	iObjNo = E.form({sFormName:oEle.sFormName, iIndex:oEle.iIndex, 
		divParent:divX, sIDPre:"reg", sIDPost:"", that:oEle.that});
	return iObjNo;
},



		
password:function(oEle){
	var divX = E.input(oEle);
	A.attribute(divX, "type", "password");
	return divX;
},



popupfull:function(oEle){
	var iI = 0, divTitle = 0, divChild, iReturn = 0, iObjNo = 0,
		divWork = 0, divParent = 0, divOver = 0, divTitle = 0,
		divTR = 0, divTD = 0, divX = 0;
	V.oGen.iScrollTop = B.scrolltop();
	window.scrollTo(0, 0);
	V.oDiv.cloak.style.display = "block";
	V.oDiv.cloak.style.height = B.height() + "px";
	A.zindex(V.oDiv.cloak);
	var divParent = E.div(V.oDiv.cloak, "maincloaktop");
	divParent = E.div(divParent, "elepopupfull");
	V.oDiv.popup = divParent;
	divParent = E.div(divParent, "elepopuptable", "", "table");
	divTR = E.div(divParent, "elepopuptr", "", "tr");
	divTD = E.div(divTR, "elepopuptd", "", "td");
	divX = E.div(divTD, "elepopuptitle");
	E.text({divParent:divX, sText:oEle.sTitle});
	E.icon({divParent:divX, sText:"&#xE06E", 
		sClassName:"popupclose", sName:"popupclosex", bAction:1});	
	divTR = E.div(divParent, "elepopuptr", "", "tr");
	divTD = E.div(divTR, "elepopuptd", "", "td");
	divWork = E.div(divTD, "elepopupwork");
	if (oEle.sMessage){
		divWork.innerHTML = oEle.sMessage;
	}
	B.actions();
	return divWork;
},


/*
popup:function(oEle){
	var iI = 0, divTitle = 0, divChild, iReturn = 0, iObjNo = 0,
		divWork = 0, divParent = 0, divOver = 0, divTitle = 0, iH = 0, 
		divTR = 0, divTD = 0, divX = 0, divWork = 0, divButtons = 0;
	V.oGen.iScrollTop = B.scrolltop();
	if (!V.oDiv.popup){
		//new
		V.oDiv.cloak.style.display = "block";
		V.oDiv.cloak.style.height = B.height() + "px";
		A.zindex(V.oDiv.cloak);
		divX = E.div(V.oDiv.cloak, "maincloaktop");
		divX = E.div(divX, "elepopup");
		var iTop = 0;
		if (V.oGen.iDisplayScreenSize > 2){
			iTop = 70;
		}
		divX.style.marginTop = (V.oGen.iScrollTop + iTop) + "px";
		V.oDiv.popup = divX;
		divParent = E.div(divParent, "elepopuptable", "", "table");
		divTR = E.div(divParent, "elepopuptr", "", "tr");
		divTD = E.div(divTR, "elepopuptd", "", "td");
		divX = E.div(divTD, "elepopuptitle");
		E.text({divParent:divX, sText:oEle.sTitle});
		E.icon({divParent:divX, sText:"&#xE06E", 
			sClassName:"popupclose", sName:"popupclosex", bAction:1});	
		divTR = E.div(divParent, "elepopuptr", "", "tr");
		divTD = E.div(divTR, "elepopuptd", "", "td");
		divWork = E.div(divTD, "elepopupwork");
		V.oDiv.popupover = E.div(divTD, "elepopupover");
	} else {
		//over
		V.oDiv.popupover.style.display = "block";
		V.oDiv.popupover.innerHTML = "";
		divWork = E.div(V.oDiv.popupover, "elepopupwork");
	}
	if (oEle.sFormName){
		//form
		iObjNo = E.form({divParent:divWork, that:oEle.that,
			sFormName:oEle.sFormName, sIDPre:oEle.sIDPre});
		B.actions();
		iReturn = iObjNo;
	} else {
		//msgbox
		E.text({divParent:divWork, sText:oEle.sMessage});
//		divTR = E.div(divParent, "elepopuptr", "", "tr");
//		divTD = E.div(divTR, "elepopuptd", "", "td");
		E.spacer({divParent:divWork, sClassName:"popupspacer"});
		
		divButtons = E.div(divWork, "elepopupbuttons");
		if (!oEle.aButtons){
			E.button({divParent:divButtons, sText:"OK", sName:"popupok", 
				sClassName:"popup act", sID:"okpopup", bDefault:1});
			B.actions();
		} else {
			divX.className = "elepopupbuttonstwo";
			var sAction = oEle.sTitle.toLowerCase().replace(/ /g, "") + oEle.sAction;
			iI = 0;
			oEle.aButtons.forEach(function(oRec){
				var divButton = E.button({divParent:divButtons, sText:oRec, 
					sClassName:"popup" + iI});
				divButton.onclick = oEle.that.actionA(sAction + "_" + 
					oEle.that.iObjNo+ "_" + oRec.toLowerCase().replace(/ /g, ""));
				iI++;
			});
		}
	}
	V.oDiv.popupover.style.height = (V.oDiv.popup.clientHeight - 48) + "px"; 
	return iReturn;,
},
*/


popup:function(oEle){
	var iI = 0, divTitle = 0, divChild, iReturn = 0, iObjNo = 0,
		divWork = 0, divParent = 0, divOver = 0, divTitle = 0, iH = 0, 
		divTR = 0, divTD = 0, divX = 0, divWork = 0, divButtons = 0;
	if (!V.oDiv.popup){
		V.oGen.iScrollTop = B.scrolltop();
		V.oDiv.cloak.style.display = "block";
		V.oDiv.cloak.style.height = B.height() + "px";
		A.zindex(V.oDiv.cloak);
		var divParent = E.div(V.oDiv.cloak, "maincloaktop");
		divParent = E.div(divParent, "elepopup");
		var iTop = 0;
		if (V.oGen.iDisplayScreenSize > 2){
			iTop = 70;
		}
		divParent.style.marginTop = (V.oGen.iScrollTop + iTop) + "px";
		V.oDiv.popup = divParent;
		divParent = E.div(divParent, "elepopuptable", "", "table");
		divTR = E.div(divParent, "elepopuptr", "", "tr");
		divTD = E.div(divTR, "elepopuptd", "", "td");
		divX = E.div(divTD, "elepopuptitle");
		E.text({divParent:divX, sText:oEle.sTitle});
		E.icon({divParent:divX, sText:"&#xE06E", 
			sClassName:"popupclose", sName:"popupclosex", bAction:1});	
		divTR = E.div(divParent, "elepopuptr", "", "tr");
		divTD = E.div(divTR, "elepopuptd", "", "td");
		divWork = E.div(divTD, "elepopupwork");
		V.oDiv.popupover = E.div(divTD, "elepopupover");
	} else {
		V.oDiv.popupover.style.display = "block";
		V.oDiv.popupover.innerHTML = "";
		divWork = E.div(V.oDiv.popupover, "elepopupwork");
	}
	if (oEle.sFormName){
		iObjNo = E.form({divParent:divWork, that:oEle.that,
			sFormName:oEle.sFormName, sIDPre:oEle.sIDPre});
		B.actions();
		iReturn = iObjNo;
	} else {
		E.text({divParent:divWork, sText:oEle.sMessage});
		E.spacer({divParent:divWork, sClassName:"popupspacer"});
		divButtons = E.div(divWork, "elepopupbuttons");
		if (!oEle.aButtons){
			E.button({divParent:divButtons, sText:"OK", sName:"popupok", 
				sClassName:"popup act", sID:"okpopup", bDefault:1});
			B.actions();
		} else {
			divButtons.className = "elepopupbuttonstwo";
			var sAction = oEle.sTitle.toLowerCase().replace(/ /g, "") + oEle.sAction;
			iI = 0;
			oEle.aButtons.forEach(function(oRec){
				var divButton = E.button({divParent:divButtons, sText:oRec, 
					sClassName:"popup" + iI});
				divButton.onclick = oEle.that.actionA(sAction + "_" + 
					oEle.that.iObjNo+ "_" + oRec.toLowerCase().replace(/ /g, ""));
				iI++;
			});
		}
	}
	V.oDiv.popupover.style.height = (V.oDiv.popup.clientHeight - 48) + "px"; 
	return iReturn;
},



radio:function(oEle){
	oEle.bBoxOnly = 1;
	oEle.sClassName = "radio";
	var divParent = E.div(oEle.divParent, "eleradiobox");
	var divX = E.div({divParent:divParent, 
		sClassName:"eleradio", sID:oEle.sID, sType:"input"});
	A.attribute(divX, "type", "radio");
	E.text({divParent:divParent, sText:oEle.sText});
	if (oEle.sName){
		divX.name = oEle.sName;
	}
	return divX;
},



select:function(oEle){
	var sText = oEle.sText;
	if (!sText){
		sText = "";
	}
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox " + sClassName);
	var divX = E.div(divBox, "eleinputselect", oEle.sID, "select");
	A.attribute(divX, "type", "select-one");
	if (oEle.aValues){
		A.value(oEle.sID, oEle.aValues);
	}
	if (oEle.sEnum){
		A.value(oEle.sID, V.oDB.oEnum[oEle.sEnum]);
	}
	return divX;
},



spacer:function(oEle){
	oEle.bBoxOnly = 1;
	var sID = oEle.sID;
	oEle.sID = "";
	var divX = E.input(oEle);
	return divX;
},



span:function(oEle){
	var sID = "";
	if (oEle.sID){
		sID = oEle.sID;
	}
	var divX = E.div(oEle.divParent, oEle.sClassName, sID, "span");
	divX.innerHTML = oEle.sText;
	A.attribute(divX, "type", "span");
	if (oEle.sName){
		A.attribute(divX, "name", oEle.sName);
	} else {
		A.attribute(divX, "name", "");
	}
	return divX;
},



telephone:function(oEle){
	var sText = oEle.sText;
	if (!sText){
		sText = "";
	}
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox " + sClassName);
	var divX = E.div(divBox, "eleinputinput", oEle.sID, "input");
	A.attribute(divX, "type", "telephone");
	return divX;
},



textarea:function(oEle){
	oEle.bBoxOnly = 1;
	var sID = oEle.sID;
	oEle.sID = "";
	var divX = E.input(oEle);
	var sClassName = "eletextarea"
	if (oEle.sClassName){
		sClassName += oEle.sClassName;
	}
	divX = E.div(divX, sClassName, sID, "textarea");
	A.attribute(divX, "type", "textarea");
	if (!oEle.iRows){
		oEle.iRows = 4;
	}
	divX.rows = oEle.iRows;
	oEle.sID = sID;
	return divX;
},



text:function(oEle){
	var divX = E.div({divParent:oEle.divParent, sType:"span"});
	divX.innerHTML = oEle.sText;
	return oEle.divParent;
},



time:function(oEle){
	var sText = oEle.sText;
	if (!sText){
		sText = "";
	}
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput" + sClassName);
	divX.style.width = "170px";
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox");
	var divX = E.div(divBox, "eleinputinput", oEle.sID, "input");
	A.attribute(divX, "type", "time");
	return divX;
},



uploadStart: function(iUploadNum){
	var oObj = V.aAA[iUploadNum];
	var divStart = 0;
	if (!oObj.bBusy){
		V.aAA[iUploadNum].bBusy = 1;
		divStart = oObj.divStart;
		divStart.childNodes[0].textContent = "CANCEL";
		var aFile = A.dg("aFile_" + iUploadNum);
		aFile = aFile.files[0];
		var aFormData = new FormData();
		aFormData.append("file", "xxx");
		aFormData.append("aFile_" + iUploadNum, aFile);
		aFormData.append("iUploadNum", iUploadNum);
		oObj.aAjax = new XMLHttpRequest(); 
		oObj.aAjax.upload.addEventListener("progress",
			function(event){
				var percent = (event.loaded/event.total) * 100; 
				oObj.divProgress.value = Math.round(percent); 
			}, false); 
		oObj.aAjax.addEventListener("load",
			function(event){
				window.setTimeout(function(){
					oObj.divProgress.value = 0;
					oObj.bBusy = 0;
					oObj.bFinished = 1;
					var divFile = V.aAA[iUploadNum].divFile;
					divFile.value = "";
					oObj.divFilename.textContent = "";
					A.attribute(V.aAA[iUploadNum].divBox, "uploaded", 1);
					var divStart = oObj.divStart;
					divStart.childNodes[0].textContent = "UPLOAD"; 
					E.action("buttondisable");
					var sAction = oObj.oEle.sAction;
					if (sAction){
						oObj.oEle.that.action(oObj.oEle.sAction + "_" +
							iUploadNum + "_" + oObj.aAjax.responseText);
					}
				}, 200);
			}, false); 
		oObj.aAjax.addEventListener("error",
			function(event){console.log("uploaderror");}, false); 
		oObj.aAjax.addEventListener("abort",
			function(event){console.log("uploadabort");}, false); 
		oObj.aAjax.open("POST", V.sSrvURL + "upload/"); 
		oObj.aAjax.send(aFormData);
	} else {
		divStart = oObj.divStart;
		divStart.childNodes[0].innerHTML = V.aData.aInit.aBanners.oGeneral.sUpload;
		oObj.aAjax.abort();
		oObj.divProgress.style.display = "none";
		oObj.divProgress.value = 0;
		V.aAA[V.oGen.iUploadNum] = 0;
		V.oGen.iUploadNum = 0;
	}
},



upload:function(oEle){
	var sText = oEle.sText;
	if (!sText){
		sText = "";
	}
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var iObjNo = A.object();
	V.aAA[iObjNo] = {
		oEle:oEle
	};
	var divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox upload", oEle.sID);
	V.aAA[iObjNo].divBox = divBox;
	A.attribute(divBox, "type", "upload");
	var divFilename = E.div(divBox, "eleuploadfilename");
	V.aAA[iObjNo].divFilename = divFilename;
	var divForm = E.div({divParent:divBox, sID:"uploadform_" + iObjNo, 
		sType:"form"});
	divForm.method = "POST";
	divForm.enctype = "multipart/form-data";
	
	var divBrowse = E.div(divForm, "eleuploadbrowse");
	E.text({divParent:divBrowse,sText:"BROWSE"});
	var divFileBox = E.div(divBrowse, "eleuploadfilebox");
	
	var divFile = E.div({divParent:divFileBox, sClassName:"eleuploadfile",
		sID:"aFile_" + iObjNo, sType:"input"});
	divFile.type = "file";
	divFile.name = "aFile_" + iObjNo;
	V.aAA[iObjNo].divFile = divFile;
	divFile.onchange = function(){
		var divFile = A.dg("aFile_" + iObjNo);
		var sFilename = divFile.files[0].name;
		var aFilename = sFilename.split(".");
		V.aAA[iObjNo].sExtension = aFilename[aFilename.length - 1].toLowerCase();
		var aExtensions = V.aAA[iObjNo].oEle.sExtensions.split(", ");
		var iSize = aExtensions.length;
		var bFound = 0;
		var iI =0;
		while ((!bFound) && (iI < iSize)){
			if (V.aAA[iObjNo].sExtension == aExtensions[iI]){
				bFound = 1;
			} else {
				iI++;
			}
		}
		if (bFound){
			V.aAA[iObjNo].divFilename.textContent = sFilename;
			V.aAA[iObjNo].sFilename = sFilename;
			E.action("buttonenable");
		} else {
			E.popup({sTitle:"File type", 
				sMessage:"Please choose a file with " +
				"one of the following extensions: " + 
				V.aAA[iObjNo].oEle.sExtensions});
		}
	};
	var divStart = E.button({divParent:divBox, sClassName:"uploadstart act",
		sText:"UPLOAD", bCanDisable:1, sID:"upload_" + iObjNo});
	V.aAA[iObjNo].divStart = divStart;
	B.actions();
	V.oGen.sIDButtonDisabled = "upload_" + iObjNo;
	E.action("buttondisable");
	var divProgress = E.div(divBox, "eleuploadprogress", 
		"uploadprogress_" + iObjNo, "progress");
	V.aAA[iObjNo].divProgress = divProgress;
	var sExtensions = oEle.sExtensions;
	if (!sExtensions){
		sExtensions = "";
	}	
	return divBox;
},


		
weekhours:function(oEle){
	var iI = 0, iSize = 0, oWH = {}, sID = "";
	var iObjNo = A.object();
	V.aAA[iObjNo] = new HO_WEEKHOURS();
	V.aAA[iObjNo].iObjNo = iObjNo;
	V.aAA[iObjNo].oEle = oEle;
	V.aAA[iObjNo].aValues = V.aAA[iObjNo].aDefaultValues;
	iSize = oEle.that.aWeekHours.length;
	for (iI = 0; iI < iSize; iI++){
		oWH = V.aAA[oEle.that.aWeekHours[iI]];
		sID = oWH.oEle.sID + "_" + oWH.iObjNo;
		V.aAA[oEle.that.aWeekHours[iI]].aValues = A.value(sID).sValue;
	}
	V.aAA[iObjNo].init();
	return iObjNo;
},



};



//== ADDRESS CLASS ===========================================================
function HO_ADDRESS(){
	this.sName = "ADDRESS";
	this.iObjNo = 0;
	this.oEle = {};
	this.aValues = [];
	this.aDefaultValues = [0, -1, -1, -1, -1, "", "", 0];
	this.aProvinces = [];
	this.iProvince = 0;
	
	if (typeof HO_ADDRESS.bInited == "undefined"){

HO_ADDRESS.prototype.actionA = function(sAction){
	return function(){
		var aAction = sAction.split("_");
		var that = V.aAA[parseInt(aAction[1])];
		that.action(sAction);
	}
};



HO_ADDRESS.prototype.action = function(sAction){
	var aAction = [], that = 0, thatt = 0, sID = "";
	aAction = sAction.split("_");
	that = V.aAA[parseInt(aAction[1])];
	thatt = that.oEle.that;
	switch (aAction[0]){
		case "provincechange":
			that.province();
		break;
		
		case "regionchange":
			that.region();
		break;
		
		case "townchange":
			that.town();
		break;
		
		case "add":
			that.oEle.that.valuegetaddress();
			var aID = that.oEle.sID.split("_");
			var sID = aID[0] + "_" + aID[1] + "_" + (parseInt(aID[2]) + 1);
			var oEle = {
					divParent:that.oEle.divParent,
					iIndex:0,
					sID:sID,
					sType:"address",
					that:that.oEle.that
			}
			E.address(oEle);
			that.oEle.that.valuesetaddress();
		break;
		
	}
};



HO_ADDRESS.prototype.draw = function(iIndex){
	var iI = 0, iJ = 0, divX = 0, that = 0, aForm = [],
	iObjNo = 0, sIDPre = "", iSize = 0, divX = 0, divBox = 0;
	that = this;
	iI = 0;
	iSize = this.oEle.that.aAddresses.length;
	aForm = V.oDB.oForm.aAddress;
//	this.oEle.that.aAddresses.forEach(function(iObjNo){
		iObjNo = that.oEle.that.aAddresses[iIndex];
		sIDPre = that.oEle.sID + "_" + iObjNo;
		divBox = A.dg(sIDPre);
		divBox.innerHTML = "";
		iJ = 1;
		aForm.forEach(function(oRec){
			oRec.divParent = divBox;
			oRec.sID = sIDPre + "_" + iJ;
			divX = E[oRec.sType](oRec);
			switch (iJ){
				case 1:
					A.value(oRec.sID, that.aProvinces);
					divX.onchange = that.actionA("provincechange_" + iObjNo);
				break;
				
				case 2:
					A.value(oRec.sID, []);
					divX.onchange = that.actionA("regionchange_" + iObjNo);
				break;
				
				case 3:
					A.value(oRec.sID, []);
					divX.onchange = that.actionA("townchange_" + iObjNo);
				break;
				
				case 4:
					A.value(oRec.sID, []);
				break;
				
				case 8:
					divX.onclick = that.actionA("add_" + iObjNo);
				break;
	
				case 9:
					divX.onclick = that.actionA("remove_" + iObjNo);
				break;
					
			}
			iJ++;
		});
		A.value(sIDPre, V.aAA[iObjNo].aValues);
		iI++;
//	});
};



HO_ADDRESS.prototype.init = function(iIndex){
//	this.oEle.that.aAddresses.push(this.iObjNo);
	var divBox = A.dg(this.oEle.sID + "_" + this.iObjNo);
	if (divBox){
		divBox.innerHTML = "";
	} else {
		divBox = E.div
			(this.oEle.divParent, "eleaddress", this.oEle.sID + "_" + this.iObjNo);
		A.attribute(divBox, "type", "address");
	}
	this.aValues = this.aDefaultValues;
	var iNumRegions = V.oDB.oData.aRegions.length;
	for (iI = 0; iI <= iNumRegions; iI++){
		oRec = V.oDB.oData.aRegions[iI];
		if ((oRec) && (oRec.iRegionType == 1)){
			this.aProvinces.push(oRec);
		}
	}
	if (!iIndex){
		iIndex = 0;
	}
	this.draw(iIndex);
};



HO_ADDRESS.prototype.province = function(){
	var iI = 0, iNumRegions = 0, aTowns = [], aRegions1 = [],
		aParentRegions1 = [], oRec = {}, bParent = 0, sIDPre ="";
	sIDPre = this.oEle.sID + "_" + this.iObjNo + "_";
	this.iProvince = parseInt(A.value(sIDPre + "1").sValue);
	iNumRegions = V.oDB.oData.aRegions.length;
	for (iI = 0; iI <= iNumRegions; iI++){
		oRec = V.oDB.oData.aRegions[iI];
		if (oRec){
			if (((oRec.iRegionType == 2) && (oRec.iParentID == this.iProvince)) ||
					(((oRec.iRegionType == 3) && (oRec.iParentID == this.iProvince)))){
				aRegions1.push(oRec);
				aParentRegions1.push(oRec.iID);
			}
		}
	}
	for (iI = 0; iI <= iNumRegions; iI++){
		oRec = V.oDB.oData.aRegions[iI];
		bParent = 0;
		if ((oRec) && (aParentRegions1.indexOf(oRec.iParentID) != -1)){
			bParent = 1;
		}
		if ((oRec) && (oRec.iRegionType == 3) && 
			((oRec.iParentID == this.iProvince) || (bParent))){
			aTowns.push(oRec);
		}
	}
	A.value(sIDPre + "2", aRegions1);
	A.value(sIDPre + "3", aTowns);
};




HO_ADDRESS.prototype.region = function(){
	var iI = 0, sIDPre = "", iRegionID = 0, iNumRegions = 0,
		aTowns = [];
	sIDPre = this.oEle.sID + "_" + this.iObjNo + "_";
	iRegionID = parseInt(A.value(sIDPre + "2").sValue);
	iNumRegions = V.oDB.oData.aRegions.length;
	for (iI = 0; iI <= iNumRegions; iI++){
		oRec = V.oDB.oData.aRegions[iI];
		if ((oRec) && (oRec.iParentID == iRegionID)){
			aTowns.push(oRec);
		}
	}
	A.value(sIDPre + "3", aTowns);
};



HO_ADDRESS.prototype.regions = function(iID){
	var iI = 0; iFound = 0; iSize = 0, oRec = {}, oRec1 = {}, sReturn = "";
	iFound = 0, iParentID = 0, aIDs = [];
	aIDs.push(iID)
	iSize = V.oDB.oData.aRegions.length;
	iI = 0;
	iFound = 0;
	while ((iI < iSize) && (!iFound)){
		oRec = V.oDB.oData.aRegions[iI];
		if (oRec.iID == iID){
			iFound = (iI + 1);
		} else {
			iI++;
		}
	}
	if (iFound){
		iFound--;
		iID = parseInt(V.oDB.oData.aRegions[iFound].iParentID);
		aIDs.unshift(iID);
		iI = 0;
		iFound = 0;
		while ((iI < iSize) && (!iFound)){
			oRec = V.oDB.oData.aRegions[iI];
			if (oRec.iID == iID){
				iFound = (iI + 1);
			} else {
				iI++;
			}
		}
		if (iFound){
			iFound--;
			iID = parseInt(V.oDB.oData.aRegions[iFound].iParentID);
			aIDs.unshift(iID);
			iI = 0;
			iFound = 0;
			while ((iI < iSize) && (!iFound)){
				oRec = V.oDB.oData.aRegions[iI];
				if (oRec.iID == iID){
					iFound = (iI + 1);
				} else {
					iI++;
				}
			}
			if (iFound){
				iFound--;
				iID = parseInt(V.oDB.oData.aRegions[iFound].iParentID);
				aIDs.unshift(iID);
			}
		}
	}
	return aIDs;
};



HO_ADDRESS.prototype.town = function(){
	var iI = 0, sIDPre = "", iNumRegions = 0,
		aSuburbs = [];
	sIDPre = this.oEle.sID + "_" + this.iObjNo + "_";
	iTownID = parseInt(A.value(sIDPre + "3").sValue);
	iNumRegions = V.oDB.oData.aRegions.length;
	for (iI = 0; iI <= iNumRegions; iI++){
		oRec = V.oDB.oData.aRegions[iI];
		if ((oRec) && (oRec.iParentID == iTownID)){
			aSuburbs.push(oRec);
		}
	}
	A.value(sIDPre + "4", aSuburbs);
};



		HO_ADDRESS.bInited = 1;
	}
}



//== CREATEPOST CLASS ===============================================================
function HO_CREATEPOST(){
	this.sName = "CREATEPOST";
	this.iObjNo = 0;
	this.aForms = [0];
	this.iTabNo = 0;
	this.iType = 0;
	this.aAddresses = [];
	
	if (typeof HO_CREATEPOST.bInited == "undefined"){

HO_CREATEPOST.prototype.actionA = function(sAction){
	return function(){
		var aAction = sAction.split("_");
		var that = V.aAA[parseInt(aAction[1])];
		that.action(sAction);
	}
};



HO_CREATEPOST.prototype.action = function(sAction){
	var aAction = [], sIDPre = "", divBox = 0, divButton = 0;
	aAction = sAction.split("_");
	switch (aAction[0]){
		case "cancel":
			this.cancel();
		break;
		
		case "savedraft":
			this.savedraft();
		break;
		
		case "postnow":
			this.postnow();
		break;
		
		case "items":
			this.iType = 1;
			sIDPre  = "reg-" + this.aForms[1] + "_1_";
			divBox = A.dg(sIDPre + "10");
			divBox.style.display = "block";
			divBox = A.dg(sIDPre + "11");
			divBox.style.display = "none";
			divBox = A.dg(sIDPre + "12");
			divBox.style.display = "none";
			divButton = A.dg(sIDPre + "6");
			divButton.className = "elebutton createpost items";
			divButton = A.dg(sIDPre + "7");
			divButton.className = "elebutton createpost";
			divButton = A.dg(sIDPre + "8");
			divButton.className = "elebutton createpost";
		break;
		
		case "time":
			this.iType = 2;
			sIDPre  = "reg-" + this.aForms[1] + "_1_";
			divBox = A.dg(sIDPre + "10");
			divBox.style.display = "none";
			divBox = A.dg(sIDPre + "11");
			divBox.style.display = "block";
			divBox = A.dg(sIDPre + "12");
			divBox.style.display = "none";
			divButton = A.dg(sIDPre + "6");
			divButton.className = "elebutton createpost";
			divButton = A.dg(sIDPre + "7");
			divButton.className = "elebutton createpost time";
			divButton = A.dg(sIDPre + "8");
			divButton.className = "elebutton createpost";
		break;
		
		case "funds":
			this.iType = 3;
			sIDPre  = "reg-" + this.aForms[1] + "_1_";
			divBox = A.dg(sIDPre + "10");
			divBox.style.display = "none";
			divBox = A.dg(sIDPre + "11");
			divBox.style.display = "none";
			divBox = A.dg(sIDPre + "12");
			divBox.style.display = "block";
			divButton = A.dg(sIDPre + "6");
			divButton.className = "elebutton createpost";
			divButton = A.dg(sIDPre + "7");
			divButton.className = "elebutton createpost";
			divButton = A.dg(sIDPre + "8");
			divButton.className = "elebutton createpost funds";
		break;
		
	}
};



HO_CREATEPOST.prototype.cancel = function(){
	var sIDPre = "";
	sIDPre = "reg-" + this.aForms[1] + "_1_";
	A.dg(sIDPre + "1").value = "";
	A.dg(sIDPre + "2").value = "";
	A.dg(sIDPre + "3").value = "";
	A.dg(sIDPre + "4").value = "";
	divBox = A.dg(sIDPre + "10");
	divBox.style.display = "none";
	divBox = A.dg(sIDPre + "11");
	divBox.style.display = "none";
	divBox = A.dg(sIDPre + "12");
	divBox.style.display = "none";
	divButton = A.dg(sIDPre + "6");
	divButton.className = "elebutton createpost";
	divButton = A.dg(sIDPre + "7");
	divButton.className = "elebutton createpost";
	divButton = A.dg(sIDPre + "8");
	divButton.className = "elebutton createpost";
	sIDPre = "reg-" + this.aForms[2] + "_1_";
	A.value(sIDPre + "0", -1);
	sIDPre = "reg-" + this.aForms[3] + "_1_";
	A.dg(sIDPre + "0").value = "";
	A.dg(sIDPre + "1").value = "";
	A.dg(sIDPre + "2").value = "";
	A.dg(sIDPre + "3").value = "";
	A.dg(sIDPre + "4").value = "";
	A.dg(sIDPre + "5").value = "";
	A.value(sIDPre + "6", -1);
};



HO_CREATEPOST.prototype.init = function(){
	var divParent = 0, iWidth = 0, iI =0, sClassName = "",
		divButton = 0, aTabs = [], iSize = 0, iObjNo = 0, oForm = {}, oGrid = {},
		oEle = {}, aValues = [], sID = "", sIDPre = "", divBox = 0,
		sAddress = "", that = 0, iFound = 0, oRec = {}, oRec1 = {},
		aBreadcrumbs = [];
	this.iTabNo = 1;
	this.aForms.push(E.page({iIndex:1, sFormName:"aCreatePost", 
		that:this}));
	B.action("elebutton.tabbutton-active-tabbutton-" + this.iObjNo + 
		"_" + this.iTabNo);
	A.history("createpost");
	aBreadcrumbs = ["Create Post"];
	E.breadcrumbs(aBreadcrumbs);
	B.actions();
	B.resize();
	sIDPre = "reg-" + this.aForms[1] + "_" + this.iTabNo + "_";
	A.dg(sIDPre + "6").onclick = this.actionA("items_" + this.iObjNo);
	A.dg(sIDPre + "7").onclick = this.actionA("time_" + this.iObjNo);
	A.dg(sIDPre + "8").onclick = this.actionA("funds_" + this.iObjNo);
	A.dg(sIDPre + "13").onclick = this.actionA("cancel_" + this.iObjNo);
	A.dg(sIDPre + "14").onclick = this.actionA("savedraft_" + this.iObjNo);
	A.dg(sIDPre + "15").onclick = this.actionA("postnow_" + this.iObjNo);
	divBox = A.dg(sIDPre + "10");
	this.aForms.push(E.form({divParent:divBox, sIDPre:"reg", 
		sFormName:"aCreatePostItems", iIndex:1}));
	divBox = A.dg(sIDPre + "11");
	this.aForms.push(E.form({divParent:divBox, sIDPre:"reg", 
		sFormName:"aCreatePostTime", iIndex:1}));
	divBox = A.dg(sIDPre + "12");
	this.aForms.push(E.form({divParent:divBox, sIDPre:"reg", 
		sFormName:"aCreatePostFunds", iIndex:1}));
	that = this;
	V.oDB.oData.oUser.aAddresses.forEach(function(oRec){
		sAddress = oRec.address_line1 + ", " + oRec.address_line2;
		iFound = 0;
		iI = 0;
		iSize = V.oDB.oData.aRegions.length;
		while ((iI < iSize) && (!iFound)){
			oRec1 = V.oDB.oData.aRegions[iI];
			if (oRec1.iID == oRec.region_id){
				iFound = (iI + 1);
			} else {
				iI++;
			}
		}
		if (iFound){
			iFound--;
			sAddress += ", " + oRec1.sValue;
		}
		that.aAddresses.push({iID:oRec.id, sValue:sAddress});
	});
	sIDPre = "reg-" + this.aForms[1] + "_1_";
	A.value(sIDPre + "0", this.aAddresses);
	sIDPre = "reg-" + this.aForms[2] + "_1_";
	A.value(sIDPre + "6", this.aAddresses);
};



HO_CREATEPOST.prototype.postnow = function(){
	var aValues = [];
	aValues = this.values();
	if (this.test(aValues)){
		A.ajax("createpostpostnow", aValues, this.saveA);
	}
};



HO_CREATEPOST.prototype.reloadgridsA = function(oData){
	var oGrid = {}, that = 0;
	console.log(oData)
	var that = V.aAA[oData.aValues[13].iObjNo];
	if (oData.sMessage){
		E.message(V.oDB.oData.oError[oData.sMessage].sMessage);
		that.cancel()
	}
	if (oData.sError){
		E.popup(V.oDB.oData.oError[oData.sError]);
	}
	/*
	if (oData.aData1){
		oGrid = V.aAA[that.aGridNos[0]];
		oGrid.aDataFiltered = [];
		oGrid.aData = oData.aData1.aData;
		oGrid.populate();
	}
	if (oData.aData2){
		oGrid = V.aAA[that.aGridNos[1]];
		oGrid.aDataFiltered = [];
		oGrid.aData = oData.aData2.aData;
		oGrid.populate();
	}
	if (oData.aData3){
		oGrid = V.aAA[that.aGridNos[2]];
		oGrid.aDataFiltered = [];
		oGrid.aData = oData.aData3.aData;
		oGrid.populate();
	}
	*/
};



HO_CREATEPOST.prototype.savedraft = function(){
	var aValues = [];
	aValues = this.values();
	if (this.test(aValues)){
		aValues.push({iObjNo:this.iObjNo});
console.log(aValues)

		A.ajax("createpostsavedraft", aValues, this.reloadgridsA);
	}
};



HO_CREATEPOST.prototype.tab = function(iTabNo){
	var aNoGrid = [1];
	sSession = "iTabNo_" + this.sName;
	A.session(sSession, this.iTabNo);
	if (aNoGrid.indexOf(this.iTabNo) != -1){
		V.oGen.iPageTop = 1;
	} else {
		V.oGen.iPageTop = 0;
	}
	B.resize();
	B.tab(["elebutton.tabbutton", "active", "tabbutton", 
		this.iObjNo +"_" + this.iTabNo]);
};



HO_CREATEPOST.prototype.test = function(aValues){
	var sErrors = "", bValid = 1, iI = 0, oRec, aBetween = [0, -1];
	if (!aValues[0].iType){
		sErrors += "Select the type of post.<br>";
	}
	for (iI = 1; iI < 5; iI++){
		oRec = aValues[iI];
		if ((oRec.bRequired) && (!oRec.sValue)){
			sErrors += "Required " + oRec.sText + "<br>";
		}
		if ((oRec.bRequired) && (oRec.sType == "select-one") && 
			(oRec.sValue == -1)){
			sErrors += "Required " + oRec.sText + "<br>";
		}
		if ((oRec.sType == "email") && (!A.valid(oRec))){
			sErrors += "Invalid " + oRec.sText + "<br>"; 
		}
		if ((oRec.sType == "password") && (!A.valid(oRec))){
			sErrors += "Invalid " + oRec.sText + "<br>"; 
		}
	}
	switch (aValues[0].iType){
		case 1:
			aBetween = [5, 6];
		break;
		
		case 2:
			aBetween = [6, 13];
		break;
		
	}
	for (iI = aBetween[0]; iI < aBetween[1]; iI++){
		oRec = aValues[iI];
		if ((oRec.bRequired) && (!oRec.sValue)){
			sErrors += "Required " + oRec.sText + "<br>";
		}
		if ((oRec.bRequired) && (oRec.sType == "select-one") && 
			(oRec.sValue == -1)){
			sErrors += "Required " + oRec.sText + "<br>";
		}
		if ((oRec.sType == "email") && (!A.valid(oRec))){
			sErrors += "Invalid " + oRec.sText + "<br>"; 
		}
		if ((oRec.sType == "password") && (!A.valid(oRec))){
			sErrors += "Invalid " + oRec.sText + "<br>"; 
		}
	}

	if (sErrors){
		E.popup({sTitle:"Errors", sMessage:sErrors});
		bValid = 0;
	}
	return bValid;
};



HO_CREATEPOST.prototype.values = function(){
	var aValues = [], iI = 0;
	aValues = [{iType:this.iType}];
	iI = 0;
	this.aForms.forEach(function(oRec){
		if (iI){
			aValues = aValues.concat(V.aAA[oRec].values());
		}
		iI++;
	});
	return aValues;
};



		HO_CREATEPOST.bInited = 1;
	}
};



//== FORM CLASS ===============================================================
function HO_FORM(){
	this.sName = "FORM",
	this.oForm = {};
	this.iGridNo = 0;
	
	if (typeof HO_FORM.bInited == "undefined"){

HO_FORM.prototype.action = function(sElement){
};



HO_FORM.prototype.values = function(){
	var aValues = [];
	var oValue = {};
	var aInclude = ("date,email,input,password,upload,select," + 
		"image,radio,textarea,telephone,time").split(",")
	var aForm = V.oDB.oForm[this.oForm.sFormName + this.oForm.iIndex];
	if (!aForm){
		return;
	}
	aForm.forEach(function(oRec){
		var sType = oRec.sType;
		if (aInclude.indexOf(oRec.sType) != -1){
			oValue = A.value(oRec.sID);
			if (!oRec.bRequired){
				oRec.bRequired = 0;
			}
			oValue.bRequired = oRec.bRequired;
			oValue.sText = oRec.sText;
			aValues.push(oValue);
		}
	});
	return aValues;
};



		HO_FORM.bInited = 1;
	}
}



//== GRID CLASS ================================================================
function HO_GRID(){
	this.aData = [];
	this.aDataFiltered = [];
	this.oDiv = {};
	this.oEle = {};
	this.iHeight = 0;
	this.iObjNo = 0;
	this.iFormNo = 0;
	this.sName = "GRID";
	this.iNumRecords = 0;
	this.iNumRecordsFiltered = 0;
	this.divColumns = [];
	
	if (typeof HO_GRID.bInited == "undefined"){


HO_GRID.prototype.actionA = function(sAction){
	return function(){
		var aAction = sAction.split("_");
		var that = V.aAA[parseInt(aAction[1])];
		that.action(sAction);
	}
};



HO_GRID.prototype.action = function(sAction, aParams){
	var aAction = [], aRec = [], aData = [], iI = 0, iJ = 0, iNumCols = 0,
		aRow = [], sValue = "", sSearch = "";
	aAction = sAction.split("_");
	switch (aAction[0]){
		case "edit":
			if (this.aDataFiltered.length){
				aData = this.aDataFiltered;
			} else {
				aData = this.aData;
			}
			aRec = aData[parseInt(aAction[1])];
			this.edit(aRec);
		break;
		
		case "searchtyping":
			var sValue = this.oDiv.search.value;
			if (sValue.length){
				this.oDiv.searchclear.parentNode.style.display = "block";
				this.oDiv.search.style.color = "#444548";
			} else {
				this.oDiv.searchclear.parentNode.style.display = "none";
				this.oDiv.search.value = "Search";
				this.oDiv.search.style.color = "#CBCDCF";
				this.oDiv.search.blur();
			}
			iNumCols = this.oEle.aColumns.length;
			for (iI = 0; iI < this.iNumRecords; iI++){
				aRow = this.aData[iI];
				for (iJ = 1; iJ < iNumCols; iJ++){
					sValue = ("" + aRow[iJ]).toLowerCase();
					sSearch = this.oDiv.search.value.toLowerCase();
					if (sValue.indexOf(sSearch) != -1){
						this.aDataFiltered.push(aRow);
						break;
					}
				}
			}
			this.iNumRecordsFiltered = this.aDataFiltered.length;
			this.populate();
		break;
		
		case "searchclear":
			this.oDiv.search.value = "";
			this.action("searchtyping");
		break;
			
	}
};



HO_GRID.prototype.edit = function(aRec){
	this.oEle.that.aEditRecord = aRec;
	this.oEle.that.action("gridedit_" + this.iObjNo);
};



HO_GRID.prototype.init = function(){
	var that = this;
	var iI = 0;
	this.oDiv.toolbar = E.div(this.oDiv.header, "gridtoolbar");
	var divX = E.div(this.oDiv.toolbar, "eleinputgridsearch")
	this.oDiv.search = E.div(divX, "eleinputgridsearchinput", 
		"gridsearch_" + this.iObjNo, "input");	
	this.oDiv.search.value = "Search";
	this.oDiv.search.style.color = "#CBCDCF";
	this.oDiv.search.onfocus = function(){
		var that = V.aAA[document.activeElement.id.split("_")[1]];
		if (!that){
			return;
		}
		var sValue = that.oDiv.search.value;
		if (sValue == "Search"){
			that.oDiv.search.value = "";
			that.oDiv.search.style.color = "#444548";
		}
	};
	this.oDiv.search.onkeyup = this.actionA("searchtyping_" + this.iObjNo);
	this.oDiv.searchclear = E.icon({divParent:divX.parentNode, sText:"&#xE06E;", 
		sClassName:"gridsearchclear"});
	this.oDiv.searchclear.onclick = this.actionA("searchclear_" + this.iObjNo);
	V.oDiv.columns = [];
	var iW = 100; 
	var divY = E.div(that.oDiv.box, "gridcolumn");
	divY.style.width = "0";
	this.divColumns.push(divY);
	this.oEle.aColumns.forEach(function(oRec){
		var divX = E.div(that.oDiv.header, "gridcolumnheading");
		var divY = E.div(that.oDiv.box, "gridcolumn");
		V.oDiv.columns.push(divY);
		if (oRec.iWidth){
			divX.style.width = (oRec.iWidth) + "%";
			divY.style.width = (oRec.iWidth) + "%";
			iW -= oRec.iWidth;
		} else {
			divX.style.width = iW + "%";
			divY.style.width = iW + "%";
		}
		that.divColumns.push(divY);
		E.text({divParent:divX, sText:oRec.sName});
	});	
};



HO_GRID.prototype.populateA = function(oData){
	var that = V.aAA[oData.iGridNo];
	that.aData = oData.aData;
	that.aDataFiltered = [];
	if (oData.aData){
		that.populate();
	}
};



HO_GRID.prototype.populate = function(){
	var aData = [], iI = 0, iJ = 0, divX = 0, divY = 0, that = 0;
	this.iNumRecords = this.aData.length;
	this.iNumRecordsFiltered = this.aDataFiltered.length;
	if (this.iNumRecordsFiltered){
		aData = this.aDataFiltered;
	} else {
		aData = this.aData;
	}
	var that = this;
	var iSize = this.oEle.aColumns.length;
	for (iI = 0; iI <= iSize; iI++){
		this.divColumns[iI].innerHTML = "";
	}
	divX = A.gcn("gridrowover", 1);
	that = this
	divX.forEach(function(oRec){
		divY = oRec.parentNode;
		if (divY == that.oDiv.box){
			divY.removeChild(oRec);
		}
	});
	aData.forEach(function(oRec){
		var divCell = E.div(that.divColumns[0], "gridcell");
		divCell.textContent = oRec[0];
		for (iI = 0; iI < iSize; iI++){
			var oCol = that.oEle.aColumns[iI]; 
			var divCell = E.div(that.divColumns[(iI + 1)], "gridcell");
			if (!oCol.oEle){
				divCell.textContent = oRec[(iI + 1)];
			} else {
				divCell.className = "gridcellbutton";
				divCell.textContent = oCol.oEle.sText;
				divCell.onclick = that.oEle.that.actionA
					(oCol.oEle.sAction + "_" + that.oEle.that.iObjNo + "_" + iJ);
			}
		}
		var divRowOver = E.div(that.oDiv.box, "gridrowover");
		if (that.oEle.iRowOverWidth){
			divRowOver.style.width = that.oEle.iRowOverWidth + "%";
		}
		if (!that.oEle.bNoEdit){
			divRowOver.onclick = A.action(that, "edit_" + iJ);
		}
		iJ++;
	});
};



		HO_GRID.bInited = 1;
	}
};



//== HOME CLASS ===============================================================
function HO_HOME(){
	this.sName = "HOME";
	this.iObjNo = 0;
	this.aForms = [0];
	this.iTabNo = 0;
	this.iFormResetPassword = 0;
	
	if (typeof HO_HOME.bInited == "undefined"){

HO_HOME.prototype.actionA = function(sAction){
	return function(){
		var aAction = sAction.split("_");
		var that = V.aAA[parseInt(aAction[1])];
		that.action(sAction);
	}
};



HO_HOME.prototype.action = function(sAction){
	var aAction = [];
	aAction = sAction.split("_");
	switch (aAction[0]){
		case "changepassword":
			this.changepasswordgo();
		break;
	}
};



HO_HOME.prototype.boxlogin = function(divBox, oAll){
	divBox.style.display = "block";
	var divText = E.text({divParent:divBox, sText:oAll.sLastLogin});
	E.span({divParent:divText, sClassName:"elespanblue", 
		sText:V.oDB.oData.oUser.oLastLogin.sDate});
	var divText = E.text({divParent:divBox, sText:oAll.sLastLoginReport});
	E.href({divParent:divBox, sText:oAll.sReport, 
		sClassName:"homedashboardlogin"});
};



HO_HOME.prototype.boxpie = function(divBox, oAll){
	divBox.style.display = "block";
	E.text({divParent:divBox, sText:oAll.sYourPostTypes})
	var divBoxCanvas = E.div(divBox, "homedashboardpie");
	var divCanvas = E.div({divParent:divBoxCanvas, sType:"canvas"});
	divCanvas.width = "200";
	divCanvas.height = "200";
	var oContext = divCanvas.getContext("2d");
	var iLastEnd = 0;
	var aPieData = V.oDB.oData.oDashboard.aPostTypes;
	var iTotal = 0;
	var aColors = [V.oDB.oEnum.oColor.sRed, 
		V.oDB.oEnum.oColor.sGreen, 
		V.oDB.oEnum.oColor.sGold
	];
	for(iI = 0; iI < aPieData.length; iI++){
		iTotal += aPieData[iI];
	}
	if (!iTotal){
		aPieData = [1, 1, 1, 1];
		iTotal = 3;
	}
	for (iI = 0; iI < 3; iI++) {
		var divLegendBox = E.div(divBox, "homedashboardpielegend");
		var divColor = E.div(divLegendBox, "homedashboardpielegendcolor");
		divColor.style.background = aColors[iI];
		var divText = E.div(divLegendBox, "homedashboardpielegendtext");
		var sText = "";
		if (!aPieData[3]){
			sText = aPieData[iI];
		}
		divText.textContent = sText + " " + 
			V.oDB.oDashboard.aPostTypes[iI];
		oContext.fillStyle = aColors[iI];
		oContext.beginPath();
		oContext.moveTo(divCanvas.width / 2, divCanvas.height / 2);
		oContext.arc(divCanvas.width / 2, divCanvas.height / 2,
			divCanvas.height / 2, iLastEnd, 
			iLastEnd + (Math.PI * 2 * (aPieData[iI] / iTotal)), false);
		oContext.lineTo(divCanvas.width / 2, divCanvas.height / 2);
		oContext.fill();
		iLastEnd += Math.PI * 2 * (aPieData[iI] / iTotal);
	}
};



HO_HOME.prototype.boxresponses = function(sIDPre, iBoxNo, oAll){
	var aColors = [],iSize = 0, iSize1 = 0; aUniquePost = [], aUniqueType = [],
	iI = 0, oRec = {}, divBox = 0;
	aColors = [V.oDB.oEnum.oColor.sRed, 
 		V.oDB.oEnum.oColor.sGreen, 
		V.oDB.oEnum.oColor.sGold
	];

	iSize1 = V.oDB.oData.oDashboard.aCommitments.length;
	aUniquePost = [];
	aUniqueType = [];
	for (iI = 0; iI < iSize1; iI++){
		oRec = V.oDB.oData.oDashboard.aCommitments[iI];
		if (aUniquePost.indexOf(oRec.post_id) == -1){
			aUniquePost.push(oRec.post_id);
			aUniqueType.push([oRec.need_type, oRec.title]);
		}
	}
	iSize = aUniquePost.length;
	if (aUniquePost.length > 3){
		iSize = 3;
	}
	for (iI = 0; iI < iSize; iI++){
		var sClassNamePre = "homebox" + aUniqueType[iI][0];
		divBox = A.dg(sIDPre + iBoxNo);
		divBox.style.display = "block";
		iBoxNo++;
		var iNum = 0;
		for (iJ = 0; iJ < iSize1; iJ++){
			var oRec = V.oDB.oData.oDashboard.aCommitments[iJ];
			if (oRec.post_id == aUniquePost[iI]){
				iNum++;
			}
		}
		divBox.className = "elebox home color" + aUniqueType[iI][0];
		E.line({divParent:divBox, sClassName:"homedashboardbox"});
		E.memo({divParent:divBox, 
			sClassName:"elespanhomedashboardlargenum color" + aUniqueType[iI][0], 
			sText:iNum});
		E.span({divParent:divBox, sText:oAll.sResponses, 
			sClassName:"elespanhomedashboardtype"});
		E.spacer({divParent:divBox, sClassName:"homedashboardspacer"});
		E.line({divParent:divBox, sClassName:"homedashboardbox"});
		E.text({divParent:divBox, sText:oAll.sToYourPost});
		E.span({divParent:divBox, sText:aUniqueType[iI][1], 
			sClassName:"elespanblue"});
		E.spacer({divParent:divBox, sClassName:"homedashboardspacer"});
	}	
	return (iBoxNo - 1);
};



HO_HOME.prototype.boxmembership = function(divBox, oAll){
	divBox.style.display = "block";
	E.text({divParent:divBox, sText:oAll.sMemberFor});
	E.span({divParent:divBox, sText:oAll.sTitle, sClassName:"elespanblue"});
	E.text({divParent:divBox, sText:oAll.sFor});
	E.spacer({divParent:divBox, sClassName:"homedashboardspacer"});
	E.line({divParent:divBox, sClassName:"homedashboardbox"});
	E.memo({divParent:divBox, sClassName:"elespanhomedashboardlargenum", 
		sText:V.oDB.oData.oDashboard.iNumDaysMember});
	E.span({divParent:divBox, sText:oAll.sDays, 
		sClassName:"elespanhomedashboardtype"});
	E.spacer({divParent:divBox, sClassName:"homedashboardspacer"});
	E.line({divParent:divBox, sClassName:"homedashboardbox"});
};



HO_HOME.prototype.changepasswordgoA = function(oData){
	var that = V.aAA[oData.aValues[0].iObjNo];
	console.log(oData);
	E.action("popupclosex");
	if (oData.sError){
		E.popup({sTitle:"Error", sMessage:oData.sError});
	}
	if (oData.sMessage){
		E.popup(V.oDB.oData.oError[oData.sMessage]);
	}
};



HO_HOME.prototype.changepasswordgo = function(){
	var aValues = [];
	aValues = V.aAA[this.iFormResetPassword].values();
	if ((!aValues[1].sValue) || (aValues[1].sValue != aValues[2].sValue)
			|| (aValues[1].sValue.length < 6) || (aValues[0].sValue.length < 6) ){
		E.action("popupclosex");
		E.popup({sTitle:"Change Pasword Error", 
			sMessage:"Password was not changed."});
	} else {
		aValues.unshift({iObjNo:this.iObjNo});
		A.ajax("homechangepassword", aValues, this.changepasswordgoA);
	}
};



HO_HOME.prototype.changepassword = function(){
	var sID = "", divX = 0;
	this.iFormResetPassword = E.popup({sTitle:"Change Password", 
		sFormName:"aHomeChangePassword", sIDPre:"homechangepassword"});
	sID = "homechangepassword-" + this.iFormResetPassword + "__4";
	divX = A.dg(sID);
	divX.onclick = this.actionA("changepassword_" + this.iObjNo);
	sID = "homechangepassword-" + this.iFormResetPassword + "__1";
	A.dg(sID).focus();
};



HO_HOME.prototype.initdashboard = function(){
	var oAll = {}, sIDPre = "", divBox = 0, iBoxNo = 0;
	oAll = V.oDB.oDashboard;
	B.action("menuleft-home-menuleft_0");
	this.aForms.push(E.page({iIndex:1, sFormName:"aHome", that:this}));
	B.action("elebutton.tabbutton-active-tabbutton-" + this.iObjNo + "_1");
	
	sIDPre = "reg-" + this.aForms[this.iTabNo] + "_" + this.iTabNo + "_";
	divBox = A.dg(sIDPre + (iBoxNo));
	iBoxNo++;
	this.boxpie(divBox, oAll);
	iBoxNo = this.boxresponses(sIDPre, iBoxNo, oAll);
	divBox = A.dg(sIDPre + (iBoxNo + 1));
	this.boxmembership(divBox, oAll);
	divBox = A.dg(sIDPre + (iBoxNo + 2));
	this.boxlogin(divBox, oAll);
	A.history("home");
};



HO_HOME.prototype.init = function(){
	this.iTabNo = 1;
	var sHash = A.history();
	if (!sHash){
		sHash = A.session("sURLHash");
	}
	var bDone = 0;
	switch (sHash){
		case "home":
			this.initdashboard();
			A.history("home");
			bDone = 1;
		break;
		
		case "createpost":
			B.action("menuleft-createpost-menuleft_1");
			A.history("createpost");
			bDone = 1;
		break;
		
		case "myposts":
			B.action("menuleft-myposts-menuleft_2");
			bDone = 1;
		break;
		
		case "users":
			B.action("menuleft-users-menuleft_3");
			A.history("user");
			bDone = 1;
		break;
		
		case "profile":
			B.action("menuleft-profile-menuleft_4");
			A.history("profile");
			bDone = 1;
		break;		
	
	}
	if (!bDone){
		this.initdashboard();
		A.history("home");
	}
	B.actions();
	B.resize();
//	this.tab(1);
};



HO_HOME.prototype.logout = function(){
	A.session("iUser", 0);
	V.oGen.iMenuNo = 0;
	V.aAA[B.oOb.iHome] = 0;
	B.oOb.iHome = 0;
	B.reset();
	A.ajax("logout", [], B.initA);
};



HO_HOME.prototype.profile = function(){
	this.userbox();
	B.action("menuleft--_4")
};



HO_HOME.prototype.tab = function(iTabNo){
	var aNoGrid = [1];
	sSession = "iTabNo_" + this.sName;
	A.session(sSession, this.iTabNo);
	if (aNoGrid.indexOf(this.iTabNo) != -1){
		V.oGen.iPageTop = 1;
	} else {
		V.oGen.iPageTop = 0;
	}
	B.resize();
	B.tab(["elebutton.tabbutton", "active", "tabbutton", 
		this.iObjNo +"_" + this.iTabNo]);
};



HO_HOME.prototype.userbox = function(){
	var divBox = A.gcn("menuuserboxdropdown");
	V.oGen.bUserBoxShow = 0;
	if (!divBox){
		return;
	}
	if (divBox.style.display == "block"){
		divBox.style.display = "none";
	} else {
		V.oGen.iScrollTop = B.scrolltop();		
		divBox.style.top = 
			(V.oGen.iHeaderHeight + V.oGen.iScrollTop + 1) + "px";
		V.oGen.bUserBoxShow = 1;
		divBox.style.display = "block";
		divBox.innerHTML = "";
		if (V.oGen.iDisplayScreenSize < 2){
			E.form({divParent:divBox, sFormName:"aUserBoxSmall", bNooObject:1});
		} else {
			E.form({divParent:divBox, sFormName:"aUserBox", bNooObject:1});
		}
	}
	A.zindex(divBox);
};



		HO_HOME.bInited = 1;
	}
};



//== HOUSE CLASS ===============================================================
function HO_HOUSE(){
	this.sName = "HOUSE";
	this.iObjNo = 0;
	this.divDocs = 0;
	this.sLink = "";
	
	if (typeof HO_HOUSE.bInited == "undefined"){

HO_HOUSE.prototype.docsA = function(oData){
	var that = V.aAA[oData.iObjNo];
	that.divDocs.innerHTML = oData.sText;
};



HO_HOUSE.prototype.docs = function(sName){
	var oEle = {}, aValues = [], sTitle = "";
	aValues.push({sValue:sName});
	aValues.push({iObjNo:this.iObjNo});
	sTitle = V.oHouse.sDocs.split("|");
	if (sName == "privacy"){
		sTitle = sTitle[3];
	}
	if (sName == "terms"){
		sTitle = sTitle[1];
	}
	oEle.sTitle = sTitle;
	this. divDocs = E.popupfull(oEle);
	A.ajax("docs", aValues, this.docsA);
};



HO_HOUSE.prototype.init = function(){
	A.history("house");
	V.oGen.iMenuNo = 0;
	B.reset();	
};



HO_HOUSE.prototype.kill = function(){
	V.aAA[B.oOb.iHouse] = 0;
	B.oOb.iHouse = 0;
};



HO_HOUSE.prototype.resetPassword = function(sLink){
//	/aLoginResetPassword
	this.sLink = sLink;
	var sTitle = V.oDB.oData.oError.oResetPassword.sTitle;
	var sMessage = V.oDB.oData.oError.oResetPassword.sMessage;
	E.popup({sTitle:sTitle,sMessage:sMessage,
		that:this,
		sAction:"resetpassword",
		aButtons:[V.oDB.oData.oInit.oBanners.oGeneral.sOK, 
			V.oDB.oData.oInit.oBanners.oGeneral.sCancel],
		bPageOver:1
	});
	var divBox = V.oDiv.popup;
	E.build({sFormName:"oForm_HouseResetPassword",
		divParent:divBox, sObjectName:"init"});
	A.dg("resetpassword_new").focus();
};



		HO_HOUSE.bInited = 1;
	}
};



//== LOGIN CLASS ===============================================================
function HO_LOGIN(){
	this.sName = "LOGIN";
	this.sEmailAddress = "";
	this.iForm = 0;
	this.iFormForgotPassword = 0;
	this.iFormResetPassword = 0;
	this.iObjNo = 0;
	this.sLink = "";
	
	if (typeof HO_LOGIN.bInited == "undefined"){

HO_LOGIN.prototype.action = function(sAction){
	var oForm = {};
	var aAction = sAction.split("-");
	this[aAction[1]]();
};



HO_LOGIN.prototype.activateA = function(oData){
	E.action("popupclosex");
	if (oData.sMessage){
		E.popup(V.oDB.oData.oError[oData.sMessage]);
	}
};



HO_LOGIN.prototype.activate = function(){
		A.ajax("activate", [{sLink:this.sLink}], this.activateA);
};



HO_LOGIN.prototype.goA = function(oData){
	E.action("buttonenable");
	if (oData.sError){
		E.popup({sTitle:"Login", sMessage:oData.sError});
	} else {
		//success
		V.oGen.iMenuNo = 1;
		A.session("iUser", 1);
		A.session("sURLHash", "home");
		E.action("popupclosex");
		B.init();
	}
};



HO_LOGIN.prototype.go = function(){
	var aErrors = [];
	oForm = V.aAA[this.iForm];
	var aValues = oForm.values();
	if (!aValues[0].sValue){
		aErrors.push("empty");
	}
	if (!A.valid(aValues[0])){
		aErrors.push(aValues[0].sType);
	}
	if (!A.valid(aValues[1])){
		aErrors.push(aValues[1].sType);
	}
	if (aErrors.length > 0){
		E.error({sTitle:"Login", aInput:aErrors});
	} else {
		//log in
		V.oGen.sIDButtonDisabled = "login-" + this.iForm + "__2";
		E.action("buttondisable");
		A.ajax("login", aValues, this.goA);
	}
};



HO_LOGIN.prototype.init = function(){
	this.iForm = E.popup({sTitle:V.oDB.oWords.sLoginTitlebar, 
		that:this, sIDPre:"login", sFormName:"aLogin"});
	var sID = "login-" + this.iForm + "__0"; 
	A.dg(sID).focus();
};



HO_LOGIN.prototype.passwordforgot = function(){
	var sID = "login-" + this.iForm + "__0"; 
	var sEmail = A.value(sID);
	this.iFormForgotPassword = E.popup({sTitle:"Forgot Password", 
		sFormName:"aLoginForgotPassword", sIDPre:"login"});
	var sID = "login-" + this.iFormForgotPassword + "__1";
	A.value(sID, sEmail);
};



HO_LOGIN.prototype.passwordresetGoA = function(oData){
	E.action("popupclosex");
	E.popup(V.oDB.oData.oError[oData.sMessage]);
};



HO_LOGIN.prototype.passwordresetGo = function(){
	var sIDPre = "login-" + this.iFormResetPassword + "__";
	var aValues = [];
	aValues.push(A.value(sIDPre + "1"));
	aValues.push(A.value(sIDPre + "2"));
	aValues.push({sType:"link", sValue:this.sLink});
	var bError = 0;
	if ((aValues[0].sValue) && (aValues[0].sValue != aValues[1].sValue)){
		E.popup(V.oDB.oData.oError.oPasswordsNotMatch);
		bError = 1;
	} else {
		if (!A.valid(aValues[0])){
			E.popup(V.oDB.oData.oError.oInvalidInput);
			bError = 1;		
		}
	}
	if (!bError){
		V.oGen.sIDButtonDisabled = sIDPre + "3";
		E.action("buttondisable");
		A.ajax("resetpassword", aValues, this.passwordresetGoA);
	}
};



HO_LOGIN.prototype.passwordreset = function(sLink){
	this.sLink = sLink;
	this.iFormResetPassword = E.popup({sTitle:"Reset Password", 
		sFormName:"aLoginResetPassword", sIDPre:"login"});
	var sID = "login-" + this.iFormResetPassword + "__1";
	B.actions();
	A.dg(sID).focus();
};



HO_LOGIN.prototype.registeractivate = function(sLink){
	this.sLink = sLink;
	E.popup({sTitle:"Activate Account", 
		sFormName:"aLoginRegisterActivate"});
	B.actions();
};




HO_LOGIN.prototype.registernow = function(){
	V.oGen.iMenuNo = 2;
	E.action("popupclosex");
	V.aAA[this.iObjNo] = 0;
	B.reset();
};



HO_LOGIN.prototype.sendresetlinkA = function(oData){
	if (oData.sMessage){
		E.action("popupclosex");
		E.popup(V.oDB.oData.oError[oData.sMessage]);
	}
};



HO_LOGIN.prototype.sendresetlink = function(){
	var sID = "login-" + this.iFormForgotPassword + "__1";
	var aValues = [A.value(sID)];
	if (A.valid(aValues[0])){
		A.ajax("forgotpassword", aValues, this.sendresetlinkA);
	}
};


		HO_LOGIN.bInited = 1;
	}
};



//== MYPOSTS CLASS ===============================================================
function HO_MYPOSTS(){
	this.sName = "MYPOSTS";
	this.iObjNo = 0;
	this.aForms = [0];
	this.iTabNo = 0;
	this.aGridNos = [];
	this.iGridRow = 0;
	this.aEditRecord = [];
	
	if (typeof HO_MYPOSTS.bInited == "undefined"){

HO_MYPOSTS.prototype.actionA = function(sAction){
	return function(){
		var aAction = sAction.split("_");
		var that = V.aAA[parseInt(aAction[1])];
		that.action(sAction);
	}
};



HO_MYPOSTS.prototype.action = function(sAction){
	var aAction = [];
	aAction = sAction.split("_");
	switch (aAction[0]){
		case "activeedit":
			this.activeedit();
		break;
	
		case "activeeditsave":
			this.activeeditsave();
		break;
	
		case "activeeditcancel":
			this.activeeditcancel();
		break;
	
		case "activemarkascomplete":
			this.activemarkascomplete();
		break;
	
		case "activemarkasdraft":
			this.activemarkasdraft();
		break;
	
		case "activeduplicatetodrafts1":
			this.activeduplicatetodrafts1();
		break;
	
		case "activeduplicatetodrafts":
			this.activeduplicatetodrafts(parseInt(aAction[2]));
		break;
		
		case "deleterecorddrafts":
			switch (aAction[2]){
				case "cancel":
					E.action("popupclosex");
				break;
				
				case "ok":
					this.draftsdelete();
				break;
				
			}
		break;
	
		case "deleterecordhistory":
			switch (aAction[2]){
				case "cancel":
					E.action("popupclosex");
				break;
				
				case "ok":
					this.historydelete(this.iGridRow);
				break;
				
			}
		break;
	
		case "deleterecordhistory1":
			switch (aAction[2]){
				case "cancel":
					E.action("popupclosex");
				break;
				
				case "ok":
					this.historydelete1();
				break;
				
			}
		break;
	
		case "draftsdelete":
			this.iGridRow = parseInt(aAction[2]);
			var sTitle = V.oDB.oData.oError.oDeleteConfirm;
			var sMessage = sTitle.sMessage;
			sTitle = sTitle.sTitle;
			E.popup({sTitle:sTitle, sMessage:sMessage, aButtons:["OK", "CANCEL"],
				that:this, sAction:"drafts"});
		break;
	
		case "draftsduplicate":
			this.draftsduplicate(parseInt(aAction[2]));
		break;
		
		case "draftseditsave":
			this.draftseditsave();
		break;
	
		case "draftseditcancel":
			this.draftseditcancel();
		break;
	
		case "draftsedit":
			this.draftsedit();
		break;
	
		case "draftspostnow":
			this.draftspostnow();
		break;
	
		case "gridedit":
			this.edit();
		break;
	
		case "historydelete":
			this.iGridRow = parseInt(aAction[2]);
			var sTitle = V.oDB.oData.oError.oDeleteConfirm;
			var sMessage = sTitle.sMessage;
			sTitle = sTitle.sTitle;
			E.popup({sTitle:sTitle, sMessage:sMessage, aButtons:["OK", "CANCEL"],
				that:this, sAction:"history"});
		break;
		
		case "historydelete1":
			var sTitle = V.oDB.oData.oError.oDeleteConfirm;
			var sMessage = sTitle.sMessage;
			sTitle = sTitle.sTitle;
			E.popup({sTitle:sTitle, sMessage:sMessage, aButtons:["OK", "CANCEL"],
				that:this, sAction:"history1"});
		break;
		
		case "historyduplicatetodrafts":
			this.historyduplicatetodrafts();
		break;
	
	}
};



HO_MYPOSTS.prototype.activeduplicatetodrafts1 = function(){
	var iPostID = parseInt(this.aEditRecord[0]);
	A.ajax("mypostsactiveduplicatetodrafts1",
		[{iPostID:iPostID}, {iObjNo:this.iObjNo}], 
		this.reloadgridsA);
};



HO_MYPOSTS.prototype.activeduplicatetodrafts = function(iGridRow){
	var oGrid = {}, aData = [], iPostID = 0;
	var oGrid = V.aAA[this.aGridNos[0]];
	if (oGrid.aDataFiltered.length){
		iPostID = oGrid.aDataFiltered[iGridRow][0];
	} else {
		iPostID = oGrid.aData[iGridRow][0];
	}
	A.ajax("mypostsactiveduplicatetodrafts",
		[{iPostID:parseInt(iPostID)}, {iObjNo:this.iObjNo}], this.reloadgridsA);
};



HO_MYPOSTS.prototype.activeeditcancel = function(){
	this.tab(13);
};



HO_MYPOSTS.prototype.activeeditsave = function(){
	var iObjNo = 0, iObjNo1 = 0, divPage = 0, iFormNo = 0, sIDPre = "";
	iObjNo = this.aForms[6];
	var aValues = V.aAA[iObjNo].values();
	aValues.unshift({iObjNo:this.iObjNo});
	aValues.unshift({iPostID:parseInt(this.aEditRecord[0])});
	A.ajax("mypostsactivesave", aValues, this.reloadgridsA);
};



HO_MYPOSTS.prototype.activeedit = function(){
	var iTabNo = 0, sIDPre = "";
	iTabNo = this.iTabNo + 2;
	E.action("tabbuttons_" + this.iObjNo + "_" + iTabNo + "-" + 13);
	this.tab(iTabNo);
	sIDPre = "reg-" + this.aForms[iTabNo] + "_"+ iTabNo + "_";
	A.dg(sIDPre + "7").onclick = this.actionA
		("activeeditsave_"+ this.iObjNo);
	A.dg(sIDPre + "8").onclick = this.actionA
		("activeeditcancel_"+ this.iObjNo);
	A.value(sIDPre + "1", this.aEditRecord[1]);
	A.value(sIDPre + "2", this.aEditRecord[6]);
	aDates = this.aEditRecord[4].split(" - ");
	if (!aDates[1]){
		aDates[1] = "";
	}
	A.value(sIDPre + "4", aDates[0]);
	A.value(sIDPre + "5", aDates[1]);
	
};



HO_MYPOSTS.prototype.activemarkascomplete = function(){
	var iPostID = parseInt(this.aEditRecord[0]);
	A.ajax("mypostsactivemarkascomplete",
		[{iPostID:iPostID},{iObjNo:this.iObjNo}], 
		this.reloadgridsA);
};



HO_MYPOSTS.prototype.activemarkasdraft = function(){
	var iPostID = parseInt(this.aEditRecord[0]);
	A.ajax("mypostsactivemarkasdraft",
		[{iPostID:iPostID},{iObjNo:this.iObjNo}], 
		this.reloadgridsA);
};



HO_MYPOSTS.prototype.address = function(iID){
	var iI = 0; iFound = 0; iSize = 0, oRec = {}, oRec1 = {}, sReturn = "";
	iSize = V.oDB.oData.oUser.aAddresses.length;
	while ((iI < iSize) && (!iFound)){
		oRec = V.oDB.oData.oUser.aAddresses[iI];
		if (oRec.id == iID){
			iFound = (iI + 1);
		} else {
			iI++;
		}
	}
	if (iFound){
		iFound--;
		oRec = V.oDB.oData.oUser.aAddresses[iFound];
		sReturn = oRec.address_line1 + "<br>" +
			oRec.address_line2 + "<br>";
	}
	iFound = 0;
	iI = 0;
	iSize = V.oDB.oData.aRegions.length;
	while ((iI < iSize) && (!iFound)){
		oRec1 = V.oDB.oData.aRegions[iI];
		if (oRec1.iID == oRec.region_id){
			iFound = (iI + 1);
		} else {
			iI++;
		}
	}
	if (iFound){
		iFound--;
		sReturn += oRec1.sValue;
	}
	return sReturn;
};



HO_MYPOSTS.prototype.draftsdelete = function(){
	var iPostID = parseInt(this.aEditRecord[0]);
	A.ajax("mypostsdraftsdelete",
		[{iPostID:iPostID}, {iObjNo:this.iObjNo}], 
		this.reloadgridsA);
};



HO_MYPOSTS.prototype.draftseditcancel = function(){
	this.tab(13);
};



HO_MYPOSTS.prototype.draftsedit = function(){
	var iTabNo = 0;
	iTabNo = this.iTabNo + 2;
	E.action("tabbuttons_" + this.iObjNo + "_" + iTabNo + "-13");
	this.tab(iTabNo);
	sIDPre = "reg-" + this.aForms[iTabNo] + "_"+ iTabNo + "_";
	A.dg(sIDPre + "6").onclick = this.actionA
		("draftseditsave_"+ this.iObjNo);
	A.dg(sIDPre + "7").onclick = this.actionA
		("draftseditcancel_"+ this.iObjNo);
	A.value(sIDPre + "0", this.aEditRecord[1]);
	A.value(sIDPre + "1", this.aEditRecord[6]);
	aDates = this.aEditRecord[4].split(" - ");
	if (!aDates[1]){
		aDates[1] = "";
	}
	A.value(sIDPre + "3", aDates[0]);
	A.value(sIDPre + "4", aDates[1]);
};



HO_MYPOSTS.prototype.draftspostnow = function(){
	var iPostID = parseInt(this.aEditRecord[0]);
	A.ajax("mypostsdraftspostnow",
		[{iPostID:iPostID}, {iObjNo:this.iObjNo}], 
		this.reloadgridsA);
};



HO_MYPOSTS.prototype.draftsduplicate = function(iGridRow){
	var oGrid = {}, aData = [], iPostID = 0;
	var oGrid = V.aAA[this.aGridNos[1]];
	if (oGrid.aDataFiltered.length){
		iPostID = oGrid.aDataFiltered[iGridRow][0];
	} else {
		iPostID = oGrid.aData[iGridRow][0];
	}
	A.ajax("mypostsdraftsduplicate",
		[{iPostID:parseInt(iPostID)},{iObjNo:this.iObjNo}], this.reloadgridsA);
};



HO_MYPOSTS.prototype.draftseditsave = function(){
	var iObjNo = 0, iObjNo1 = 0, divPage = 0, iFormNo = 0, sIDPre = "";
	iObjNo = V.aAA[this.aForms[2]].aForms[2];
	var aValues = V.aAA[iObjNo].values();
	aValues.unshift({iObjNo:this.iObjNo});
	aValues.unshift({iPostID:parseInt(this.aEditRecord[0])});
	A.ajax("mypostsdraftssave", aValues, this.reloadgridsA);
};



HO_MYPOSTS.prototype.editclose = function(iFormNo){
	var oEdit = {}, iI = 0, divX = 0, sID = "", aBreadCrumbs = [];
	oEdit = V.aAA[iFormNo];
	iI = 1;
	oEdit.aForms.forEach(function(oRec){
		sID = "tabwork-" + iFormNo + "_" + iI;
		divX = A.dg(sID);
		if (divX){
			divX.parentNode.removeChild(divX);
		}
		V.aAA[oRec] = 0;
		iI++;
	});
	V.aAA[iFormNo].aForms = [];
	divX = A.dg("tabbuttonsbox-" + iFormNo)
	divX.parentNode.removeChild(divX);
	B.action("elebutton.tabbutton-active-tabbutton-" + 
		this.iObjNo + "_" + this.iTabNo);
	V.oGen.iPageTop = 0;
	B.resize();
	aBreadcrumbs = ["My Posts"];
	E.breadcrumbs(aBreadcrumbs);
};



HO_MYPOSTS.prototype.edit = function(){
	var iTabNo = 0, sIDPre = "";
	switch (this.iTabNo){
		case 1:
			iTabNo = 4;
		break;
		
		case 2:
			iTabNo = 7;
		break;
		
		case 3:
			iTabNo = 10;
		break;
		
	}  
	B.action("elebutton.tabbutton-active-tabbutton-" + 
			this.iObjNo + "_" + iTabNo);
	E.action("tabbuttons_" + this.iObjNo + "_" + iTabNo + 
		"-" + (iTabNo + 1) + "-13");
	
	sBreadcrumb = ",,,,Active,,,Drafts,,,History".split(",")[iTabNo];
	aBreadcrumbs = ["My Posts", sBreadcrumb];
	E.breadcrumbs(aBreadcrumbs);
	iObjNo = this.aForms[iTabNo];
	A.dg("tabbutton-" + this.iObjNo + "_13").className = 
		"elebutton tabbutton back";
	sIDPre = "reg-" + iObjNo + "_" + iTabNo + "_"; 
	A.dg(sIDPre + "0").innerHTML = this.aEditRecord[1];
	A.dg(sIDPre + "1").innerHTML = this.aEditRecord[3];
	A.dg(sIDPre + "2").innerHTML = this.aEditRecord[6];
	A.dg(sIDPre + "4").innerHTML = this.address(parseInt(this.aEditRecord[5]));
	A.dg(sIDPre + "6").innerHTML = this.aEditRecord[8];
	aDates = this.aEditRecord[4].split(" - ");
	if (!aDates[1]){
		aDates[1] = "";
	}
	A.dg(sIDPre + "8").innerHTML = aDates[0];
	A.dg(sIDPre + "10").innerHTML = aDates[1];
	switch (this.iTabNo){
		case 4:
			A.dg(sIDPre + "12").onclick = this.actionA
				("activeedit_"+ this.iObjNo);
			A.dg(sIDPre + "13").onclick = this.actionA
				("activemarkascomplete_"+ this.iObjNo);
			A.dg(sIDPre + "14").onclick = this.actionA
				("activemarkasdraft_"+ this.iObjNo);
			A.dg(sIDPre + "15").onclick = this.actionA
				("activeduplicatetodrafts1_"+ this.iObjNo);		
		break;
	
		case 7:
			A.dg(sIDPre + "12").onclick = this.actionA
				("draftsedit_"+ this.iObjNo);
			A.dg(sIDPre + "13").onclick = this.actionA
				("draftsdelete_"+ this.iObjNo);
			A.dg(sIDPre + "14").onclick = this.actionA
				("draftspostnow_"+ this.iObjNo);
		break;
	
		case 10:
			A.dg(sIDPre + "12").onclick = this.actionA
				("historydelete1_"+ this.iObjNo);
			A.dg(sIDPre + "13").onclick = this.actionA
				("historyduplicatetodrafts_"+ this.iObjNo);
		break;
	
	}
};



HO_MYPOSTS.prototype.historydelete1 = function(){
	var iPostID = parseInt(this.aEditRecord[0]);
	A.ajax("mypostshistorydelete1",
		[{iPostID:iPostID}, {iObjNo:this.iObjNo}], 
		this.reloadgridsA);
};



HO_MYPOSTS.prototype.historydelete = function(iGridRow){
	var oGrid = {}, aData = [], iPostID = 0;
	var oGrid = V.aAA[this.aGridNos[2]];
	if (oGrid.aDataFiltered.length){
		iPostID = oGrid.aDataFiltered[iGridRow][0];
	} else {
		iPostID = oGrid.aData[iGridRow][0];
	}
	A.ajax("mypostshistorydelete",
		[{iPostID:parseInt(iPostID)},{iObjNo:this.iObjNo}], this.reloadgridsA)
};



HO_MYPOSTS.prototype.historyduplicatetodrafts = function(){
	var iPostID = parseInt(this.aEditRecord[0]);
	A.ajax("mypostshistoryduplicatetodrafts",
		[{iPostID:iPostID}, {iObjNo:this.iObjNo}], 
		this.reloadgridsA);
};



HO_MYPOSTS.prototype.init = function(){
	var oAll = {}, divParent = 0, iWidth = 0, iI =0, sClassName = "",
		divButton = 0, aTabs = [], iSize = 0, iObjNo = 0, oForm = {}, oGrid = {},
		oEle = {}, aValues = [], iTabNo = 0;
	oAll = V.oHouse;
	divParent = A.gcn("maintop two");
	divParent = E.div
		(divParent, "tabbuttonsbox", "tabbuttonsbox-" + this.iObjNo);
	divParent = E.div(divParent, "tabbuttonsboxin");
	iSize = oAll.aTabsMyPosts.length;
	for (iI = 0; iI < iSize; iI++){
		var sClassName = "tabbutton";
		var divButton = E.button({divParent:divParent, 
		sClassName:sClassName + " act", 
		sID:"tabbutton-" + this.iObjNo + "_" + (iI + 1), 
		sText:oAll.aTabsMyPosts[iI]});
	}
	E.action("tabbuttons_" + this.iObjNo + "_1-2-3");	
	for (iI = 1; iI <= iSize; iI++){
		var oPage = {iIndex:iI, sFormName:"aMyPosts", that:this};
		this.aForms.push(E.page(oPage));
	}
	iTabNo = A.session("iTabNo_" + this.sName);
	if ((!iTabNo) || (iTabNo > 3)){
		iTabNo = 1;
	}
	B.action("elebutton.tabbutton-active-tabbutton-" + 
		this.iObjNo + "_" + iTabNo);
	for (iI = 1; iI <= iSize; iI++){
		oForm = V.aAA[this.aForms[iI]];
		if (oForm.iGridNo){
			oGrid = V.aAA[oForm.iGridNo];
			aValues = [{sName:oGrid.oEle.sName, iGridNo:oForm.iGridNo}];
			if (!oGrid.oEle.bNoPopulate){
				A.ajax("grid", aValues, oGrid.populateA);
			}
			this.aGridNos[iI] = oForm.iGridNo;
		}
	}
	A.history("myposts");
	B.actions();
	B.resize();
};



HO_MYPOSTS.prototype.reloadgridsA = function(oData){
	E.action("popupclosex");
	var oGrid = {}, that = 0;
	var that = V.aAA[oData.aValues[1].iObjNo];
	that.tab(13);
	if (oData.sMessage){
		E.message(V.oDB.oData.oError[oData.sMessage].sMessage);
	}
	if (oData.sError){
		E.popup(V.oDB.oData.oError[oData.sError]);
	}
	if (oData.aData1){
		oGrid = V.aAA[that.aGridNos[1]];
		oGrid.aDataFiltered = [];
		oGrid.aData = oData.aData1.aData;
		oGrid.populate();
	}
	if (oData.aData2){
		oGrid = V.aAA[that.aGridNos[2]];
		oGrid.aDataFiltered = [];
		oGrid.aData = oData.aData2.aData;
		oGrid.populate();
	}
	if (oData.aData3){
		oGrid = V.aAA[that.aGridNos[3]];
		oGrid.aDataFiltered = [];
		oGrid.aData = oData.aData3.aData;
		oGrid.populate();
	}
};



HO_MYPOSTS.prototype.tab = function(iTabNo){
	var aNoGrid = [], aBreadcrumbs;
	aNoGrid = [4, 6, 7, 9, 10, 12];
	aBreadcrumbs = ["My Posts"];
	if (iTabNo == 13){
		if ((this.iTabNo == 6) || (this.iTabNo == 9)){
			V.oGen.iPageTop = 2;
		}
		switch (this.iTabNo){
			case 4:
			case 5:
			case 6:
				this.iTabNo = 1;
			break;
			
			case 7:
			case 8:
			case 9:
				this.iTabNo = 2;
			break;
			
			case 10:
			case 11:
			case 12:
				this.iTabNo = 3;
			break;
			
		}
		E.action("tabbuttons_" +this.iObjNo + "_1-2-3");
	} else {
		switch (iTabNo){
			case 4:
			case 5:
				aBreadcrumbs.push("Active");
			break;
			
			case 6:
				aBreadcrumbs.push("Active");
				aBreadcrumbs.push("Edit");
			break;
			
			case 7:
			case 8:
				aBreadcrumbs.push("Drafts");
			break;
			
			case 9:
				aBreadcrumbs.push("Drafts");
				aBreadcrumbs.push("Edit");
			break;
			
			case 10:
			case 11:
				aBreadcrumbs.push("History");
			break;
			
			case 12:
				aBreadcrumbs.push("History");
				aBreadcrumbs.push("Edit");
			break;
			
		}
		this.iTabNo = iTabNo;
	}
	sSession = "iTabNo_" + this.sName;
	A.session(sSession, this.iTabNo);
	if (aNoGrid.indexOf(this.iTabNo) != -1){
		V.oGen.iPageTop = 2;
	} else {
		V.oGen.iPageTop = 0;
	}
	if ((iTabNo == 6) || (iTabNo == 9)){
		E.action("tabbuttons_" +this.iObjNo + "_");
		V.oGen.iPageTop = 1;
	}
	B.resize();
	B.tab(["elebutton.tabbutton", "active", "tabbutton", 
		this.iObjNo +"_" + this.iTabNo]);
	E.breadcrumbs(aBreadcrumbs);
	E.action("tabbuttonswidth_" +this.iObjNo);
};



		HO_MYPOSTS.bInited = 1;
	}
};




//== PROFILE CLASS ===============================================================
function HO_PROFILE(){
	this.sName = "PROFILE";
	this.iObjNo = 0;
	this.aForms = [];
	this.iTabNo = 1;
	this.aWeekHours = [];
	this.aAddresses = [];
	this.oData = {};
	
	if (typeof HO_PROFILE.bInited == "undefined"){

HO_PROFILE.prototype.actionA = function(sAction){
	return function(){
		var aAction = sAction.split("_");
		var that = V.aAA[parseInt(aAction[1])];
		that.action(sAction);
	}
};



HO_PROFILE.prototype.action = function(sAction){
	var aAction = [];
	aAction = sAction.split("_");
	switch (aAction[0]){
		case "save":
			this.save();
		break;
	}
};


	
HO_PROFILE.prototype.data = function(){
	var sIDPre = "", divX = 0, iI = 0, aHours = []; var that = 0, thatt = 0,
		iObjNo = 0, aValues = [];
	sIDPre = "reg-" + this.aForms[0] + "_1_";
	A.value(sIDPre + "1", this.oData.oUser.name);
	A.value(sIDPre + "2", this.oData.oUser.surname);
	A.value(sIDPre + "3", this.oData.oUser.email);
	sIDPre = "reg-" + this.aForms[1] + "_2_";
	A.value(sIDPre + "1", this.oData.oUser.sOrganisationName);
	A.value(sIDPre + "4", parseInt(this.oData.oUser.iOrganisationCategoryID));
	A.value(sIDPre + "5", parseInt(this.oData.oUser.organisation_type));
	A.value(sIDPre + "6", this.oData.oUser.description);
	if (this.oData.oLogo.oInfo){
		divX = A.dg(sIDPre + "3");
		divX.parentNode.style.display = "block";
		divX = E.div(divX, "img100", "", "img");
		divX.src = "data:" + this.oData.oLogo.oInfo.mime + ";base64," +
			this.oData.oLogo.sB64;
	}
	sIDPre = "reg-" + this.aForms[2] + "_3_";
	A.value(sIDPre + "1", this.oData.oUser.contact_person);
	A.value(sIDPre + "2", this.oData.oUser.contact_email);
	A.value(sIDPre + "3", this.oData.oUser.contact_number);

	
	var oDays = {
		Sunday:1,Monday:2,Tuesday:3,Wednesday:4,Thursday:5,Friday:6,Saturday:7};
	var aHours = A.json(this.oData.aHours);
	V.aAA[this.aWeekHours[0]] = 0;
	this.aWeekHours = [];
	var iI = 0;
	that = this;
	aHours.forEach(function(oRec){
		iObjNo = A.object();
		V.aAA[iObjNo] = new HO_WEEKHOURS();
		that.aWeekHours.push(iObjNo);
		V.aAA[iObjNo].iObjNo = iObjNo;
		V.aAA[iObjNo].oEle = {that:that, sID:"reg-" + that.aForms[2] + "_3_4"};
		V.aAA[iObjNo].init();
		divX = A.dg(V.aAA[iObjNo].oEle.sID);
		A.attribute(divX, "type", "weekhours");
		V.aAA[iObjNo].aValues = [
			oDays[aHours[iI].days_of_week.split("-")[0]],
			oDays[aHours[iI].days_of_week.split("-")[1]],
			aHours[iI].opening_time,
			aHours[iI].closing_time,
		];
		iI++;
	});
	iI = 0;
	this.aWeekHours.forEach(function(oRec){
		iObjNo = that.aWeekHours[iI];
		thatt = V.aAA[iObjNo]
		A.value(thatt.oEle.sID, thatt.aValues);
		V.aAA[iObjNo].aValues = aHours[iI];
		iI++;
	});
	

	iI = 0;
	that = this;
	this.oData.aPlaces = A.json(this.oData.aPlaces);
	this.oData.aPlaces.forEach(function(oRec){
		iObjNo = that.aAddresses[iI];
		if (!iObjNo){
			sID = "tabwork-"+ that.iObjNo + "_4"; 
			sIDPre = "reg-" + that.aForms[3] + "_4_" + (iI + 1);
			iObjNo = E.address({that:that, sID:sIDPre, divParent:A.dg(sID), 
				bNoInit:1});
			E.div(A.dg(sID, "eleaddress", sIDPre));
			V.aAA[iObjNo].init(iI);
		}
		aValues = V.aAA[iObjNo].regions(parseInt(oRec.region_id));
		aValues.push(oRec.address_line1);
		aValues.push(oRec.address_line2);
		aValues.push(parseInt(oRec.main));
		aValues.unshift(parseInt(oRec.id));
		sIDPre = "reg-" + that.aForms[3] + "_4_" + (iI + 1) + "_" + iObjNo;
		V.aAA[iObjNo].aValues = aValues;
		A.value(sIDPre, aValues);
		iI++;
	});
	
	
	
	
	sIDPre = "reg-" + this.aForms[4] + "_5_";
	A.value(sIDPre + "1", this.oData.oUser.website_link);
	A.value(sIDPre + "2", this.oData.oUser.facebook_link);
	A.value(sIDPre + "3", this.oData.oUser.twitter_handle);
	A.value(sIDPre + "4", this.oData.oUser.instagram_link);
	
};



HO_PROFILE.prototype.initA = function(oData){
	var that = V.aAA[oData.aValues[0].iObjNo];
	console.log(oData)
	that.oData = oData.oData;
	that.data();
};



HO_PROFILE.prototype.init = function(){
	var oAll = {}, divParent = 0, iWidth = 0, iI =0, sClassName = "",
	divButton = 0, aTabs = [], iSize = 0, iObjNo = 0, oForm = {}, oGrid = {},
	oEle = {}, aValues = [], iTabNo = 0, divX = 0;
	oAll = V.oHouse;
	divParent = A.gcn("maintop two");
	divParent = E.div
		(divParent, "tabbuttonsbox", "tabbuttonsbox-" + this.iObjNo);
	divParent = E.div(divParent, "tabbuttonsboxin");
	iSize = oAll.aTabsRegister.length;
	for (iI = 0; iI < iSize; iI++){
		var sClassName = "tabbutton";
		var divButton = E.button({divParent:divParent, 
		sClassName:sClassName + " act", 
		sID:"tabbutton-" + this.iObjNo + "_" + (iI + 1), 
		sText:oAll.aTabsRegister[iI]});
	}
	E.action("tabbuttons_" + this.iObjNo + "_1-2-3-4-5");	
	for (iI = 1; iI <= iSize; iI++){
		var oPage = {iIndex:iI, sFormName:"aReg", that:this};
		this.aForms.push(E.page(oPage));
		var divParent = A.dg("tabwork-" + this.iObjNo + "_" + iI);
	}
	divX = E.div(V.oDiv.footer, "mainheaderleft");
	divX = E.div(V.oDiv.footer, "mainheaderwork");
	divX = E.div(divX, "tabbuttonsbox", "tabbuttonsbox-" + this.iObjNo);
	divX = E.div(divX, "tabbuttonsboxin");
	divButton = E.button({divParent:divX, 
		sClassName:"footerbutton", 
		sID:"profilefooter-" + this.iObjNo, 
		sText:"SAVE"});
	divButton.onclick = this.actionA("save_" + this.iObjNo + "_" + iI);
	iTabNo = A.session("iTabNo_" + this.sName);
	if (!iTabNo){
		iTabNo = 1;
	}
	B.action("elebutton.tabbutton-active-tabbutton-" + 
		this.iObjNo + "_" + iTabNo);
	for (iI = 1; iI <= iSize; iI++){
		oForm = V.aAA[this.aForms[iI]];
	}
	A.history("profile");
	B.actions();
	B.resize();
	A.ajax("profile", [{iObjNo:this.iObjNo}], this.initA);
};



HO_PROFILE.prototype.saveA = function(oData){
	var that = 0;
	that = V.aAA[oData.aValues[0].iObjNo];
	V.oDB.oData.oUser.aAddresses = oData.aAddresses; 
	console.log(that)
	console.log(oData)
	if (oData.sMessage){
		E.message(V.oDB.oData.oError[oData.sMessage].sMessage);
	}
	if (oData.sError){
		E.popup(V.oDB.oData.oError[oData.sError]);
	}
};



HO_PROFILE.prototype.save = function(){
	var sIDPre = "", aValues = [], iTabNo = 0, aAddresses = [], aAddress = [];
	iTabNo = this.iTabNo;
	aValues = [{iObjNo:this.iObjNo}, {iTabNo:iTabNo}];
	sIDPre = "reg-" + this.aForms[(iTabNo - 1)] + "_" + iTabNo + "_";
	switch (iTabNo){
		case 1:
			aValues.push(A.value(sIDPre + "1"));
			aValues.push(A.value(sIDPre + "2"));
			aValues.push(A.value(sIDPre + "3"));
		break;
		
		case 2:
			aValues.push(A.value(sIDPre + "1"));
			aValues.push(A.value(sIDPre + "4"));
			aValues.push(A.value(sIDPre + "5"));
			aValues.push(A.value(sIDPre + "6"));
		break;
		
		case 3:
			aValues.push(A.value(sIDPre + "1"));
			aValues.push(A.value(sIDPre + "2"));
			aValues.push(A.value(sIDPre + "3"));
		break;
		
		case 4:
			iI = 1;
			that = this;
			this.aAddresses.forEach(function(oRec){
				iObjNo = that.aAddresses[(iI - 1)];
				aAddress = A.value(sIDPre + iI + "_" + iObjNo);
				if (V.aAA[iObjNo].aValues[0]){
					aAddress.sValue[0] = V.aAA[iObjNo].aValues[0];
				}
				aValues.push(aAddress);
				
				iI++;
			})
		break;
		
		case 5:
			aValues.push(A.value(sIDPre + "1"));
			aValues.push(A.value(sIDPre + "2"));
			aValues.push(A.value(sIDPre + "3"));
			aValues.push(A.value(sIDPre + "4"));
		break;
		
	}
	if (aValues.length){
		A.ajax("profilesave", aValues, this.saveA);
	}
};



HO_PROFILE.prototype.tab = function(iTabNo){
	var aNoGrid = [1, 2, 3, 4, 5];
	this.iTabNo = iTabNo;
	sSession = "iTabNo_" + this.sName;
	A.session(sSession, iTabNo);
	if (aNoGrid.indexOf(iTabNo) != -1){
		V.oGen.iPageTop = 2;
	} else {
		V.oGen.iPageTop = 0;
	}
	B.resize();
	B.tab(["elebutton.tabbutton", "active", "tabbutton", 
		this.iObjNo +"_" + iTabNo]);
	E.action("tabbuttonswidth_" +this.iObjNo);
};



HO_PROFILE.prototype.valuegetaddress = function(){
	var iI = 0, oAd = {}, iObjNo = 0, that = 0, aValues = [], iRegion = 0;
	that = this;
	this.aAddresses.forEach(function(oRec){
		iObjNo = oRec
		var oAd = V.aAA[oRec];
		aValues = A.value(oAd.oEle.sID + "_" + iObjNo).sValue;
		aValues[0] = oAd.aValues[0];
		oAd.aValues = aValues;
		if (aValues[4]){
			iRegion = aValues[4];
		} else {
			iRegion = aValues[3];
		}
		V.oDB.oData.oUser.aAddresses[iI] = {
			address_line1: aValues[5],
			address_line2: aValues[6],
			id:aValues[0],
			main:aValues[7],
			region_id:iRegion
		}
		iI++;
	});	
};



HO_PROFILE.prototype.valuesetaddress = function(){
	var iI = 0, oAd = {}, iObjNo = 0, that = 0;
	that = this;
	this.aAddresses.forEach(function(oRec){
		iObjNo = oRec
		var oAd = V.aAA[oRec];
		oRec = V.oDB.oData.oUser.aAddresses[iI];
		if (oRec){
			var iRegion = parseInt(oRec.region_id);
			aValues = V.aAA[iObjNo].regions(iRegion);
			aValues.push(oRec.address_line1);
			aValues.push(oRec.address_line2);
			aValues.push(parseInt(oRec.main));
			aValues.unshift(parseInt(oRec.id));
			sIDPre = "reg-" + that.aForms[3] + "_4_" + (iI + 1) + "_" + iObjNo;
			V.aAA[iObjNo].aValues = aValues;
			A.value(sIDPre, aValues);
		}
		iI++;
	});
};



		HO_PROFILE.bInited = 1;
	}
};



//== REGISTER CLASS ============================================================
function HO_REGISTER(){
	this.sName = "REGISTER";
	this.sLogoFilename = "";
	this.iObjNo = 0;
	this.iStepNow = 1;
	this.iStepNext = 0;
	this.iStepsMax = 5;
	this.aForms = [];
	this.aWeekHours = [];
	this.aAddresses = [];
	this.aValues = [0, [], [], [], [], []];
	
	if (typeof HO_REGISTER.bInited == "undefined"){

HO_REGISTER.prototype.action = function(sAction){
	var aAction = sAction.split("_");
	switch (aAction[0]){
		case "uploaddone":
			this.sLogoFilename = aAction[2];
			this.logouploaded();
		break;
	}
};



HO_REGISTER.prototype.footer = function(sAction){
	var aAction =sAction.split("-");
	switch (aAction[1]){
		case "nextstep":
			this.iStepNow++;
			if (this.iStepNow > this.iStepsMax){
				this.iStepNow = this.iStepsMax;
				var divX = A.dg(aAction[2]);
				divX.childNodes[0].textContent = "SAVE & CREATE";
			}
			B.action("elebutton.regbutton-x-regbutton_" + this.iStepNow);
		break;
		case "cancel":
			
		break;
	}
};



HO_REGISTER.prototype.init = function(){
	var oAll = {}, divParent = 0, iWidth = 0, iI =0, sClassName = "",
	divButton = 0, aTabs = [], iSize = 0, iObjNo = 0, oForm = {}, oGrid = {},
	oEle = {}, aValues = [];
	oAll = V.oHouse;
	divParent = A.gcn("maintop two");
	divParent = E.div
		(divParent, "'sbox", "tabbuttonsbox-" + this.iObjNo);
	divParent = E.div(divParent, "tabbuttonsboxin");
	iWidth = 0;
	iSize = oAll.aTabsRegister.length;
	this.iStepsMax = iSize;
	for (iI = 0; iI < iSize; iI++){
		var sClassName = "regbutton";
		var divButton = E.button({divParent:divParent, 
			sClassName:sClassName + " act", 
			sID:"regbutton-" + this.iObjNo + "_" + (iI + 1), 
			sText:oAll.aTabsRegister[iI]});
		E.icon({divParent:divButton, sClassName:"regbutton", sText:"&#xE00B;"});
		E.span({divParent:divButton, sClassName:"regbuttonnum", sText:(iI + 1)});
		iWidth += (divButton.clientWidth + 45);
	}
	divParent.style.width = (iWidth) + "px";
	
	for (iI = 1; iI <= iSize; iI++){
		this.aForms.push(E.page({iIndex:iI, sFormName:"aReg", that:this}));
	}
	divParent = A.dg("registerfooter");
	divParent = E.div
		(divParent, "tabbuttonsbox", "tabbuttonsbox-" + this.iObjNo);
	divParent = E.div(divParent, "tabbuttonsboxin");
	iWidth = 0 ;
	for (iI = 0; iI < 2; iI++){
		var divButton = E.button({divParent:divParent, 
			sClassName:"footerbutton act", 
			sID:"regfooter-" + this.iObjNo + "_" + (iI + 1), 
			sText:oAll.aFooterButtonsReg[iI]});
		iWidth += (divButton.clientWidth + 45);
	}
	divParent.style.width = (iWidth) + "px";
	this.step();
	A.history("register");
	B.actions();
	B.resize();
	
	
};



HO_REGISTER.prototype.kill = function(){
	this.aWeekHours.forEach(function(oRec){
		V.aAA[oRec] = 0;
	});
	this.aAddresses.forEach(function(oRec){
		V.aAA[oRec] = 0;
	});
	this.aForms.forEach(function(oRec){
		V.aAA[oRec] = 0;
	});
	V.aAA[B.oOb.iRegister] = 0;
	B.oOb.iRegister = 0;
};



HO_REGISTER.prototype.logouploadedA = function(oData){
	var that = V.aAA[oData.iObjNo]
	var divLogo = "reg-" + that.aForms[1] + "_2_3";
	divLogo = A.dg(divLogo);
	divLogo.parentNode.style.display = "block";
	divLogo.innerHTML = "";
	var divImg = E.div(divLogo, "img100", "", "img");
	divImg.src = V.sSrvURL + oData.sNewFilename;
};



HO_REGISTER.prototype.logouploaded = function(){
	A.ajax("logocrop", 
		[{sLogo:this.sLogoFilename}, {iObjNo:this.iObjNo}], this.logouploadedA);
};



HO_REGISTER.prototype.next = function(){
	this.iStepNext = 1;
	var aAction = ("---" + this.iObjNo + "_" + this.iStepNow).split("-");
	this.step(aAction);
};



HO_REGISTER.prototype.step = function(aAction){
	if (!aAction){
		aAction = ("---" + this.iObjNo + "_" + this.iStepNow).split("-");
	} else {
		var iForm = this.aForms[this.iStepNow - 1];
		this.aValues[this.iStepNow] = V.aAA[iForm].values();
		var bValid = this.test();
		if (!bValid){
			return;
		}
	}
	this.iStepNow = parseInt(aAction[3].split("_")[1]);
	if (this.iStepNext){
		this.iStepNext = 0;
		this.iStepNow++;
		aAction[3] = this.iObjNo + "_" + this.iStepNow;
	}
	if (this.iStepNow > this.iStepsMax){
		this.savecreate();
		return;
	}
	var divButton = A.dg("regfooter-" + this.iObjNo + "_2").childNodes[0];
	if (this.iStepNow == this.iStepsMax){
		divButton.textContent = V.oHouse.aFooterButtonsReg[2];
	} else {
		divButton.textContent = V.oHouse.aFooterButtonsReg[1];
	}
	var aDivWorkTab = A.gcn("tabwork", 1);
	var that = this;
	aDivWorkTab.forEach(function(oRec){
		var sClassName = "regbutton";
		var aID = oRec.id.split("-");
		var divButton = A.dg("regbutton-" + aID[1]);
		var divIcon = divButton.childNodes[1];
		var aID2 = aID[1].split("_");
		if (parseInt(aID2[1]) == that.iStepsMax){
			sClassName = "regbuttonlast";
		}
		if (aID[1] == aAction[3]){
			oRec.style.display = "block";
			divButton.className = "elebutton " + sClassName + " active";
			divIcon.className = "eleicon " + sClassName + " active";
		} else {
			oRec.style.display = "none";
			divButton.className = "elebutton " + sClassName;
			divIcon.className = "eleicon " + sClassName;
		}
	});
};



HO_REGISTER.prototype.savecreateA = function(oData){
	var iI = 0, iObjNo = 0, iErrors = 0, sErrors = "";
	iObjNo = oData.aValues[0].iObjNo;
	V.aAA[iObjNo].iStepNow = 5;
	iErrors = oData.aErrors.length;
	if (iErrors){
		oData.aErrors.forEach(function(oRec){
			sErrors += oRec + "<br>"
		});
		E.popup({sTitle:"Errors", sMessage:sErrors});
	}
};



HO_REGISTER.prototype.savecreate = function(){
	var aWeekHours = [], aAddresses = [], oAddress = {}, oWeekHour = {},
		oValue = {};
	this.aWeekHours.forEach(function(oRec){
		oWeekHour = V.aAA[oRec];
		oValue = A.value(oWeekHour.oEle.sID + "_" + oWeekHour.iObjNo);
		aWeekHours.push(oValue.sValue);
	});
	this.aAddresses.forEach(function(oRec){
		oAddress = V.aAA[oRec];
		oValue = A.value(oAddress.oEle.sID + "_" + oAddress.iObjNo);
		aAddresses.push(oValue.sValue);
	});
	this.aValues[0] = {iObjNo:this.iObjNo, sLogo:this.sLogoFilename,
		aWeekHours:aWeekHours,aAddresses:aAddresses};
	A.ajax("register", this.aValues, this.savecreateA);
};



HO_REGISTER.prototype.test = function(){
	var aValues = this.aValues[this.iStepNow];
	var sErrors = "";
	var bValid = 1;
	aValues.forEach(function(oRec){
		if ((oRec.bRequired) && (!oRec.sValue)){
			sErrors += "Required " + oRec.sText + "<br>";
		}
		if ((oRec.bRequired) && (oRec.sType == "select-one") && 
			(oRec.sValue == -1)){
			sErrors += "Required " + oRec.sText + "<br>";
		}
		if ((oRec.sType == "email") && (!A.valid(oRec))){
			sErrors += "Invalid " + oRec.sText + "<br>"; 
		}
		if ((oRec.sType == "password") && (!A.valid(oRec))){
			sErrors += "Invalid " + oRec.sText + "<br>"; 
		}
	});
	if (sErrors){
		E.popup({sTitle:"Errors", sMessage:sErrors});
		bValid = 0;

		
		
bValid = 1;

	
	
	}
	return bValid;
};



		HO_REGISTER.bInited = 1;
	}
};



//== USERS CLASS ===============================================================
function HO_USERS(){
	this.sName = "USERS";
	this.iObjNo = 0;
	this.aGridNos = [];
	this.aForms = [0];
	this.iTabNo = 0;
	this.aEditRecord = [];
	
	if (typeof HO_USERS.bInited == "undefined"){

HO_USERS.prototype.actionA = function(sAction){
	return function(){
		var aAction = sAction.split("_");
		var that = V.aAA[parseInt(aAction[1])];
		that.action(sAction);
	}
};



HO_USERS.prototype.action = function(sAction){
	var aAction = [];
	aAction = sAction.split("_");
	switch (aAction[0]){
		case "add":
			this.add();
		break;
	
		case "addsave":
			this.addsave(parseInt(aAction[2]));
		break;
	
		case "delete":
			var sTitle = V.oDB.oData.oError.oDeleteConfirm;
			var sMessage = sTitle.sMessage;
			sTitle = sTitle.sTitle;
			E.popup({sTitle:sTitle, sMessage:sMessage, aButtons:["OK", "CANCEL"],
				that:this, sAction:"users"});
		break;
	
		case "deleterecordusers":
			switch (aAction[2]){
				case "cancel":
					E.action("popupclosex");
				break;
				
				case "ok":
					this.deleteuser();
				break;
				
			}
		break;
	
		case "gridedit":
			this.edit(parseInt(aAction[1]));
		break;
	
		case "addclose":
		case "editclose":
			this.editclose();
		break;
	
		case "editsave":
			this.editsave(parseInt(aAction[2]));
		break;
	
	}
};



HO_USERS.prototype.addsave = function(iFormNo){
	var aValues = [], bValid = 0;
	aValues = V.aAA[iFormNo].values();
	bValid = this.editsavetest(aValues);
	if (bValid){
		aValues.push({"sValue":""});
		aValues.push({"sValue":this.iObjNo});
		A.ajax("usersaddsave", aValues, this.editsaveA);
	}
};



HO_USERS.prototype.add = function(){
	var iTabNo = 0, sIDPre = "";
	switch (this.iTabNo){
		case 1:
			iTabNo = 3;
		break;
		
	}  
	this.tab(iTabNo);
};




HO_USERS.prototype.deleteuser = function(){
	var aValues = [{iUserID:parseInt(this.aEditRecord[0])}, 0, 0, 0, 0,
		{sValue:this.iObjNo}];
	A.ajax("usersdelete", aValues, this.editsaveA);
};



HO_USERS.prototype.editsavetest = function(aValues){
	var sErrors = "";
	var bValid = 1;
	aValues.forEach(function(oRec){
		if ((oRec.bRequired) && (!oRec.sValue)){
			sErrors += "Required " + oRec.sText + "<br>";
		}
		if ((oRec.bRequired) && (oRec.sType == "select-one") && 
			(oRec.sValue == -1)){
			sErrors += "Required " + oRec.sText + "<br>";
		}
		if ((oRec.sType == "email") && (!A.valid(oRec))){
			sErrors += "Invalid " + oRec.sText + "<br>"; 
		}
		if ((oRec.sType == "password") && (!A.valid(oRec))){
			sErrors += "Invalid " + oRec.sText + "<br>"; 
		}
	});
	if (sErrors){
		E.popup({sTitle:"Errors", sMessage:sErrors});
		bValid = 0;
	}
	return bValid;
};



HO_USERS.prototype.editsaveA = function(oData){
	var that = 0, iGridNo = 0;
	that = V.aAA[oData.aValues[5].sValue];
	iGridNo = that.aGridNos[0];
	V.aAA[iGridNo].aDataFiltered = [];
	V.aAA[iGridNo].aData = oData.aData.aData;
	V.aAA[iGridNo].populate();
	that.editclose(iGridNo);
	E.action("popupclosex");
	if (oData.sMessage){
		E.message(V.oDB.oData.oError[oData.sMessage].sMessage);
	}
	if (oData.sError){
		E.popup(V.oDB.oData.oError[oData.sError]);
	}
};



HO_USERS.prototype.editsave = function(iFormNo){
	var aValues = [], bValid = 0;
	aValues = V.aAA[iFormNo].values();
	bValid = this.editsavetest(aValues);
	if (bValid){
		aValues.push({"sValue":this.aEditRecord[0]});
		aValues.push({"sValue":this.iObjNo});
		A.ajax("userseditsave", aValues, this.editsaveA);
	}
};



HO_USERS.prototype.editclose = function(iFormNo){
	V.oGen.iPageTop = 2;
	this.tab(1)
	B.resize();
	aBreadcrumbs = ["Users"];
	E.breadcrumbs(aBreadcrumbs);
};



HO_USERS.prototype.edit = function(){
	var iTabNo = 0, sIDPre = "";
	switch (this.iTabNo){
		case 1:
			iTabNo = 2;
		break;
		
	}  
	B.action("elebutton.tabbutton-active-tabbutton-" + 
			this.iObjNo + "_" + iTabNo);
	E.action("tabbuttons_" + this.iObjNo + "_" + iTabNo + 
		"-" + (iTabNo + 1) + "-13");
	
	sBreadcrumb = ",,Edit,Add".split(",")[iTabNo];
	aBreadcrumbs = ["Users", sBreadcrumb];
	E.breadcrumbs(aBreadcrumbs);
	iObjNo = this.aForms[iTabNo];
	A.dg("tabbutton-" + this.iObjNo + "_4").className = 
		"elebutton tabbutton back";
	sIDPre = "reg-" + iObjNo + "_" + iTabNo + "_"; 
	A.value(sIDPre + "1", this.aEditRecord[1]);
	A.value(sIDPre + "2", this.aEditRecord[2]);
	A.value(sIDPre + "3", this.aEditRecord[3]);
	A.value(sIDPre + "4", this.aEditRecord[4]);
	switch (this.iTabNo){
		case 2:
			A.dg(sIDPre + "6").onclick = this.actionA
				("editsave_"+ this.iObjNo);
			A.dg(sIDPre + "7").onclick = this.actionA
				("delete_"+ this.iObjNo);
			A.dg(sIDPre + "8").onclick = this.actionA
				("editclose_"+ this.iObjNo);
		break;
	
		case 3:
			A.dg(sIDPre + "6").onclick = this.actionA
				("saveedit_"+ this.iObjNo);
			A.dg(sIDPre + "7").onclick = this.actionA
				("delete_"+ this.iObjNo);
			A.dg(sIDPre + "8").onclick = this.actionA
				("close_"+ this.iObjNo);
		break;
	
	}
};



HO_USERS.prototype.init = function(){
	var divParent = 0, iI =0, sClassName = "",
	divButton = 0, aTabs = [], iSize = 0, iObjNo = 0, oForm = {}, oGrid = {},
	oEle = {}, aValues = [], sID = "";
	this.iTabNo = 1;
	divParent = A.gcn("maintop two");
	divParent = E.div
		(divParent, "tabbuttonsbox", "tabbuttonsbox-" + this.iObjNo);
	divParent = E.div(divParent, "tabbuttonsboxin");
	
	oAll = V.oHouse;
	
	iSize = oAll.aTabsUsers.length;
	for (iI = 0; iI < iSize; iI++){
		var sClassName = "tabbutton";
		var divButton = E.button({divParent:divParent, 
		sClassName:sClassName + " act", 
		sID:"tabbutton-" + this.iObjNo + "_" + (iI + 1), 
		sText:oAll.aTabsUsers[iI]});
	}
	E.action("tabbuttons_" + this.iObjNo + "_1-2-3");	
	for (iI = 1; iI <= iSize; iI++){
		var oPage = {iIndex:iI, sFormName:"aUsers", that:this};
		this.aForms.push(E.page(oPage));
	}
	iTabNo = A.session("iTabNo_" + this.sName);
	iTabNo = 1;
	for (iI = 1; iI <= iSize; iI++){
		oForm = V.aAA[this.aForms[iI]];
		if (oForm.iGridNo){
			oGrid = V.aAA[oForm.iGridNo];
			aValues = [{sName:oGrid.oEle.sName, iGridNo:oForm.iGridNo}];
			if (!oGrid.oEle.bNoPopulate){
				A.ajax("grid", aValues, oGrid.populateA);
			}
			this.aGridNos[iI] = oForm.iGridNo;
		}
	}
	A.history("users");
	B.actions();
	B.resize();
	this.tab(iTabNo);
};



HO_USERS.prototype.tab = function(iTabNo){
	var aNoGrid = [], divX, aBreadcrumbs;
	aNoGrid = [2, 3, 4];
	aBreadcrumbs = ["Users"];
	if (iTabNo == 4){
		if ((this.iTabNo == 2) || (this.iTabNo == 3)){
			V.oGen.iPageTop = 2;
		}
		switch (this.iTabNo){
			case 2:
			case 3:
				this.iTabNo = 1;
			break;
			
		}
		E.action("tabbuttons_" +this.iObjNo + "_1");
	} else {
		E.action("tabbuttons_" +this.iObjNo + "_1");
		switch (iTabNo){
			case 1:
			break;
		
			case 2:
				aBreadcrumbs.push("Edit");
			break;
			
			case 3:
				aBreadcrumbs.push("Add");
			break;			
			
		}
		this.iTabNo = iTabNo;
	}
	sSession = "iTabNo_" + this.sName;
	A.session(sSession, this.iTabNo);
	if (aNoGrid.indexOf(this.iTabNo) != -1){
		V.oGen.iPageTop = 2;
	} else {
		V.oGen.iPageTop = 0;
	}
	if ((iTabNo == 2) || (iTabNo == 3)){
		E.action("tabbuttons_" +this.iObjNo + "_");
		V.oGen.iPageTop = 1;
	}
	B.resize();
	B.tab(["elebutton.tabbutton", "active", "tabbutton", 
		this.iObjNo +"_" + this.iTabNo]);
	divX = A.dg("tabbutton-" + this.iObjNo + "_1");
	divX.className = "elebutton tabbutton normal";	
	E.breadcrumbs(aBreadcrumbs);
	divX.onclick = this.actionA("add_" + this.iObjNo);
	E.action("tabbuttonswidth_" + this.iObjNo);
};



		HO_USERS.bInited = 1;
	}
};



//== WEEKHOURS CLASS ===========================================================
function HO_WEEKHOURS(){
	this.sName = "WEEKHOURS";
	this.iObjNo = 0;
	this.oEle = {};
	this.aValues = [];
	this.aDefaultValues = [1, 6, "08:00", "17:00"];
	
	if (typeof HO_WEEKHOURS.bInited == "undefined"){

HO_WEEKHOURS.prototype.actionA = function(sAction){
	return function(){
		var aAction = sAction.split("_");
		var that = V.aAA[parseInt(aAction[1])];
		that.action(sAction);
	}
};



HO_WEEKHOURS.prototype.action = function(sAction){
	var aAction = sAction.split("_");
	var that = V.aAA[parseInt(aAction[1])];
	var thatt = that.oEle.that;
	switch (aAction[0]){
		case "plus":
			E.weekhours(that.oEle);
		break;
		
		case "minus":
			if (thatt.aWeekHours.length > 1){
				var iObjNo = parseInt(aAction[2]);
				var iObjNo2 = thatt.aWeekHours[iObjNo]
				thatt.aWeekHours.splice(iObjNo, 1);
				V.aAA[iObjNo2] = 0;
				that.draw(A.dg(that.oEle.sID));
			}
		break;
		
	}
};



HO_WEEKHOURS.prototype.draw = function(divBox){
	var iI = 0, divX = 0, aDaysLong = [], aOptions = [], that = 0,
	iObjNo = 0, sIDPre = "", divTable = 0, divTR = 0, divTD = 0,
	iSize = 0, divIcon = 0, divButton = 0;
	aDaysLong = V.oDB.oWords.oDate.sDays.split("|");
	aOptions = [];
	divBox.innerHTML = "";
	aDaysLong.forEach(function(oRec){
		aOptions.push({iID:(iI + 1), sValue:oRec});
		iI++;
	});
	that = this;
	iI = 0;
	iSize = this.oEle.that.aWeekHours.length;
	this.oEle.that.aWeekHours.forEach(function(iObjNo){
		iObjNo = that.oEle.that.aWeekHours[iI];
		sIDPre = that.oEle.sID + "_" + iObjNo;
		
		E.memo({divParent:divBox, sText:"Days from", sClassName:"eleweekhourstext"})
		divX = E.div(divBox, "eleweekhours",sIDPre + "-dayfrom", "select");
		A.attribute(divX, "type", "select-one")
		A.value(sIDPre + "-dayfrom", aOptions);

		E.text({divParent:divBox, sText:"To"})
		divX = E.div(divBox, "eleweekhours",sIDPre + "-dayto", "select");
		A.attribute(divX, "type", "select-one")
		A.value(sIDPre + "-dayto", aOptions);
		E.text({divParent:divBox, sText:"<br>"})
		
		E.memo({divParent:divBox, sText:"Time from", sClassName:"eleweekhourstext"})
		divX = E.div(divBox, "eleweekhourstime",sIDPre + "-timefrom", "input");
		A.attribute(divX, "type", "time")

		E.text({divParent:divBox, sText:"To"})
		divX = E.div(divBox, "eleweekhourstime",sIDPre + "-dayto", "input");
		A.attribute(divX, "type", "time")
		E.text({divParent:divBox, sText:"<br>"})
		
		divButton = E.button({divParent:divBox, sText:"REMOVE",// sText:"&#xE06C;", 
			sID:sIDPre + "-minus", sClassName:"footerbutton"});
		divButton.onclick = that.actionA("minus_" + iObjNo + "_" + iI);

		if ((iI + 1) == iSize){
			divButton = E.button({divParent:divBox, sText:"ADD",// sText:"&#xE06C;", 
				sID:sIDPre + "-minus", sClassName:"footerbutton"});
			divButton.onclick = that.actionA("plus_" + iObjNo + "_" + iI);
		}
		E.spacer({divParent:divBox, sClassName:"popupspacer"})
		
		
		/*
		divTable = E.div(divBox, "eletable", sIDPre, "table");
		divTable.style.padding = "0";
		A.attribute(divTable, "type", "weekhours");
		divTR = E.div(divTable, "eletablerow", "", "tr");
		divTD = E.div(divTR, "eletablecol", "", "td");
		divTD.textContent = "Days From ";
		divTD = E.div(divTR, "eletablecol", "", "td");
		divX = E.div(divTD, "eleweekhoursdays",sIDPre + "-dayfrom", "select");
		A.attribute(divX, "type", "select-one")
		A.value(sIDPre + "-dayfrom", aOptions);
		divTD = E.div(divTR, "eletablecol", "", "td");
		divTD.textContent = " To ";
		divTD = E.div(divTR, "eletablecol", "", "td");
		divX = E.div(divTD, "eleweekhoursdays",sIDPre + "-dayto", "select");
		A.attribute(divX, "type", "select-one")
		A.value(sIDPre + "-dayto", aOptions);
		divX.parentNode.style.width = "11em";
		divTR = E.div(divTable, "eletablerow", "", "tr");
		divTD = E.div(divTR, "eletablecol", "", "td");
		divTD.textContent = "Time From ";
		divTD = E.div(divTR, "eletablecol", "", "td");
		divX = E.div(divTD, "eleweekhourstime",sIDPre + "-timefrom", "input");
		A.attribute(divX, "type", "time");
		divTD = E.div(divTR, "eletablecol", "", "td");
		divTD.textContent = " To ";
		divTD = E.div(divTR, "eletablecol", "", "td");
		divX = E.div(divTD, "eleweekhourstime",sIDPre + "-timeto", "input");
		A.attribute(divX, "type", "time");
		divTR = E.div(divTable, "eletablerow", "", "tr");
		divTD = E.div(divTR, "eletablecol", "", "td");
		divButton = E.button({divParent:divTD, sText:"ADD",// sText:"&#xE06C;", 
			sID:sIDPre + "-minus"});
		divIcon.onclick = that.actionA("minus_" + iObjNo + "_" + iI);
		if ((iI + 1) == iSize){
			divTD = E.div(divTR, "eletablecol", "", "td");
			divIcon = E.icon({divParent:divTD, sText:"&#xE06B;", 
				sClassName:"eleweekhoursicon", sID:sIDPre + "-plus"});
			divIcon.onclick = that.actionA("plus_" + iObjNo);
		}
		*/
		A.value(sIDPre, V.aAA[iObjNo].aValues);
		iI++;
	});
};



HO_WEEKHOURS.prototype.init = function(){
	this.oEle.that.aWeekHours.push(this.iObjNo);
	var divBox = A.dg(this.oEle.sID);
	if (!divBox){
		this.oEle.bBoxOnly = 1;
		this.oEle.sClassName = "weekhours";
		var divBox = E.input(this.oEle);
		this.aValues = this.aDefaultValues;
	}
	this.draw(divBox);
};



		HO_WEEKHOURS.bInited = 1;
	}
}


//finished loading
V.oGen.bJSLoaded = 1;
