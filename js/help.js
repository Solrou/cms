var V = {
	sSrvURL: "helpoutsrv/",
	aAA:[], oData:{},	oDiv:{},
	oGen:{
		iAAStart:1, iAAMax:0, divActive:0, bAnimOn:1,
		sAppName:"", oBrowser:{}, 
		sDefaultButtonID:"", iElementID:1000,
		bHouseLoaded:0, iIdleTime:0, 
		iLastKey:0, iLoaderDir:0, iLoaderWidth:0, iMenuLastIndex:0,
		iMainLoopInterval:0, oMouse:{},
		sMessageTimed:"", oSession:{}, bTouchDevice:0, bUserBoxOn:0, iZIndex:5,
	},
	oSize:{iMainWidth:0, iInnerHeight:0, iScrollbarWidth:0},
};



//APP CLASS ====================================================================
var A = {
		
actionA: function(that1, sAction, aParams){
	var that = {};
	if (typeof sAction != "string"){
		that.that = that1.aParams[0];
		that.sAction = that1.aParams[1];
		that.aParams = that1.aParams[2];
	} else {
		that.that = that1;
		that.sAction = sAction;
		that.aParams = aParams;
	}
	return that;
},



ajaxA: function(oAjax){
	if (!oAjax.iRetriesLeft){
		oAjax.iRetriesLeft = V.oGen.iAjaxRetries;
	}
	if (!oAjax.bDontShowWait){
		V.oGen.iLoaderWidth = 20;
		V.oGen.iLoaderDir = 20;
		if (V.oDiv.loader){
			V.oDiv.loader.style.display = "block";
			A.loader();
		}
	}
	var iTime = A.timeUnix();
	V.oGen.oSession.iTimeClient = iTime;
	var oURLObj = {action:oAjax.aURL[0]};
	var iSize = oAjax.aURL.length;
	iSize = (iSize-1) / 2;
	for (var iI = 0; iI < iSize; iI++){
		if (!iI){
			
		}
		var iIndex = iI * 2 + 1;
		oURLObj[oAjax.aURL[iIndex]] = oAjax.aURL[iIndex + 1];
	}
	if (oURLObj.button){
		var sID = oURLObj.button + "_g";
		A.dg(sID).style.display = "block";
	}
	oURLObj.urlhash = location.hash.slice(1);
	oURLObj.tab = V.oGen.oSession.iTab;
console.log(JSON.stringify(oURLObj));
	oURLObj.session = V.oGen.oSession.sID;
	oURLObj.time = iTime;
	oAjax.oXHR = new XMLHttpRequest();
	if ("withCredentials" in oAjax.oXHR){
		oAjax.oXHR.open("POST", V.sSrvURL, true);
	} else if (typeof XDomainRequest != "undefined"){
		oAjax.oXHR = new XDomainRequest();
		oAjax.oXHR.open("POST", V.sSrvURL);
	} else {
		return;
	}
	oAjax.oXHR.setRequestHeader("Content-type", 
		"application/x-www-form-urlencoded; charset=utf-8");
	oAjax.oXHR.onload = function(){
		if ((!oAjax.bDontShowWait) && (!oAjax.bDontStopWait)){
			if (V.oDiv.loader){
				V.oDiv.loader.style.display = "none";
			}
		}
		var oData = A.getJSON(oAjax.oXHR.responseText);
		V.oGen.oSession.iTimeServer = oData.iTimeServer;
		V.oGen.oSession.sURLHash = oData.sURLHash;
		V.oGen.oSession.iTab = oData.iTab;
console.log(V.oGen.oSession)
		V.aAA[oAjax.iAjaxNo].iStatus = 1;
		if ((oData) && ("iHalt" in oData)){
			if ("aErrors" in V.oData){
				V.oError = V.oData.oError[oData.sError];
			} else {
				E.memo(0, {divParent:V.oDiv.body, sValue:oData.sErrorAlert,
					aPos:[10, 10, 400, 100]});
			}
			var aGreyed = document.getElementsByClassName("ele-buttongreyed");
			var iSize = aGreyed.length;
			for (var iI = 0; iI < iSize; iI++){
				aGreyed[iI].style.display = "none";
			}
		} else {
			if (oAjax.fCallback){
				if (!oData){
					oData = {};
				}
				oAjax.fCallback(oData);
			}
		}
	};
	if (!oAjax.iTimeout){
		oAjax.iTimeout = V.oGen.iAjaxTimeout * 1000;
	}
	oAjax.oXHR.timeout = oAjax.iTimeout;
	oAjax.oXHR.ontimeout = function(){
		//handle timeout
	};
	var sURLObj = btoa(JSON.stringify(oURLObj));
	oAjax.oXHR.send("data=" + sURLObj);	
},



ajax: function(aObj){
	var iObjNo = A.nextObject();
	V.aAA[iObjNo] = aObj;
	V.aAA[iObjNo].sType =  "ajax";
	V.aAA[iObjNo].iStatus =  0;
	V.aAA[iObjNo].iObjNo =  iObjNo;
},



animateA: function(oObj){
	if (!oObj){
		return;
	}
	if (!oObj.divParent){
		if (oObj.iAnimFrame){
			window.cancelAnimationFrame(oObj.iAnimFrame);
		}
		V.aAA[oObj.iAnimNo].iStatus = 1;
		return;
	}
	if (V.oGen.bAnimOn){
		oObj.iAnimFrame = requestAnimationFrame(function(){
			A.animateA(oObj);
		});
		if (oObj.aFade){
			oObj.aFade[0] += oObj.aFade[2];
			oObj.divParent.style.opacity = (oObj.aFade[0]/oObj.aFade[3]);
			if (oObj.aFade[0] == oObj.aFade[1]){
				window.cancelAnimationFrame(oObj.iAnimFrame);
				V.aAA[oObj.iAnimNo].iStatus = 1;
				if (oObj.fFunction){
					oObj.fFunction(oObj.aParam);
				}
			}
		}
		if (oObj.aPos){
			oObj.aPos[2][0]++;
			var iFrac = oObj.aPos[2][0] / oObj.aPos[2][1];
			var iH = iFrac * oObj.aPos[1][3];
			oObj.divParent.style.height = iH + "em";	
			if (iFrac >= 1){
				window.cancelAnimationFrame(oObj.iAnimFrame);
				V.aAA[oObj.iAnimNo].iStatus = 1;
				if (oObj.fFunction){
					oObj.fFunction(oObj.aParam);
				}
			}
		}
	} else {
		//anim off
		if (oObj.aFade){
			V.aAA[oObj.iAnimNo].iStatus = 1;
			oObj.divParent.style.opacity = oObj.aFade[1] / oObj.aFade[3];
			if (oObj.fFunction){
				oObj.fFunction(oObj.aParam);
			}
		}		
		if (oObj.aPos){
			V.aAA[oObj.iAnimNo].iStatus = 1;
			oObj.divParent.style.height = oObj.aPos[1][3] + "em";
			if (oObj.fFunction){
				oObj.fFunction(oObj.aParam);
			}
		}
	}
},



animate: function(oObj){
	var iAnimNo = A.nextObject();
	V.aAA[iAnimNo] = {
		sType: "anim",
		divParent: oObj.divParent,
		aFade: oObj.aFade,
		aPos: oObj.aPos,
		fFunction: oObj.fCallback,
		aParams: oObj.aParams,
		iStatus: 2
	};
	if (oObj.aFade){
		if (oObj.aFade[0] > oObj.aFade[1]){
			if (V.oGen.bMobile){
				oObj.aFade[0] /= 2;
			}
			oObj.aFade[2] = -1;
			oObj.aFade[3] = oObj.aFade[0];
		} else {
			if (V.oGen.bMobile){
				oObj.aFade[1] /= 2;
			}
			oObj.aFade[2] = 1;
			oObj.aFade[3] = oObj.aFade[1];
		}
		V.aAA[iAnimNo].aFade = oObj.aFade;
	}
	if (oObj.aPos){
		oObj.aPos[2] = [0, 10];
	}
	V.aAA[iAnimNo].iAnimNo = iAnimNo;
	A.animateA(V.aAA[iAnimNo]);
},



browser: function(){
	var oBrowser = {};
	var sUA = navigator.userAgent, sTem, sMatch = sUA.match
		((/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || 
		([])); 
	if (/trident/i.test(sMatch[1])){
		sTem = ((/\brv[ :]+(\d+)/g.exec(sUA)) || ([])); 
		oBrowser = {sName:"IE", sVersion:((sTem[1]) || (""))};
	}   
	if (sMatch[1] === "Chrome"){
		sTem = sUA.match(/\bOPR\/(\d+)/);
		if (sTem !== null){
			oBrowser = {sName:'Opera', sVersion:sTem[1]};}
	}   
	sMatch = sMatch[2] ? [sMatch[1], sMatch[2]]: 
  	[navigator.appName, navigator.appVersion, "-?"];
	if ((sTem = sUA.match(/version\/(\d+)/i)) !== null){
		sMatch.splice(1, 1, sTem[1]);
	}
	oBrowser = {sName: sMatch[0], sVersion: sMatch[1]};
	if (navigator.appVersion.indexOf("Mac") != -1){
		V.oGen.oBrowser.sOSName = "MacOS"; 
	}
	if (navigator.appVersion.indexOf("Windows") != -1){
		V.oGen.oBrowser.sOSName = "Windows"; 
	}
	if (!V.oGen.oBrowser.sOSName){
		V.oGen.oBrowser.sOSName = "";
	}
	V.oGen.oBrowser.sName = oBrowser.sName;
	V.oGen.oBrowser.sVersion = oBrowser.sVersion;
},



dg: function(sID){
	return document.getElementById(sID);
},



escape: function(sValue){
	if (!sValue){
		return "";
	}
	sValue = "" + sValue;
	sValue = sValue.replace(/&lt;/gi, "<");
	sValue = sValue.replace(/&gt;/gi, ">");
	sValue = sValue.replace(/&quot;/gi, '"');
	sValue = sValue.replace(/&apos;/gi, "'");
	sValue = sValue.replace(/&amp;/gi, "&");
	sValue = sValue.replace(/&#37;/gi, "%");
	sValue = sValue.replace(/&#45;/gi, "-");
	sValue = sValue.replace(/&amp;/gi, "&");
	sValue = sValue.replace(/&euro;/gi, "\u20AC");
	sValue = sValue.replace(/&pound;/gi, "\u00A3");
	sValue = sValue.replace(/%20/gi, " ");
	return sValue;
},



findMainDivs:function(){
	V.oDiv.cloak = A.gcn("modalcloak");
	V.oDiv.cloaktop = A.gcn("modalcloaktop");
	V.oDiv.header = A.gcn("bodyheader");
	V.oDiv.footer = A.gcn("bodyfooter");
	V.oDiv.main = A.gcn("bodymain");
	if (!V.oDiv.main){
		V.oDiv.main = A.gcn("bodymainli");
	}
	V.oDiv.work = A.gcn("bodywork");
	V.oDiv.left = A.gcn("bodyleft");
	console.log(V.oDiv)
},



findObject:function(sName){
	var iI = V.oGen.iAAStart;
	var iFound = 0;
	while ((!iFound) && (iI <= V.oGen.iAAMax)){
		var oRec = V.aAA[iI]; 
		if ((oRec.sName) && (oRec.sName == sName)){
			iFound = iI;
		} else {
			iI++;
		}
	}
	if (iFound){
		return V.aAA[iFound];
	}
},



gcn:function(sClassName, bAll){
	var aReturn = document.getElementsByClassName(sClassName);
	if (bAll){
		return aReturn;
	} else {
		return aReturn[0];
	}
},



getHTMLChars: function(sValue){
	if (!sValue){
		return "";
	}
	sValue = "" + sValue;
	sValue = sValue.replace(/&lt;/gi, "<");
	sValue = sValue.replace(/&gt;/gi, ">");
	sValue = sValue.replace(/&quot;/gi, '"');
	sValue = sValue.replace(/&apos;/gi, "'");
	sValue = sValue.replace(/&amp;/gi, "&");
	sValue = sValue.replace(/&#37;/gi, "%");
	sValue = sValue.replace(/&#45;/gi, "-");
	sValue = sValue.replace(/&amp;/gi, "&");
	sValue = sValue.replace(/&euro;/gi, "\u20AC");
	sValue = sValue.replace(/&pound;/gi, "\u00A3");
	sValue = sValue.replace(/%20/gi, " ");
	return sValue;
},



getJSON: function(sData, bNotBase64){
	if (!sData){
		sData="{}";
	} else {
		if (!bNotBase64){
			sData = sData.split("\n")[0];
			sData = atob(sData);
		}
	}
	var oData = JSON.parse(sData);
	return oData;
},



initializeAnimation:function(){
	if (!window.requestAnimationFrame){
		window.requestAnimationFrame = (function(){
			return window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.oRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
		function(fCallback, oElement){
			window.setTimeout(fCallback, 1000/60);
		};})();}
	if (!window.cancelAnimationFrame){
		window.cancelAnimationFrame = (function(){
			return window.webkitcancelAnimationFrame ||
			window.mozcancelAnimationFrame ||
			window.ocancelAnimationFrame ||
			window.mscancelAnimationFrame ||
			function(fCallback, oElement){
				window.setTimeout(fCallback, 1000/60);
		};})();}
},



initializeBodyDivs:function(){
	V.oDiv.body = document.getElementsByTagName("body")[0];
	V.oDiv.head = document.getElementsByTagName("head")[0];
	V.oDiv.cloak = A.gcn("modalcloak");
	V.oDiv.cloaktop = A.gcn("modalcloaktop");
	V.oDiv.header = A.gcn("bodyheader");
	V.oDiv.footer = A.gcn("bodyfooter");
	V.oDiv.main = A.gcn("bodymain");
	if (!V.oDiv.main){
		V.oDiv.main = A.gcn("bodymainli");
	}
	V.oDiv.work = A.gcn("bodywork");
	V.oDiv.left = A.gcn("bodyleft");
},



initializeEventHandlers:function(){
	V.oDiv.body.onkeyup = A.onKeyPress;
	V.oDiv.body.onclick = A.onMouseClick;
	V.oDiv.body.onmousemove = A.onMouseMove;
	V.oDiv.body.onmouseup = A.onMouseUp;
	V.oDiv.body.onmousedown = A.onMouseDown;
	var sMouseWheelEvent=(/Firefox/i.test(navigator.userAgent)) ? 
		"DOMMouseScroll" : "mousewheel"; 
	if (document.attachEvent){
		document.attachEvent("on" + sMouseWheelEvent, A.mouseWheel);
	} else {
		if (document.addEventListener){
			document.addEventListener(sMouseWheelEvent, A.mouseWheel, false);
		}
	}
	V.oGen.bTouchDevice = (("ontouchstart" in window) || 
		(navigator.msMaxTouchPoints));
	if (V.oGen.bTouchDevice){
		V.oDiv.body.ontouchstart = A.onMouseDown;
		V.oDiv.body.ontouchend = A.onMouseUp;
		V.oDiv.body.ontouchmove = A.onMouseMove;
	}
	window.onhashchange = A.onHashChange;
	window.onbeforeunload = A.onBeforeUnload;
},



initializeHandshakeA:function(oData){
	V.oGen.oSession.sID = oData.sSessionID;
	sessionStorage.setItem(V.oGen.sAppName, JSON.stringify(V.oGen.oSession));
	A.ajax({aURL:["init_init"], fCallback:A.initializeA, bDontShowWait:1});
},



initializeSession:function(){
	V.oGen.sAppName = document.title.replace(/ /g, "");
	V.oGen.oSession = sessionStorage.getItem(V.oGen.sAppName);
	if (V.oGen.oSession){
		V.oGen.oSession = A.getJSON(V.oGen.oSession, 1);
	} else {
		V.oGen.oSession = {iTimeClient:0, iTimeServer:0, sURLHash:"", 
			iTab3:1, iTab5:1};
		sessionStorage.setItem(V.oGen.sAppName, JSON.stringify(V.oGen.oSession));
	}
},



initializeA:function(oData){
	if (oData.sSessionID){
		A.initializeHandshakeA(oData);
		return;
	}
	V.oData = oData;
	if (typeof window.orientation !== 'undefined'){
		V.oGen.bMobile = 1;
		V.oData.oInit.oSizes.iFontSize = V.oData.oInit.oSizes.iFontSizeMobile;
	}
	window.clearInterval(V.oGen.iMainLoopInterval);
	V.oGen.iMainLoopInterval = window.setInterval(A.loopMain,
		V.oData.oInit.oGeneral.iIntervalMain);
	var iObjNo = A.nextObject();
	if (!oData.oUser.iUserID){
		V.aAA[iObjNo] = new ENTER_HOUSE();
	} else {
		V.aAA[iObjNo] = new ENTER_INIT();
	}
	var sGet = A.url("a");
	if (sGet){
		window.setTimeout(function(){
			try {
				sGet = atob(sGet);
				var aGet = sGet.split("|");
				switch (aGet[0]){
					case "passwordreset":
						V.aAA[iObjNo].resetPassword(aGet[1]);
					break;
					case "registeractivate":
						V.aAA[iObjNo].registerActivate(aGet[1]);
					break;
					case "adduseractivate":
						V.aAA[iObjNo].adduserActivate(aGet[1]);
					break;
				}
			} catch(e){
				return;
			} finally {}
		}, 500);
	}
	var bFirstStart = window.location.href.split("#");
	if (bFirstStart[1]){
		bFirstStart = 0;
	} else {
		bFirstStart = 1;
	}
	V.aAA[iObjNo].initialize(bFirstStart);
	window.onresize = A.resizeBodyA;
},



initialize:function(){
	A.initializeBodyDivs();
	A.initializeEventHandlers();
	A.initializeAnimation();
	A.initializeSession();
	A.browser();
	V.oGen.iMainLoopInterval = window.setInterval(A.loopMain, 250);
	if ((!V.oGen.oSession.sID) || (V.oGen.oSession.sID == "undefined")){
		V.oGen.oSession.sID = "";
		A.ajax({aURL:["init_handshake"], 
			fCallback:A.initializeHandshakeA, bDontShowWait:1});
	} else {
		A.ajax({aURL:["init_init"], 
			fCallback:A.initializeA, bDontShowWait:1});
	}		
},



loader: function(){
	if ((V.oDiv.loader) && (V.oDiv.loader.style.display == "block")){
		requestAnimationFrame(A.loader);
		V.oGen.iLoaderWidth += V.oGen.iLoaderDir;
		var iAbs = Math.abs(V.oGen.iLoaderDir);
		if ((V.oGen.iLoaderWidth > (V.oSize.iMainWidth - 140 - 2 * iAbs)) || 
			(V.oGen.iLoaderWidth < iAbs)){
			V.oGen.iLoaderDir *= -1;
		}
		V.oDiv.loader.style.width = V.oGen.iLoaderWidth + "px";
	}
},



loopMain: function(){
	//ajax
	var iI = V.oGen.iAAStart;
	var iFound = 0;
	while ((!iFound) && (iI <= V.oGen.iAAMax)){
		var oRecord = V.aAA[iI];
		if ((oRecord) && (oRecord.sType)){
			if (oRecord.iStatus == 1){
				V.aAA[iI] = 0;
			} else {
				if (!oRecord.iStatus){
					iFound = iI;
					V.aAA[iI].iStatus = 2;
				}
			}
		}
		iI++;
	}
	if (iFound){
		V.aAA[iFound].iAjaxNo = iFound;
		A.ajaxA(V.aAA[iFound]);
	}
	//timed messaged
	if (V.sMessageTimed){
		V.oDiv.messagetimed.style.padding = "0 3em 0 3em";
		V.oDiv.messagetimed.innerHTML = V.sMessageTimed;
		V.sMessageTimed = "";
		window.setTimeout(function(){
			V.oDiv.messagetimed.style.padding = "0";
			V.oDiv.messagetimed.innerHTML = "";
		}, 1500);
	}
},



nextObject:function(){
	var iI = V.oGen.iAAStart;
	while ((V.aAA[iI] !== undefined) && (V.aAA[iI] !== 0)){
		iI++;
	}
	if (iI > V.oGen.iAAMax){
		V.oGen.iAAMax = iI;
	}
	V.aAA[iI] = {};
	return iI;
},



onBeforeUnload: function(oEvent){
	V.oGen.oSession.sLastURL = window.location.href;
	sessionStorage.setItem(V.oGen.sAppName, JSON.stringify(V.oGen.oSession));
},



onHashChange:function(oEvent){
	var oHouse = A.findObject("house");
	var oInit = A.findObject("init");
	var sLocation = location.hash.slice(1);
	if (oHouse){
		//loggedout
		switch (sLocation){
			case "home":
				V.oDiv.work.innerHTML = "";
				oHouse.homeShow();
			break;
			case "register":
				V.oDiv.work.innerHTML = "";
				oHouse.registerStart();
			break;
		}
	} else {
		//loggedin
		var aHashes = "|home|createpost|myposts|users|profile".split("|");
		var iIndex = aHashes.indexOf(sLocation);
		oInit.menuActive(iIndex);
	}
},



onKeyPress: function(oEvent){
	V.oGen.iLastKey = oEvent.keyCode || oEvent.which;
	V.oGen.divActive = document.activeElement;
	V.oGen.iIdleTime = 0;
	if ((V.oGen.sDefaultButtonID) && (V.oGen.iLastKey == 13)){
		var divButton = A.dg(V.oGen.sDefaultButtonID);
		divButton.click();
	}
},



onMouseClick:function(oEvent){
	var divActive = oEvent.target;
	var divParent = 0;
	if (divActive.parentNode){
		divParent = divActive.parentNode;
	}
	var sClassName = divActive.className;
	var iIndex = V.oGen.oMouse.iDivIndex;
	if ((iIndex != 1) && (!V.oGen.iMenuLastIndex)){
		V.oGen.iMenuLastIndex = iIndex;
		iIndex = 0;
	}
	if (typeof sClassName !== "string"){
		return;
	}
	var oClassName = {};
	if (sClassName){
		oClassName = {iMenuLastIndex:V.oGen.iMenuLastIndex, 
		iIndex:iIndex,
		aSpace: sClassName.split(" "),
			aUnder: sClassName.split("_"), sClassName:sClassName,
			divActive:divActive, divParent:divParent
		};
	}
	var divOld = 0;
	if ((V.oGen.oMouse.oClassName) && (V.oGen.oMouse.oClassName.divActive)){
		divOld = V.oGen.oMouse.oClassName.divActive;
	}
	oClassName.divOld = divOld; 
	V.oGen.oMouse.oClassName = oClassName;
},



onMouseDown: function(oEvent){
	var aTouchObj = oEvent.changedTouches;
	if (aTouchObj){
		oEvent = aTouchObj[0];
	}
	V.oGen.oMouse.divParent = oEvent.target;
	var aCursor = [];
	if (oEvent.pageX || oEvent.pageY){
		aCursor[0] = oEvent.pageX;
		aCursor[1] = oEvent.pageY;
	} else {
		aCursor[0] = oEvent.clientX +
			(document.documentElement.scrollLeft || document.body.scrollLeft) -
			document.documentElement.clientLeft;
		aCursor[1] = oEvent.clientY +
			(document.documentElement.scrollTop || document.body.scrollTop) -
			document.documentElement.clientTop;
	}
	V.oGen.oMouse.iPosDownX = aCursor[0];
	V.oGen.oMouse.iPosDownY = aCursor[1];
	var aType = V.oGen.oMouse.divParent.className;
	if ((aType) && (typeof aType == "string")){
		aType = aType.split(" ");
	} else {
		aType = [""];
	}
	if (aType[0] == "ele-popupover"){
		var aRect = V.oGen.oMouse.divParent.getBoundingClientRect();
		V.oGen.oMouse.iLeft = V.oGen.oMouse.iPosX - parseInt(aRect.left);
		V.oGen.oMouse.iTop = V.oGen.oMouse.iPosY - parseInt(aRect.top);
		V.oGen.oMouse.iClientWidth = V.oGen.oMouse.divParent.clientWidth;
		V.oGen.oMouse.iClientHeight = V.oGen.oMouse.divParent.clientHeight;
		V.oGen.oMouse.iBoundX = 0;
		V.oGen.oMouse.iBoundY = 0;
		A.zindex(V.oGen.oMouse.divParent.parentNode);
	}
},



onMouseMove: function(oEvent){
	oEvent = oEvent || window.event;
	var aCursor = [];
	if (oEvent.pageX || oEvent.pageY){
		aCursor[0] = oEvent.pageX;
		aCursor[1] = oEvent.pageY;
	} else {
		aCursor[0] = oEvent.clientX+
			(document.documentElement.scrollLeft || document.body.scrollLeft) -
			document.documentElement.clientLeft;
		aCursor[1] = oEvent.clientY +
			(document.documentElement.scrollTop || document.body.scrollTop) -
			document.documentElement.clientTop;
	}
	V.oGen.oMouse.iPosX = aCursor[0];
	V.oGen.oMouse.iPosY = aCursor[1];
	V.oGen.oMouse.divParentOver = oEvent.target;
	if (V.oGen.sDatePickerShowID){
		if ((V.oGen.oMouse.divParentOver) && 
			(V.oGen.oMouse.divParentOver.className)){
			if (V.oGen.oMouse.divParentOver.className.indexOf("datepicker") == -1){
				E.action("datepicker_show|" + V.oGen.sDatePickerShowID);
			}
		}
	}
	
	if (V.oGen.bUserBoxOn){
		if ((V.oGen.oMouse.divParentOver) && 
				(V.oGen.oMouse.divParentOver.className)){
				if (V.oGen.oMouse.divParentOver.className.indexOf("userbox") == -1){
					E.action("userbox_show");
				}
			}		
	}
	
	if (!V.oGen.oMouse.divParent){
		return;
	}
	var sClass = V.oGen.oMouse.divParent.className;
	if ((!sClass) || (typeof sClass != "string")){
		return;
	}
	var aClass = sClass.split(" ");
	V.oGen.oMouse.iMaxWidth = V.oSize.iMaxWidth - 
		V.oSize.iScrollbarWidth - 2;
	V.oGen.oMouse.iOffsetX = V.oGen.oMouse.iPosX + 
		V.oGen.oMouse.iClientWidth - V.oGen.oMouse.iLeft;
	V.oGen.oMouse.iMaxHeight = V.oSize.iMaxHeight - 2;
	V.oGen.oMouse.iOffsetY = V.oGen.oMouse.iPosY + 
	V.oGen.oMouse.iClientHeight - V.oGen.oMouse.iTop;
	var aID = V.oGen.oMouse.divParent.id.split("_");
	/*
	if (aClass[0] == "ele-popupover"){
		if ((V.oGen.oMouse.iPosX > V.oGen.oMouse.iLeft + V.oGen.oMouse.iBoundX) && 
			(V.oGen.oMouse.iOffsetX < V.oGen.oMouse.iMaxWidth)){
			V.oGen.oMouse.divParent.parentNode.style.left =
				(V.oGen.oMouse.iPosX - V.oGen.oMouse.iLeft) + "px";
		}
		if ((V.oGen.oMouse.iPosY > V.oGen.oMouse.iTop + V.oGen.oMouse.iBoundY) &&
			(V.oGen.oMouse.iOffsetY < V.oGen.oMouse.iMaxHeight)){
			V.oGen.oMouse.divParent.parentNode.style.top =
				(V.oGen.oMouse.iPosY - V.oGen.oMouse.iTop) + "px";
		}
	}
	*/
	aClass = sClass.split("_")
	if (aClass[0] == "ele-imageboxcrop"){
		var aObj = A.findObject("house");
		if (!aObj){
			aObj = A.findObject("profile");
		}
		V.aAA[aObj.iObjLogo].setCrop(aClass[1]);
	}
},



onMouseUp: function(){
	if (!V.oGen.oMouse.divParent){
		return;
	}
	V.oGen.oMouse.divParent = 0;
},



onMouseWheel: function(oEvent){
	oEvent = oEvent || window.event;
	var divTarget = oEvent.target;
	if (!divTarget.className){
		return;
	}
	var sClassName = divTarget.className;
	if ((typeof sClassName != "string") || (sClassName.substr(0, 4) != "grid")){
		return;
	}
	var iGridNo = 0;
	var divParent = 0;
	var iParent = 1;
	while ((!iGridNo) && (iParent)){
		divParent = divTarget.parentNode;
		if (divParent){
			divTarget = divParent;
			if (divParent.className == "gridbox"){
				iGridNo = parseInt(divParent.id.split("_")[1]);
			}
		}
	}
	if (iGridNo){
		var oGrid = V.aAA[iGridNo];
		if ((oEvent.wheelDelta < 0) || (oEvent.detail > 0)){
			oGrid.action("pagenext");
		} else {
			oGrid.action("pageprevious");
		}
	}
},



pushHistory:function(sHash){
	var sHashNow = window.location.href.split("#")[1];
	var sTitle = document.title.split(" - ")[0];
	if (sHashNow != sHash){
		window.history.pushState({"html":"", "pageTitle":""}, 
			"", ".#" + sHash);
	}
},



pushHistory:function(sHash){
	var sHashNow = window.location.href.split("#")[1];
	var sTitle = document.title.split(" - ")[0];
	if (sHashNow != sHash){
		window.history.pushState({"html":"", "pageTitle":""}, 
			"", ".#" + sHash);
	}
},



removeAccent:function(sInString){
	var oMap = {
		"à": "a", "á": "a", "â": "a", "ã": "a", "ä": "a", "å": "a",
		"ç": "c",
		"è": "e", "é": "e", "ê": "e", "ë": "e",
		"ì": "i", "í": "i", "î": "i", "ï": "i",
		"ñ": "n",
		"ò": "o", "ó": "o", "ô": "o", "õ": "o", "ö": "o", "ø": "o",
		"ß": "s",
		"ù": "u", "ú": "u", "û": "u", "ü": "u",
		"ÿ": "y"
  };
	var sReturn = "";
	var iLen = sInString.length;
	for (var iI = 0; iI < iLen; iI++) {
		sReturn += oMap[sInString.charAt(iI)] || sInString.charAt(iI);
	}
	return sReturn;
},



resetBody: function(){
	
	console.log("resetBody")
	
	V.oDiv.body.innerHTML = "";
	V.oDiv.headerout = E.div(V.oDiv.body, "bodyheaderout");
	V.oDiv.headerout.style.height = V.oData.oInit.oSizes.iHeaderHeight + "px";
	V.oDiv.main = E.div(V.oDiv.body, "bodymain");
	V.oDiv.header = E.div(V.oDiv.main, "bodyheader");
//	V.oDiv.header.style.top = -V.oData.oInit.oSizes.iHeaderHeight + "px";
	V.oDiv.header.style.height = V.oData.oInit.oSizes.iHeaderHeight + "px";
	V.oDiv.work = E.div(V.oDiv.main, "bodywork");
	V.oDiv.footer = E.div(V.oDiv.main, "bodyfooter");
	V.oDiv.footer.style.height = V.oData.oInit.oSizes.iFooterHeight + "px";
	V.oDiv.footer.style.bottom = -V.oData.oInit.oSizes.iFooterHeight + "px";
//	V.oDiv.footerout = E.div(V.oDiv.body, "bodyfooterout");
//	V.oDiv.footerout.style.height = V.oData.oInit.oSizes.iFooterHeight + "px";
	V.oDiv.left = E.div(V.oDiv.main, "bodyleft");
	if (V.oGen.bMobile){
		V.oData.oInit.oSizes.iLeftWidth = 70;
	}
	V.oDiv.left.style.width = V.oData.oInit.oSizes.iLeftWidth + "px";
	V.oDiv.breadcrumbs = E.div(V.oDiv.header, "ele-breadcrumbs");
	V.oDiv.breadcrumbs.style.left = (V.oData.oInit.oSizes.iLeftWidth + 32) + "px";
	V.oDiv.body.style.fontSize = V.oData.oInit.oSizes.iFontSize + "px";
	var divBar = E.div(V.oDiv.header, "loaderbarfull");
	V.oDiv.loader = E.div(divBar, "loaderbar");
	V.oDiv.footer.style.height = V.oData.oInit.oSizes.iFooterHeight + "px";
	V.oDiv.cloak = E.div(V.oDiv.body, "modalcloak");
	V.oDiv.cloaktop = E.div(V.oDiv.body, "modalcloaktop");
	if (V.oData.oUser.iUserID > 1){
		V.oDiv.messagetimed = E.div(V.oDiv.header, "messagetimed");
		V.oDiv.main.className = "bodymainli";
		V.oDiv.left
	}	
	A.animate({divParent:V.oDiv.body, aFade:[0, 10]});
	A.resizeBodyA();
},



resizeBodyA: function(iTimeout){
	if (!iTimeout){
		iTimeout = 100;
	}
	window.setTimeout(function(){
		A.resizeBody();
	}, iTimeout);
},



resizeBody: function(){
	var iW = window.innerWidth;
	if (iW < 1140){
		if (!V.oGen.bMobile){
			V.oGen.bMobile = 1;
		}
	} else {
		if (V.oGen.bMobile){
			V.oGen.bMobile = 0;
		}		
	}
	V.oSize.iMainWidth = iW - V.oData.oInit.oSizes.iLeftWidth - 100;
	var iH = window.innerHeight;
	iH -= V.oDiv.header.clientHeight;
	iH -= V.oDiv.footer.clientHeight;
	V.oDiv.main.style.height = (iH + 25) + "px";
	var iLeft = V.oData.oInit.oSizes.iLeftWidth;
	if ((V.oGen.oSession.sURLHash == "register") && (V.oGen.bMobile)){
//		iLeft = 60;
	}
	V.oDiv.work.style.left = iLeft + "px";
	
	var divWorkPageTabs = A.gcn("workpagetab", 1);
	var iSize = divWorkPageTabs.length;
	for (var iI = 0; iI < iSize; iI++){
		divWorkPageTabs[iI].style.height = (iH - 80) + "px";
	}

	
	if (V.oData.oUser.iUserID){
		V.oDiv.work.style.position = "absolute";
		V.oDiv.body.style.overflowY = "hidden";
		V.oDiv.work.style.overflowY = "hidden";
		V.oDiv.work.style.height = (iH - 10) + "px";
	} else {
		V.oDiv.work.style.position = "relative";
		V.oDiv.work.style.overflowY = "hidden";
		V.oDiv.left.style.height = "1358px";
		V.oDiv.work.style.height = "1358px";
	}
	
	
	if (V.oDiv.work1){
		V.oDiv.work.style.width = (iW - V.oData.oInit.oSizes.iLeftWidth) + "px"; 
		V.oDiv.work1.style.width = (iW - V.oData.oInit.oSizes.iLeftWidth -
			0) + "px"; 
	}
	
	
/*
	var iH = V.oDiv.main.clientHeight;
	iH -= V.oDiv.header.clientHeight;
	iH -= V.oDiv.footer.clientHeight;
	V.oDiv.work.style.height = iH + "px"; 

	var iW = (V.oDiv.body.clientWidth - V.oSize.iScrollbarWidth);
	V.oSize.iMainWidth = iW;
//	V.oDiv.main.style.width = iW + "px";
	V.oSize.iMaxWidth = iW;
	V.oSize.iMaxHeight = iH;
	if (V.oDiv.work1){
		V.oDiv.work.style.width = (iW - V.oData.oInit.oSizes.iLeftWidth) + "px"; 
		V.oDiv.work1.style.width = (iW - V.oData.oInit.oSizes.iLeftWidth -
			30) + "px"; 
		V.oDiv.work1.style.height = (iH - 70) + "px"; 
		V.oDiv.work2.style.width = (iW - V.oData.oInit.oSizes.iLeftWidth -
				30) + "px";  
		V.oDiv.work2.style.height = (iH - 70) + "px"; 
	}
	V.oDiv.work.style.height = (iH - V.oData.oInit.oSizes.iFooterHeight) + "px"; 
*/
	var divGrids = document.getElementsByClassName("gridbox");
	var iNumGrids = divGrids.length;
	for (var iI = 0; iI < iNumGrids; iI++){
		var aID = divGrids[iI].id.split("_");
		var iGridNo = parseInt(aID[1]);
		V.aAA[iGridNo].resize();
	}
	
},



rgbToHex:function(sRGB){
	var aRGB = sRGB.split(",");
	aRGB[0] = aRGB[0].split("(")[1];
	aRGB[1] = aRGB[1].replace(/ /g, "");
	aRGB[2] = aRGB[2].replace(/ /g, "").split(")")[0];
	var s24 = 1 << 24;
	var sR = parseInt(aRGB[0]) << 16;
	var sG = parseInt(aRGB[1]) << 8;
	var sB = parseInt(aRGB[2]);
	var sReturn = "#" + 
		((s24 + sR + sG + sB).toString(16).slice(1)).toUpperCase();
	return sReturn;
},


scrollbarWidth: function(){
	var divOuter = E.div(V.oDiv.body, "scrollbarwidth1");
  var iWidthNoScroll = divOuter.offsetWidth;
  divOuter.style.overflow = "scroll";
  var divInner = E.div(divOuter, "scrollbarwidth2");
  var iWidthWithScroll = divInner.offsetWidth;
  V.oSize.iScrollbarWidth = (iWidthNoScroll - iWidthWithScroll);
},



selectNone: function(divName){
	if (!divName){
		divName = A.dg("page");
	}
	if (typeof divName.onselectstart != "undefined"){
		divName.onselectstart=function(){return false;};
	}
	else if (typeof divName.style.MozUserSelect != "undefined"){
		divName.style.MozUserSelect = "none";
	}
	else {
		divName.onmousedown = function(){return false;};
	}
},



setType: function(divName, sType){
	if (V.oGen.oBrowser.sName == "MSIE"){
		var divTemp = divName.cloneNode(false);
		divTemp.type = sType;
		divName.parentNode.replaceChild(divTemp, divName);
	} else {
		divName.setAttribute("type", sType);
	}
},



sort: function(aArray, iCol, bNumerical, bReverse){
	function fSort(sFirst, sSecond){
		var iCol = V.oGen.iSortColumn;
		if (sFirst[iCol].toLowerCase() < sSecond[iCol].toLowerCase()){
			return -1;
		}
		if (sFirst[iCol].toLowerCase() > sSecond[iCol].toLowerCase()){
			return 1;
		}
		return 0;
	}
	function fSortNum(sFirst, sSecond){
		var iCol = V.oGen.iSortColumn;
		if (parseFloat(sFirst[iCol]) < parseFloat(sSecond[iCol])){
			return -1;
		}
		if (parseFloat(sFirst[iCol]) > parseFloat(sSecond[iCol])){
			return 1;
		}
		return 0;
	}
	V.oGen.iSortColumn = iCol;
	if (!bNumerical){
		aArray = aArray.sort(fSort);
	} else {
		aArray = aArray.sort(fSortNum);
	}
	if (bReverse){
		aArray = aArray.reverse();
	}
	return aArray;
},



timeUnix: function(){
	return parseInt(((new Date()).getTime())/1000);
},



url: function(sParam){
	var sURL = window.location.href;
	sParam = sParam.replace(/[\[\]]/g, "\\$&");
	var oRegex = new RegExp("[?&]" + sParam + "(=([^&#]*)|&|#|$)");
	var aResult = oRegex.exec(sURL);
	if (!aResult) {
		return null;
	}
	if (!aResult[2]) {
		return "";
	}
  return decodeURIComponent(aResult[2].replace(/\+/g, " "));
},



validDate: function(sDate){
	var iValid = 1;
	var aDate = sDate.split("-");
	aDate[0] = parseInt(aDate[0]);
	aDate[1] = parseInt(aDate[1]);
	aDate[2] = parseInt(aDate[2]);
	if ((!aDate[0]) || (!aDate[1]) || (!aDate[2])){
		iValid = 0;
	}
	var iTest = Date.parse(sDate);
	if (isNaN(iTest) === true){
		iValid = 0;
	} else {
		var sTest = new Date(iTest);
		var iYear = sTest.getFullYear();
		var iMonth = sTest.getMonth() + 1;
		if (iYear != aDate[0]){
			iValid = 0;
		}
		var iMonthDif = iMonth - aDate[1];
		if (Math.abs(iMonthDif)>2){
			iValid = 0;
		}
	}
	return iValid;
},



validEmailAddress: function(sEmailAddress){
	var sReg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if (!sReg.test(sEmailAddress) && (sEmailAddress)){
		return 0;
	} else {
		return 1;
	}
},



valueGet: function(sID, iIsNotID){
	var sReturn = "";
	var iI = 0;
	var iJ = 0;
	var iFound = 1;
	var divInput = A.dg(sID);
	if (!divInput){
		return "";
	}
	var sType = divInput.type;
	if (typeof sType == "undefined"){
		sType = "div";
	}
	var iCount = 0;
	var divOption = 0;
	switch (sType){
		case "upload":
			sReturn = divInput.uploaddone;
		break;
		
		case "address":
			var aFields = ("|x|x|region_id|region_id2|" +
				"address_line_1|address_line_2|main_address|x|x").split("|");
			var aID = sID.split("_");
			var that = A.findObject("house");
			if (aID[0] == "workpage"){
				that = A.findObject("profile");				
			}
			var iSize = that.aObjAddresses.length;
			var aValues = [];
			for (iI = 0; iI < iSize; iI++){
				var thatt = V.aAA[that.aObjAddresses[iI]];
				var aValue = {};
				var sIDPre = sID + "_" + iI + "_";
				iJ = 1;
				while (A.dg(sIDPre + iJ)){
					var sValue = A.valueGet(sIDPre + iJ);
					aValue[aFields[iJ]] = sValue;
					iJ++;
				}
				if (aValue.region_id2 != -1){
					aValue.region_id = aValue.region_id2;
				}
				delete aValue.x;
				delete aValue.region_id2;
				aValue.latitude = 0;
				aValue.longitude = 0;
				if (aValue.main_address){
					aValue.main_address = true;
				} else {
					aValue.main_address = false;
				}
				aValues.push(aValue)
			}
			sReturn = JSON.stringify(aValues);
		break;
		
		case "time":
			sReturn = A.dg(sID).value;
		break;
		
		case "div":
			sReturn = divInput.textContent;
		break;
		
		case "checkbox":
			if (divInput.checked){
				sReturn = 1;
			} else {
				sReturn = 0;
			}
		break;
		
		case "radio":
			if (divInput.checked){
				sReturn = 1;
			} else {
				sReturn = 0;
			}
		break;
		
		case "weekhours":
			var aValues = [];
			var iI = 0;
			while (A.dg(sID + "_dayfrom_" + iI)){
				var sDayFrom = A.valueGet(sID + "_dayfrom_" + iI, 1);
				var sDayTo = A.valueGet(sID + "_dayto_" + iI, 1);
				var sTimeFrom = A.valueGet(sID + "_timefrom_" + iI); 
				var sTimeTo = A.valueGet(sID + "_timeto_" + iI); 
				aValues.push({days_of_week:sDayFrom + "-" + sDayTo, 
					opening_time:sTimeFrom, closing_time:sTimeTo});
				iI++;
			}
			sReturn = JSON.stringify(aValues);
		break;
		
		case "text":
		case "email":
			if (!iIsNotID){
				sReturn = A.escape(divInput.value);
			} else {
				sReturn = divInput.value;
			}
		break;
		
		case "password":
			sReturn = A.escape(divInput.value);
		break;
		
		case "textarea":
			if (iIsNotID){
				sReturn = A.escape(divInput.value);
			} else {
				sReturn = divInput.value;
			}
		break;
		
		case "select-one":
			var iIndex = divInput.selectedIndex;
			if ((iIndex == -1) || (!divInput.options.length)){
				sReturn = "";
			} else {
				if (iIsNotID){
					sReturn = divInput.options[iIndex].text;
				} else {
					var iValue = divInput.options[iIndex].value;
					if (!iValue){
						iValue = 0;
					}
					sReturn = parseInt(iValue);
				}
			}
		break;
		
		case "select-multiple":
			sReturn = [];
			iCount = 0;
			while (divInput.options[iCount]){
				divOption = divInput.options[iCount];
				if (divOption.selected){
					sReturn.push(parseInt(divOption.value));
				}
				iCount++;
			}
			sReturn = JSON.stringify(sReturn);
		break;
		
		case "selectcheck":
			sReturn = [];
			iCount = 1;
			while (A.dg(sID + "_" + iCount)){
				divOption = A.dg(sID + "_" + iCount);
				var iCheck = 0;
				if (divOption.checked){
					iCheck = 1;
				}
				sReturn[iCount] = iCheck;
				iCount++;
			}
			sReturn = JSON.stringify(sReturn);
		break;
	}
	return sReturn;
},



valueSet: function(sID, sValue, iIsNotID){
	var iI =0;
	var iJ = 0;
	var iFound = 0;
	var divOption = 0;
	var divInput = A.dg(sID);
	if (!divInput){
		return;
	}
	var sType = divInput.type;
	if (divInput.className == "ele-output"){
		sType="output";
	}
	switch (sType){
		case "weekhours":
			var aValues = JSON.parse(sValue);
			var iNumValues = aValues.length;
			for (iI = 0; iI < iNumValues; iI++){
				var oRec = aValues[iI];
				var aDays = oRec.days_of_week.split("-");
				var divDayFrom = A.dg(sID + "_dayfrom_" + iI);
				if (!divDayFrom){
					E.action("weekhours_plus");
				}
				A.valueSet(sID + "_dayfrom_" + iI, aDays[0], 1);
				A.valueSet(sID + "_dayto_" + iI, aDays[1], 1);
				A.dg(sID + "_timefrom_" + iI).value = oRec.opening_time;
				A.dg(sID + "_timeto_" + iI).value = oRec.closing_time;
			}
		break;
		
		case "address":
			var aFields = ("|x|x|region_id|region_id2|" +
				"address_line_1|address_line_2|main_address|x|x").split("|");
			var aID = sID.split("_");
			var that = 0;
			var sParentName = "";
			var sIDPre = "";
			if (V.oData.oUser.iUserID){
				sParentName = "profile";
				sIDPre = "workpage_5-";
				that = A.findObject(sParentName);
			} else {
				sParentName = "house";
				sIDPre = "register-4_1_";
				that = A.findObject(sParentName);
			}
			var oRec = sValue;
				var sID = sIDPre + "4_1_" + aID[3] + "_";
				var iFound = 1;
				var iID = parseInt(oRec.parent_id);
				var aRegions = [parseInt(oRec.region_id), iID];
				while (iFound){
					var iParentID = V.aAA[that.aObjAddresses[iI]].findParent(iID);
					if (iParentID){
						aRegions.push(iParentID);
						iID = iParentID
					} else {
						iFound = 0;
					}
				var divProvince = A.dg(sID + "1");
				if (!divProvince){
					//error
					return;
				}
				iI = parseInt(aID[3]);
				if (aRegions.length == 3){
					A.valueSet(sID + "1", aRegions[2]);
					V.aAA[that.aObjAddresses[iI]].iAddressNo = iI;
					V.aAA[that.aObjAddresses[iI]].changeProvince();
					A.valueSet(sID + "2", aRegions[1]);
					V.aAA[that.aObjAddresses[iI]].changeRegion();
					A.valueSet(sID + "3", aRegions[0]);
					V.aAA[that.aObjAddresses[iI]].changeTown();
				}
				if (aRegions.length == 4){
					A.valueSet(sID + "1", aRegions[3]);
					V.aAA[that.aObjAddresses[iI]].iAddressNo = iI;
					V.aAA[that.aObjAddresses[iI]].changeProvince();
					A.valueSet(sID + "2", aRegions[2]);
					V.aAA[that.aObjAddresses[iI]].changeRegion();
					A.valueSet(sID + "3", aRegions[1]);
					V.aAA[that.aObjAddresses[iI]].changeTown();
					A.valueSet(sID + "4", aRegions[0]);					
				}
				A.valueSet(sID + "5", oRec.address_line1);
				A.valueSet(sID + "6", oRec.address_line2);
				A.valueSet(sID + "7", oRec.main);
			}
			/*
			var iSize = that.aObjAddresses.length;
			var aValues = [];
			for (iI = 0; iI < iSize; iI++){
				var thatt = V.aAA[that.aObjAddresses[iI]];
				aValue = {};
				var sIDPre = aID[0] + "_" + aID[1] + "_" + iI + "_";
				iJ = 1;
				while (A.dg(sIDPre + iJ)){
					var sValue = A.valueGet(sIDPre + iJ);
					aValue[aFields[iJ]] = sValue;
					iJ++;
				}
				if (aValue.region_id2 != -1){
					aValue.region_id = aValue.region_id2;
				}
				delete aValue.x;
				delete aValue.region_id2;
				aValue.latitude = 0;
				aValue.longitude = 0;
				aValues.push(aValue)
			}
			sReturn = JSON.stringify(aValues);
			*/
		break;
		
		case "time":
			if (!sValue){
				sValue = "00:00";
			}
			var aValue = sValue.split(":");
			A.valueSet(sID + "_h", aValue[0], 1);
			A.valueSet(sID + "_m", aValue[1], 1);
		break;
		case "text":
		case "email":
		case "password":
			divInput.value = A.getHTMLChars(sValue);
		break;
		
		case "output":
			divInput.textContent = A.getHTMLChars(sValue);
		break;
		
		case "textarea":
			divInput.value = (A.getHTMLChars(sValue)).replace(/\|/g, "\n");
		break;
		
		case "checkbox":
		case "radio":
			if (parseInt(sValue)){
				divInput.checked = "checked";
			} else {
				divInput.checked = "";
			}
		break;
		
		case "select-one":
			divInput = A.dg(sID);
			if (iIsNotID){
				iFound = 0;
				iI = 0;
				divOption = 0;
				while ((!iFound) && (divOption = divInput.options[iI])){
					var sOption = divOption.text;
					if (sOption == sValue){
						iFound = 1;
						divInput.options.selectedIndex = iI;
					} else {
						iI++;
					}
				}
			} else {
				divInput.value = parseInt(sValue);
			}
		break;
		
		case "select-multiple":
			var iSize = sValue.length;
			for (iI = 0; iI < iSize; iI++){
				iFound = 0;
				iJ = 0;
				divOption = 0;
				while ((!iFound) && (divOption = divInput.options[iJ])){
					var iOption = parseInt(divOption.value);
					if (iOption == sValue[iI]){
						iFound = 1;
						divOption.selected = "selected";
					} else {
						iJ++;
					}
				}				
			}
		break;
		
		case "selectcheck":
			iI = 1;
			while (A.dg(sID + "_" + iI)){
				divOption = A.dg(sID + "_" + iI);
				divOption.checked = sValue[iI];
				iI++;
			}
		break;
	}
},



zindex: function(divName){
	if (divName){
		V.oGen.iZIndex++;
		divName.style.zIndex = V.oGen.iZIndex;
	}
}




};

A.action = function(sAction, fCallback, aParams){
	this.oObj = this;
	this.oObj.aParams = aParams;
	this.oObj.sAction = sAction;
	switch (sAction){
		case "change":
			this.oObj.onchange = fCallback;
		break;
		case "click":
			this.oObj.onclick = fCallback;
		break;
		case "input":
			this.oObj.oninput = fCallback;
		break;
		case "focus":
			this.oObj.onfocus = fCallback;
		break;
		case "blur":
			this.oObj.onblur = fCallback;
		break;
		case "load":
			this.oObj.onload = fCallback;
		break;
		case "keypress":
			this.oObj.onkeyup = fCallback;
		break;
		case "mousedown":
			this.oObj.onmousedown = fCallback;
		break;
		case "mousemove":
			this.oObj.onmousemove = fCallback;
		break;
		case "mouseout":
			this.oObj.onmouseout = fCallback;
		break;
		case "mouseover":
			this.oObj.onmouseover = fCallback;
		break;
		case "mouseup":
			this.oObj.onmouseup = fCallback;
		break;
		case "timeupdate":
			this.oObj.ontimeupdate = fCallback;
		break;
	}
};



// END OF APP CLASS ============================================================


//== ELEMENTS CLASS ============================================================
var E = {

whiles: function(oInput){
	var iFound = 0;
	var iI = 0;
	var iSize = 0;
	var oRecord = {};
	if (!oInput.aParams[1]){
		iFound = 0;
		iI = 0;
		iSize = oInput.aArray.length;
		while ((!iFound) && (iI < iSize)){
			oRecord = oInput.aArray[iI];
			if (oRecord[oInput.aParams[0]] == oInput.aValues[0]){
				iFound = iI + 1;
			} else {
				iI++;
			}
		}
		return iFound -1;
	}
	var aAll = [];
	iI = 0;
	oInput.aArray.forEach(function(oRecord){
		if (oRecord[oInput.aParams[0]] == oInput.aValues[0]){
			aAll.push({oRecord:oRecord, iIndex:iI});
		}		
		iI++;
	});
	iFound = 0;
	iI = 0;
	iSize = aAll.length;
	while ((!iFound) && (iI < iSize)){
		oRecord = aAll[iI].oRecord;
		if (oRecord[oInput.aParams[1]] == oInput.aValues[1]){
			iFound = aAll[iI].iIndex + 1;
		} else {
			iI++;
		}
	}
	return iFound - 1;
},



action: function(sAction, aParams){
	var that = A.actionA(this, sAction, this.aParams);
	var aAction = that.sAction.split("_");
console.log(that.sAction);
	var divBox = 0;
	var sID = "";
	var sValue = "";
	switch (aAction[0]){
		case "time":
			var sID = that.sAction.substr(5);
			var sValue = A.valueGet(sID);
			if (sValue.length == 2){
				A.valueSet(sID, sValue + ":");
			}
		break;
		case "weekhours":
			var iObjNo = parseInt(aAction[3]);
			if (!iObjNo){
				return 0;
			}
			V.aAA[iObjNo].sValues = A.valueGet(V.aAA[iObjNo].oEle.sID);
			switch (aAction[1]){
				case "plus":
					sID = V.aAA[iObjNo].oEle.sID;
					V.aAA[iObjNo].oEle.that.iNumBusinessHours++;
					E.weekhoursDraw(iObjNo);
					A.valueSet(V.aAA[iObjNo].oEle.sID, V.aAA[iObjNo].sValues);
					E.weekhoursDefault
						(sID, (V.aAA[iObjNo].oEle.that.iNumBusinessHours - 1));
				break;
				case "minus":
					if (V.aAA[iObjNo].oEle.that.iNumBusinessHours > 1){
						var aValues = JSON.parse(V.aAA[iObjNo].sValues);
						aValues.splice(parseInt(aAction[2]), 1);
						V.aAA[iObjNo].sValues = JSON.stringify(aValues);
						V.aAA[iObjNo].oEle.that.iNumBusinessHours--;
						E.weekhoursDraw(iObjNo);
						A.valueSet(V.aAA[iObjNo].oEle.sID, V.aAA[iObjNo].sValues);
					}
				break;
			}
		break;
		case "editclose":
			V.oDiv.work1.style.display = "block";
			V.oDiv.work2.style.display = "none";
			V.oDiv.work2.innerHTML = "";
			var aBreadcrumbs = V.oGen.aBreadcrumbs.slice(0, -1);
			E.breadcrumbs(aBreadcrumbs);
		break;
		case "uploadstart":
			E.uploadStart(parseInt(aAction[1]));
		break;
		case "popupclose":
			E.popupClose();
		break;
		case "userdetails":
			A.findObject("init").action("user_showuser");
		break;
		case "dropdownboxclose":
			if (V.oDiv.dropdown){
				V.oDiv.dropdown.parentNode.removeChild(V.oDiv.dropdown);
				V.oDiv.dropdown = 0;
			}
		break;
		case "pagetab":
			E.pagetabs({sAction:"select", iObjNo:aAction[1]});
		break;
		
		case "pagetabclose":
			E.pagetabs({sAction:"close", iObjNo:aAction[1]});
		break;
		
		case "tab":
			var iI = 1;
			var divTab = 0;
			var divPage = 0;
			while (A.dg("tab_" + aAction[1] + "_" + iI)){
				divTab = A.dg("tab_" + aAction[1] + "_" + iI);
				divTab.className = "ele-buttontabbox";
				divPage = A.dg("workpage_" + aAction[1] + "_" + iI);
				divPage.style.display = "none";
				iI++;
			}
			var sIDPost = aAction[1] + "_" + aAction[2];
			divPage = A.dg("workpage_" + sIDPost);
			divTab = A.dg("tab_" + sIDPost);
			if ((divPage) && (divTab)){
				divTab.className = "ele-buttontabboxactive";
				divPage.style.display = "block";
			}
			aAction[1] = parseInt(aAction[1]);
			aAction[2] = parseInt(aAction[2]);
			switch (aAction[1]){
			case 3:
				A.findObject("myposts").iTabActive = aAction[2];
				var aBreadcrumbs = ["Dashboard", "My Posts"];
				switch (aAction[2]){
					case 1:
						aBreadcrumbs.push("Active");
					break;
					case 2:
						aBreadcrumbs.push("Drafts");
					break;
					case 3:
						aBreadcrumbs.push("History");
					break;
				}
				E.breadcrumbs(aBreadcrumbs);
			break;
			}
			V.oGen.oSession["iTab" + aAction[1]] = parseInt(aAction[2]);
			sessionStorage.setItem(V.oGen.sAppName, JSON.stringify(V.oGen.oSession));
		break;
		
		case "userbox":
			var iHeight = 0;
			if (V.oGen.bUserBoxOn){
				iHeight = 46;
				V.oGen.bUserBoxOn = 0;
			} else {
				iHeight = 160;
				V.oGen.bUserBoxOn = 1;
				A.zindex(V.oDiv.userbox);
			}
			V.oDiv.userbox.style.height = iHeight + "px";
		break;
		
		case "datepicker":
			aAction = aAction[1].split("|");
			sID = aAction[1];
			var aNowDate = (new Date().toISOString().slice(0, 10)).split("-");
			var iDay = 0;
			var iMonth = 0;
			var iYear = 0;
			var sDay = "";
			var sMonth = "";
			var sYear = "";
			var aDate = 0;
			var iID = 0;
			var sDateInput = A.valueGet(sID);
			if (!A.validDate(sDateInput)){
				sDateInput = aNowDate[0] + "-" + aNowDate[1] + "-" + aNowDate[2];
			}
			switch (aAction[0]){
				case "changeyear":
					sValue = A.valueGet(sID + "_year");
					if ((parseInt(sValue) == sValue) && (sValue.length == 4)){
						var aInputDate = A.valueGet(sID).split("-");
						if (!aInputDate[0]){
							aInputDate = aNowDate;
						}
						var sDateNew = sValue + "-" + aInputDate[1] + "-" + aInputDate[2];
						A.valueSet(sID, sDateNew);
						E.dateDraw(sID);
					}
				break;
				case "show":
					divBox = A.dg(sID + "_box");
					if (divBox){
						if (divBox.style.display == "block"){
							divBox.style.display = "none";
							V.oGen.sDatePickerShowID = "";
						} else {
							divBox.style.display = "block";
							E.dateDraw(sID);
							V.oGen.sDatePickerShowID = sID;
						}
					}
				break;
				case "set":
					E.dateDraw(sID);
				break;
				case "prevmonth":
					aDate = sDateInput.split("-");
					iDay = aDate[2];
					iMonth = parseInt(aDate[1])-1;
					iYear = parseInt(aDate[0]);
					if (iMonth === 0){
						iMonth = 12;
						iYear--;
					}
					if (iMonth < 10){
						iMonth="0" + iMonth;
					}
					A.valueSet(sID,iYear + "-" + iMonth + "-" + iDay);
					E.dateDraw(sID);
				break;
				case "nextmonth":
					aDate = sDateInput.split("-");
					iDay = aDate[2];
					iMonth = parseInt(aDate[1])+1;
					iYear = parseInt(aDate[0]);
					if (iMonth == 13){
						iMonth = 1;
						iYear++;
					}
					if (iMonth < 10){
						iMonth = "0" + iMonth;
					}
					A.valueSet(sID, iYear + "-" + iMonth + "-" + iDay);
					E.dateDraw(sID);
				break;
				case "prevyear":
					aDate = sDateInput.split("-");
					if (!aDate[0]){
						aDate = aNowDate;
					}
					sDay = aDate[2];
					sMonth = aDate[1];
					iYear = parseInt(aDate[0])-1;
					A.valueSet(sID, iYear + "-" + sMonth + "-" + sDay);
					E.dateDraw(sID);
				break;
				case "nextyear":
					aDate = sDateInput.split("-");
					if (!aDate[0]){
						aDate = aNowDate;
					}
					sDay = aDate[2];
					sMonth = aDate[1];
					iYear = parseInt(aDate[0])+1;
					A.valueSet(sID, iYear + "-" + sMonth + "-" + sDay);
					E.dateDraw(sID);
				break;
				case "clickday":
					A.valueSet(sID, aAction[2]);
					A.dg(sID).focus();
					E.dateDraw(sID);
					divBox = A.dg(sID + "_box");
					divBox.style.display = "none";
					V.oGen.sDatePickerShowID = "";
				break;
			}
		break;
		
		case "maxchars":
			sID = aAction[1];
			if (sID == "register-2"){
				sID = "register-2_1";
			}
			sValue = A.valueGet(sID);
			var divMaxChars = A.dg(sID + "_chars");
			var aMax = divMaxChars.textContent.split("/");
			var iMax = parseInt(aMax[1]);
			var iLength = sValue.length;
			if (iLength > iMax){
				iLength = iMax;
			}
			var sMax = iLength + "/" + aMax[1];
			divMaxChars.textContent = sMax;
			A.valueSet(sID, sValue.substr(0, iMax));
		break;
		
		case "passwordstrength":
			sID = aAction[1];
			if (aAction[2]){
				sID += "_" +aAction[2];
			}
			var sPassword = A.valueGet(sID);
			var iScore = 0;
			var aLetters = {};
			for (iI = 0; iI < sPassword.length; iI++){
				aLetters[sPassword[iI]] = (aLetters[sPassword[iI]] || 0) + 1;
				iScore += 5.0 / aLetters[sPassword[iI]];
			}
			var aVariations = {
				digits: /\d/.test(sPassword),
				lower: /[a-z]/.test(sPassword),
				upper: /[A-Z]/.test(sPassword),
				nonWords: /\W/.test(sPassword),
			};
			var iVariationCount = 0;
			for (var iCheck in aVariations){
				iVariationCount += (aVariations[iCheck] === true) ? 1 : 0;
			}
			iScore += (iVariationCount - 1) * 10;
			if (iScore > 100){
				iScore = 100;
			}
			if (iScore < 0){
				iScore = 0;
			}
			var divStrength = A.dg(sID + "_strength");
			divStrength.style.width =( 100 - iScore) + "%";
		break;
	}
},
		
		

address: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var aText = oEle.sText.split("|");
	var iIndex = oEle.iIndex;
	var divBox = E.div(oEle.divParent, "ele-addressbox",
		oEle.sID);
	divBox.type = "address";
	E.select({divParent:divBox, sText:aText[0], aPos:[0,1,40,0,0,1], 
		that:oEle.that,	sAction:oEle.sAction + "_province_" + oEle.iIndex, 
		bRequired:1, sID:oEle.sID + "_1"});
	E.select({divParent:divBox, sText:aText[1], aPos:[0,0,40], 
		that:oEle.that,	sAction:oEle.sAction + "_region_" + oEle.iIndex, 
		sID:oEle.sID + "_2"});
	E.select({divParent:divBox, sText:aText[2], aPos:[0,0,40], 
		that:oEle.that,	sAction:oEle.sAction + "_town_" + oEle.iIndex, 
		bRequired:1, sID:oEle.sID + "_3"});
	E.select({divParent:divBox, sText:aText[3], aPos:[0,0,40], 
		sID:oEle.sID + "_4"});
	E.input({divParent:divBox, sText:aText[4], aPos:[0,0,40], 
		sID:oEle.sID + "_5"});
	E.input({divParent:divBox, sText:aText[5], aPos:[0,0,40], 
		sID:oEle.sID + "_6"});
	E.radiobox({divParent:divBox, sText:aText[6], aPos:[0,0,40], 
		sName:oEle.that.sName, sID:oEle.sID + "_7"});
/*	E.button({divParent:divBox, sText:aText[7], aPos:[0,0,12,0,0,1], 
		that:oEle.that,	sAction:oEle.sAction + "_addressadd", 
		sID:oEle.sID + "_8"});*/
	E.button({divParent:divBox, sText:aText[8], aPos:[0,0,12,0, 0, 1], 
		that:oEle.that,	
		sAction:oEle.sAction + "_addressremove_" + oEle.iIndex, 
		sID:oEle.sID + "_8"});
	
	var aProvinces = [V.oData.oInit.oEnum.sSelect[0]];
	var iNumRegions = V.oData.aRegions.length;
	var iI = 0;
	for (iI = 0; iI <= iNumRegions; iI++){
		var oRec = V.oData.aRegions[iI];
		if ((oRec) && (oRec.iRegionType == 1)){
			aProvinces.push(oRec);
		}
	}	
	var divSelect = A.dg(oEle.sID + "_1");
	E.selectPopulate(divSelect, aProvinces);
	divSelect = A.dg(oEle.sID + "_2");
	E.selectPopulate(divSelect, [V.oData.oInit.oEnum.sSelect[0]]);
	divSelect = A.dg(oEle.sID + "_3");
	E.selectPopulate(divSelect, [V.oData.oInit.oEnum.sSelect[0]]);
	divSelect = A.dg(oEle.sID + "_4");
	E.selectPopulate(divSelect, [V.oData.oInit.oEnum.sSelect[0]]);
},



breadcrumbs: function(aBreadcrumbs){
	V.oDiv.breadcrumbs.innerHTML = "";
	if (aBreadcrumbs){
		V.oGen.aBreadcrumbs = aBreadcrumbs;
	}
	var iSize = V.oGen.aBreadcrumbs.length;
	var divSpan = 0;
	var divSpan1 = 0;
	for (var iI = 0; iI < iSize; iI++){
		divSpan = E.div({divParent:V.oDiv.breadcrumbs, 
			sClassName:"ele-breadcrumb", sID:"breadcrumb_" + iI, sType:"span"});
		divSpan.textContent = V.oGen.aBreadcrumbs[iI];
		if (iI != (iSize - 1)){
			divSpan1 = E.div({divParent:V.oDiv.breadcrumbs, 
				sClassName:"ele-breadcrumb1", sType:"span"});
			divSpan1.textContent = " > ";
			A.action.call(divSpan, "click", E.action, [E, "breadcrumb_" + iI]);
		} else {
			divSpan.className = "ele-breadcrumblast";
		}
	}
},



box: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divBox = E.div(oEle.divParent, "ele-box", oEle.sID);
	if (oEle.sClassName){
		divBox.className += " " + oEle.sClassName;
	} else {
		if (oEle.aPos[0] < 0){
			divBox.style.right = (-oEle.aPos[0]) + "px";	
		} else {
			divBox.style.left = oEle.aPos[0] + "px";
		}
		if (oEle.aPos[1] < 0){
			divBox.style.bottom = (-oEle.aPos[1]) + "px";	
		} else {
			divBox.style.top = oEle.aPos[1] + "px";
		}
		divBox.style.width = oEle.aPos[2] + "px";
		divBox.style.height = oEle.aPos[3] + "px";
	}
	if (oEle.bDisplay){
		divBox.style.display = "block";
	}
	return divBox;
},



button: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	oEle.sType = "button";
	var divBox = E.div(oEle.divParent, "ele-buttonbox", oEle.sID);
	A.selectNone(divBox);
	if (oEle.sClassName){
		divBox.className = "ele-buttonbox" + oEle.sClassName;
	}
	if (oEle.bDefault){
		V.oGen.sDefaultButtonID = oEle.sID; 
		divBox.id = oEle.sID;
	}
	
	if (!oEle.bNoPos){
		if (oEle.aPos[2]){
			divBox.style.width = oEle.aPos[2] + "em";	
		} else {
			var iFontSize = 16;
			if (V.oData.oInit){
				iFontSize = V.oData.oInit.oSizes.iFontSize;
			}
			var iWidth = oEle.sText.length;
			if (iWidth < 10){
				iWidth = 10;
			}
			divBox.style.width = (iWidth + 5) + "em";			
		}
		if (oEle.aPos[0]){
			divBox.style.position = "absolute";
			if (oEle.aPos[0] < 0){
				divBox.style.right = (-oEle.aPos[0]) + "em";	
			} else {
				divBox.style.left = oEle.aPos[0] + "em";
			}
			if (oEle.aPos[1] < 0){
				divBox.style.bottom = (-oEle.aPos[1]) + "em";	
			} else {
				divBox.style.top = oEle.aPos[1] + "em";
			}
		} else {
			if (oEle.aPos[4]){
				divBox.style.marginLeft = oEle.aPos[4] + "em";
			}
			if (oEle.aPos[5]){
				divBox.style.marginTop = oEle.aPos[5] + "em";
			}
		}
	/*	
		*/
	}
	var divDesc = E.div(divBox, "ele-buttondesc");
	if (oEle.sClassName){
		divDesc.className = "ele-buttondesc" + oEle.sClassName;
	}
	divDesc.textContent = oEle.sText;
	if (oEle.sAction){
		A.action.call
			(divBox, "click", oEle.that.action, [oEle.that, oEle.sAction]);
	}
	if (oEle.bGrey){
		var divGrey = E.div(divBox, "ele-buttongreyed", oEle.sID + "_g");
		divGrey.style.width = (oEle.aPos[2] + 0.2) + "em";
		divGrey.style.opacity = 0.7;
		if (oEle.bDisabled){
			divGrey.style.display = "block";
		}
	}
	return divBox;
},



build: function(oObj){
	var aForm = V.oData.oInit[oObj.sFormName];
	var divParent = oObj.divParent;
	aForm.forEach(function(oEle){
	    oEle.divParent = divParent;
	    oEle.bCreate = 1;
	    oEle.that = A.findObject(oObj.sObjectName);
	    var divEle = E.element(oEle);
	    if (oEle.bParent){
	    	divParent = divEle;
	    }
	});	
},



checkbox: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divBox = E.div(oEle.divParent, "ele-inputbox");
	divBox.style.height = "3em";
	divBox.style.left = oEle.aPos[0] + "em";
	divBox.style.top = oEle.aPos[1] + "em";
	if ((oEle.aPos[2] > 27) && (V.oGen.bMobile)){
		oEle.aPos[2] = 27;
	}
	divBox.style.width = oEle.aPos[2] + "em";
	if (oEle.aPos[4]){
		divBox.style.marginLeft = oEle.aPos[4] + "em";
	}
	if (oEle.aPos[5]){
		divBox.style.marginTop = oEle.aPos[5] + "em";
	}
	var divInputBox = E.div(divBox, "ele-checkbox");
	var divInput = E.div({divParent:divInputBox, 
		sClassName:"ele-checkboxinput", sID:oEle.sID, sType:"input"});
	A.setType(divInput, "checkbox");
	var divDesc = E.div(divBox, "ele-checkboxdesc");
	var divLabel = document.createElement("label");
	divDesc.appendChild(divLabel);
	divLabel.textContent = oEle.sText;
	divLabel.htmlFor = divInput.id;
	if (oEle.sAction){
		A.action.call
			(divBox, "click",oEle.that.click, [oEle.that, oEle.sAction]);
	}
	return divInput;
},



dateDraw: function(sID){
	var sDate = A.valueGet(sID);
	var oDate = {};
	if ((!sDate) || (!A.validDate(sDate))){
		oDate = new Date();
		sDate = oDate.toISOString().split("T")[0];
	}
	var aDate = sDate.split("-");
	var iYear = parseInt(aDate[0]);
	var iDayInput = parseInt(aDate[2]);
	var iMonthNow = parseInt(aDate[1]) - 1;
	oDate = new Date(iYear, iMonthNow, 1);
	var iDayOfWeek = oDate.getDay();
	oDate = new Date(iYear, iMonthNow, 1 - iDayOfWeek);
	var aDays = V.oData.oInit.oBanners.oDatePicker.sDays.split("|");
	var aMonths = V.oData.oInit.oBanners.oDatePicker.sMonths.split("|");
	var divBox = A.dg(sID + "_box");
	divBox.innerHTML = "";
	var divMonth = E.div(divBox, "ele-datepickermonth", sID + "_month");
	divMonth.innerHTML=aMonths[iMonthNow];
	E.icon({	
		aPos:[5, 5, 20, 20],
		sClassName:"datepickermove",
		sIcon:"E001",
		divParent:divBox,
		sAction:"datepicker_prevmonth|" + sID
	});
	E.icon({
		aPos:[120, 5, 20, 20],
		sClassName:"datepickermove",
		sIcon:"E002",
		divParent:divBox,
		sAction:"datepicker_nextmonth|" + sID
	});
	E.icon({
		aPos:[140, 5, 20, 20],
		sClassName:"datepickermove",
		sIcon:"E001",
		divParent:divBox,
		sAction:"datepicker_prevyear|" + sID
	});
	E.icon({
		aPos:[195, 5, 20, 20],
		sClassName:"datepickermove",
		sIcon:"E002",
		divParent:divBox,
		sAction:"datepicker_nextyear|" + sID
	});
	var divYear = E.div(divBox, "ele-datepickeryear");
	var divInput = E.div({divParent:divYear, 
		sClassName:"ele-inputdatepickeryear", 
		sID:sID + "_year", sType:"input"});
	divInput.value = iYear;
	A.action.call(divInput, "keypress", E.action,
		[E, "datepicker_changeyear|" + sID]);
	var iNumDays = aDays.length;
	var iNumWeeks = 7;
	var iTop = 25;
	var iDayNo = 0;
	for (var iCountWeek = 0; iCountWeek < iNumWeeks; iCountWeek++){
		var iLeft = 0;
		for (var iCountDay = 0; iCountDay < iNumDays; iCountDay++){
			var sClass = "ele-datepickercell";
			var sClassExtra = "";
			if ((!iCountDay) || (iCountDay == 6)){
				sClassExtra = "we";
			}
			if (!iCountWeek){
				sClassExtra = "top";
			}
			var divCell = E.div(divBox, sClass + sClassExtra);
			divCell.style.left = iLeft + "px";
			divCell.style.top = iTop + "px";
			if (!iCountWeek){
				divCell.innerHTML = aDays[iCountDay];
			} else {
				var iDate = oDate.getDate();
				var iMonth = oDate.getMonth();
				if (iMonth != iMonthNow){
					divCell.className = "ele-datepickercellmon";
				}
				if ((iMonth == iMonthNow) && (iDate == iDayInput)){
					divCell.className = "ele-datepickercellselected";
				}
				divCell.innerHTML = iDate;
				var sMonthNow = (iMonth + 1);
				if (sMonthNow<10){
					sMonthNow = "0" + sMonthNow; 
				}
				var sDayNow = oDate.getDate();
				if (sDayNow<10){
					sDayNow = "0" + sDayNow; 
				}
				sDate = iYear + "-" + sMonthNow + "-" + sDayNow;
				A.action.call(divCell, "click", E.action,
					[E, "datepicker_clickday|" + sID + "|" + sDate]);
				iDayNo++;
				oDate = new Date(iYear, iMonthNow, 1 - iDayOfWeek + iDayNo);
			}
			iLeft += 30;
		}
		iTop += 21;
	}
},



date: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var aPos = oEle.aPos;
	var sDate = oEle.sDate;
	if ((!sDate) || (!A.validDate(sDate))){
		var oDate = new Date();
		sDate = oDate.toISOString().split("T")[0];
	}
	if (oEle.bNoValue){
		sDate = "";
	}
	oEle.sValue = sDate;
	var divInput = E.input(oEle);
	divInput.parentNode.style.width = "110px";
	divInput.disabled = "disabled";
	divInput.style.background = "white";
	if (oEle.sAction){
		A.action.call(divInput, "blur", E.action, [E, "datepicker_blur"]);
	}
	var sID = oEle.sID;
	var divParent = divInput.parentNode.parentNode;
	var divIcon = E.div(divParent, "ele-datepickericon");
	divIcon.innerHTML = "&#xE06F;";
	A.action.call
		(divIcon, "click", E.action, [E, "datepicker_show|" + sID]);
	if (!aPos[6]){
		aPos[6] = 0;
	}
	if (!aPos[7]){
		aPos[7] = 0;
	}
	var divBox = E.div(oEle.divParent, "ele-datepicker", sID + "_box");
	divBox.style.left = (aPos[0] + 11.5 + aPos[6]) + "em";
	divBox.style.top = (aPos[1] + 3 + aPos[7]) + "em";
	A.selectNone(divBox);
	return divInput;
},



div: function(oEle, sClassName, sID){
	if (sClassName){
		if (!sID){
			sID = "";
		}
		oEle = {divParent:oEle, sClassName:sClassName, sID:sID};
	}	
	var sNewType = "div";
	if (oEle.sType){
		sNewType = oEle.sType;
	}
	var divAType = document.createElement(sNewType);
	if (oEle.sClassName){
		divAType.className = oEle.sClassName;
	}
	if (oEle.sID){
		divAType.setAttribute("id", oEle.sID);
	}
	if (oEle.divParent){
		oEle.divParent.appendChild(divAType);
	}
	return divAType;
},



dropdown: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	E.action("dropdownboxclose");
	var sClassName = "ele-dropdownbox";
	if (oEle.sClassName){
		sClassName += oEle.sClassName;
	}
	var divBox = E.div(oEle.divParent, sClassName);
	V.oDiv.dropdown = divBox;
	var divBox1 = E.div(divBox, "ele-dropdownboxclose");
	var divIcon = E.div(divBox1, sClassName + "closeicon");
	if (oEle.sClassName == "full"){
//		var iW = oEle.divParent.clientWidth - 2;
		var iW = window.innerWidth;
//		var iH = 1358;
		var iH = window.outerHeight;
		divBox.style.left = 0;
		divBox.style.top = 0;
		divBox.style.width = (iW - 25) + "px";
		oEle.aPos = [0, 0, 0, iH / V.oData.oInit.oSizes.iFontSize];
	} else {
		if (oEle.aPos[0] < 0){
			divBox.style.right = -oEle.aPos[0] + "em";
		} else {
			divBox.style.left = oEle.aPos[0] + "em";
		}
		divBox.style.top = oEle.aPos[1] + "em";
		divBox.style.width = oEle.aPos[2] + "em";
	}
	divIcon.innerHTML = "&#xE06E;";
	A.action.call(divBox1, "click", E.action, [E, "dropdownboxclose"]);
	var aPos = [oEle.aPos[0], oEle.aPos[1], oEle.aPos[2], 0];
	aPos[3] = 0;
		A.animate({divParent:divBox, 
			aPos:[aPos, oEle.aPos], fCallback:oEle.fCallback});
	A.zindex(divBox);
	return divBox;
},



element: function(oEle){
	if (oEle.sIDPre){
		oEle.sID = oEle.sIDPre + "_" + oEle.sText.replace(/ /g, "");
	} else {
		if (!oEle.sID){
			if (!oEle.iID){
				oEle.sID = "ele-" + E.nextElementID();
			} else {
				oEle.sID = "ele-" + oEle.iID;
			}
		}
	}
	if (!oEle.aPos){
		oEle.aPos = [0, 0, 0, 0, 0, 0];
	}
	if (!oEle.divParent){
		oEle.divParent = 0;
	}
	if (!oEle.sText){
		oEle.sText = "";
	}
	if (!oEle.sClassName){
		oEle.sClassName = "";
	}
	if (oEle.bCreate){
		return E[oEle.sType](oEle);
	} else {
		oEle.bCreate = 0;
		return oEle;
	}
},



focus: function(divElement){
	if (!divElement) {
		return;
	}
	if ((V.oGen.oBrowser.sName == "MSIE") && (V.oGen.oBrowser.sVersion < 9)){
		window.setTimeout(function(){
			divElement.focus(); 
		}, 100);
	} else {
		divElement.focus();
	}
},



grid: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var iGridNo = A.nextObject();
	oEle.iGridNo = iGridNo;
	V.aAA[iGridNo] = new ENTER_GRID();
	V.aAA[iGridNo].oEle = oEle;
	var divGrid = V.aAA[iGridNo].initialize(oEle);
	return divGrid;
},



header: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divBox = E.div(V.oDiv.header, "ele-header" + oEle.sClassName);
	if (!oEle.sClassName){
		divBox.style.left = oEle.aPos[0] + "em";
		divBox.style.top = oEle.aPos[1] + "em";
	}
	divBox.textContent = oEle.sText;
	return divBox;
},



href: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
//	var divBox = E.div(oEle.divParent, "ele-inputbox");
/*	divBox.style.left = oEle.aPos[0] + "em";
	divBox.style.top = oEle.aPos[1] + "em";
	divBox.style.width = oEle.aPos[2] + "em";
	if (oEle.aPos[3]){
		divBox.style.height = oEle.aPos[3] + "em";
	}
	*/
	var divBox = E.div(oEle.divParent, 
		"ele-hrefdesc" + oEle.sClassName, oEle.sID);
	divBox.textContent = oEle.sText;
	if (oEle.aPos[2]){
		divBox.style.width = oEle.aPos[2] + "em";
	}
	if (oEle.aPos[4]){
		divBox.style.marginLeft = oEle.aPos[4] + "em";
	}
	if (oEle.aPos[5]){
		divBox.style.marginTop = oEle.aPos[5] + "em";
	}
	if (oEle.sClassName){
		divBox.className += " " + oEle.sClassName;
	}
	if (oEle.sAction){
		A.action.call(divBox, "click", oEle.that.action, 
			[oEle.that, oEle.sAction]);
	}
	return divBox;
},



icon: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	/*
	var divBox = E.box({divParent:oEle.divParent, aPos:oEle.aPos,
		sID:oEle.sID});
	divBox.style.display = "block";
	*/
	var divIcon = E.div(oEle.divParent, "ele-icon ele-icon" + oEle.sClassName);
	divIcon.innerHTML = String.fromCharCode(parseInt(oEle.sIcon, 16));
	divIcon.style.left = oEle.aPos[0] + "px";
	divIcon.style.top = oEle.aPos[1] + "px";
	divIcon.style.width = oEle.aPos[2] + "px";
	divIcon.style.height = oEle.aPos[3] + "px";
	if (oEle.sAction){
		A.action.call
			(divIcon, "click", E.action, [E, oEle.sAction]);
	}
	return divIcon;
},



imagebox: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divBox = E.div(oEle.divParent, "ele-imagebox", oEle.sID);
	if (oEle.aPos[0] < 0){
		divBox.style.right = (-oEle.aPos[0]) + "px";	
	} else {
		divBox.style.left = oEle.aPos[0] + "px";
	}
	if (oEle.aPos[1] < 0){
		divBox.style.bottom = (-oEle.aPos[1]) + "px";	
	} else {
		divBox.style.top = oEle.aPos[1] + "px";
	}
	divBox.style.width = oEle.aPos[2] + "px";
	divBox.style.height = oEle.aPos[3] + "px";
	var divImg = E.div({divParent:divBox, sClassName:"cssimg", 
		sType:"img", sID:oEle.sID}
	);

	if (oEle.sClass){
		divBox.className += " " + oEle.sClass;
	}
	return divImg;
},



inputarea: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divBox = E.div(oEle.divParent, "ele-inputbox");
//	divBox.style.left = oEle.aPos[0] + "em";
//	divBox.style.top = oEle.aPos[1] + "em";
	if (!oEle.aPos[2]){
		oEle.aPos[2] = divBox.parentNode.clientWidth / 
			V.oData.oInit.oSizes.iFontSize - 3;		
	}
	if ((oEle.aPos[2] > 25) && (V.oGen.bMobile)){
		oEle.aPos[2] = 25;
	}
	divBox.style.width = (oEle.aPos[2] - 0.5) + "em";
	divBox.style.height = (oEle.aPos[3] + 3.2) + "em";
	if (oEle.aPos[4]){
		divBox.style.marginLeft = oEle.aPos[4] + "em";
	}
	if (oEle.aPos[5]){
		divBox.style.marginTop = oEle.aPos[5] + "em";
	}
	var divDesc = E.div(divBox, "ele-inputdesc");
	var sText = oEle.sText;
	if (oEle.bRequired){
		sText += "*";
	}
	divDesc.textContent = sText;
	var divBoxOut = E.div(divBox, "ele-inputareaout");
	divBoxOut.style.height = oEle.aPos[3] + "em";
	var divInput = E.div({divParent:divBoxOut, sClassName:"ele-inputarea", 
		sID:oEle.sID, sType:"textarea"});
	return divInput;
},



input: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divBox = E.div(oEle.divParent, "ele-inputbox");
//	divBox.style.left = oEle.aPos[0] + "em";
//	divBox.style.top = oEle.aPos[1] + "em";
	if (!oEle.aPos[2]){
		oEle.aPos[2] = divBox.parentNode.clientWidth / 
			V.oData.oInit.oSizes.iFontSize - 3;		
	}
	if ((oEle.aPos[2] > 25) && (V.oGen.bMobile)){
		oEle.aPos[2] = 25;
	}
	divBox.style.width = oEle.aPos[2] + "em";
	if (oEle.aPos[4]){
		divBox.style.marginLeft = oEle.aPos[4] + "em";
	}
	if (oEle.aPos[5]){
		divBox.style.marginTop = oEle.aPos[5] + "em";
	}
	var divDesc = E.div(divBox, "ele-inputdesc");
	var sText = oEle.sText;
	if (oEle.bRequired){
		sText += "*";
	}
	divDesc.textContent = sText;
	var divInputBox = E.div(divBox, "ele-input");
	var divInput = E.div({divParent:divInputBox, sClassName:"ele-inputinput", 
		sID:oEle.sID, sType:"input"});
	if (oEle.bFocus){
		E.focus(divInput);
	}
	if (oEle.sClassName){
		divInput.parentNode.className += " " + oEle.sClassName;
		divInput.className += " " + oEle.sClassName + "input";
	}
	if (oEle.sValue){
		A.valueSet(oEle.sID, oEle.sValue);
	}
	if (oEle.iMaxChars){
		divInput.maxLength = oEle.iMaxChars;
		var divCharsBox = E.div(divBox, "ele-inputcharsbox", oEle.sID + "_chars");
		divCharsBox.textContent = "0/" + oEle.iMaxChars;
		A.action.call(divInput, "keypress", E.action, [E, "maxchars_" + oEle.sID]);
		A.action.call(divInput, "blur", E.action, [E, "maxchars_" + oEle.sID]);
	}
	if (oEle.bEmail){
		divInput.type = "email";
		divInput.autocapitalize = "none";
	}
	return divInput;
},



line: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divLine = E.div(oEle.divParent, "ele-line", oEle.sID);
	divLine.style.left = oEle.aPos[0] + "px";
	divLine.style.top = oEle.aPos[1] + "px";
	var sWidth = "";
	if (oEle.aPos[2]){
		sWidth = oEle.aPos[2] + "px";
	} else {
		sWidth = "100%";
	}
	divLine.style.width = sWidth;
	if (oEle.aPos[4]){
		divLine.style.marginLeft = oEle.aPos[4] + "em";
	}
	if (oEle.aPos[5]){
		divLine.style.marginTop = oEle.aPos[5] + "em";
	}
	return divLine;
},



memo: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var sClassName = "";
	if (oEle.sClassName){
		sClassName = oEle.sClassName;
	}
	var divBox = E.div(oEle.divParent, "ele-memo" + sClassName, oEle.sID);
	if (!oEle.sClassName){
		divBox.style.width = oEle.aPos[2] + "em";
		divBox.style.height = oEle.aPos[3] + "em";
		if (oEle.aPos[0]){
			divBox.style.position = "absolute";
			divBox.style.left = oEle.aPos[0] + "em";
			divBox.style.top = oEle.aPos[1] + "em";
		} else {
			if (oEle.aPos[4]){
				divBox.style.marginLeft = oEle.aPos[4] + "em";
			}
			if (oEle.aPos[5]){
				divBox.style.marginTop = oEle.aPos[5] + "em";
			}
		}
	}
	if (oEle.bHTML){
		divBox.innerHTML = oEle.sText;
	} else {
		divBox.textContent = oEle.sText;
	}
	if (oEle.sRecord){
		divBox.innerHTML = V.oData.oInit[oEle.sRecord];
	}
	return divBox;
},



menuitem: function(oEle){
	var divBox = E.div(oEle.divParent, "menu_" + oEle.sType,
		"menu_" + oEle.sType + "_" + oEle.iIndex);
	var sAction = oEle.oObj.sName.replace(/ /g, "").toLowerCase();
	if (oEle.aPos[0]){
		if (oEle.aPos[0] < 0){
			divBox.style.right = (-oEle.aPos[0]) + "em";
		} else {
			divBox.style.left = (oEle.aPos[0]) + "em";
		} 
	}
	if (oEle.aPos[2]){
		divBox.style.width = oEle.aPos[2] + "em";
	}
	divBox.style.top = oEle.aPos[1] + "em";
	var divText = E.div(divBox, "menu_" + oEle.sType + "_text");
	divText.textContent = oEle.oObj.sName;
	divBox = E.div(divBox, "menu_" + oEle.sType + "_over");
	A.action.call(divBox, "click", oEle.that.action, 
		[oEle.that, "menu_" + sAction + "_" + oEle.iIndex]);
	return divBox;
},



messageboxClose: function(oObj){
	var divBox = V.oDiv.messagebox;
	if (!divBox.iFin){
		return;
	}
	if ((oObj) && (oObj.bKill)){
		var divParent = V.oDiv.messagebox.parentNode;
		if (divParent){
			divParent.removeChild(V.oDiv.messagebox);
		}
		if (divParent == V.oDiv.cloak){
			V.oDiv.cloak.parentNode.style.display = "none";
		}
		V.oDiv.messagebox = 0;
		V.oGen.oMouse.iDivIndex = 0;
		return;
	}
	A.animate({divParent:divBox, aFade:[10, 0],
		fCallback:function(){
			var divParent = V.oDiv.messagebox.parentNode;
			divParent.removeChild(V.oDiv.messagebox);
			if (divParent == V.oDiv.cloak){
				V.oDiv.cloak.parentNode.style.display = "none";
			}
			if ((oObj) && (oObj.fCallback)){
				oObj.fCallback();
			}
			V.oDiv.messagebox = 0;
			V.oGen.oMouse.iDivIndex = 0;
		}
	});
},



messagebox: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divParent = V.oDiv.work;
	if (oEle.bModal){
		V.oDiv.cloak.parentNode.style.display = "block";
		A.zindex(V.oDiv.cloak.parentNode);
		A.zindex(V.oDiv.cloak);
		divParent = V.oDiv.cloak;
	}
	var divBox = E.div(divParent, "ele-messagebox");
	if (oEle.aPos[0] < 0){
		divBox.style.right = (-oEle.aPos[0]) + "em";
	} else {
		divBox.style.left = oEle.aPos[0] + "em";
	}
	divBox.style.top = oEle.aPos[1] + "em";
	divBox.style.width = oEle.aPos[2] + "em";
	divBox.style.height = oEle.aPos[3] + "em";
	if (oEle.bArrow){
		E.div(divBox, "ele-messageboxarrow");
		E.div(divBox, "ele-messageboxarrowcover");
	}
	if (oEle.sText){
		var divTitle = E.div(divBox, "ele-messageboxtitlebar");
		var divText = E.div(divTitle, "ele-messageboxtitlebardesc");
		divText.textContent = oEle.sText;
		if (oEle.bModal){
			var divBox1 = E.div(divBox, "ele-messageboxclose");
			var divIcon = E.div(divBox1, "ele-messageboxcloseicon");
			divIcon.innerHTML = "&#xE000;";
			A.action.call(divBox1, "click", E.messageboxClose, [E]);
		}
	}
	V.oDiv.messagebox = divBox;
	A.animate({divParent:divBox, aFade:[0, 10],
		fCallback:function(){
			V.oDiv.messagebox = divBox;
			V.oDiv.messagebox.iFin = 1;
		}
	});
	return divBox;
},



nextElementID: function(){
	var iID = V.oGen.iElementID;
	V.oGen.iElementID++;
	return iID;
},



output: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divBox = E.div(oEle.divParent, "ele-inputbox");
	divBox.style.left = oEle.aPos[0] + "em";
	divBox.style.top = oEle.aPos[1] + "em";
	if (!oEle.aPos[2]){
		oEle.aPos[2] = divBox.parentNode.clientWidth / 
			V.oData.oInit.oSizes.iFontSize - 3;		
	}
	divBox.style.width = oEle.aPos[2] + "em";
	var divDesc = E.div(divBox, "ele-inputdesc");
	divDesc.textContent = oEle.sText;
	var divInputBox = E.div(divBox, "ele-input");
	var divOutput = E.div(divInputBox, "ele-output", oEle.sID);
	return divOutput;
},



password: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	oEle.sType = "password";
	oEle.bCreate = 1;
	var divInput = E.input(oEle);
	A.setType(divInput, "password");
	if (oEle.bShowStrength){
		divInput.parentNode.style.borderTopRightRadius = "0";
		var divStrength = E.div(divInput.parentNode, "ele-passwordstrengthbox");
		E.div(divStrength, "ele-passwordstrength");
		var divStrength1 = 
			E.div(divStrength, "ele-passwordstrengthmeter", oEle.sID + "_strength");
		divStrength1.style.width = "100%";
//		divStrength.style.left = (oEle.aPos[0] + oEle.aPos[2] - 100) + "px";
//		divStrength.style.top = (oEle.aPos[1] + 7) + "px";
		A.action.call(divInput, "keypress",
			E.action, [E, "passwordstrength_" + oEle.sID]);
	}
	return divInput;
},



popupClose: function(){
	V.oGen.sDefaultButtonID = V.oGen.sDefaultButtonID2;
	V.oGen.sDefaultButtonID2 = "";
	if (V.oDiv.cloaktop){
		V.oDiv.cloaktop.parentNode.removeChild(V.oDiv.cloaktop);
		V.oDiv.cloak.innerHTML = "";
		V.oDiv.cloak.style.display = "none";
		V.oDiv.cloaktop = E.div(V.oDiv.cloak, "modalcloaktop");
	}
	V.oDiv.popup = 0;
},



popup: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	if (!V.oDiv.cloak){
		V.oDiv.body.textContent = oEle.sMessage;
		return;
	}
	V.oDiv.cloak.style.display = "block";
	V.oDiv.cloaktop.style.display = "block";
	A.zindex(V.oDiv.cloak);
	A.zindex(V.oDiv.cloaktop);
	var divWindow = E.div(V.oDiv.cloaktop, "ele-popup");
	var iButtonTop = 17.65;
	var iButtonLeft = 20.8;
	var iScrollTop = window.pageYOffset || document.documentElement.scrollTop;
	if (!oEle.aPos[0]){
		oEle.aPos = [parseInt((V.oDiv.body.clientWidth - 500) / 2), 
			(70 + iScrollTop), 400, 300];
		if (V.oGen.bMobile){
			oEle.aPos = [10, 10, 340, 300];
		}
		divWindow.style.left = oEle.aPos[0] + "px";
		divWindow.style.top = oEle.aPos[1] + "px";
		divWindow.style.width = oEle.aPos[2] + "px";
		divWindow.style.height = oEle.aPos[3] + "px";
	} else {
		divWindow.style.left = 
			parseInt((V.oDiv.body.clientWidth - oEle.aPos[2]) / 2) + "px";
		divWindow.style.top = (70 + iScrollTop) + "px";
		divWindow.style.width = oEle.aPos[2] + "px";
		divWindow.style.height = oEle.aPos[3] + "px";
		iButtonTop = (oEle.aPos[3]) - 50;
		iButtonLeft = oEle.aPos[0] - 5;
	}
	V.oDiv.popup = divWindow;
	var divBar = E.div(divWindow, "ele-popuptitlebar" + oEle.sClassName);
	var divDesc = E.div(divBar, "ele-popuptitledesc" + oEle.sClassName);
	divDesc.textContent = oEle.sTitle;	

	var divMessage = E.div(divWindow, "ele-popupmessage");
	divMessage.innerHTML = oEle.sMessage;
	var divOver = E.div(divWindow,"ele-popupover");
	var divBox1 = E.div(divOver, "ele-popupclosebox");
	var divIcon = E.div(divBox1, "ele-popupcloseicon");
	divIcon.innerHTML = "&#xE06E;";
	A.action.call(divBox1, "click", E.action, [E, "popupclose"]);
	if (!oEle.aButtons){
		V.oGen.sDefaultButtonID2 = V.oGen.sDefaultButtonID;
		var sAction = "popupclose";
		var that = E;
		if (oEle.sAction){
			sAction = oEle.sAction;
			that = oEle.that;
		}
		E.button({
			that:that,
			sText:V.oData.oInit.oBanners.oGeneral.sOK,
			sAction:sAction,
			aPos:[-1, -1, 12, 0, -0.7, -0.8],
			divParent:divWindow, bDefault:1, sID:"errormessageok"
		});
	} else {
		if (oEle.aButtons != -1){
			var iSize = oEle.aButtons.length;
			var iRight = 1;
			for (var iI = 0; iI < iSize; iI++){
				E.button({divParent:divWindow, aPos:[-iRight, -1, 12],
					that:oEle.that, sAction:oEle.sAction + "_" + iI,
					sText:oEle.aButtons[iI]});
				iRight += 13;
			}
		}
	}
	if (oEle.bPageOver){
		V.oDiv.popuppageover = E.div(divOver, "elepopuppageover");
		A.zindex(V.oDiv.popuppageover);
	}
	return divOver;
},



radiobox: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divBox = E.div(oEle.divParent, "ele-inputbox");
	divBox.style.height = "3em";
	divBox.style.left = oEle.aPos[0] + "em";
	divBox.style.top = oEle.aPos[1] + "em";
	if ((oEle.aPos[2] > 27) && (V.oGen.bMobile)){
		oEle.aPos[2] = 27;
	}
	divBox.style.width = oEle.aPos[2] + "em";
	if (oEle.aPos[4]){
		divBox.style.marginLeft = oEle.aPos[4] + "em";
	}
	if (oEle.aPos[5]){
		divBox.style.marginTop = oEle.aPos[5] + "em";
	}
	var divInputBox = E.div(divBox, "ele-checkbox");
	var divInput = E.div({divParent:divInputBox, 
		sClassName:"ele-radioboxinput", sID:oEle.sID, sType:"input"});
	A.setType(divInput, "radio");
	var divDesc = E.div(divBox, "ele-checkboxdesc");
	var divLabel = document.createElement("label");
	divDesc.appendChild(divLabel);
	divLabel.textContent = oEle.sText;
	divLabel.htmlFor = divInput.id;
	if (oEle.sAction){
		A.action.call
			(divBox, "click",oEle.that.click, [oEle.that, oEle.sAction]);
	}
	if (oEle.sName){
		divInput.name = oEle.sName;
	}
	return divInput;
},



selectPopulate: function(divSelect, aOptions){
	if (typeof aOptions == "string"){
		aOptions = aOptions.split("|");
	}
	if (!divSelect){
		return;
	}
	divSelect.innerHTML = "";
	var iNumOptions = aOptions.length;
	for (var iI = 0; iI < iNumOptions; iI++){
		var oRec = aOptions[iI];
		var divOption = E.div({divParent:divSelect, sType:"option"});
		divOption.type = "option";
		var iID = oRec.iID;
		if (typeof iID == "undefined"){
			iID = (iI + 1);
		}
		divOption.value = iID;
		var sValue = oRec.sValue;
		if (!sValue){
			sValue = oRec;
		}
		divOption.text = sValue;
	}
},



select: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divBox = E.div(oEle.divParent, "ele-inputbox");
//	divBox.style.left = oEle.aPos[0] + "em";
//	divBox.style.top = oEle.aPos[1] + "em";
	if (!oEle.aPos[2]){
		oEle.aPos[2] = divBox.parentNode.clientWidth / 
			V.oData.oInit.oSizes.iFontSize - 3;		
	}
	if ((oEle.aPos[2] > 25) && (V.oGen.bMobile)){
		oEle.aPos[2] = 25;
	}
	divBox.style.width = oEle.aPos[2] + "em";
	if (oEle.aPos[4]){
		divBox.style.marginLeft = oEle.aPos[4] + "em";
	}
	if (oEle.aPos[5]){
		divBox.style.marginTop = oEle.aPos[5] + "em";
	}
	var divDesc = E.div(divBox, "ele-inputdesc");
	var sText = oEle.sText;
	if (oEle.bRequired){
		sText += "*";
	}
	divDesc.textContent = sText;
	var divInputBox = E.div(divBox, "ele-select");
	var divInput = E.div({divParent:divInputBox, sClassName:"ele-inputinput", 
		sID:oEle.sID, sType:"select"});
	if (oEle.bFocus){
		E.focus(divInput);
	}
	if (oEle.sClassName){
		divInput.parentNode.className += " " + oEle.sClassName;
		divInput.className += " " + oEle.sClassName + "input";
	}
	if (oEle.sAction){
		A.action.call
			(divInput, "change",oEle.that.action, [oEle.that, oEle.sAction]);
	}
	if (oEle.sEnum){
		var aOptions = V.oData.oInit.oEnum[oEle.sEnum];
		if (aOptions[0].iID != -1){
			aOptions.unshift(V.oData.oInit.oEnum.sSelect[0]);
		}
		E.selectPopulate(divInput, aOptions);
	}
	return divInput;
},



step: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divIcon = 0;
	var divBox = E.div(oEle.divParent, "ele-step" + oEle.sClassName, oEle.sID);
	divBox.style.left = oEle.aPos[0] + "em";
	divBox.style.top = oEle.aPos[1] + "em";
	var aText = oEle.sText.split("|");
	var divDesc = E.div(divBox, "ele-stepdesc");
	divDesc.innerHTML = aText[1];
	divIcon = E.div(divBox, "ele-stepicon");
	divIcon.innerHTML = "&#xE00B;";
	divDesc = E.div(divBox, "ele-stepicondesc");
	divDesc.innerHTML = aText[0];
	if (oEle.sAction){
		A.action.call(divBox, "click", 
			oEle.that.action, [oEle.that, oEle.sAction]);
	}
	return divBox;
},



tabs: function(oEle){
	var iI = 0;
	var divParent = A.dg(oEle.sParentID);
	var aID = divParent.id.split("_");
	var iNumTabs = oEle.aNames.length;
	var iLeft = 2;
	for (iI = 0; iI < iNumTabs; iI++){
		var divPage = E.div(divParent, "workpagetab", 
			divParent.id + "_" + (iI + 1));
		if (oEle.bScrollbar){
			divPage.parentNode.style.overflowY = "scroll";
		}
		if (oEle.sParentID == "workpage_5"){
			divPage.style.left = "23px";
			divPage.style.top = "80px";
		}
		var sIDPost = aID[1] + "_" + (iI+ 1);
		if (oEle.aNames.length > 1){
			var divButton = E.button({divParent:divParent, sText:oEle.aNames[iI],
				sID:"tab_" + sIDPost});
			divButton.style.position = "absolute";
			divButton.style.left = iLeft + "em";
			divButton.style.top = "2.1em";
			A.action.call(divButton, "click", E.action, [E, "tab_" + sIDPost]);
			var iWidth = parseInt(oEle.aNames[iI].length / 1.2) + 5;
			if (iWidth < 14){
				iWidth = 14;
			}
			console.log(iWidth)
			divButton.style.width = iWidth + "em";
			iLeft += iWidth + 2;
		}
	}
},



time: function(oEle){
	var aValue = [];
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	oEle.bCreate = 1;
	oEle.aPos[2] = 6;
	var divInput = E.input(oEle);
	if (oEle.aPos[6]){
		divInput.parentNode.parentNode.childNodes[0].style.width = 
			oEle.aPos[6] + "em";
	}
	divInput.parentNode.style.width = "6em";
	divInput.setAttribute("type", "time");
	A.action.call(divInput, "keypress", E.action, [E, "time_" + oEle.sID]);
	return divInput;
},




timeXXX: function(oEle){
	var aValue = [];
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divBox = E.div(oEle.divParent, "ele-inputbox");
	divBox.style.left = oEle.aPos[0] + "em";
	divBox.style.top = oEle.aPos[1] + "em";
	divBox.style.width = "6em";
	var divDesc = E.div(divBox, "ele-inputdesc");
	var sText = oEle.sText;
	if (oEle.bRequired){
		sText += "*";
	}
	divDesc.textContent = sText;
	var divInputBox = E.div(divBox, "ele-input", oEle.sID);
	divInputBox.type = "time";
	divInputBox.style.overflow = "hidden";
	var sClass = "";
	if (V.oGen.oBrowser.sOSName == "MacOS"){
		sClass = "mac";
	}
	var divBoxHour = E.div(divInputBox, "ele-selecttimehourbox" + sClass);
	var divSelect = E.div({divParent:divBoxHour,
		sClassName:"ele-selecttimehour", sID:oEle.sID + "_h", sType:"select"});
	E.selectPopulate(divSelect, "|07|08|09|10|11|" + 
		"12|13|14|15|16|17|18|19|20|21|22|23|00|01|02|03|04|05|06");
	var divBoxMinute = E.div(divInputBox, "ele-selecttimeminutebox");
	divSelect = E.div({divParent:divBoxMinute, 
		sClassName:"ele-selecttimeminute", sID:oEle.sID + "_m", sType:"select"});
	E.selectPopulate(divSelect, "|00|05|10|15|20|25|30|35|40|45|50|55");
	var divSpacer = E.div(divInputBox, "ele-timepickerspacer");
	divSpacer.textContent = ":";
	if (oEle.sValue){
		aValue = oEle.sValue.split(":");
		A.valueSet(oEle.sID + "_h", aValue[0], 1);
		A.valueSet(oEle.sID + "_m", aValue[1], 1);
	}
},



uploadimage: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divAll = E.div(oEle.divParent, "ele-imageboxbox", oEle.sID);
	A.selectNone(divAll);
	var divBox = E.div(divAll, "ele-inputbox");
	if (oEle.sText){
		var divText = E.div(divBox, "ele-inputdesc");
		divText.textContent = oEle.sText.split("|")[0];
	}
	var divImgBox = E.div(divAll, "ele-imagebox", oEle.sID + "_image");
	divImgBox.style.width = oEle.aPos[2] + "px";
	divImgBox.style.height = oEle.aPos[3] + "px";
	
	var aText = oEle.sText.split("|");
	
	var divControls = E.div(divAll, "ele-imageboxcontrols");
	divControls.style.width = oEle.aPos[2] + "px";
	E.button({divParent:divControls, aPos:[0, 0, 11, 0, 0, 1], 
		sText:aText[1], sAction:"logo_remove", that:oEle.that});
	if (!V.oGen.bMobile){
		E.memo({divParent:divControls, sClassName:"imagebox", 
			sText:aText[4]})
		E.button({divParent:divControls, aPos:[0, 0, 11, 0, 0, 1], 
			sText:aText[2], sAction:"logo_crop", that:oEle.that});
		E.button({divParent:divControls, aPos:[0, 0, 11, 0, 13, -2.6], 
			sText:aText[3], sAction:"logo_reset", that:oEle.that});
	}
},



uploadStart: function(iUploadNum){
	V.oGen.iUploadNum = iUploadNum;
	var oObj = V.aAA[iUploadNum];
	var divStart = 0;
	if (!V.aAA[iUploadNum].iBusy){
		V.aAA[iUploadNum].iBusy = 1;
		divStart = oObj.divStart;
		divStart.childNodes[0].innerHTML = 
			V.oData.oInit.oBanners.oGeneral.sCancelUpload;
		oObj.divProgress.style.display = "block";
		var aFile = A.dg("aFile_" + iUploadNum);
		aFile = aFile.files[0];
		var aFormData = new FormData();
		aFormData.append("file", "xxx");
		aFormData.append("aFile_" + iUploadNum, aFile);
		aFormData.append("iUploadNum", iUploadNum);
		oObj.aAjax = new XMLHttpRequest(); 
		oObj.aAjax.upload.addEventListener("progress",
			function(event){
				var percent = (event.loaded/event.total) * 100; 
				oObj.divProgress.value = Math.round(percent); 
			}, false); 
		oObj.aAjax.addEventListener("load",
			function(event){
				window.setTimeout(function(){
					oObj.divProgress.style.display = "none";
					oObj.divProgress.value = 0;
					oObj.iBusy = 0;
					oObj.iFinished = 1;
					var divFile = V.aAA[iUploadNum].divFile;
					divFile.value = "";
					A.dg("filename_" + iUploadNum).innerHTML = "";
					V.aAA[iUploadNum].divAll.uploaddone = 1;
					var divStart = oObj.divStart;
					divStart.childNodes[0].innerHTML = 
						V.oData.oInit.oBanners.oGeneral.sUpload;
					var divGrey = A.dg(oObj.divStart.id+"_g");
					divGrey.style.display = "block";
					var sAction = oObj.sAction;
					if (sAction){
						oObj.that.action(oObj.sAction + "_"+ oObj.aAjax.responseText);
					}
				}, 200);
			}, false); 
		oObj.aAjax.addEventListener("error",
			function(event){console.log("uploaderror");}, false); 
		oObj.aAjax.addEventListener("abort",
			function(event){console.log("uploadabort");}, false); 
		oObj.aAjax.open("POST", V.sSrvURL + "upload/"); 
		oObj.aAjax.send(aFormData);
	} else {
		divStart = oObj.divStart;
		divStart.childNodes[0].innerHTML = V.aData.aInit.aBanners.oGeneral.sUpload;
		oObj.aAjax.abort();
		oObj.divProgress.style.display = "none";
		oObj.divProgress.value = 0;
		V.aAA[V.oGen.iUploadNum] = 0;
		V.oGen.iUploadNum = 0;
	}
},



upload: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	/*
	var aPos = oEle.aPos;
	aPos[1] += 1;
	*/
	var sDesc = oEle.sText;
	if (!sDesc){
		sDesc = "";
	}
	if (oEle.bRequired){
		sDesc += "*";
	}
	var sAction = oEle.sAction;
	var aAction = [];
	if (sAction){
		aAction = [oEle.that.action, oEle.that, sAction];
	}
	var sAction2 = oEle.sAction2;
	if (!sAction2){
		sAction2 = "";
	}
	var aAction2 = 0;
	if (sAction2){
		aAction2 = [oEle.that.action, oEle.that, sAction2];
	}
	var sExtensions = oEle.sExtensions;
	if (!sExtensions){
		sExtensions = "";
	}
	var sUploadID = "";
	var divUploadID = oEle.divParent;
	if (divUploadID){
		sUploadID = divUploadID.id;
	}
	var iNum = A.nextObject();
	V.aAA[iNum] = {divParent:oEle.divParent, aPos:oEle.aPos, sDesc:sDesc,
		sRequest:V.sSrvURL + "upload/",
		that:oEle.that,
		sAction:oEle.sAction, sExtensions:sExtensions,
		divBox:0, divProgress:0, iBusy:0, divStart:0,
		iFinished:0, iProgressTimeout:0, sFilename:"", sExtension:"",
		sID:sUploadID
	};
	var divBox = E.div(oEle.divParent, "ele-inputbox");
	divBox.style.height = "9.4em";
//	divAll.style.left = aPos[0] + "em";
//	divAll.style.top = aPos[1] + "em";
	if (oEle.aPos[4]){
		divBox.style.marginLeft = oEle.aPos[4] + "em";
	}
	if (oEle.aPos[5]){
		divBox.style.marginTop = oEle.aPos[5] + "em";
	}
	
	if (sDesc){
		var divDesc = E.div(divBox, "ele-inputdesc");
/*		divDesc.style.top = "-22px";*/
		A.selectNone(divDesc);
		divDesc.textContent = sDesc;
	}
	var divAll = E.div(divBox, "ele-uploadbox", oEle.sID);
	divAll.type = "upload";
	divAll.uploaddone = 0;

	V.aAA[iNum].iBoxiID = E.nextElementID();
	var divBox = E.memo({divParent:divAll,
		aPos:[0.5, 0.8,23,2], sValue:" ", iID:V.aAA[iNum].iBoxiID});
	V.aAA[iNum].iFilenameiID = E.nextElementID();
	V.aAA[iNum].divBox = divBox;
	V.aAA[iNum].divAll = divAll;
	var divForm = E.div({divParent:divBox, sID:"uploadform_" + iNum, 
		sType:"form"});
	divForm.method = "POST";
	divForm.enctype = "multipart/form-data";
	var divSubmit = E.div(divForm, "ele-buttonupload", "submitform_" + iNum);
	var divDesc = E.div(divSubmit, "ele-uploadfiledesc");
	divDesc.textContent =  V.oData.oInit.oBanners.oGeneral.sBrowse;
//	divSubmit.innerHTML = V.oData.oInit.oBanners.oGeneral.sBrowse;
	var divFile = E.div({divParent:divSubmit, sClassName:"ele-uploadfile",
		sID:"aFile_" + iNum, sType:"input"});
	divFile.type = "file";
	divFile.name = "aFile_" + iNum;
	divFile.onchange = function(){
		var divFile = A.dg("aFile_"+iNum);
		var sFilename = divFile.files[0].name;
		var aFilename = sFilename.split(".");
		V.aAA[iNum].sExtension = aFilename[aFilename.length - 1].toLowerCase();
		var aExtensions = V.aAA[iNum].sExtensions.split(", ");
		var iSize = aExtensions.length;
		var iFound = 0;
		var iCount =0;
		while ((!iFound) && (iCount < iSize)){
			if (V.aAA[iNum].sExtension == aExtensions[iCount]){
				iFound = 1;
			} else {
				iCount++;
			}
		}
		if (iFound){
			var divFilename = A.dg("filename_" + iNum);
			divFilename.textContent = sFilename;
			V.aAA[iNum].sFilename = sFilename;
			var divGrey = A.dg(V.aAA[iNum].divStart.id + "_g");
			divGrey.style.display = "none";
		} else {
			var sMessage = V.oData.oError.oUploadInvalidExtension.sMessage + 
				oEle.sExtensions;
			var oError = {sTitle:V.oData.oError.oUploadInvalidExtension.sTitle,
				sMessage:sMessage};
			E.popup(oError);
			
		}
	};
	V.aAA[iNum].divFile = divFile;
	V.aAA[iNum].iButtoniID = E.nextElementID();
	V.aAA[iNum].divStart = E.button({divParent:divAll,
		iID:V.aAA[iNum].iButtoniID,
		aPos:[14, 2, 10, 0, 16, -2.2],
		sText:V.oData.oInit.oBanners.oGeneral.sUpload, that:E,
		bGrey:1, sAction:"uploadstart_" + iNum});
	var divGrey = A.dg(V.aAA[iNum].divStart.id + "_g");
	divGrey.style.display = "block";
	E.memo({divParent:divAll,
		sValue:" ", iID:V.aAA[iNum].iFilenameiID, sClassName:"uploadfile",
		sID:"filename_" + iNum});
	var divBox1 = E.div(divAll, "ele-uploadprogressbox");
	var divProgress = E.div({divParent:divBox1, sClassName:"ele-uploadprogress",
		sID:"progressbar_" + iNum, sType:"progress"});
	divProgress.value = 0;
	divProgress.max = 100;
	V.aAA[iNum].divProgress = divProgress;
	V.oGen.iUploadNum = iNum;
},



userbox: function(oEle){
	oEle = E.element(oEle);
	var divBox = E.div(oEle.divParent, "ele-userbox");
	A.action.call(divBox, "click", E.action, [E, "userdetails"]);
	var sImg = "data:image/png;base64," + V.oData.oUser.sAvatar + "";
	var sH = "<table cellpadding='0' cellmargin='0'><tr>" + 
		"<td class='ele-userboxtext'>" + 
		V.oData.oUser.sFirstName +" " + V.oData.oUser.sSurname + "</td>" + 
		"<td width='40'><div class='menuspaceruser' ></div></td>" + 
		"<td><div class='ele-userboxavatar' " +
		"style='background:url(" + sImg + ")'" +
//		"<img src='"+sImg+"'</img>" + 
		"</div></td>" + 
		"<td width='20'></td>" + 
		"<td class='ele-userboxtext'>" +
		V.oData.oUser.sOrganisationName + "</td>" + 
//		"<td width='20'></td>" + 		
//		"<td class='ele-userboxicon'>&#xE004;</td>" + 
		"</tr></table>"
	divBox.innerHTML = sH;
	/*
	V.oDiv.userboxover = E.div(divBox, "ele-userboxover");
	V.oDiv.userboxover.onmouseout = function(oEvent){
		var divActive = oEvent.target;
		console.log(divActive.className)
			console.log(divActive.parentNode.className)
		if ((divActive.className == "userboxover") &&
			(divActive.parentNode.className == "userbox")){
//			console.log(V.oGen.oMouse)
			console.log(divActive.parentNode.className)
			
		}
	}
	*/
	V.oDiv.userbox = divBox;
	return divBox;
},



weekhoursDefault: function(sID, iIndex){
	A.valueSet(sID + "_dayfrom_" + iIndex, 
		V.oData.oInit.oDefaultValues.sWeekFirstDay, 1);
	A.valueSet(sID + "_dayto_" + iIndex, 
		V.oData.oInit.oDefaultValues.sWeekLastDay, 1);
	A.valueSet(sID + "_timefrom_" + iIndex, 
		V.oData.oInit.oDefaultValues.oBusinessHours.sOpen);
	A.valueSet(sID + "_timeto_" + iIndex, 
		V.oData.oInit.oDefaultValues.oBusinessHours.sClose);
},



weekhoursDraw: function(iObjNo){
	var divIcon = 0;
	var iI = 0;
	var oObj = V.aAA[iObjNo];
	var divBox = oObj.divBox;
	var oEle = oObj.oEle;
	divBox.innerHTML = "";
	var aText = oEle.sText.split("|");
	if (oEle.bRequired){
		aText[0] += "*";
	}
	var divDesc = E.div(divBox, "ele-weekhoursboxdesc0");
	divDesc.textContent = aText[0];
	for (iI = 0; iI < oEle.that.iNumBusinessHours; iI++){
		var divRow = E.div(divBox, "ele-weekhoursrow");
		divDesc = E.div(divRow, "ele-weekhoursboxdesc1");
		divDesc.textContent = aText[1];
		var divSelectFrom = E.select({divParent:divRow, 
			aPos:[0, 0, 10, 0, 4, 0.1], sID:oEle.sID + "_dayfrom_" + iI});
		E.selectPopulate
			(divSelectFrom, V.oData.oInit.oBanners.oDatePicker.sDaysLong);
		divDesc = E.div(divRow, "ele-weekhoursboxdesc3");
		divDesc.textContent = aText[3];
		var divSelectTo = E.select({divParent:divRow, 
			aPos:[0, 0, 10, 0, 17, -6], sID:oEle.sID + "_dayto_" + iI});
		E.selectPopulate
			(divSelectTo, V.oData.oInit.oBanners.oDatePicker.sDaysLong);
		divDesc = E.div(divRow, "ele-weekhoursboxdesc2");
		divDesc.textContent = aText[2];
		E.time({divParent:divRow, aPos:[0, 0, 0, 0, 4, -3],
			sID:oEle.sID + "_timefrom_" + iI});
		divDesc = E.div(divRow, "ele-weekhoursboxdesc4");
		divDesc.textContent = aText[3];
		E.time({divParent:divRow, aPos:[0, 0, 0, 0, 13.5, -6],
			sID:oEle.sID + "_timeto_" + iI});
		if ((iI + 1) == oEle.that.iNumBusinessHours){
			divIcon = E.div(divRow, "ele-weekhoursiconplus");
			divIcon.innerHTML = "&#xE06B;";
			A.action.call(divIcon, "click", 
				E.action, [E, "weekhours_plus_" + iI + "_" + iObjNo]);
		}
		if (V.aAA[iObjNo].oEle.that.iNumBusinessHours > 1){
			divIcon = E.div(divRow, "ele-weekhoursiconminus");
			divIcon.innerHTML = "&#xE06C;";
			A.action.call(divIcon, "click", 
				E.action, [E, "weekhours_minus_" + iI + "_" + iObjNo]);
		}
	}
},



weekhours: function(oEle){
	if (!oEle.bCreate){
		oEle = E.element(oEle);	
	}
	var divBox = E.div(oEle.divParent, "ele-weekhoursbox", oEle.sID);
	divBox.type = "weekhours";
	if (oEle.aPos[4]){
		divBox.style.marginLeft = oEle.aPos[4] + "em";
	}
	if (oEle.aPos[5]){
		divBox.style.marginTop = oEle.aPos[5] + "em";
	}
	var iObjNo = A.nextObject();
	V.aAA[iObjNo] = {oEle:oEle, divBox:divBox, iIndex:0};
	E.weekhoursDraw(iObjNo);
	return iObjNo;
},



};
//== END OF ELEMENTS CLASS =====================================================



//== ADDRESSES CLASS ===========================================================
function ENTER_ADDRESSES(){
	this.sParentName = "";
	this.sIDPre = "";
	this.aAddresses = [];
	this.iAddressNo = 0;
	this.iRemoveIndex = -1;
	
	if (typeof ENTER_ADDRESSES.bInited == "undefined"){
		
ENTER_ADDRESSES.prototype.add = function(bIsProfile){
	var aValues = [];
	var iI = 0;
	var iJ = 0;
	var iFound = 0;
	var iSize = 0;
	var iSize1 = 0;
	var oForm = V.oData.oInit.oForm_RegisterStep4;
	var iI = 0;
	var that = A.findObject(this.sParentName);
	var sIDPre = "";
	var divParent = 0;
	if (!bIsProfile){
		sIDPre = this.sIDPre.split("_")[0];
		for (iI = 2; iI < 5; iI++){
			var divEle = A.dg(sIDPre + "_" + iI);
			divParent = divEle.parentNode;
			divEle.parentNode.removeChild(divEle);
		}
	} else {
		sIDPre = this.sIDPre.substr(0, this.sIDPre.length - 1);
	}
	var oEle = oForm[1];
	var aIDPre = sIDPre.split("_");
	var sIDParent = aIDPre[0] + "_" + aIDPre[1] + "_" + aIDPre[2];
	var divAddressListBox = A.dg(sIDParent);
	oEle.divParent = divAddressListBox;
	var iObjNo = A.nextObject();
	that.aObjAddresses.push(iObjNo);
	oEle.iIndex = that.aObjAddresses.length - 1; 
	
	E.address(oEle);
	if (!bIsProfile){
		for (iI = 2; iI < 5; iI++){
			var oEle = oForm[iI];
			oEle.divParent = divParent;
			oEle.that = that;
			E[oEle.sType](oEle);
		}
	}
	return 1;
};



ENTER_ADDRESSES.prototype.changeProvince = function(sPage){
	var sIDPre = this.sIDPre;
	if (sPage == "register"){
		sIDPre = this.sIDPre + "0_";
	}
	var iProvinceID = A.valueGet(sIDPre +"1");
	var iNumRegions = V.oData.aRegions.length;
	var aTowns = [V.oData.oInit.oEnum.sSelect[0]];
	var aRegions1 = [V.oData.oInit.oEnum.sSelect[0]];
	var aParentRegions1 = [];
	for (iI = 0; iI <= iNumRegions; iI++){
		oRec = V.oData.aRegions[iI];
		if (oRec){
			if (((oRec.iRegionType == 2) && (oRec.iParentID == iProvinceID)) ||
					(((oRec.iRegionType == 3) && (oRec.iParentID == iProvinceID)))){
				aRegions1.push(oRec);
				aParentRegions1.push(oRec.iID);
			}
		}
	}
	for (iI = 0; iI <= iNumRegions; iI++){
		oRec = V.oData.aRegions[iI];
		var bParent = 0;
		if ((oRec) && (aParentRegions1.indexOf(oRec.iParentID) != -1)){
			bParent = 1;
		}
		if ((oRec) && (oRec.iRegionType == 3) && 
			((oRec.iParentID == iProvinceID) || (bParent))){
			aTowns.push(oRec);
		}
	}
	divSelect = A.dg(sIDPre + "2");
	E.selectPopulate(divSelect, aRegions1);
	var divSelect = A.dg(sIDPre + "3");
	E.selectPopulate(divSelect, aTowns);
};



ENTER_ADDRESSES.prototype.changeRegion = function(sPage){
	var sIDPre = this.sIDPre;
	if (sPage == "register"){
		sIDPre = this.sIDPre + "0_";
	}
	var iRegionID = A.valueGet(sIDPre + "2");
	var iNumRegions = V.oData.aRegions.length;
	var aTowns = [V.oData.oInit.oEnum.sSelect[0]];
	for (iI = 0; iI <= iNumRegions; iI++){
		oRec = V.oData.aRegions[iI];
		if ((oRec) && (oRec.iParentID == iRegionID)){
			aTowns.push(oRec);
		}
	}
	var divSelect = A.dg(sIDPre + "3");
	E.selectPopulate(divSelect, aTowns);
};



ENTER_ADDRESSES.prototype.changeTown = function(sPage){
	var sIDPre = this.sIDPre;
	if (sPage == "register"){
		sIDPre = this.sIDPre + "0_";
	}
	var iTownID = A.valueGet(sIDPre + "3");
	var iNumRegions = V.oData.aRegions.length;
	var aSuburbs = [V.oData.oInit.oEnum.sSelect[0]];
	for (iI = 0; iI <= iNumRegions; iI++){
		oRec = V.oData.aRegions[iI];
		if ((oRec) && (oRec.iParentID == iTownID)){
			aSuburbs.push(oRec);
		}
	}
	var divSelect = A.dg(sIDPre + "4");
	E.selectPopulate(divSelect, aSuburbs);
};



ENTER_ADDRESSES.prototype.checkMain = function(){
	
	
	
	
	return
	
	
	
	
	
	
	
	
	
	
	var sIDPre = this.sIDPre + this.iAddressNo +"_";
	var iSize = this.aAddresses.length;
	var sID = sIDPre + "7";
	if (iSize < 1){
		A.valueSet(sID, 1);
		A.dg(sID).disabled = "disabled";
	} else {
		A.dg(sID).disabled = "";
	}
};



ENTER_ADDRESSES.prototype.findParent = function(iRegionID){
	var iFound = 0;
	var iSize = V.oData.aRegions.length;
	var iI = 0;
	while ((!iFound) && (iI < iSize)){
		var oRec = V.oData.aRegions[iI];
		if (oRec.iID == iRegionID){
			iFound = iI + 1;
		} else {
			iI++;
		}
	}
	if (iFound){
		return parseInt(oRec.iParentID);
	} else {
		return 0;
	}
};



ENTER_ADDRESSES.prototype.removeGo = function(){
	var that = A.findObject(this.sParentName);
	var sID = this.sIDPre.substr(0, this.sIDPre.length - 1);
	var divBox = A.dg(sID);
	divBox.parentNode.removeChild(divBox);
};



ENTER_ADDRESSES.prototype.remove = function(iIndex){
	this.iRemoveIndex = parseInt(iIndex);
	var sTitle = V.oData.oError.oRegisterRemoveAddress.sTitle;
	var sMessage = V.oData.oError.oRegisterRemoveAddress.sMessage;
	var that = A.findObject(this.sParentName);
	E.popup({
		sTitle:sTitle,
		sMessage:sMessage,
		that:that,
		sAction:"register_addressremovego",
		aButtons:[V.oData.oInit.oBanners.oGeneral.sOK, 
			V.oData.oInit.oBanners.oGeneral.sCancel]
	});
};



ENTER_ADDRESSES.prototype.select = function(bIsID){
	var sIDPre = this.sIDPre + this.iAddressNo +"_";
	var iI = 0;
	var iFound = 0;
	var iSize = 0;
	var iIndex = A.valueGet(sIDPre + "7");
	if (iIndex == -1){
		A.valueSet(sIDPre + "1", -1);
		A.valueSet(sIDPre + "2", -1);
		A.valueSet(sIDPre + "3", -1);
		A.valueSet(sIDPre + "4", -1);
		A.valueSet(sIDPre + "5", "");
		A.valueSet(sIDPre + "6", "");
		A.valueSet(sIDPre + "7", "");
	} else {
		var aRec = this.aAddresses[iIndex];		
		if (bIsID){
			iSize = this.aAddresses.length;
			while ((!iFound) && (iI < iSize)){
				if (this.aAddresses[iI][9] == iIndex){
					iFound = iI + 1;
					aRec = this.aAddresses[iI];
				} else {
					iI++;
				}
			}
		}
		if (!aRec){
			return;
		}
		iI = 0;
		A.valueSet(sIDPre + iI, aRec[iI]);
		this.changeProvince();
		for (iI = 1; iI < 7; iI++){
			A.valueSet(sIDPre + iI, aRec[iI]);
		}
	}
	if (iIndex != -1){
		A.dg(sIDPre + "10_g").style.display = "none";
	} else {
		A.dg(sIDPre + "10_g").style.display = "block";
	}
};



		ENTER_ADDRESSES.bInited = 1;
	}
}

//== END OF ADDRESSES CLASS ====================================================



//== LOGO CLASS ================================================================
function ENTER_LOGO(){
	this.aMouseCrop = [10, 10, 10, 10];
	this.sIDPre = "";
	this.sParentObjName = "";
	this.oDiv = {};
	this.iTabNo = 2;
	this.bCropped = 0;
	
	if (typeof ENTER_LOGO.bInited == "undefined"){
		
ENTER_LOGO.prototype.load = function(sCompanyLogo){
	var aURL = ["init_logoload", "filename", sCompanyLogo,
		"parent", this.sParentObjName];
	A.ajax({aURL:aURL, fCallback:this.uploaded});
};



ENTER_LOGO.prototype.showControls = function(bDisplay1, bDisplay2){
	
	
//	return
	var sIDPre = this.sIDPre + "_"+ this.iTabNo + "-_"; 
	if (bDisplay1){
		A.dg(sIDPre + "5").style.display = "block";
		A.dg(sIDPre + "7").style.display = "block";
		A.dg(sIDPre + "8").style.display = "block";
	} else {
		A.dg(sIDPre + "5").style.display = "none";
		A.dg(sIDPre + "6").style.display = "none";
		A.dg(sIDPre + "7").style.display = "none";
//		A.dg(this.sIDPre + "-2_7_g").style.display = "none";
		A.dg(sIDPre + "8").style.display = "none";
//		A.dg(this.sIDPre + "-2_8_g").style.display = "none";		
	}
	if (!bDisplay2){
		A.dg(sIDPre + "6").style.display = "none";
//		A.dg(this.sIDPre + "-2_7_g").style.display = "none";
//		A.dg(this.sIDPre + "-2_8_g").style.display = "block";
	}
	if (bDisplay2 == 1){
		A.dg(sIDPre + "6").style.display = "block";
//		A.dg(this.sIDPre + "-2_7_g").style.display = "block";
//		A.dg(this.sIDPre + "-2_8_g").style.display = "none";
	}
	if (bDisplay2 == 2){
		A.dg(sIDPre + "6").style.display = "none";
//		A.dg(this.sIDPre + "-2_7_g").style.display = "none";
//		A.dg(this.sIDPre + "-2_8_g").style.display = "none";
	}
};



ENTER_LOGO.prototype.resetCrop = function(sCompanyLogo){
	var aURL = ["init_croplogoreset", "filename", sCompanyLogo,
		"parent", this.sParentObjName];
	A.ajax({aURL:aURL, fCallback:this.uploaded});
};



ENTER_LOGO.prototype.setCrop = function(sBox){
	var iI = 0;
	var oRect = this.oDiv.imgbox.getBoundingClientRect();
	var iX = 0;
	var iY = 0;
	var iScrollTop = window.pageYOffset || document.documentElement.scrollTop;
	if (sBox == "lt"){
		iX = V.oGen.oMouse.iPosX - oRect.left;
		iY = V.oGen.oMouse.iPosY - oRect.top;
		this.aMouseCrop[2] = iX;
		this.aMouseCrop[3] = iY;
		this.oDiv.lefttop.style.width = this.aMouseCrop[2] + "px";
		this.oDiv.lefttop.style.height = 
			(this.aMouseCrop[3] - iScrollTop) + "px";
		this.oDiv.leftcut.style.width = 
			(this.aMouseCrop[2]) + "px";
		this.oDiv.leftcut.style.height = 
			(oRect.height - this.aMouseCrop[3] + iScrollTop) + "px";
		this.oDiv.topcut.style.width = 
			(oRect.width - this.aMouseCrop[2]) + "px";
		this.oDiv.topcut.style.height = 
			(this.aMouseCrop[3] - iScrollTop) + "px";
	} else {
		iX = oRect.right - V.oGen.oMouse.iPosX;
		iY = oRect.bottom - V.oGen.oMouse.iPosY;
		this.aMouseCrop[4] = iX;
		this.aMouseCrop[5] = iY;
		this.oDiv.rightbottom.style.width = this.aMouseCrop[4] + "px";
		this.oDiv.rightbottom.style.height = 
			(this.aMouseCrop[5] + iScrollTop) + "px";
		this.oDiv.rightcut.style.width = 
			(this.aMouseCrop[4]) + "px";
		this.oDiv.rightcut.style.height = 
			(oRect.height - this.aMouseCrop[5] - iScrollTop) + "px";
		this.oDiv.bottomcut.style.width = 
			(oRect.width - this.aMouseCrop[4]) + "px";
		this.oDiv.bottomcut.style.height = 
			(this.aMouseCrop[5] + iScrollTop) + "px";
	}
	this.aMouseCrop[6] = V.oGen.oMouse.iPosX;
	this.aMouseCrop[7] = V.oGen.oMouse.iPosY;
	for (iI = 2; iI < 6; iI++){
		if (this.aMouseCrop[iI] < 10){
			this.aMouseCrop[iI] = 10;
		}
	}
	this.aMouseCrop[8] = iScrollTop;
console.log(this.aMouseCrop)
	this.bCropped = 1;
};



ENTER_LOGO.prototype.saveCropA = function(oData){
	console.log(oData)
	if (!oData.sB64){
		return;
	}
	var that = A.findObject(oData.sParentObjName);
	var thatt = V.aAA[that.iObjLogo];
	thatt.bCropped = 0;
	var divImg = thatt.oDiv.imgsrc;
	if (divImg){
		divImg.parentNode.removeChild(divImg);
	}
	var divImg = E.div({divParent:thatt.oDiv.imgbox, 
		sClassName:"img100", sType:"img"});
	divImg.src = "data:image/" + oData.sExt + ";base64," + oData.sB64;
	thatt.oDiv.imgsrc = divImg
	thatt.aMouseCrop = 
		[oData.iSquareSize, oData.iSquareSize, 10, 10, 10, 10];
	that.sCompanyLogo = oData.sFilename;
	thatt.oDiv.lefttop.style.width = "10px";
	thatt.oDiv.lefttop.style.height = "10px";
	thatt.oDiv.rightbottom.style.width = "10px";
	thatt.oDiv.rightbottom.style.height = "10px";
	thatt.oDiv.leftcut.style.width = "0";
	thatt.oDiv.rightcut.style.width = "0";
	thatt.oDiv.topcut.style.height = "0";
	thatt.oDiv.bottomcut.style.height = "0";
//	thatt.showControls(1, 0);
};



ENTER_LOGO.prototype.saveCrop = function(sCompanyLogo){
	if (this.bCropped){
		var aURL = ["init_croplogo", "size", JSON.stringify(this.aMouseCrop),
			"filename", sCompanyLogo, "parent", this.sParentObjName];
		A.ajax({aURL:aURL, fCallback:this.saveCropA});
	}
};



ENTER_LOGO.prototype.remove = function(){
//	this.showControls(0, 2);
	var divUpload = A.dg(this.sIDPre + "-" + this.iTabNo + "_3");
	divUpload.uploaddone = 0;
	var divImg = this.oDiv.imgsrc;
	if (divImg){
		divImg.parentNode.removeChild(divImg);
		this.oDiv.imgsrc = 0;
	}
	
	divUpload.style.display = "none";
};



ENTER_LOGO.prototype.resetImgBox = function(divImgBox, iSquareSize){
	divImgBox.innerHTML = "";
	var divImg = E.div({divParent:divImgBox, 
		sClassName:"img100", sType:"img"});
	
	var oRect = divImgBox.getBoundingClientRect();
	var divLT = E.div(divImgBox, "ele-imageboxcrop_lt");
	var divRB = E.div(divImgBox, "ele-imageboxcrop_rb");
	this.oDiv.imgbox = divImgBox;
	this.oDiv.imgsrc = divImg;
	this.oDiv.lefttop = divLT;
	this.oDiv.rightbottom = divRB;
	
	this.oDiv.leftcut = E.div(divImgBox, "ele-imagecropcutl");
	this.oDiv.rightcut = E.div(divImgBox, "ele-imagecropcutr");
	this.oDiv.topcut = E.div(divImgBox, "ele-imagecropcutt");
	this.oDiv.bottomcut = E.div(divImgBox, "ele-imagecropcutb");
	this.aMouseCrop = 
		[iSquareSize, iSquareSize, 10, 10, 10, 10];
	return divImg;
};



ENTER_LOGO.prototype.uploaded = function(oData){
	console.log(oData)
	if (!oData.sB64){
		return;
	}
	var that = A.findObject(oData.sParentObjName);
	var thatt = V.aAA[that.iObjLogo];
	thatt.bCropped = 0;
	that.sCompanyLogo = oData.sFilename;
	var sID = V.aAA[that.iObjLogo].sIDPre + "-" + thatt.iTabNo + "_3";
	A.dg(sID).style.display = "block";
	var divImg = thatt.oDiv.imgsrc;
	if (divImg){
		divImg.parentNode.removeChild(divImg);
		thatt.oDiv.imgsrc = 0;
	}
	var divImgBox = A.dg(sID + "_image");
	var divImg = thatt.resetImgBox(divImgBox, oData.iSquareSize);
	divImg.src = "data:image/" + oData.sExt + ";base64," + oData.sB64;
};



		ENTER_LOGO.bInited = 1;
	}
}

//== END OF LOGO CLASS =========================================================



//== HOUSE CLASS ===============================================================
function ENTER_HOUSE(){
	this.iMenuActive = -1;
	this.sName = "house";
	this.iRegisterStep = 1;
	this.iRegisterStepMax = 5;
	this.sCompanyLogo = "";
	this.iNumBusinessHours = 1;
	this.sLink = "";
	this.sEmailAddress = "";
	this.aObjAddresses = [];
	this.iAddressNo = 0;
	this.iObjLogo = 0;
	this.sPicBox = "box_2";
	this.aPlaces = [];
	
	if (typeof ENTER_HOUSE.bInited == "undefined"){

ENTER_HOUSE.prototype.action = function(sAction, aParams){
	var that = A.actionA(this, sAction, this.aParams);
	var aAction = that.sAction.split("_");
console.log(that.sAction);
	switch (aAction[0]){
		case "logo":
			switch (aAction[1]){
				case "remove":
					V.aAA[that.that.iObjLogo].remove();
					that.that.sCompanyLogo = "";				
				break;
				case "crop":
					V.aAA[that.that.iObjLogo].saveCrop(that.that.sCompanyLogo);
				break;
				case "reset":
					V.aAA[that.that.iObjLogo].resetCrop(that.that.sCompanyLogo);
				break;
			}
		break;
		case "house":
			switch (aAction[1]){
					case "menu":
					case "menuact":
					if (that.that.iMenuActive == parseInt(aAction[2])){
						return;
					}
					switch (aAction[2]){
						case "0":
							that.that.menuActive(that.that.iMenuActive);
							that.that.loginShow();
						break;
						case "1":
							that.that.menuActive(2);
							that.that.registerStart();
						break;
						case "3":
							V.oGen.oSession.sURLHash = "home";
							that.that.menuActive(3);
							that.that.homeShow();
						break;
					}					
				break;
				
				case "house1iconsocial":
					that.that.social(parseInt(aAction[2]));			
				break;
				
				case "ele-buttonwhite":
					window.scrollTo(0,0);
					that.that.menuActive(2);
					that.that.registerStart();
				break;
				
				case "house5android":
					location.href = 
						V.oData.oInit.oHouseAll.aAppLinks[0];
				break;
				
				case "house5apple":
					location.href = 
						V.oData.oInit.oHouseAll.aAppLinks[1];
				break;
				
				case "footerdocslinks":
					that.that.documentation(aAction[2]);
				break;
				
			}
		break;
		case "uploaddone":
			A.ajax({aURL:["init_registeruploaddone", "filename", aAction[1],
				"parent", "house"],
				fCallback:V.aAA[that.that.iObjLogo].uploaded});
		break;
		case "login":
			switch (aAction[1]){
				case "forgotpassword":
					that.that.loginForgotPassword();
				break;	
				case "forgotpasswordgo":
					that.that.loginForgotPasswordGo();
				break;	
				case "go":
					that.that.loginGo();
				break;
				case "registernow":
					E.popupClose();
					that.that.registerStart();
				break;
				
				case "fail":
					V.oDiv.popuppageover.style.display = "none";
				break;
			}
		break;
		
		case "reset":
			switch (aAction[1]){
				case "go":
					that.that.resetPasswordGo();
				break;
			}
		break;
		
		case "reseterror":
			V.oDiv.popuppageover.innerHTML = "";
			V.oDiv.popuppageover.style.display = "none";
		break;
		/*
				case "register":
			var iIndex = parseInt(aAction[2]);
			switch (aAction[1]){
				case "province":
					V.aAA[that.that.aObjAddresses[iIndex]].changeProvince("profile");
				break;
				case "region":
					V.aAA[that.that.aObjAddresses[iIndex]].changeRegion("profile");
				break;
				case "town":
					V.aAA[that.that.aObjAddresses[iIndex]].changeTown("profile");
				break;
				case "addressselect":
					V.aAA[that.that.aObjAddresses[that.that.iAddressNo]].select(1);
				break;
				case "addressadd":
					V.aAA[that.that.aObjAddresses[iIndex]]
						.add(that.that.bAddFirstAddress);
					that.that.bAddFirstAddress = 0;
				break;
				case "addressremove":
					V.aAA[that.that.aObjAddresses[iIndex]].remove();
				break;
				case "addressremovego":
					if (aAction[2] == "1"){
						E.popupClose();
					} else {
						E.popupClose();
						V.aAA[that.that.aObjAddresses[iIndex]].removeGo();
						var iAddressObj = that.that.aObjAddresses[iIndex];
						V.aAA[iAddressObj] = 0;
						that.that.aObjAddresses[iIndex] =  -1;
					}
				break;
				/*
				case "removelogo":
					V.aAA[that.that.iObjLogo].remove();
					that.that.sCompanyLogo = "";
				break;				
				case "croplogo":
					V.aAA[that.that.iObjLogo].saveCrop(that.that.sCompanyLogo);
				break;
				case "croplogoreset":
					V.aAA[that.that.iObjLogo].resetCrop(that.that.sCompanyLogo);
				break;
				
			}
		break;

		
		
		
		*/
		case "register":
			var iIndex = parseInt(aAction[2]);
			switch (aAction[1]){
				case "cancel":
					that.that.registerCancel();
				break;
				case "addaddress":
					that.that.registerAddAddress(iIndex);
				break;
				case "province":
					V.aAA[that.that.aObjAddresses[iIndex]].changeProvince("profile");
				break;
				case "region":
					V.aAA[that.that.aObjAddresses[iIndex]].changeRegion("profile");
				break;
				case "town":
					V.aAA[that.that.aObjAddresses[iIndex]].changeTown("profile");
				break;


				case "nextstep":
					that.that.registerNextStep();
				break;
				case "showpage":
					that.that.registerNextStep(parseInt(aAction[2]));
				break;
				case "savecreate":
					that.that.registerSaveCreate();
				break;
				case "province":
					V.aAA[that.that.aObjAddresses[that.that.iAddressNo]]
						.changeProvince("register");
				break;
				case "region":
					V.aAA[that.that.aObjAddresses[that.that.iAddressNo]]
						.changeRegion("register");
				break;
				case "town":
					V.aAA[that.that.aObjAddresses[that.that.iAddressNo]]
						.changeTown("register");
				break;
				case "addressselect":
					V.aAA[that.that.aObjAddresses[that.that.iAddressNo]].select();
				break;
				case "done":
					that.that.registerDone();
				break;
				case "activate":
					that.that.registerActivateGo();
				break;
				case "addressadd":
					that.that.iAddressNo++;
					var iAddressNo = that.that.iAddressNo; 
					that.that.aObjAddresses[iAddressNo] = A.nextObject();
					V.aAA[that.that.aObjAddresses[iAddressNo]] = 
						new ENTER_ADDRESSES();
					V.aAA[that.that.aObjAddresses[iAddressNo]].sParentName = "house";
					V.aAA[that.that.aObjAddresses[iAddressNo]].sIDPre = "register-4_1_";
					V.aAA[that.that.aObjAddresses[iAddressNo]].iAddressNo = 
						that.that.iAddressNo;
					var iReturn = V.aAA[that.that.aObjAddresses[iAddressNo]].add();
					if (!iReturn){
						V.aAA[that.that.aObjAddresses[iAddressNo]] = 0;
						that.that.iAddressNo--;
						that.that.aObjAddresses.splice(-1, 1);
					}
				break;
				case "addressremove":
					V.aAA[that.that.aObjAddresses[iIndex]].remove(iIndex);
				break;
				case "addressremovego":
					if (aAction[2] == "1"){
						E.popupClose();
					} else {
						E.popupClose();
						var iObjNo = that.that.aObjAddresses[iIndex];
						
						
						
////////////// index problem /////////////////////
						
						
						
						if (iObjNo >= 0){
							V.aAA[iObjNo].removeGo();
							var iAddressObj = that.that.aObjAddresses[iIndex];
							V.aAA[iAddressObj] = 0;
							that.that.aObjAddresses[iIndex] =  -1;
						}
					}
				break;
				case "removelogo":
					V.aAA[that.that.iObjLogo].remove();
					that.that.sCompanyLogo = "";
				break;				
				case "croplogo":
					V.aAA[that.that.iObjLogo].saveCrop(that.that.sCompanyLogo);
				break;
				case "croplogoreset":
					V.aAA[that.that.iObjLogo].resetCrop(that.that.sCompanyLogo);
				break;
				
			}			
		break;
		case "docs":
			that.that.documentation(aAction[1]);
		break;
		
		case "app":
			location.href = V.oData.oInit.oHouseAll.aAppLinks[parseInt(aAction[1])];
		break;
		
		case "resetpassword":
			if (aAction[1] == "1"){
				E.popupClose();
			} else {
				that.that.resetPasswordGo();
			}
		break;
		
		case "activateuser":
			if (aAction[1] == "1"){
				E.popupClose();
			} else {
				that.that.activateUserGo();
			}
		break;
	}
	
};



ENTER_HOUSE.prototype.documentation = function(iIndex){
	iIndex -= 8;
	E.dropdown({divParent:V.oDiv.work, sClassName:"full",
		fCallback:function(){
			window.scrollTo(0, 0);
			var divBox = E.div(V.oDiv.dropdown, "ele-dropdownboxfullcontents");
			divBox.parentNode.style.overflowY = "scroll";
			switch (iIndex){
				case 0:
					divBox.innerHTML = "<h2>" + 
						V.oData.oInit.oBanners.oGeneral.sDocsTermsOfUse + "</h2>" + 
						V.oData.oInit.sDocsTermsOfUse;
				break;
				case 1:
					divBox.innerHTML = "<h2>" + 
						V.oData.oInit.oBanners.oGeneral.sDocsPrivacyPolicy + "</h2>" + 
						V.oData.oInit.sDocsPrivacyPolicy;
				break;
			}
		}
	});
};



ENTER_HOUSE.prototype.homeShow = function(){
	A.pushHistory("home");
	var divMenu = A.gcn("menuitems");
	if (divMenu){
		V.oDiv.main.className = "bodymain";
		divMenu.style.right = "0";
		V.oDiv.left.style.width = "0";
		V.oDiv.work.style.left = "0";
	}
	V.oDiv.body.innerHTML = "";
	V.oDiv.cloak = E.div(V.oDiv.body, "modalcloak");
	V.oDiv.cloaktop = E.div(V.oDiv.body, "modalcloaktop");
	var oAll = V.oData.oInit.oHouseAll;
	divHeaderOut = E.div(V.oDiv.body, "bodyheaderout");
	E.div(divHeaderOut, "bodyleft");
	var divMain = E.div(V.oDiv.body, "bodymain");
	V.oDiv.header = E.div(divMain, "bodyheader");
	this.menu();
	V.oDiv.work = E.div(divMain, "bodywork");
	
	var divBody = E.div(V.oDiv.work, "body1");
	var divIcon = E.div(divBody, "house1icon");
	divIcon.innerHTML = oAll.sIconLogo;
	var divCenter = E.div(divBody, "divcenterhouse1box");
	var divBox = E.div(divCenter, "house1box");
	var divLine = E.div(divBox, "house1boxline");
	var divImg = E.div({divParent:divLine, sClassName:"img100", sType:"img"});
	divImg.src = "data/houseline.png"
	var divText = E.div(divBox, "house1boxtext");
	divText.innerHTML = oAll.sText1;
	var divLine = E.div(divBox, "house1boxline");
	var divImg = E.div({divParent:divLine, sClassName:"img100", sType:"img"});
	divImg.src = "data/houseline.png"	
	var divCenter = E.div(divBody, "divcenterhouse1icons");
	var divTable = E.div({divParent:divCenter, sType:"table"});
	divTable.style.border = "none";
	divTable.style.width = "100%";
	var divTR = E.div({divParent:divTable, sType:"tr"});
	var iSize = oAll.aSocials.length;
	var iI = 0;
	for (iI = 0; iI < iSize; iI++){
		var divTD = E.div({divParent:divTR, sType:"td"}); 
		var divIcon = E.div(divTD, "house1iconsocial act");
		divIcon.innerHTML = oAll.aSocials[iI].sIcon;
	}
	
	var divBody = E.div(V.oDiv.work, "body2");
	var divCenter = E.div(divBody, "divcenter");
	var divPhones = E.div(divCenter, "house2phones");
	var divImg = E.div({divParent:divPhones, sClassName:"img100", sType:"img"});
	divImg.src = "data/housephones.png"

	var divBody = E.div(V.oDiv.work, "body3");
	var divText = E.div(divBody, "house3text1");
	divText.textContent = oAll.sText21;
	var divText = E.div(divBody, "house3text2");
	divText.textContent = oAll.sText22;
	var divButton = E.div(divBody, "house3button act");
	divButton.textContent = oAll.sButtonRegister;
	
	var divBody = E.div(V.oDiv.work, "body4");
	var divCenter = E.div(divBody, "divcenterbody4");
	var divTable = E.div({divParent:divCenter, sType:"table", 
		sClassName:"houseicons"});
	var divTR = E.div({divParent:divTable, sType:"tr"});
	for (iI = 0; iI < 3; iI++){
		var divTD = E.div({divParent:divTR, sType:"td"});
		divTD.style.border = "none";
		if (iI != 1){
			divTD.style.width = "30%";
		}
		var divBox = E.div(divTD, "houseiconsbox");
		var divCount = E.div(divBox, "house4count");
		divCount.textContent = (iI + 1);
		var oRec = oAll.aIcons2[iI];
		var divText = E.div(divBox, "house4text1");
		divText.textContent = oRec.sText1;
		var divText = E.div(divBox, "house4text2");
		divText.textContent = oRec.sText2;
		var divIcon = E.div(divBox, "house4icon1");
		divIcon.innerHTML = oAll.sIconStep;
		var divIcon = E.div(divBox, "house4icon2");
		divIcon.innerHTML = oRec.sIcon;
		var divTD = E.div({divParent:divTR, sType:"td", 
			sClassName:"houseiconsspacer"});
	}

	var divBody = E.div(V.oDiv.work, "body5");
	var divText = E.div(divBody, "house3text1");
	divText.textContent = oAll.sText31;
	var divText = E.div(divBody, "house3text2");
	divText.textContent = oAll.sText32;
	var divBox = E.div(divBody, "divcenterbody5");
	var divApp = E.div(divBox, "house5android act");
	var divImg = E.div({divParent:divApp, sClassName:"img100", sType:"img"});
	divImg.src = "data/houseandroid.png"
	var divApp = E.div(divBox, "house5apple act");
	var divImg = E.div({divParent:divApp, sClassName:"img100", sType:"img"});
	divImg.src = "data/houseapple.png"
	
	var divBody = E.div(V.oDiv.work, "body6");
	var divCenter = E.div(divBody, "divcenterbody4");
	var divTable = E.div({divParent:divCenter, sType:"table", 
		sClassName:"houseicons"});
	var divTR = E.div({divParent:divTable, sType:"tr"});
	for (iI = 0; iI < 3; iI++){
		var divTD = E.div({divParent:divTR, sType:"td"});
		divTD.style.border = "none";
		if (iI != 1){
			divTD.style.width = "30%";
		}
		var divBox = E.div(divTD, "houseiconsbox");
		var divCount = E.div(divBox, "house4count");
		divCount.textContent = (iI + 1);
		var oRec = oAll.aIcons3[iI];
		var divText = E.div(divBox, "house4text1");
		divText.textContent = oRec.sText1;
		var divText = E.div(divBox, "house4text2");
		divText.textContent = oRec.sText2;
		var divIcon = E.div(divBox, "house4icon1");
		divIcon.innerHTML = oAll.sIconStep;
		var divIcon = E.div(divBox, "house6icon2");
		divIcon.innerHTML = oRec.sIcon;
		var divTD = E.div({divParent:divTR, sType:"td", 
			sClassName:"houseiconsspacer"});
	}
	
	var divFooter = E.div(V.oDiv.work, "bodyfooter");
	var divDocs = E.div(divFooter, "footerdocs");
	var aText = V.oData.oInit.oHouseAll.sDocs.split("|");
	divDocs.innerHTML = aText[0] + "<span class='footerdocslinks act'>" + 
		aText[1] + "</span>" + 
		aText[2] + "<span class='footerdocslinks act'>" + aText[3] +
		"</span>";
	V.oDiv.main = divMain;
	V.oGen.bHouseLoaded = 2;
	this.initializeActions();
};



ENTER_HOUSE.prototype.initializeActions = function(){
	if (V.oGen.bHouseLoaded == 2){
		V.oDiv.header = A.gcn("bodyheader");
		V.oDiv.breadcrumbs = E.div(V.oDiv.header, "ele-breadcrumbs");
		var aActs = A.gcn("act", 1);
		var divX = 1;
		var iI = 0;
		while (divX){
			divX = aActs[0];
			if (divX){
				var sClassName = divX.className;
				sClassName = sClassName.replace(/ act/g , "");
				divX.className = sClassName;
				var sAction = "house_" + sClassName.replace(/ /g, "") + "_" + iI;
				A.action.call(divX, "click", this.action, [this, sAction]);
				iI++;
			}
		}
	}
	V.oGen.bHouseLoaded = 1;
};



ENTER_HOUSE.prototype.initialize = function(bFirstStart){
	if (!this.iObjLogo){
		this.iObjLogo = A.nextObject();
		V.aAA[this.iObjLogo] = new ENTER_LOGO();
		V.aAA[this.iObjLogo].sIDPre = "register";
		V.aAA[this.iObjLogo].sParentObjName = "house";
		this.aObjAddresses[this.iAddressNo] = A.nextObject();
		V.aAA[this.aObjAddresses[this.iAddressNo]] = new ENTER_ADDRESSES();
		V.aAA[this.aObjAddresses[this.iAddressNo]].iAddressNo = this.iAddressNo;
		V.aAA[this.aObjAddresses[this.iAddressNo]].sParentName = "house";
		V.aAA[this.aObjAddresses[this.iAddressNo]].sIDPre = "register-4_1_";
	}
	if (V.oGen.oSession.sURLHash == "register"){
		this.menuActive(2);
		this.registerStart();
	} else {
		this.homeShow();
//			fCallback:V.aAA[iObjNo].initialize});
//V.oDiv.body.style.opacity = 1;
	}
	if (!bFirstStart){
		A.animate({divParent:V.oDiv.body, aFade:[0, 10]});
	}
};



ENTER_HOUSE.prototype.loginGoA = function(oData){
	console.log(oData)
	if (oData.sError){
		var that = A.findObject("house");
		V.oDiv.popuppageover.style.display = "block";
		V.oDiv.popuppageover.innerHTML = "";
		E.memo({divParent:V.oDiv.popuppageover,
			sText:oData.sError,aPos:[1, 1.5, 20, 4]
		});
		E.button({divParent:V.oDiv.popuppageover, that:that,
			sText:V.oData.oInit.oBanners.oGeneral.sOK, aPos:[-1, -1],
			sAction:"login_fail"
		})
	}
	if (oData.oUser.iUserID){
		E.popupClose();
		V.oData = oData;
		V.oGen.sDefaultButtonID = "";
		var iObjNo = A.nextObject();
		V.aAA[iObjNo] = new ENTER_INIT();
		A.animate({divParent:V.oDiv.body, aFade:[10, 0],	
			fCallback:V.aAA[iObjNo].initialize});
	} else {
	//	A.dg("0_0_99_g").style.display = "none";
	}
};



ENTER_HOUSE.prototype.loginGo = function(){
	var sEmailAddress = A.valueGet("login_Email");
	var sPassword = A.valueGet("login_Password");
	var divForgot = A.dg("login_ForgotPassword");
	if ((sEmailAddress) && (sPassword)){
		var bRememberme = A.valueGet("login_Rememberme");
		A.ajax({aURL:["init_loggingin",
			"emailaddress", sEmailAddress,
			"password", sPassword],
			fCallback:this.loginGoA});
	}
};



ENTER_HOUSE.prototype.loginForgotPasswordGoA = function(oData){
	var that = A.findObject("house");
	E.action("popupclose");
	E.popup(V.oData.oError[oData.sMessage]);
};



ENTER_HOUSE.prototype.loginForgotPasswordGo = function(){
	var sEmailAddress = A.valueGet("loginforgotpassword_Email");
	if (sEmailAddress){
		A.ajax({aURL:["init_forgotpassword",
			"emailaddress", sEmailAddress],
			fCallback:this.loginForgotPasswordGoA});
	}
};



ENTER_HOUSE.prototype.loginForgotPassword = function(){
	this.sEmailAddress = A.valueGet("login_Email");
	V.oDiv.popuppageover.style.display = "block";
	V.oDiv.popuppageover.innerHTML = "";
	E.build({sFormName:"oForm_LoginForgotPassword",
		divParent:V.oDiv.popuppageover, sObjectName:"house"});
	if (this.sEmailAddress){
		A.valueSet("loginforgotpassword_Email", this.sEmailAddress);
	}
};



ENTER_HOUSE.prototype.loginShow = function(){
	var divBox = E.popup({sTitle:"Login", sMessage:"", aButtons:-1,
		bPageOver:1});
	E.build({sFormName:"oForm_Login",
		divParent:divBox, sObjectName:"house"});
};



ENTER_HOUSE.prototype.menuActive = function(iIndex){
	var divMenu = A.gcn("menuactive");
	if (divMenu){
		divMenu.className = "menu";
	}
	divMenu = A.dg("menu_" + iIndex);
	if (divMenu){
		divMenu.className = "menuactive";
	}
	this.iMenuActive = iIndex;
};



ENTER_HOUSE.prototype.menu = function(){
	V.oDiv.header.innerHTML = "";
	var divMenuItems = E.div(V.oDiv.header, "menuitems");
	var aMenus = V.oData.oInit.aMenus.aRight;
	var iSize = aMenus.length;
	var iI = 0;
	for (iI = 0; iI < iSize; iI++){
		var oRecord = aMenus[iI];
		var divMenu = E.div(divMenuItems, "menu act", "menu_" + (iI + 1));
		var divDesc = E.div(divMenu, "menudesc");
		A.action.call
			(divMenu, "click", this.action, [this, "house_menuact_" + iI]);
		divDesc.textContent = oRecord.sName;
		if (!iI){
			E.div(divMenuItems, "menuspacer");
		}
	}
	return divMenuItems;
};



ENTER_HOUSE.prototype.registerActivateA = function(oData){
	E.action("dropdownboxclose");
	E.popup(V.oData.oError[oData.sMessage]);
	A.pushHistory("home");
};



ENTER_HOUSE.prototype.registerActivateGo = function(){
	A.ajax({aURL:["init_registeractivate",
		"link", this.sLink],
		fCallback:this.registerActivateA});
};



ENTER_HOUSE.prototype.registerActivate = function(sLink){
	V.oData.oError.oRegisterActivate.that = this;
	this.sLink = sLink;
	E.popup(V.oData.oError.oRegisterActivate)
};



ENTER_HOUSE.prototype.registerAddAddress = function(iPageNo){
	var sIDPre = "register";
	var iObjNo = A.nextObject();
	this.aObjAddresses.push(iObjNo);
	var iIndex = this.aObjAddresses.length - 1; 
	V.aAA[iObjNo] = new ENTER_ADDRESSES();
	V.aAA[iObjNo].sParentName = "house";
	V.aAA[iObjNo].sIDPre = sIDPre + "-4_1_" + iIndex + "_";
	var oEle = V.oData.oInit.oForm_RegisterStep4[1];
	oEle.sID = sIDPre + "-4_1_" + iIndex;
	var divParent = A.dg(sIDPre + "-4_1");
	oEle.divParent = divParent;
	oEle.iIndex = iIndex;
	E.address(oEle);
	this.aPlaces.push({id:0});
};



ENTER_HOUSE.prototype.registerCancel = function(){
	V.oDiv.work.innerHTML = "";
	this.homeShow();
};



ENTER_HOUSE.prototype.registerCheckPageInput = function(){
	var aInputs = ["text", "select-one", "password", "textarea", 
		"weekhours", "checkbox", "upload", "email", "address"];
	var aValues = [];
	var sErrors = "";
	var iJ = 0;
	var iK = 0;
	var sForm = "oForm_RegisterStep" + this.iRegisterStep;
	var oForm = V.oData.oInit[sForm];
	while (A.dg("register-" + this.iRegisterStep + "_" + iJ)){
		sID = "register-" + this.iRegisterStep + "_" + iJ;
		var divSave = A.dg(sID);
		var sType = divSave.type;
		if (aInputs.indexOf(sType) > -1){
			var sValue = A.valueGet(sID);
			var oEle = oForm[iJ];
			var sTestValue = sValue;
			if (oEle.sType == "select"){
				if (sValue == -1){
					sTestValue = "";
				} else {
					sTestValue = "x";
				}
			}
			if ((oEle.bURL) && (sValue)){
				if ((sValue.substr(0, 7) != V.oData.oInit.oBanners.oGeneral.sHTTP) &&
					(sValue.substr(0, 8) != V.oData.oInit.oBanners.oGeneral.sHTTPs)){
					sValue = V.oData.oInit.oBanners.oGeneral.sHTTP + sValue;
					A.valueSet(sID, sValue);
				}
			}
			if ((oEle.bRequired) && (!sTestValue)){
				sErrors += V.oData.oInit.oBanners.oGeneral.sRequired + 
					oEle.sText + ", ";
			}
			if ((oEle.bEmail) && (!A.validEmailAddress(sValue))){
				sErrors += V.oData.oInit.oBanners.oGeneral.sInvalid + 
					oEle.sText + ", ";
			}
			if (oEle.sType == "address"){
				var aTest = A.getJSON(sValue, 1);
				var iSize = aTest.length;
				var iRegionError = 0;
				for (iK = 0; iK < iSize; iK++){
					if (aTest[iK].region_id == -1){
						iRegionError = 1;
					} 
				}
				if (iRegionError){
					sErrors += V.oData.oInit.oBanners.oGeneral.sAddressInvalid + ", ";
				}
			}
			aValues.push({sID:sID, sValue:sValue});
		}
		iJ++;
	}
	if (this.iRegisterStep == 1){
		var sPassword = A.valueGet("register-1_4");
		var sPassword2 = A.valueGet("register-1_5");
		if (sPassword != sPassword2){
			sErrors += V.oData.oInit.oBanners.oGeneral.sPasswordsNotMatch + ", ";
		}
	}
	var bReturn =1;
	if (sErrors){
		var sError = V.oData.oError.oRequiredFields.sMessage;
		sError += "<br><br>" + sErrors.slice(0, -2) + ".";
		bReturn = 0;
		E.popup({sTitle:V.oData.oError.oRequiredFields.sTitle,
			sMessage:sError});
	}
bReturn=1
	return bReturn;
};



ENTER_HOUSE.prototype.registerDone = function(){
	E.popupClose();
	this.iNumBusinessHours = 1;
	this.sCompanyLogo = "";
	this.action("menu_house_3");
};



ENTER_HOUSE.prototype.registerNextStep = function(iPageNo){
	var bValid = this.registerCheckPageInput();
	if (!bValid){
		return;
	}
	window.scrollTo(0, 0);
	var divBox = A.dg("registerbox_" + this.iRegisterStep);
	divBox.style.display = "none";
	var divStep = A.dg("registerstep_" + this.iRegisterStep);
	var divIcon = divStep.childNodes[1];
	divIcon.className = "ele-stepicon";
	var sClassName = "ele-step";
	if (this.iRegisterStep == this.iRegisterStepMax){
		sClassName = "ele-steplast";
	}
	divStep.className = sClassName;
	if (!iPageNo){
		this.iRegisterStep++;
	} else {
		this.iRegisterStep = iPageNo;
	}
	if (this.iRegisterStep > this.iRegisterStepMax){
		this.iRegisterStep = 1;
	}
	if (V.oGen.bMobile){
		var oForm = V.oData.oInit.oForm_RegisterSteps;
		var aDesc = oForm[4 + this.iRegisterStep].sText.split("|");
		divDesc = divStep.childNodes[0];
		divDesc.textContent = aDesc[1];
		divDesc = divStep.childNodes[2];
		divDesc.textContent = aDesc[0];
		divStep.id = "registerstep_" + this.iRegisterStep;
	} else {
		divStep = A.dg("registerstep_" + this.iRegisterStep);
	}
	divIcon = divStep.childNodes[1];
	divIcon.className = "ele-stepiconactive";
	sClassName = "ele-step";
	if (this.iRegisterStep == this.iRegisterStepMax){
		sClassName = "ele-steplast";
	}
	divStep.className = sClassName + "active";
	divBox = A.dg("registerbox_" + this.iRegisterStep);
	divBox.style.display = "block";
	var divFirst = A.dg("register-" + this.iRegisterStep + "_0");
	if (divFirst){
		divFirst.focus();
	}
	if (this.iRegisterStep == 4){
		V.aAA[this.aObjAddresses[this.iAddressNo]].checkMain();
	}
};



ENTER_HOUSE.prototype.registerSaveCreateA = function(oData){
console.log(oData);
	var that = A.findObject("house");
	var sTitle = "";
	var sMessage = "";
	if (oData.oOrganisation.response_code == -1){
		sMessage = oData.oOrganisation.message + "<br>";
		sTitle = V.oData.oError.oRegistrationSuccess.sTitle; 
		sMessage += V.oData.oError.oRegistrationSuccess.sMessage;
		E.popup({sTitle:sTitle, sMessage:sMessage, 
			sAction:"register_done", that:that});
	} else {
		sTitle = V.oData.oError.oRegistrationFail.sTitle;
		sMessage = V.oData.oError.oRegistrationFail.sMessage;
		if (oData.oOrganisation.message){
			sMessage += "<br>" + oData.oOrganisation.message;
		}
		E.popup({sTitle:sTitle, sMessage:sMessage});
	}
};



ENTER_HOUSE.prototype.registerSaveCreate = function(){
	var aInputs = ["text", "select-one", "password", "textarea", 
		"weekhours", "checkbox", "upload", "email", "address"];
	var aValues = [];
	var sErrors = "";
	var iI = 0;
	var sID = "";
	for (iI = 1; iI <= this.iRegisterStepMax; iI++){
		var sForm = "oForm_RegisterStep" + iI;
		var oForm = V.oData.oInit[sForm];
		var iJ = 0;
		var iK = 0;
		while (A.dg("register-" + iI + "_" + iJ)){
			sID = "register-" + iI + "_" + iJ;
			var divSave = A.dg(sID);
			var sType = divSave.type;
			if (aInputs.indexOf(sType) > -1){
				var sValue = A.valueGet(sID);
				var oEle = oForm[iJ];
				var sTestValue = sValue;
				if (oEle.sType == "select"){
					if (sValue == -1){
						sTestValue = "";
					} else {
						sTestValue = "x";
					}
				}
				if ((oEle.bURL) && (sValue)){
					if ((sValue.substr(0, 7) != V.oData.oInit.oBanners.oGeneral.sHTTP) &&
						(sValue.substr(0, 8) != V.oData.oInit.oBanners.oGeneral.sHTTPs)){
						sValue = V.oData.oInit.oBanners.oGeneral.sHTTP + sValue;
						A.valueSet(sID, sValue);
					}
				}
				if ((oEle.bRequired) && (!sTestValue)){
					sErrors += V.oData.oInit.oBanners.oGeneral.sRequired + 
						oEle.sText + ", ";
				}
				if ((oEle.bEmail) && (!A.validEmailAddress(sValue))){
					sErrors += V.oData.oInit.oBanners.oGeneral.sInvalid + 
						oEle.sText + ", ";
				}
				if (oEle.sType == "address"){
					var aTest = A.getJSON(sValue, 1);
					var iSize = aTest.length;
					var iRegionError = 0;
					for (iK = 0; iK < iSize; iK++){
						if (aTest[iK].region_id == -1){
							iRegionError = 1;
						} 
					}
					if (iRegionError){
						sErrors += V.oData.oInit.oBanners.oGeneral.sAddressInvalid + ", ";
					}
				}
				aValues.push({sID:sID, sValue:sValue});
			}
			iJ++;
		}		
	}
	var sPassword = A.valueGet("register-1_4");
	var sPassword2 = A.valueGet("register-1_5");
	if (sPassword != sPassword2){
		sErrors += V.oData.oInit.oBanners.oGeneral.sPasswordsNotMatch + ", ";
	}
//sErrors = "";
	if (sErrors){
		var sError = V.oData.oError.oRequiredFields.sMessage;
		sError += "<br><br>" + sErrors.slice(0, -2) + ".";
		E.popup({sTitle:V.oData.oError.oRequiredFields.sTitle,
			sMessage:sError});
	} else {
		var iNumValues = aValues.length;
		var aURL = ["init_registercompany"];
		for (iI = 0; iI < iNumValues; iI++){
			aURL.push(aValues[iI].sID);
			aURL.push(aValues[iI].sValue);
		}
		aURL.push("logo");
		aURL.push(this.sCompanyLogo);
//		aURL.push("addresses");
//		aURL.push(JSON.stringify(this.aAddresses));
		A.ajax({aURL:aURL, fCallback:this.registerSaveCreateA});
	}
};



ENTER_HOUSE.prototype.registerStart = function(){
	var iI = 0;
	var oRec = {};
	A.findMainDivs();
	A.resizeBody();
	var divHO = A.gcn("bodyheaderout");
	var divMenu = this.menu();
	V.oDiv.header.style.width = "100%";
	var iW = V.oDiv.header.offsetWidth;
	var iW2 = divHO.offsetWidth;
	V.oDiv.work.innerHTML = "";
	if (V.oDiv.main.className == "bodymain"){
		V.oDiv.main.className = "bodymainli";
		var sWidth = "iLeftWidthRegister";
		var iRight  =(iW2 - iW - 112);
		if (V.oGen.bMobile){
			sWidth += "Mobile";
			iRight += 112;
		}
		var iWidth = V.oData.oInit.oSizes[sWidth];
//		divMenu.style.marginRight = iRight + "px";
		V.oDiv.left.style.width = iWidth + "px";
		V.oDiv.work.style.left =  iWidth + "px";
		var iWidth1 = V.oDiv.main.clientWidth;
		V.oDiv.work.style.width =  (iWidth1 - iWidth) + "px";
	}
	var divBox = E.div(V.oDiv.header, "worklogoregister");
	A.zindex(divBox);
	var divIcon = E.div(divBox, "worklogoicon");
	divIcon.innerHTML = "&#xE05B;";
	A.action.call(divIcon, "click", this.action, [this, "house_menu_3"]);
	var divMemo = E.memo({
		divParent:V.oDiv.header, sClassName:"registerbreadcrumb",
		sText:V.oData.oInit.oBanners.oGeneral.sBreadcrumbRegister});
	divMemo.style.left = (5 +V.oData.oInit.oSizes[sWidth]) + "px";
	A.pushHistory("register");

	E.action("dropdownboxclose");
	var that = A.findObject("house");
	V.oDiv.footer.innerHTML = "";
	V.oDiv.footer.style.height = 0;
	that.iRegisterStep = 1;
	this.menuActive(2);
	var oForm = V.oData.oInit.oForm_RegisterSteps;
	var iNumEles = oForm.length;
	for (iI = 0; iI < iNumEles; iI++){
		oRec = oForm[iI];
		oRec.that = that;
		oRec.divParent = V.oDiv.work;
		if (V.oGen.bMobile){
			if (iI < 6){
				E[oRec.sType](oRec);
			}
		} else {
			E[oRec.sType](oRec);
		}
	}
	
	divStep = A.dg("registerstep_" + this.iRegisterStep);
	var divIcon = divStep.childNodes[1];
	divIcon.className = "ele-stepiconactive";
	
	for (iI = 1; iI <= that.iRegisterStepMax; iI++){
		var divBox = A.dg("registerbox_" + iI);
		if (iI != 1){
			divBox.style.display = "none";
		} else {
			divBox.style.display = "block";
		}
		divBox.innerHTML = "";
		var sForm = "oForm_RegisterStep" + iI;
		oForm = V.oData.oInit[sForm];
		if (oForm){
			iNumEles = oForm.length;
			for (var iJ = 0; iJ < iNumEles; iJ++){
				oRec = oForm[iJ];
				oRec.that = that;
				oRec.divParent = divBox;
				oRec.sID = "register-" + iI + "_" + iJ;
 				if (oRec.sType != "address"){
 					var divX = E[oRec.sType](oRec);
 				} else {
					//do not show addresses, create container div
 					var sIDPre = "register";
					var divBox1 = E.div(oRec.divParent, "addressboxlist", oRec.sID);
					divBox1.type = "address";
					var iK =0; 
					this.aObjAddresses[iK] = A.nextObject();
					V.aAA[this.aObjAddresses[iK]] = new ENTER_ADDRESSES();
					V.aAA[this.aObjAddresses[iK]].sParentName = "house";
					V.aAA[this.aObjAddresses[iK]].sIDPre = sIDPre + "-4_1_" + iK + "_";
					var oEle = V.oData.oInit.oForm_RegisterStep4[1];
					oEle.sID = sIDPre + "-4_1_" + iK;
					var divParent1 = A.dg(sIDPre + "-4_1");
					oEle.divParent = divParent1;
					oEle.iIndex = iK;
					E.address(oEle);
					this.aPlaces = [{}];
				}
			}
		}
	}
	var divFirst = A.dg("register-" + this.iRegisterStep + "_0");
	if (divFirst){
		divFirst.focus();
	}
	E.weekhoursDefault("register-3_3", 0);
//	V.aAA[this.iObjLogo].showControls(0, 2);
};

/*
 * 				if (oRec.sType != "address"){
					var divX = E[oRec.sType](oRec);
				} else {
					//do not show addresses, create container div
					var divBox = E.div(divParent, "addressboxlist", oRec.sID);
					divBox.type = "address";
				}
			}
			if ((iI == 2) && (iJ == 4)){
				this.iWeekHoursObjNo = divX;
			}



*/

ENTER_HOUSE.prototype.resetPasswordA = function(oData){
	E.action("dropdownboxclose");
	E.popup(V.oData.oError[oData.sMessage]);
	A.pushHistory("home");
};



ENTER_HOUSE.prototype.activateUserA = function(oData){
	E.action("dropdownboxclose");
	E.popup(V.oData.oError[oData.sMessage]);
	A.pushHistory("home");
};



ENTER_HOUSE.prototype.resetPasswordGo = function(){
	var sNew = A.valueGet("resetpassword_new");
	var sRepeat = A.valueGet("resetpassword_repeat");
	var sError = "";
	if ((sNew) && (sNew != sRepeat)){
		sError += V.oData.oInit.oBanners.oGeneral.sPasswordsNotMatch + "<br>";
	}
	if (!sNew){
		sError += V.oData.oInit.oBanners.oGeneral.sPasswordEmpty + "<br>";
	}
	if (sError){
		V.oDiv.popuppageover.style.display = "block";
		A.zindex(V.oDiv.popuppageover);
		E.memo({divParent:V.oDiv.popuppageover,
			aPos:[1,1,20,3],sText:sError, bHTML:1
		});
		E.button({divParent:V.oDiv.popuppageover,
			sText:V.oData.oInit.oBanners.oGeneral.sOK,
				aPos:[-1, -1], that:this, sAction:"reseterror"});
	} else {
		A.ajax({aURL:["init_resetpassword", "password", sNew, "link", this.sLink], 
			fCallback:this.resetPasswordA,});
	}
};



ENTER_HOUSE.prototype.activateUserGo = function(){
	var sNew = A.valueGet("activatepassword_new");
	var sRepeat = A.valueGet("activatepassword_repeat");
	var sError = "";
	if ((sNew) && (sNew != sRepeat)){
		sError += V.oData.oInit.oBanners.oGeneral.sPasswordsNotMatch + "<br>";
	}
	if (!sNew){
		sError += V.oData.oInit.oBanners.oGeneral.sPasswordEmpty + "<br>";
	}
	if (sError){
		V.oDiv.popuppageover.style.display = "block";
		A.zindex(V.oDiv.popuppageover);
		E.memo({divParent:V.oDiv.popuppageover,
			aPos:[1,1,20,3],sText:sError, bHTML:1
		});
		E.button({divParent:V.oDiv.popuppageover,
			sText:V.oData.oInit.oBanners.oGeneral.sOK,
				aPos:[-1, -1], that:this, sAction:"reseterror"});
	} else {
		A.ajax({aURL:["init_activateuser", "password", sNew, "link", this.sLink], 
			fCallback:this.activateUserA,});
	}
};



ENTER_HOUSE.prototype.resetPassword = function(sLink){
	this.sLink = sLink;
	var sTitle = V.oData.oError.oResetPassword.sTitle;
	var sMessage = V.oData.oError.oResetPassword.sMessage;
	E.popup({sTitle:sTitle,sMessage:sMessage,
		that:this,
		sAction:"resetpassword",
		aButtons:[V.oData.oInit.oBanners.oGeneral.sOK, 
			V.oData.oInit.oBanners.oGeneral.sCancel],
		bPageOver:1
	});
	var divBox = V.oDiv.popup;
	E.build({sFormName:"oForm_HouseResetPassword",
		divParent:divBox, sObjectName:"init"});
	A.dg("resetpassword_new").focus();
};



ENTER_HOUSE.prototype.adduserActivate = function(sLink){
	this.sLink = sLink;
	var sTitle = V.oData.oError.oActivateUser.sTitle;
	var sMessage = V.oData.oError.oActivateUser.sMessage;
	E.popup({sTitle:sTitle,sMessage:sMessage,
		that:this,
		sAction:"activateuser",
		aButtons:[V.oData.oInit.oBanners.oGeneral.sOK, 
			V.oData.oInit.oBanners.oGeneral.sCancel],
		bPageOver:1
	});
	var divBox = V.oDiv.popup;
	E.build({sFormName:"oForm_HouseActivateUser",
		divParent:divBox, sObjectName:"init"});
	A.dg("activatepassword_new").focus();
};



ENTER_HOUSE.prototype.social = function(iIndex){
	iIndex -= 2;
	if (iIndex == 2){
		location.href = V.oData.oInit.oHouseAll.aSocials[iIndex].sURL;
	} else {
		window.open(V.oData.oInit.oHouseAll.aSocials[iIndex].sURL, "_child");
	}
};


		ENTER_HOUSE.bInited = 1;
	}
}
//== END OF HOUSE CLASS ========================================================



//== INIT CLASS ================================================================
function ENTER_INIT(){
	this.sName = "init";
	
	if (typeof ENTER_INIT.bInited == "undefined"){

ENTER_INIT.prototype.action = function(sAction, aParams){
	var that = A.actionA(this, sAction, this.aParams);
	var aAction = that.sAction.split("_");
console.log(that.sAction)
	switch (aAction[0]){
		case "user":
			switch (aAction[1]){
				case "showuser":
					that.that.showUser();
				break;
				case "logout":
					that.that.logout();
				break;
				case "changepassword":
					that.that.changePassword();
				break;
				case "profile":
					that.that.menuActive(5);
					E.action("dropdownboxclose");
					V.oDiv.work1.style.display = "block";
					V.oDiv.work2.style.display = "none";
					var oProfile = A.findObject("profile");
					if (!oProfile.bDataLoaded){
						A.ajax({aURL:["profile_data", "picbox", "_2"], 
							fCallback:oProfile.dataA,});
					}
				break;
			}
		break;
		
		case "menu":
			that.that.menuActive(aAction[2]);
			E.action("dropdownboxclose");
			V.oDiv.work1.style.display = "block";
			V.oDiv.work2.style.display = "none";
			switch (aAction[1]){
				case "home":
				break;
				case "createpost":
				break;
				case "myposts":
					break;
				case "profile":
				break;
				case "users":
				break;
			}
		break;
		
		case "changepassword":
			if (aAction[1] == "1"){
				E.popupClose();
			} else {
				that.that.changePasswordGo();
			}
		break;
	}
};
		
		

ENTER_INIT.prototype.changePasswordGoA = function(oData){
	console.log(oData)
	E.popupClose();
	if (oData.sMessage){
		E.popup(V.oData.oError[oData.sMessage]);
	}
	if (oData.sError){
		E.popup({sTitle:V.oData.oError.oDefault.sTitle, sMessage:oData.sError});
	}
};



ENTER_INIT.prototype.changePasswordGo = function(){
	var sIDPre = "changepassword_";
	var sCurrent = A.valueGet(sIDPre + "current");
	var sNew = A.valueGet(sIDPre + "new");
	var sRepeat = A.valueGet(sIDPre + "repeat");
	var sError = "";
	if (!sCurrent){
		sError += V.oData.oError.oRequiredCurrentPassword.sMessage + "<br>";
	}
	if ((sNew) && (sNew != sRepeat)){
		sError += V.oData.oInit.oBanners.oGeneral.sPasswordsNotMatch + "<br>";
	}
	if (!sNew){
		sError += V.oData.oInit.oBanners.oGeneral.sPasswordEmpty + "<br>";
	}
	if (sError){
		E.popupClose();
		E.popup({sTitle:V.oData.oError.oDefault.sTitle,
			sMessage:sError});
	} else {
		A.ajax({aURL:["init_changepassword", "current", sCurrent, "new", sNew], 
			fCallback:this.changePasswordGoA,});

	}
};



ENTER_INIT.prototype.changePassword = function(){
	E.action("dropdownboxclose");
	var sTitle = V.oData.oError.oChangePassword.sTitle;
	var sMessage = V.oData.oError.oChangePassword.sMessage;
	E.popup({sTitle:sTitle,sMessage:sMessage,
		that:this,
		sAction:"changepassword",
		aButtons:[V.oData.oInit.oBanners.oGeneral.sOK, 
			V.oData.oInit.oBanners.oGeneral.sCancel]
	});
	var divBox = V.oDiv.popup;
	divBox.style.height = "400px";
	E.build({sFormName:"oForm_HomeChangePassword",
		divParent:divBox, sObjectName:"init"});
	A.dg("changepassword_current").focus();
};



ENTER_INIT.prototype.initializeAddresses = function(){
	var aOptions = [V.oData.oInit.oEnum.sSelect[0]];
	var iNumAddresses = V.oData.oUser.aAddresses.length;
	var iNumRegions = V.oData.aRegions.length;
	var iI = 0;
	var iJ = 0;
	var iFound = 0;
	var bMain = -1;
	for (iI = 0; iI < iNumAddresses; iI++){
		iFound = 0;
		iJ = 0;
		var oRec = V.oData.oUser.aAddresses[iI];
		if (oRec.main){
			bMain = oRec.id;
		}
		while ((!iFound) && (iJ < iNumRegions)){
			var oRec1 = V.oData.aRegions[iJ];
			if (oRec1.iID == oRec.region_id){
				iFound = (iJ + 1);
			} else {
				iJ++;
			}
		}
		if (iFound){
			aOptions.push({iID:(oRec.id), 
				sValue:oRec.address_line1 + " " + oRec.address_line2 + " " + 
				V.oData.aRegions[(iFound - 1)].sValue});
		}
	}
	return {aOptions:aOptions, bMain:bMain};
};



ENTER_INIT.prototype.initialize = function(){
	var that = A.findObject("init");
	if (!V.oGen.oSession.sURLHash){
		V.oGen.oSession.sURLHash = "home";
	}
	A.resetBody();
	E.userbox({divParent:V.oDiv.header, that:that});
	V.oDiv.work1 = E.div(V.oDiv.work, "bodywork1");
	V.oDiv.work2 = E.div(V.oDiv.work, "bodywork2", "workpage_0");
	that.menu();
	E.div(V.oDiv.work1, "bodyworkpage", "workpage_5");
	var aObjs = ["HOME", "CREATEPOST", "MYPOSTS", "PROFILE", "USERS"];
	var iNumObjs = aObjs.length;
	for (var iI = 0; iI < iNumObjs; iI++){
		var iObjNo = A.nextObject();
		V.aAA[iObjNo] = new window["ENTER_" + aObjs[iI]]();
		V.aAA[iObjNo].initialize();
	}
	
	V.aThemeColors = V.oData.oInit.aThemes[0].aColors;
	var divLogo = E.div(V.oDiv.header, "worklogo");
	divLogo.style.marginTop = -V.oData.oInit.oSizes.iHeaderHeight + "px";
	var divIcon = E.div(divLogo, "worklogoicon");
	divIcon.innerHTML = "&#xE05B;";
	A.action.call(divIcon, "click", that.action, [that, "menu_home_1"]);
	var sURLHash = V.oGen.oSession.sURLHash;
	if (!sURLHash){
		sURLHash = "home";
		V.oGen.oSession.sURLHash = sURLHash;
	}
	switch (sURLHash){
		case "home":
			that.action("menu_home_1");		
		break;
		case "createpost":
			that.action("menu_createpost_2");		
		break;
		case "myposts":
			that.action("menu_myposts_3");		
			if (V.oGen.oSession.iTab3){
				E.action("tab_3_" + V.oGen.oSession.iTab3);
			}
		break;
		case "users":
			that.action("menu_users_4");		
		break;
		case "profile":
			that.action("menu_profile_5");
			if (V.oGen.oSession.iTab5){
				E.action("tab_5_" + V.oGen.oSession.iTab5);
			}
		break;
	}
};



ENTER_INIT.prototype.logout = function(){
	sessionStorage.clear();
	V.oDiv.work1 = 0;
	V.oDiv.cloak.style.opacity = 0.4;
	V.oDiv.cloak.style.display = "block";
	V.oDiv.cloaktop.style.display = "block";
	A.zindex(V.oDiv.cloak);
	A.zindex(V.oDiv.cloaktop);
	A.ajax({aURL:["init_loggingout"], 
		fCallback:A.initializeA, bDontShowWait:1});
};



ENTER_INIT.prototype.menuActive = function(iIndex){
	iIndex = parseInt(iIndex);
	var aHashes = "|home|createpost|myposts|users|profile".split("|");
	A.pushHistory(aHashes[iIndex]);
	var divActive = document.getElementsByClassName("menu_initactive");
	var divPage = 0;
	if (divActive[0]){
		divActive = divActive[0];
		divActive.className = "menu_init";
		var aID = divActive.id.split("_");
		divPage = A.dg("workpage_" + aID[2]);
		divPage.style.display = "none";
	}
	divActive = A.dg("menu_init_" + iIndex);
	if (divActive){
		divActive.className = "menu_initactive";
	}
	divPage = A.dg("workpage_" + iIndex);
	var aBreadcrumbs = ["Dashboard"];
	switch (iIndex){
		case 2:
			aBreadcrumbs.push("Create Post");
		break;
		case 3:
			aBreadcrumbs.push("My Posts");
			var iTabActive = A.findObject("myposts").iTabActive;
			switch (iTabActive){
				case 1:
					aBreadcrumbs.push("Active");
				break;
				case 2:
					aBreadcrumbs.push("Drafts");
				break;
				case 3:
					aBreadcrumbs.push("History");
				break;
			}
		break;
		case 4:
			aBreadcrumbs.push("Users");
		break;
		case 5:
			aBreadcrumbs.push("Company Profile");
		break;
	}
	divPage.style.display = "block";
	E.breadcrumbs(aBreadcrumbs);
};



ENTER_INIT.prototype.menu = function(){
	var iI = 0;
	var aMenus = V.oData.oInit.aMenus;
	var iTop = 7;
	var iSize = aMenus.length;
	E.div(V.oDiv.left, "menu_initspacer");
	
	for (iI = 0; iI < iSize; iI++){
		var oRecord = aMenus[iI];
		var oEle = {sType:"init", aPos:[0, 0, 0, 0, 0, 3], oObj:oRecord, 
			iIndex: (iI + 1)};
		iTop += 2.38;
		var divBox = E.div(V.oDiv.left, "menu_init",
			"menu_init_" + (iI + 1));
		var sAction = oRecord.sName.replace(/ /g, "").toLowerCase();
//		divBox.style.top = oEle.aPos[1] + "em";
		if ((iI === 0) || (iI == (iSize -1))){
			divBox.style.display = "none";
		} else {
			var divText = E.div(divBox, "menu_" + oEle.sType + "_text");
			divText.textContent = oRecord.sName;
			A.action.call(divBox, "click", this.action, 
				[this, "menu_" + sAction + "_" + (iI + 1)]);
		}
		
		if (iI == 1){
			divBox.className = "menu_init menu_initfirst";
		}
		divPage = E.div(V.oDiv.work1, "bodyworkpage", "workpage_" + (iI + 1));
	}
};



ENTER_INIT.prototype.showUser = function(){
	var aDivButtons = A.gcn("ele-buttonboxuserbox", 1);
	while (aDivButtons[0]){
		aDivButtons[0].parentNode.removeChild(aDivButtons[0]);
	}
	var iHeight = parseInt(V.oDiv.userbox.style.height);
	if (iHeight == 160){
		iHeight = 46;
		V.oGen.bUserBoxOn = 0;
	} else {
		iHeight = 160;
		V.oGen.bUserBoxOn = 1;
	}
	V.oDiv.userbox.style.height = iHeight + "px";
	A.zindex(V.oDiv.userbox);
	E.build({sFormName:"oForm_User", 
		divParent:V.oDiv.userbox, sObjectName:"init"});
};



		ENTER_INIT.bInited = 1;
	}
}
//== END OF INIT CLASS =========================================================



//== HOME CLASS ================================================================
function ENTER_HOME(){
	this.sName = "home";
	
	if (typeof ENTER_HOME.bInited == "undefined"){


ENTER_HOME.prototype.action = function(sAction, aParams){
	var that = A.actionA(this, sAction, this.aParams);
	switch (that.sAction){
		case "logout":
			that.that.logout();
		break;
	}
};
		
		
		
ENTER_HOME.prototype.initialize = function(){
	var sIDBox = "workpage_1";
	var divBox = A.dg(sIDBox);
	var divBox1 = 0;
	var iI = 0;
	var iJ = 0;
	divBox.innerHTML = "";
	var sForm = "oForm_Home";
	var oForm = V.oData.oInit[sForm];
	if (oForm){
		var iNumEles = oForm.length;
		for (iI = 0; iI < iNumEles; iI++){
			var oRec = oForm[iI];
			oRec.that = this;
			if (oRec.iTabNo){
				oRec.divParent = A.dg(sIDBox + "_" + oRec.iTabNo);
			} else {
				oRec.divParent = divBox;
			}
			oRec.sID = "home-" + iI;
			var divBox1 = E[oRec.sType](oRec);
			V.oDiv["homework" + iI] = divBox;
		}
	}
	
	var iSize1 = V.oData.oDashboard.aCommitments.length;
	var aUniquePost = [];
	var aUniqueType = [];
	for (iI = 0; iI < iSize1; iI++){
		var oRec = V.oData.oDashboard.aCommitments[iI];
		if (aUniquePost.indexOf(oRec.post_id) == -1){
			aUniquePost.push(oRec.post_id);
			aUniqueType.push([oRec.need_type, oRec.title]);
		}
	}
	iSize = aUniquePost.length;
	if (aUniquePost.length > 3){
		iSize = 3;
	}
	for (iI = 0; iI < iSize; iI++){
		var sClassNamePre = "homebox" + aUniqueType[iI][0];
		divBox1 = E.div(divBox, sClassNamePre);
		divBox1.style.top = (iI * 180 + 30) + "px";
		var divBoxIn = E.div(divBox1, "homeboxin");
		var divNum = E.div(divBoxIn, sClassNamePre + "num");
		var iNum = 0;
		for (iJ = 0; iJ < iSize1; iJ++){
			var oRec = V.oData.oDashboard.aCommitments[iJ];
			if (oRec.post_id == aUniquePost[iI]){
				iNum++;
			}
		}
		divNum.textContent = iNum;
		var divText = E.div(divBoxIn, "homeboxtext");
		divText.textContent = V.oData.oInit.oHomeBox.sResponse;
		divText = E.div(divBoxIn, "homeboxtext2");
		var sText = V.oData.oInit.oHomeBox.sToYourPost;
		sText += "<span class='homeboxblue'> " + 
		aUniqueType[iI][1] + "</span>"; 
		divText.innerHTML = sText;
	}	

	divBox = A.dg("home-0");
	var divBoxCanvas = E.div(divBox, "dashboardpie");
	E.memo({divParent:divBox, sClassName:"dashboardpieheading", 
		sText:V.oData.oInit.oHomeBox.sYourPostTypes});
	var divCanvas = E.div({divParent:divBoxCanvas, sType:"canvas"});
	divCanvas.width = "220";
	divCanvas.height = "220";
	var oContext = divCanvas.getContext("2d");
	var iLastEnd = 0;
	var aPieData = V.oData.oDashboard.aPostTypes;
	var iTotal = 0;
	var aColors = [
		V.oData.oInit.aThemes[3].aColors[0], 
		V.oData.oInit.aThemes[1].aColors[0], 
		V.oData.oInit.aThemes[0].aColors[10]];

	for(iI = 0; iI < aPieData.length; iI++){
		iTotal += aPieData[iI];
	}
	var iTop = 110;
	if (!iTotal){
		aPieData = [1, 1, 1, 1];
		iTotal = 3;
	}
	for (iI = 0; iI < 3; iI++) {
		var divLegendBox = E.div(divBox, "dashboardpielegend");
		divLegendBox.style.top = iTop + "px";
		iTop += 40;
		var divColor = E.div(divLegendBox, "dashboardpielegendcolor");
		divColor.style.background = aColors[iI];
		var divText = E.div(divLegendBox, "dashboardpielegendtext");
		var sText = "";
		if (!aPieData[3]){
			sText = aPieData[iI];
		}
		divText.textContent = sText + " " + 
			V.oData.oInit.oHomeBox.aTypes[iI];
		oContext.fillStyle = aColors[iI];
		oContext.beginPath();
		oContext.moveTo(divCanvas.width / 2, divCanvas.height / 2);
		oContext.arc(divCanvas.width / 2, divCanvas.height / 2,
			divCanvas.height / 2, iLastEnd, 
			iLastEnd + (Math.PI * 2 * (aPieData[iI] / iTotal)), false);
		oContext.lineTo(divCanvas.width / 2, divCanvas.height / 2);
		oContext.fill();
		iLastEnd += Math.PI * 2 * (aPieData[iI] / iTotal);
	}
	
	divBox = A.dg("home-1");
	var aText = V.oData.oInit.oHomeBox.sMemberSince.split("|");
	divBoxIn = E.div(divBox, "homebox4in");
	divBoxIn.innerHTML = aText[0];
	divBoxIn = E.div(divBox, "homebox4in2");
	divBoxIn = E.div(divBox, "homebox4num");
	divBoxIn.textContent = (parseInt(V.oData.oDashboard.iNumDaysMember) + 1);
	divBoxIn = E.div(divBox, "homebox4days");
	divBoxIn.textContent = aText[1];
};



		ENTER_HOME.bInited = 1;
	}
}
//== END OF HOME CLASS =========================================================



//== CREATEPOST CLASS ==========================================================
function ENTER_CREATEPOST(){
	this.sName = "createpost";
	this.oSelectedType = {iID:0};
	
	if (typeof ENTER_CREATEPOST.bInited == "undefined"){


ENTER_CREATEPOST.prototype.action = function(sAction, aParams){
	var that = A.actionA(this, sAction, this.aParams);
	var iI = 0;
	var sColor = "";
	var divBox = 0;
	var divButton = 0;
	var aAction = that.sAction.split("_");
console.log(that.sAction);
	switch (aAction[0]){
		case "type":
			var divBoxes = document.getElementsByClassName("createpostbox");
			var iNumBoxes = divBoxes.length;
			for (iI = 0; iI < iNumBoxes; iI++){
				divBoxes[iI].style.display = "none";
			}
			divButton = document.getElementsByClassName
				("ele-buttonboxcreatepostactive")[0];
			if (divButton){
				divButton.className = "ele-buttonboxcreatepost"; 
				sColor = V.oData.oInit.aThemes[0].aColors[0];
				divButton.style.background = sColor;
			}
			that.that.oSelectedType = {sType:aAction[1], iID:0};
			switch (aAction[1]){
				case "Items":
					divButton = A.dg("createpost-5");
					divButton.className = "ele-buttonboxcreatepostactive";
					sColor = V.oData.oInit.aThemes[2].aColors[0];
					divButton.style.background = sColor;
					divBox = A.dg("createpost-11");
					divBox.style.display = "block";
					that.that.oSelectedType.iID = 11;
				break;
				case "Time":
					divButton = A.dg("createpost-6");
					divButton.className = "ele-buttonboxcreatepostactive";
					sColor = V.oData.oInit.aThemes[3].aColors[0];
					divButton.style.background = sColor;
					divBox = A.dg("createpost-12");
					divBox.style.display = "block";
					that.that.oSelectedType.iID = 12;
				break;
				case "Funds":
					divButton = A.dg("createpost-7");
					divButton.className = "ele-buttonboxcreatepostactive";
					sColor = V.oData.oInit.aThemes[1].aColors[0];
					divButton.style.background = sColor;
					divBox = A.dg("createpost-13");
					divBox.style.display = "block";
					that.that.oSelectedType.iID = 13;
				break;
			}
		break;
		
		case "save":
			switch (aAction[1]){
				case "cancel":
					that.that.cancel();
				break;
				case "draft":
					that.that.create(1);
				break;
				case "postnow":
					that.that.create(2);
				break;
			}
		break;
		
		case "cancel":
			switch (aAction[1]){
			case "0":
				E.popupClose();
				that.that.initialize();
			break;
			case "1":
				E.popupClose();
			break;
			}
		break;
		
	}
};
		
		

ENTER_CREATEPOST.prototype.cancel = function(){
	var sTitle = V.oData.oError.oCreatePostCancel.sTitle;
	var sMessage = V.oData.oError.oCreatePostCancel.sMessage;
	E.popup({
		sTitle:sTitle,
		sMessage:sMessage,
		that:this,
		sAction:"cancel",
		aButtons:[V.oData.oInit.oBanners.oGeneral.sOK, 
			V.oData.oInit.oBanners.oGeneral.sCancel]
	});
};



ENTER_CREATEPOST.prototype.createA = function(oData){
	console.log(oData);
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
	if (oData.sError){
		E.popup({sTitle:V.oData.oError.oDefault.sTitle, sMessage:oData.sError});
		return;
	}
	
	var iGridNo = 0;
	var oGrid = {};
	var that = A.findObject("myposts");
	if (oData.aData1){
		iGridNo = 1;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData1.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}	
	if (oData.aData2){
		iGridNo = 2;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData2.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}	
	if (oData.aData3){
		iGridNo = 3;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData3.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}
	that = A.findObject("createpost");
	that.oSelectedType = {iID:0};
	that.initialize();
};



ENTER_CREATEPOST.prototype.create = function(iPostStatus){
	if (!this.oSelectedType.iID){
		E.popup(V.oData.oError.oChoosePostType);
		return;
	}
	var oValues = this.values();
	if (!oValues.sErrors){
		var aValues = oValues.aValues;
		var iNumValues = aValues.length;
		var aURL = ["createpost_create", "type", this.oSelectedType.sType,
			"poststatus", iPostStatus];
		for (iI = 0; iI < iNumValues; iI++){
			aURL.push(aValues[iI].sID);
			aURL.push(aValues[iI].sValue);
		}
		aURL.push("id");
		aURL.push(V.oData.oUser.iOrganisationID);
		A.ajax({aURL:aURL, fCallback:this.createA});
	}
};



ENTER_CREATEPOST.prototype.initialize = function(){
	var divBox = A.dg("workpage_2");
	var iJ = 0;
	divBox.innerHTML = "";
	var sForm = "oForm_CreatePost";
	var oForm = V.oData.oInit[sForm];
	if (oForm){
		var iNumEles = oForm.length;
		for (iJ = 0; iJ < iNumEles; iJ++){
			var oRec = oForm[iJ];
			oRec.that = this;
			oRec.divParent = divBox;
			if (oRec.iIDParent){
				oRec.divParent = A.dg("createpost-" + oRec.iIDParent);
			}
			if (!oRec.sID){
				oRec.sID = "createpost-" + iJ;
			}
			E[oRec.sType](oRec);
		}
	}
	var that = A.findObject("init");
	var aOptions = that.initializeAddresses();
	var sID = "createpost-14";
	var divSelect = A.dg(sID);
	E.selectPopulate(divSelect, aOptions.aOptions);
	A.valueSet(sID, aOptions.bMain);
	sID = "createpost-21";
	divSelect = A.dg(sID);
	E.selectPopulate(divSelect, aOptions.aOptions);
	A.valueSet(sID, aOptions.bMain);
	A.valueSet(sID, aOptions.bMain);
};



ENTER_CREATEPOST.prototype.values = function(){
	var aInputs = ["text", "select-one", "password", "textarea", "weekhours",
		"time", "email"];
	var sIDPre = "createpost-";
	var iI = 0;
	var aValues = [];
	var sErrors = "";
	var sID = "";
	var oForm = V.oData.oInit.oForm_CreatePost;
	while (A.dg(sIDPre + iI)){
		sID = sIDPre + iI;
		var divSave = A.dg(sID);
		var sType = divSave.type;
		if (aInputs.indexOf(sType) > -1){
			var sValue = A.valueGet(sID);
			var oEle = oForm[iI];
			var sTestValue = sValue;
			if (oEle.sType == "select"){
				if (sValue == -1){
					sTestValue = "";
				} else {
					sTestValue = "x";
				}
			}
			var bError = 0;
			if ((oEle.bRequired) && (!sTestValue)){
				if (!oEle.iIDParent){
					bError = 1;
				} else {
					if (this.oSelectedType.iID == oEle.iIDParent){
						bError = 1;
					}
				}
			}
			if (bError){
				sErrors += V.oData.oInit.oBanners.oGeneral.sRequired + 
					oEle.sText + ", ";
			}
			if ((oEle.bEmail) && (!A.validEmailAddress(sValue))){
				sErrors += V.oData.oInit.oBanners.oGeneral.sInvalid + 
					oEle.sText + ", ";
			}
			aValues.push({sID:sID, sValue:sValue});
		}
		iI++;
	}
	if (sErrors){
		var sError = V.oData.oError.oRequiredFields.sMessage;
		sError += "<br><br>" + sErrors.slice(0, -2) + ".";
		E.popup({sTitle:V.oData.oError.oRequiredFields.sTitle,
			sMessage:sError});
	}
	return {sErrors:sErrors, aValues:aValues};
};



		ENTER_CREATEPOST.bInited = 1;
	}
}
//== END OF CREATEPOST CLASS ===================================================



//== MYPOSTS CLASS =============================================================
function ENTER_MYPOSTS(){
	this.sName = "myposts";
	this.aGridNos = [0];
	this.aGridRecord = [];
	this.iActiveTab = 1;
	
	if (typeof ENTER_MYPOSTS.bInited == "undefined"){


ENTER_MYPOSTS.prototype.action = function(sAction, aParams){
	var that = A.actionA(this, sAction, this.aParams);
	var aAction = that.sAction.split("_");
console.log(that.sAction);
	switch (aAction[0]){
	
		case "activeedit":
			switch (aAction[1]){
				
				case "save":
					that.that.activeSave();
				break;
				
				case "cancel":
					E.action("editclose");
				break;
				
				case "edit":
					that.that.activeEditEdit();
				break;
				
				case "markascomplete":
					that.that.activeMarkAsComplete();
				break;
				
				case "markasdraft":
					that.that.activeMarkAsDraft();
				break;
				
				case "duplicatetodrafts":
					that.that.activeDuplicateToDrafts(1);
				break;
				
			}
		break;
		
		case "draftsedit":
			switch (aAction[1]){
			
				case "edit":
					that.that.draftsEditEdit();
				break;
				
				case "save":
					that.that.draftsSave();
				break;
				
				case "cancel":
					E.action("editclose");
				break;
				
				case "delete":
					that.that.draftsDelete();
				break;
				
				case "savedraft":
					that.that.draftsSave(0);
				break;
				
				case "postnow":
					that.that.draftsPostNow();
				break;
				
			}
		break;
		
		case "historyedit":
			switch (aAction[1]){
			
				case "delete":
					that.that.historyDelete();
				break;
				
				case "edit":
					that.that.historyEditEdit();
				break;
				
				case "save":
					that.that.historySave();
				break;
				
				case "markascomplete":
					that.that.historyMarkAsComplete();
				break;
				
				case "markasdraft":
					that.that.historyMarkAsDraft();
				break;
				
				case "duplicatetodrafts":
					that.that.historyDuplicateToDrafts(1);
				break;
				
			}
		break;
		
		case "draftsdelete":
			switch (aAction[1]){
			case "0":
				E.popupClose();
				that.that.draftsDeleteGo();
			break;
			case "1":
				E.popupClose();
			break;
			}
		break;
		
		case "historydelete":
			switch (aAction[1]){
			case "0":
				E.popupClose();
				that.that.historyDeleteGo();
			break;
			case "1":
				E.popupClose();
			break;
			}
		break;
		
		case "parent":
			var iGridNo = parseInt(aAction[2]);
			var iRowNo = parseInt(aAction[3]);
			that.that.aGridRecord = V.aAA[iGridNo].findRecordIndex(iGridNo, iRowNo);
			if (that.that.aGridRecord){
				switch (aAction[1]){
					case "editgrid":
						var aBreadcrumbs = V.oGen.aBreadcrumbs;
						aBreadcrumbs.push("Edit");
						E.breadcrumbs(aBreadcrumbs);
						if (aAction[2] == that.that.aGridNos[1]){
							that.that.activeEdit();
						}
						if (aAction[2] == that.that.aGridNos[2]){
							that.that.draftsEdit();
						}
						if (aAction[2] == that.that.aGridNos[3]){
							that.that.historyEdit();
						}
					break;
								
					case "activeduplicatetodraftsgrid":
						that.that.activeDuplicateToDrafts();
					break;
					
					case "activeedit":
						that.that.activeEdit();
					break;
					
					case "draftsduplicategrid":
						that.that.draftsDuplicate();
					break;
					
					case "draftsedit":
						that.that.draftsEdit();
					break;
					
					case "historydeletegrid":
						that.that.historyDelete();
					break;
					
					case "historyedit":
						that.that.historyEdit();
					break;
					
				}
			}
		break;
		
		case "datagrid":
		break;
		
	}
	
};
		
		

ENTER_MYPOSTS.prototype.activeDuplicateToDraftsA = function(oData){
	console.log(oData);
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
	if (oData.sError){
		E.popup({sTitle:V.oData.oError.oDefault.sTitle, sMessage:oData.sError});
		return;
	}
	var that = A.findObject("myposts");
	if (oData.aData2){
		iGridNo = 2;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData2.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
		oGrid.sort();
	}
	if (oData.bEdit){
		E.action("editclose");
	}
};



ENTER_MYPOSTS.prototype.activeDuplicateToDrafts = function(bEdit){
	var aValues = this.aGridRecord;
	var iNumValues = aValues.length;
	var aURL = ["myposts_activeduplicatetodrafts"];
	for (iI = 0; iI < iNumValues; iI++){
		aURL.push("col_" + iI);
		aURL.push(aValues[iI]);
	}
	aURL.push("id");
	aURL.push(V.oData.oUser.iOrganisationID);
	aURL.push("edit");
	if (bEdit){
		aURL.push(1);
	} else {
		aURL.push(0);
	}
	A.ajax({aURL:aURL, fCallback:this.activeDuplicateToDraftsA});
};



ENTER_MYPOSTS.prototype.activeMarkAsComplete = function(){
	A.ajax({aURL:["myposts_saveactivemarkascomplete", "id", this.aGridRecord[0]], 
		fCallback:this.activeSaveA});
};



ENTER_MYPOSTS.prototype.activeMarkAsDraft = function(){
	A.ajax({aURL:["myposts_saveactivemarkasdraft", "id", this.aGridRecord[0]], 
		fCallback:this.activeSaveA});
};



ENTER_MYPOSTS.prototype.activeSaveA = function(oData){
	console.log(oData);
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
	if (oData.sError){
		E.popup({sTitle:V.oData.oError.oDefault.sTitle, sMessage:oData.sError});
		return;
	}
	var iGridNo = 0;
	var oGrid = {};
	var that = A.findObject("myposts");
	if (oData.aData1){
		iGridNo = 1;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData1.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}	
	if (oData.aData2){
		iGridNo = 2;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData2.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}	
	if (oData.aData3){
		iGridNo = 3;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData3.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}
	
	E.action("editclose");
};



ENTER_MYPOSTS.prototype.activeSave = function(){
	var oValues = this.values("active");
	if (!oValues.sErrors){
		var aValues = oValues.aValues;
		var iNumValues = aValues.length;
		var aURL = ["myposts_saveactive", "id", this.aGridRecord[0]];
		for (iI = 0; iI < iNumValues; iI++){
			aURL.push(aValues[iI].sID);
			aURL.push(aValues[iI].sValue);
		}
		A.ajax({aURL:aURL, fCallback:this.activeSaveA});
	}
};



ENTER_MYPOSTS.prototype.activeEditEdit = function(){
	V.oDiv.work1.style.display = "none";
	V.oDiv.work2.style.display = "block";
	V.oDiv.work2.innerHTML = "";

	var oGrid = V.aAA[this.aGridNos[1]];
	var sForm = "oForm_MyPostsEditActiveEdit";
	var sIDPre = "mypostseditactiveedit-";
	var oForm = V.oData.oInit[sForm];
	var divBox = V.oDiv.work2;
	var sIDBox = "workpage_0";
	var iNumEles = oForm.length;
	for (var iJ = 0; iJ < iNumEles; iJ++){
		oRec = oForm[iJ];
		oRec.that = this;
		if (oRec.iTabNo){
			oRec.divParent = A.dg(sIDBox + "_" + oRec.iTabNo);
		} else {
			oRec.divParent = divBox;
		}
		oRec.sID = sIDPre + iJ;
		var divEle = E[oRec.sType](oRec);
	}
	E.action("tab_0_1");
	var divIcon = E.div(A.dg("workpage_0_1"), "iconeditclose");
	divIcon.innerHTML = "&#xE06E;";
	A.action.call
		(divIcon, "click", E.action, [E, "editclose"]);
	A.valueSet(sIDPre + "2", this.aGridRecord[1]);
	A.valueSet(sIDPre + "3", this.aGridRecord[6]);
	var that = A.findObject("init");
	var aOptions = that.initializeAddresses();
	var divSelect = A.dg(sIDPre + "4");
	E.selectPopulate(divSelect, aOptions.aOptions);
	A.valueSet(sIDPre + "4", this.aGridRecord[5]);
	var aDates = this.aGridRecord[4].split(" ");-
	A.valueSet(sIDPre + "5", aDates[0]);
	if (!aDates[2]){
		aDates[2] = "";
	}
	A.valueSet(sIDPre + "6", aDates[2]);
};



ENTER_MYPOSTS.prototype.activeEdit = function(){
	V.oDiv.work1.style.display = "none";
	V.oDiv.work2.style.display = "block";
	V.oDiv.work2.innerHTML = "";

	var oGrid = V.aAA[this.aGridNos[1]];
	var sForm = "oForm_MyPostsEditActive";
	var sIDPre = "mypostseditactive-";
	var oForm = V.oData.oInit[sForm];
	var divBox = V.oDiv.work2;
	var sIDBox = "workpage_0";
	var iNumEles = oForm.length;
	for (var iJ = 0; iJ < iNumEles; iJ++){
		oRec = oForm[iJ];
		oRec.that = this;
		if (oRec.iTabNo){
			oRec.divParent = A.dg(sIDBox + "_" + oRec.iTabNo);
		} else {
			oRec.divParent = divBox;
		}
		oRec.sID = sIDPre + iJ;
		var divEle = E[oRec.sType](oRec);
		if (oRec.sType == "grid"){
			this.aGridNos[4] = parseInt(divEle.id.split("_")[1]);
		}
	}
	E.action("tab_0_1");
	var divIcon = E.div(A.dg("workpage_0_1"), "iconeditclose");
	divIcon.innerHTML = "&#xE06E;";
	A.action.call
		(divIcon, "click", E.action, [E, "editclose"]);
	A.dg(sIDPre + "2").textContent = this.aGridRecord[1];
	A.dg(sIDPre + "3").textContent = this.aGridRecord[3];
	A.dg(sIDPre + "4").textContent = this.aGridRecord[6];
	var iSize = V.oData.oUser.aAddresses.length;
	var iFound = 0;
	iJ = 0;
	while ((!iFound) && (iJ < iSize)){
		var oRec = V.oData.oUser.aAddresses[iJ];
		if (oRec.id == this.aGridRecord[5]){
			iFound = iJ + 1;
		} else {
			iJ++;
		}
	}
	if (iFound){
		var iSize = V.oData.aRegions.length;
		var iFound = 0;
		iJ = 0;
		while ((!iFound) && (iJ < iSize)){
			var oRec1 = V.oData.aRegions[iJ];
			if (oRec1.iID == oRec.region_id){
				iFound = iJ + 1;
			} else {
				iJ++;
			}
		}
		var sTown = "";
		if (iFound){
			sTown = oRec1.sValue;
		}
		var that = A.findObject("init");
		var aOptions = that.initializeAddresses();
		A.dg(sIDPre + "6").innerHTML = oRec.address_line1 + "<br>" +
			oRec.address_line2 + "<br>" + sTown;
	}
	var aDates = this.aGridRecord[4].split(" ");-
	A.valueSet(sIDPre + "5", aDates[0]);
	if (!aDates[2]){
		aDates[2] = "";
	}
	A.dg(sIDPre + "8").innerHTML += "<br>" + aDates[0];
	A.dg(sIDPre + "9").innerHTML += "<br>" + aDates[2];
	A.ajax({aURL:["grid_data", "name", "mypostsedit", "no", 
		this.aGridNos[4], "id", this.aGridRecord[0]], 
		fCallback:V.aAA[this.aGridNos[4]].dataA});
};



ENTER_MYPOSTS.prototype.draftsDeleteGo = function(){
	var aValues = this.aGridRecord;
	var iNumValues = aValues.length;
	var aURL = ["myposts_draftsdelete"];
	aURL.push("id");
	aURL.push(this.aGridRecord[0]);
	A.ajax({aURL:aURL, fCallback:this.draftsDeleteA});
};



ENTER_MYPOSTS.prototype.draftsDeleteA = function(oData){
	console.log(oData);
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
	if (oData.sError){
		E.popup({sTitle:V.oData.oError.oDefault.sTitle, sMessage:oData.sError});
		return;
	}
	var that = A.findObject("myposts");
	if (oData.aData2){
		iGridNo = 2;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData2.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
		oGrid.sort();
	}
	E.action("editclose");
};



ENTER_MYPOSTS.prototype.draftsDelete = function(){
	var sTitle = V.oData.oError.oMyPostsDelete.sTitle;
	var sMessage = V.oData.oError.oMyPostsDelete.sMessage;
	E.popup({
		sTitle:sTitle,
		sMessage:sMessage,
		that:this,
		sAction:"draftsdelete",
		aButtons:[V.oData.oInit.oBanners.oGeneral.sOK, 
			V.oData.oInit.oBanners.oGeneral.sCancel]
	});
};



ENTER_MYPOSTS.prototype.draftsDuplicateA = function(oData){
	console.log(oData);
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
	if (oData.sError){
		E.popup({sTitle:V.oData.oError.oDefault.sTitle, sMessage:oData.sError});
		return;
	}
	var that = A.findObject("myposts");
	if (oData.aData2){
		iGridNo = 2;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData2.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
		oGrid.sort();
	}	
};



ENTER_MYPOSTS.prototype.draftsDuplicate = function(){
	var aValues = this.aGridRecord;
	var iNumValues = aValues.length;
	var aURL = ["myposts_draftsduplicate"];
	for (iI = 0; iI < iNumValues; iI++){
		aURL.push("col_" + iI);
		aURL.push(aValues[iI]);
	}
	aURL.push("id");
	aURL.push(V.oData.oUser.iOrganisationID);
	A.ajax({aURL:aURL, fCallback:this.draftsDuplicateA});
};



ENTER_MYPOSTS.prototype.draftsEditEdit = function(){
	V.oDiv.work1.style.display = "none";
	V.oDiv.work2.style.display = "block";
	V.oDiv.work2.innerHTML = "";

	var oGrid = V.aAA[this.aGridNos[1]];
	var sForm = "oForm_MyPostsEditDraftsEdit";
	var sIDPre = "mypostseditdraftsedit-";
	var oForm = V.oData.oInit[sForm];
	var divBox = V.oDiv.work2;
	var sIDBox = "workpage_0";
	var iNumEles = oForm.length;
	for (var iJ = 0; iJ < iNumEles; iJ++){
		oRec = oForm[iJ];
		oRec.that = this;
		if (oRec.iTabNo){
			oRec.divParent = A.dg(sIDBox + "_" + oRec.iTabNo);
		} else {
			oRec.divParent = divBox;
		}
		oRec.sID = sIDPre + iJ;
		var divEle = E[oRec.sType](oRec);
	}
	E.action("tab_0_1");
	var divIcon = E.div(A.dg("workpage_0_1"), "iconeditclose");
	divIcon.innerHTML = "&#xE06E;";
	A.action.call
		(divIcon, "click", E.action, [E, "editclose"]);
	A.valueSet(sIDPre + "2", this.aGridRecord[1]);
	A.valueSet(sIDPre + "3", this.aGridRecord[6]);
	var that = A.findObject("init");
	var aOptions = that.initializeAddresses();
	var divSelect = A.dg(sIDPre + "4");
	E.selectPopulate(divSelect, aOptions.aOptions);
	A.valueSet(sIDPre + "4", this.aGridRecord[5]);
	var aDates = this.aGridRecord[4].split(" ");-
	A.valueSet(sIDPre + "5", aDates[0]);
	if (!aDates[2]){
		aDates[2] = "";
	}
	A.valueSet(sIDPre + "6", aDates[2]);
};



ENTER_MYPOSTS.prototype.draftsEdit = function(){
	V.oDiv.work1.style.display = "none";
	V.oDiv.work2.style.display = "block";
	V.oDiv.work2.innerHTML = "";

	var oGrid = V.aAA[this.aGridNos[1]];
	var sForm = "oForm_MyPostsEditDrafts";
	var sIDPre = "mypostseditdrafts-";
	var oForm = V.oData.oInit[sForm];
	var divBox = V.oDiv.work2;
	var sIDBox = "workpage_0";
	var iNumEles = oForm.length;
	for (var iJ = 0; iJ < iNumEles; iJ++){
		oRec = oForm[iJ];
		oRec.that = this;
		if (oRec.iTabNo){
			oRec.divParent = A.dg(sIDBox + "_" + oRec.iTabNo);
		} else {
			oRec.divParent = divBox;
		}
		oRec.sID = sIDPre + iJ;
		var divEle = E[oRec.sType](oRec);
		if (oRec.sType == "grid"){
			this.aGridNos[5] = parseInt(divEle.id.split("_")[1]);
		}
	}
	E.action("tab_0_1");
	var divIcon = E.div(A.dg("workpage_0_1"), "iconeditclose");
	divIcon.innerHTML = "&#xE06E;";
	A.action.call
		(divIcon, "click", E.action, [E, "editclose"]);
	A.dg(sIDPre + "2").textContent = this.aGridRecord[1];
	A.dg(sIDPre + "3").textContent = this.aGridRecord[3];
	A.dg(sIDPre + "4").textContent = this.aGridRecord[6];
	var iSize = V.oData.oUser.aAddresses.length;
	var iFound = 0;
	iJ = 0;
	while ((!iFound) && (iJ < iSize)){
		var oRec = V.oData.oUser.aAddresses[iJ];
		if (oRec.id == this.aGridRecord[5]){
			iFound = iJ + 1;
		} else {
			iJ++;
		}
	}
	if (iFound){
		var iSize = V.oData.aRegions.length;
		var iFound = 0;
		iJ = 0;
		while ((!iFound) && (iJ < iSize)){
			var oRec1 = V.oData.aRegions[iJ];
			if (oRec1.iID == oRec.region_id){
				iFound = iJ + 1;
			} else {
				iJ++;
			}
		}
		var sTown = "";
		if (iFound){
			sTown = oRec1.sValue;
		}
		var that = A.findObject("init");
		var aOptions = that.initializeAddresses();
		A.dg(sIDPre + "6").innerHTML = oRec.address_line1 + "<br>" +
			oRec.address_line2 + "<br>" + sTown;
	}
	var aDates = this.aGridRecord[4].split(" ");-
	A.valueSet(sIDPre + "5", aDates[0]);
	if (!aDates[2]){
		aDates[2] = "";
	}
	A.dg(sIDPre + "8").innerHTML += "<br>" + aDates[0];
	A.dg(sIDPre + "9").innerHTML += "<br>" + aDates[2];
	A.ajax({aURL:["grid_data", "name", "mypostsedit", "no", 
		this.aGridNos[5], "id", this.aGridRecord[0]], 
		fCallback:V.aAA[this.aGridNos[5]].dataA});
};



ENTER_MYPOSTS.prototype.draftsPostNow = function(){
	A.ajax({aURL:["myposts_savedraftspostnow", "id", this.aGridRecord[0]], 
		fCallback:this.activeSaveA});
};



ENTER_MYPOSTS.prototype.draftsSaveA = function(oData){
	console.log(oData);
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
	if (oData.sError){
		E.popup({sTitle:V.oData.oError.oDefault.sTitle, sMessage:oData.sError});
		return;
	}
	var iGridNo = 0;
	var oGrid = {};
	var that = A.findObject("myposts");
	if (oData.aData1){
		iGridNo = 1;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData1.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}	
	if (oData.aData2){
		iGridNo = 2;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData2.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}	
	if (oData.aData3){
		iGridNo = 3;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData3.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}
	
	E.action("editclose");
};



ENTER_MYPOSTS.prototype.draftsSave = function(){
	var oValues = this.values("drafts");
	if (!oValues.sErrors){
		var aValues = oValues.aValues;
		var iNumValues = aValues.length;
		var aURL = ["myposts_savedrafts", "id", this.aGridRecord[0]];
		for (iI = 0; iI < iNumValues; iI++){
			aURL.push(aValues[iI].sID);
			aURL.push(aValues[iI].sValue);
		}
		A.ajax({aURL:aURL, fCallback:this.draftsSaveA});
	}
};



ENTER_MYPOSTS.prototype.historyDeleteGo = function(){
	var aValues = this.aGridRecord;
	var iNumValues = aValues.length;
	var aURL = ["myposts_historydelete"];
	aURL.push("id");
	aURL.push(this.aGridRecord[0]);
	A.ajax({aURL:aURL, fCallback:this.historyDeleteA});
};



ENTER_MYPOSTS.prototype.historyDeleteA = function(oData){
	console.log(oData);
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
	if (oData.sError){
		E.popup({sTitle:V.oData.oError.oDefault.sTitle, sMessage:oData.sError});
		return;
	}
	var that = A.findObject("myposts");
	if (oData.aData3){
		iGridNo = 3;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData3.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
		oGrid.sort();
	}	
	E.action("editclose");
};



ENTER_MYPOSTS.prototype.historyDelete = function(){
	var sTitle = V.oData.oError.oMyPostsDelete.sTitle;
	var sMessage = V.oData.oError.oMyPostsDelete.sMessage;
	E.popup({
		sTitle:sTitle,
		sMessage:sMessage,
		that:this,
		sAction:"historydelete",
		aButtons:[V.oData.oInit.oBanners.oGeneral.sOK, 
			V.oData.oInit.oBanners.oGeneral.sCancel]
	});
};



ENTER_MYPOSTS.prototype.historyDuplicateToDraftsA = function(oData){
	console.log(oData);
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
	if (oData.sError){
		E.popup({sTitle:V.oData.oError.oDefault.sTitle, sMessage:oData.sError});
		return;
	}
	var that = A.findObject("myposts");
	if (oData.aData2){
		iGridNo = 2;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData2.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
		oGrid.sort();
	}
	if (oData.bEdit){
		E.action("editclose");
	}
};



ENTER_MYPOSTS.prototype.historyDuplicateToDrafts = function(bEdit){
	var aValues = this.aGridRecord;
	var iNumValues = aValues.length;
	var aURL = ["myposts_historyduplicatetodrafts"];
	for (iI = 0; iI < iNumValues; iI++){
		aURL.push("col_" + iI);
		aURL.push(aValues[iI]);
	}
	aURL.push("id");
	aURL.push(V.oData.oUser.iOrganisationID);
	aURL.push("edit");
	if (bEdit){
		aURL.push(1);
	} else {
		aURL.push(0);
	}
	A.ajax({aURL:aURL, fCallback:this.historyDuplicateToDraftsA});
};


/*
ENTER_MYPOSTS.prototype.historyEditEdit = function(){
	V.oDiv.work1.style.display = "none";
	V.oDiv.work2.style.display = "block";
	V.oDiv.work2.innerHTML = "";

	var oGrid = V.aAA[this.aGridNos[1]];
	var sForm = "oForm_MyPostsEditHistoryEdit";
	var sIDPre = "mypostsedithistoryedit-";
	var oForm = V.oData.oInit[sForm];
	var divBox = V.oDiv.work2;
	var sIDBox = "workpage_0";
	var iNumEles = oForm.length;
	for (var iJ = 0; iJ < iNumEles; iJ++){
		oRec = oForm[iJ];
		oRec.that = this;
		if (oRec.iTabNo){
			oRec.divParent = A.dg(sIDBox + "_" + oRec.iTabNo);
		} else {
			oRec.divParent = divBox;
		}
		oRec.sID = sIDPre + iJ;
		var divEle = E[oRec.sType](oRec);
	}
	E.action("tab_0_1");
	var divIcon = E.div(A.dg("workpage_0_1"), "iconeditclose");
	divIcon.innerHTML = "&#xE06E;";
	A.action.call
		(divIcon, "click", E.action, [E, "editclose"]);
	A.valueSet(sIDPre + "2", this.aGridRecord[1]);
	A.valueSet(sIDPre + "3", this.aGridRecord[6]);
	var that = A.findObject("init");
	var aOptions = that.initializeAddresses();
	var divSelect = A.dg(sIDPre + "4");
	E.selectPopulate(divSelect, aOptions.aOptions);
	A.valueSet(sIDPre + "4", this.aGridRecord[5]);
	var aDates = this.aGridRecord[4].split(" ");-
	A.valueSet(sIDPre + "5", aDates[0]);
	if (!aDates[2]){
		aDates[2] = "";
	}
	A.valueSet(sIDPre + "6", aDates[2]);
};



ENTER_MYPOSTS.prototype.historyMarkAsComplete = function(){
	E.popup({sTitle:"History Post", sMessage:"Mark as complete record: " +
		this.aGridRecord[0]});
};
*/


ENTER_MYPOSTS.prototype.historyMarkAsDraft = function(){
	A.ajax({aURL:["myposts_savehistorymarkasdraft", "id", this.aGridRecord[0]], 
		fCallback:this.activeSaveA});
};



ENTER_MYPOSTS.prototype.historySaveA = function(oData){
	console.log(oData);
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
	if (oData.sError){
		E.popup({sTitle:V.oData.oError.oDefault.sTitle, sMessage:oData.sError});
		return;
	}
	var iGridNo = 0;
	var oGrid = {};
	var that = A.findObject("myposts");
	if (oData.aData1){
		iGridNo = 1;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData1.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}	
	if (oData.aData2){
		iGridNo = 2;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData2.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}	
	if (oData.aData3){
		iGridNo = 3;
		oGrid = V.aAA[that.aGridNos[iGridNo]];
		oGrid.aData = oData.aData3.aData;
		oGrid.aDataFiltered = 0;
		that.action("datagrid_" + that.aGridNos[iGridNo]);
		oGrid.populate();
	}
	
	E.action("editclose");
};



ENTER_MYPOSTS.prototype.historySave = function(iStatus){
	var oValues = this.values("history");
	if (!oValues.sErrors){
		var aValues = oValues.aValues;
		var iNumValues = aValues.length;
		var aURL = ["myposts_savehistory", "id", this.aGridRecord[0]];
		for (iI = 0; iI < iNumValues; iI++){
			aURL.push(aValues[iI].sID);
			aURL.push(aValues[iI].sValue);
		}
		aURL.push("status");
		aURL.push(iStatus);
		A.ajax({aURL:aURL, fCallback:this.historySaveA});
	}
};



ENTER_MYPOSTS.prototype.historyEdit = function(){
	V.oDiv.work1.style.display = "none";
	V.oDiv.work2.style.display = "block";
	V.oDiv.work2.innerHTML = "";

	var oGrid = V.aAA[this.aGridNos[1]];
	var sForm = "oForm_MyPostsEditHistory";
	var sIDPre = "mypostsedithistory-";
	var oForm = V.oData.oInit[sForm];
	var divBox = V.oDiv.work2;
	var sIDBox = "workpage_0";
	var iNumEles = oForm.length;
	for (var iJ = 0; iJ < iNumEles; iJ++){
		oRec = oForm[iJ];
		oRec.that = this;
		if (oRec.iTabNo){
			oRec.divParent = A.dg(sIDBox + "_" + oRec.iTabNo);
		} else {
			oRec.divParent = divBox;
		}
		oRec.sID = sIDPre + iJ;
		var divEle = E[oRec.sType](oRec);
	}
	E.action("tab_0_1");
	var divIcon = E.div(A.dg("workpage_0_1"), "iconeditclose");
	divIcon.innerHTML = "&#xE06E;";
	A.action.call
		(divIcon, "click", E.action, [E, "editclose"]);
	A.dg(sIDPre + "2").textContent = this.aGridRecord[1];
	A.dg(sIDPre + "3").textContent = this.aGridRecord[3];
	A.dg(sIDPre + "4").textContent = this.aGridRecord[6];
	var iSize = V.oData.oUser.aAddresses.length;
	var iFound = 0;
	iJ = 0;
	while ((!iFound) && (iJ < iSize)){
		var oRec = V.oData.oUser.aAddresses[iJ];
		if (oRec.id == this.aGridRecord[5]){
			iFound = iJ + 1;
		} else {
			iJ++;
		}
	}
	if (iFound){
		var iSize = V.oData.aRegions.length;
		var iFound = 0;
		iJ = 0;
		while ((!iFound) && (iJ < iSize)){
			var oRec1 = V.oData.aRegions[iJ];
			if (oRec1.iID == oRec.region_id){
				iFound = iJ + 1;
			} else {
				iJ++;
			}
		}
		var sTown = "";
		if (iFound){
			sTown = oRec1.sValue;
		}
		var that = A.findObject("init");
		var aOptions = that.initializeAddresses();
		A.dg(sIDPre + "6").innerHTML = oRec.address_line1 + "<br>" +
			oRec.address_line2 + "<br>" + sTown;
	}
	var aDates = this.aGridRecord[4].split(" ");
	A.valueSet(sIDPre + "5", aDates[0]);
	if (!aDates[2]){
		aDates[2] = "";
	}
	A.dg(sIDPre + "8").innerHTML += "<br>" + aDates[0];
	A.dg(sIDPre + "9").innerHTML += "<br>" + aDates[2];
};
/*
		if (oRec.sType == "grid"){
			this.aGridNos[5] = parseInt(divEle.id.split("_")[1]);
		}
	A.dg(sIDPre + "2").textContent = this.aGridRecord[1];
	A.dg(sIDPre + "3").textContent = this.aGridRecord[3];
	A.dg(sIDPre + "4").textContent = this.aGridRecord[6];
		var that = A.findObject("init");
	var aDates = this.aGridRecord[4].split(" ");-
	A.valueSet(sIDPre + "5", aDates[0]);
	if (!aDates[2]){
		aDates[2] = "";
	}
	A.ajax({aURL:["grid_data", "name", "mypostsedit", "no", 
		this.aGridNos[5], "id", this.aGridRecord[0]], 
		fCallback:V.aAA[this.aGridNos[5]].dataA});

*/
ENTER_MYPOSTS.prototype.initialize = function(){
	var sIDBox = "workpage_3";
	var divBox = A.dg(sIDBox);
	divBox.innerHTML = "";
	var sForm = "oForm_MyPosts";
	var oForm = V.oData.oInit[sForm];
	if (oForm){
		var iNumEles = oForm.length;
		for (var iJ = 0; iJ < iNumEles; iJ++){
			var oRec = oForm[iJ];
			oRec.that = this;
			if (oRec.iTabNo){
				oRec.divParent = A.dg(sIDBox + "_" + oRec.iTabNo);
			} else {
				oRec.divParent = divBox;
			}
			oRec.sID = "myposts-" + iJ;
			var divEle = E[oRec.sType](oRec);
			if (oRec.sType == "grid"){
				this.aGridNos.push(parseInt(divEle.id.split("_")[1]));
			}
		}
	}
	var iTab = V.oGen.oSession.iTab3;
	if (!iTab){
		iTab = 1;
	}
	E.action("tab_3_" + iTab);
};



ENTER_MYPOSTS.prototype.values = function(sIDPre){
	var aInputs = ["text", "select-one", "password", "textarea", "weekhours",
		"time", "email"];
	var aValues = [];
	var sErrors = "";
	var sID = "";
	var oForm = {};

	switch (sIDPre){
		case "active":
			oForm = V.oData.oInit.oForm_MyPostsEditActiveEdit;
		break;
		case "drafts":
			oForm = V.oData.oInit.oForm_MyPostsEditDraftsEdit;
		break;
		case "history":
			oForm = V.oData.oInit.oForm_MyPostsEditHistory;
		break;
	}
	sIDPre = "mypostsedit" + sIDPre + "edit-";
	var iI = 1;
	while (A.dg(sIDPre + iI)){
		sID = sIDPre + iI;
		var divSave = A.dg(sID);
		var sType = divSave.type;
		if (aInputs.indexOf(sType) > -1){
			var sValue = A.valueGet(sID);
			var oEle = oForm[iI];
			var sTestValue = sValue;
			if (oEle.sType == "select"){
				if (sValue == -1){
					sTestValue = "";
				} else {
					sTestValue = "x";
				}
			}
			var bError = 0;
			if ((oEle.bRequired) && (!sTestValue)){
				if (!oEle.iIDParent){
					bError = 1;
				} else {
					if (this.oSelectedType.iID == oEle.iIDParent){
						bError = 1;
					}
				}
			}
			if (bError){
				sErrors += V.oData.oInit.oBanners.oGeneral.sRequired + 
					oEle.sText + ", ";
			}
			if ((oEle.bEmail) && (!A.validEmailAddress(sValue))){
				sErrors += V.oData.oInit.oBanners.oGeneral.sInvalid + 
					oEle.sText + ", ";
			}
			aValues.push({sID:sID, sValue:sValue});
		}
		iI++;
	}
	if (sErrors){
		var sError = V.oData.oError.oRequiredFields.sMessage;
		sError += "<br><br>" + sErrors.slice(0, -2) + ".";
		E.popup({sTitle:V.oData.oError.oRequiredFields.sTitle,
			sMessage:sError});
	}
	return {sErrors:sErrors, aValues:aValues};
};



		ENTER_MYPOSTS.bInited = 1;
	}
}
//== END OF MYPOSTS CLASS ======================================================



//== PROFILE CLASS =============================================================
function ENTER_PROFILE(){
	this.sName = "profile";
	this.bDataLoaded = 0;
	this.iObjLogo = 0;
	this.aObjAddresses = [];
	this.sCompanyLogo = "";
	this.sPicBox = "";
	this.iNumBusinessHours = 1;
	this.iWeekHoursObjNo = 0;
	this.bAddFirstAddress = 1;
	this.iAddressNo = 0;
	this.aPlaces = [];
	
	if (typeof ENTER_PROFILE.bInited == "undefined"){


ENTER_PROFILE.prototype.action = function(sAction, aParams){
	var that = A.actionA(this, sAction, this.aParams);
	var iI = 0;
	var sColor = "";
	var divBox = 0;
	var divButton = 0;
	var aAction = that.sAction.split("_");
console.log(that.sAction);
	switch (aAction[0]){
		case "save":
			that.that.save(parseInt(aAction[1]));
		break;
		
		
		case "addaddress":
			that.that.addAddress(parseInt(aAction[1]));
		break;

		case "logo":
			switch (aAction[1]){
				case "remove":
					V.aAA[that.that.iObjLogo].remove();
					that.that.sCompanyLogo = "";				
				break;
				case "crop":
					V.aAA[that.that.iObjLogo].saveCrop(that.that.sCompanyLogo);
				break;
				case "reset":
					V.aAA[that.that.iObjLogo].resetCrop(that.that.sCompanyLogo);
				break;
			}
		break;

		case "uploaddone":
			A.ajax({aURL:["init_registeruploaddone", "filename", aAction[1],
				"parent", "profile"],
				fCallback:V.aAA[that.that.iObjLogo].uploaded});
		break;
		
		case "register":
			var iIndex = parseInt(aAction[2]);
			switch (aAction[1]){
				case "province":
					V.aAA[that.that.aObjAddresses[iIndex]].changeProvince("profile");
				break;
				case "region":
					V.aAA[that.that.aObjAddresses[iIndex]].changeRegion("profile");
				break;
				case "town":
					V.aAA[that.that.aObjAddresses[iIndex]].changeTown("profile");
				break;
				case "addressselect":
					V.aAA[that.that.aObjAddresses[that.that.iAddressNo]].select(1);
				break;
				case "addressadd":
					V.aAA[that.that.aObjAddresses[iIndex]]
						.add(that.that.bAddFirstAddress);
					that.that.bAddFirstAddress = 0;
				break;
				case "addressremove":
					V.aAA[that.that.aObjAddresses[iIndex]].remove();
				break;
				case "addressremovego":
					if (aAction[2] == "1"){
						E.popupClose();
					} else {
						E.popupClose();
						V.aAA[that.that.aObjAddresses[iIndex]].removeGo();
						var iAddressObj = that.that.aObjAddresses[iIndex];
						V.aAA[iAddressObj] = 0;
						that.that.aObjAddresses[iIndex] =  -1;
					}
				break;
				/*
				case "removelogo":
					V.aAA[that.that.iObjLogo].remove();
					that.that.sCompanyLogo = "";
				break;				
				case "croplogo":
					V.aAA[that.that.iObjLogo].saveCrop(that.that.sCompanyLogo);
				break;
				case "croplogoreset":
					V.aAA[that.that.iObjLogo].resetCrop(that.that.sCompanyLogo);
				break;
				*/
			}
		break;
	}
};



ENTER_PROFILE.prototype.addAddress = function(iPageNo){
	var sIDPre = "workpage_5";
	var iObjNo = A.nextObject();
	this.aObjAddresses.push(iObjNo);
	var iIndex = this.aObjAddresses.length - 1; 
	V.aAA[iObjNo] = new ENTER_ADDRESSES();
	V.aAA[iObjNo].sParentName = "profile";
	V.aAA[iObjNo].sIDPre = sIDPre + "-4_1_" + iIndex + "_";
	var oEle = V.oData.oInit.oForm_RegisterStep4[1];
	oEle.sID = sIDPre + "-4_1_" + iIndex;
	var divParent = A.dg(sIDPre + "-4_1");
	oEle.divParent = divParent;
	oEle.iIndex = iIndex;
	E.address(oEle);
	this.aPlaces.push({id:0});
};



ENTER_PROFILE.prototype.dataA = function(oData){
	var that = A.findObject("profile");
	that.bDataLoaded = 1;
	var iI = 0;
	var iJ = 0;
	var iFound = 0;
	var iFound1 = 0;
	console.log(oData)
	var sIDPre = "workpage_5";
	A.valueSet(sIDPre + "-1_1", oData.oUser.name);
	A.valueSet(sIDPre + "-1_2", oData.oUser.surname);
	A.valueSet(sIDPre + "-1_3", oData.oUser.email);
	A.valueSet(sIDPre + "-2_1", oData.oUser.sOrganisationName);
	A.valueSet(sIDPre + "-2_4", oData.oUser.iOrganisationCategoryID);
	A.valueSet(sIDPre + "-2_5", oData.oUser.organisation_type);
	A.valueSet(sIDPre + "-2_6", oData.oUser.description);
	A.valueSet(sIDPre + "-3_1", oData.oUser.contact_person);
	A.valueSet(sIDPre + "-3_2", oData.oUser.contact_email);
	A.valueSet(sIDPre + "-3_3", oData.oUser.contact_number);
	that.iNumBusinessHours = A.getJSON(oData.aHours, 1).length;
	E.weekhoursDraw(that.iWeekHoursObjNo);
	A.valueSet(sIDPre + "-3_4", oData.aHours);
	
	var aPlaces = A.getJSON(oData.aPlaces, 1);
	var iSize = aPlaces.length;
	for (iI =0; iI < iSize; iI++){
		that.aObjAddresses[iI] = A.nextObject();
		V.aAA[that.aObjAddresses[iI]] = new ENTER_ADDRESSES();
		V.aAA[that.aObjAddresses[iI]].sParentName = "profile";
		V.aAA[that.aObjAddresses[iI]].sIDPre = sIDPre + "-4_1_" + iI + "_";
		var oEle = V.oData.oInit.oForm_RegisterStep4[1];
		oEle.sID = sIDPre + "-4_1_" + iI;
		var divParent = A.dg(sIDPre + "-4_1");
		oEle.divParent = divParent;
		oEle.iIndex = iI;
		E.address(oEle);
		A.valueSet(sIDPre + "-4_1_" + iI, aPlaces[iI]);
	}
	that.aPlaces = aPlaces;
	/*
	var that1 = A.findObject("init");
	var aOptions = that1.initializeAddresses();
	var sID = sIDPre + "4_7";
	var divSelect = A.dg(sID);
	E.selectPopulate(divSelect, aOptions.aOptions);
	A.valueSet(sID, aOptions.bMain);
	
	var aAddresses = [];
	var aRegions = [];
	var oRecA = {};
	var iSizeA = V.oData.oUser.aAddresses.length;
	var iSizeR = V.oData.aRegions.length;
	for (iI = 0; iI < iSizeA; iI++){
		var oRecAA = V.oData.oUser.aAddresses[iI];
		iFound1 = 1;
		while ((!iFound) && (iJ < iSizeR)){
			var oRecR = V.oData.aRegions[iJ];
			if  (oRecAA.region_id == oRecR.iID){
				iFound = iJ + 1;
			} else {
				iJ++;
			}
		}
		if (iFound){
			aRegions[0] = V.oData.aRegions[iFound -1];
			if (aRegions[0].iParentID == "0"){
				iFound1 = 0;
			}
			iFound = 0;
			iJ = 0;
			oRecA = aRegions[0];
			while ((iFound1) && (!iFound) && (iJ < iSizeR)){
				oRecR = V.oData.aRegions[iJ];
				if  (oRecA.iParentID == oRecR.iID){
					iFound = iJ + 1;
				} else {
					iJ++;
				}
			}
			if (iFound){
				aRegions[1] = V.oData.aRegions[iFound -1];
				if (aRegions[1].iParentID == "0"){
					iFound1 = 0;
				}
				iFound = 0;
				iJ = 0;
				oRecA = aRegions[1];
				while ((iFound1) && (!iFound) && (iJ < iSizeR)){
					oRecR = V.oData.aRegions[iJ];
					if  (oRecA.iParentID == oRecR.iID){
						iFound = iJ + 1;
					} else {
						iJ++;
					}
				}
				if (iFound){
					aRegions[2] = V.oData.aRegions[iFound -1];
					if (aRegions[2].iParentID == "0"){
						iFound1 = 0;
					}
					iFound = 0;
					iJ = 0;
					oRecA = aRegions[2];
					while ((iFound1) && (!iFound) && (iJ < iSizeR)){
						oRecR = V.oData.aRegions[iJ];
						if  (oRecA.iParentID == oRecR.iID){
							iFound = iJ + 1;
						} else {
							iJ++;
						}
					}
					if (iFound){
						aRegions[3] = V.oData.aRegions[iFound -1];
						if (aRegions[3].iParentID == "0"){
							iFound1 = 0;
						}
						iFound = 0;
						iJ = 0;
						oRecA = aRegions[3];
						while ((iFound1) && (!iFound) && (iJ < iSizeR)){
							oRecR = V.oData.aRegions[iJ];
							if  (oRecA.iParentID == oRecR.iID){
								iFound = iJ + 1;
							} else {
								iJ++;
							}
						}
						if (iFound){
							aRegions[4] = V.oData.aRegions[iFound -1];
						}
					}
				}
			}
		}
		if (aRegions[3]){
			var aAddress = [aRegions[3].iID, aRegions[2].iID, aRegions[1].iID, aRegions[0].iID, 
				oRecAA.address_line1, oRecAA.address_line2, oRecAA.main, oRecAA.id];
			aAddresses.push(aAddress);
		} else { 
			var aAddress = [aRegions[2].iID, aRegions[1].iID, aRegions[0].iID, -1, 
				oRecAA.address_line1, oRecAA.address_line2, oRecAA.main, oRecAA.id];
			aAddresses.push(aAddress);
		}		
	}
	
	
	
	V.aAA[that.aObjAddresses[that.iAddressNo]].aAddresses = aAddresses;
	V.aAA[that.aObjAddresses[that.iAddressNo]].select(1);
	
	
	*/
	A.valueSet(sIDPre + "5_1", oData.oUser.website_link);
	A.valueSet(sIDPre + "5_2", oData.oUser.facebook_link);
	A.valueSet(sIDPre + "5_3", oData.oUser.twitter_handle);
	A.valueSet(sIDPre + "5_4", oData.oUser.instagram_link);
	
	that.sCompanyLogo = oData.oUser.logo;
	V.aAA[that.iObjLogo].load(that.sCompanyLogo);
	if (that.sCompanyLogo){
		A.dg(sIDPre + "-2_2").uploaddone = 1;
	}
	that.sPicBox = oData.sPicBox;
};



ENTER_PROFILE.prototype.initialize = function(){
	var sID = "workpage_5";
	var iI = 0;
	var iJ = 0;
	var sForm = "";
	var oForm = {};
	var iNumEles = 0;
	var aNames = [];
	var aWidths = [];
	var divBox = A.dg(sID);
	var divParent = 0;
	divBox.innerHTML = "";
	this.iObjLogo = A.nextObject();
	V.aAA[this.iObjLogo] = new ENTER_LOGO();
	V.aAA[this.iObjLogo].sIDPre = sID;
	V.aAA[this.iObjLogo].sParentObjName = "profile";
	sForm = "oForm_RegisterSteps";
	oForm = V.oData.oInit[sForm];
	iNumEles = oForm.length;
	for (iI = 0; iI < iNumEles; iI++){
		var oRec = oForm[iI];
		if (oRec.sType == "step"){
			var aName = oRec.sText.split("|");
			aNames.push(aName[1]);
			aWidths.push(aName[1].length + 3);
		}
	}
	E.tabs({aNames:aNames, aWidths:aWidths, sParentID:sID, bScrollbar:1});
	var iTab = V.oGen.oSession.iTab5;
	if (!iTab){
		iTab = 1;
	}
	E.action("tab_5_" + iTab);
	
	var iNumTabs = aNames.length;
	for (iI = 0; iI < iNumTabs; iI++){
		sForm = "oForm_RegisterStep" + (iI + 1);
		oForm = V.oData.oInit[sForm];
		iNumEles = oForm.length - 2;
		divParent = A.dg(sID + "_" + (iI + 1));
		for (iJ = 0; iJ < iNumEles; iJ++){
			var oRec = oForm[iJ];
			oRec.divParent = divParent;
			oRec.that = this;
			oRec.sID = sID + "-" + (iI + 1) + "_" + iJ;
			if (!oRec.bNoProfile){
				if (oRec.sType != "address"){
					var divX = E[oRec.sType](oRec);
				} else {
					//do not show addresses, create container div
					var divBox = E.div(divParent, "addressboxlist", oRec.sID);
					divBox.type = "address";
				}
			}
			if ((iI == 2) && (iJ == 4)){
				this.iWeekHoursObjNo = divX;
			}
		}
		E.button({divParent:divParent, aPos:[0, 34, 12, 0, 0, 2], 
			sText:V.oData.oInit.oBanners.oGeneral.sButtonSave,
			sID:sID+ "-" + (iI + 1) + "_" + iJ,
			that:this, sAction:"save_" + (iI + 1)
		});
		if (iI == 3){
			//add button for profile addresses
			E.button({divParent:divParent, aPos:[0, 34, 12, 0, 14, -2.6], 
				sText:V.oData.oInit.oBanners.oGeneral.sButtonAdd,
				sID:sID+ "-" + (iI + 1) + "_" + iJ,
				that:this, sAction:"addaddress_" + (iI + 1)
			});
		}
	}
	var aProvinces = [V.oData.oInit.oEnum.sSelect[0]];
	var iNumRegions = V.oData.aRegions.length;
	for (iI = 0; iI <= iNumRegions; iI++){
		oRec = V.oData.aRegions[iI];
		if ((oRec) && (oRec.iRegionType == 1)){
			aProvinces.push(oRec);
		}
	}	
	/*
	divSelect = A.dg(sID + "-4_1");
	E.selectPopulate(divSelect, aProvinces);
	divSelect = A.dg(sID + "-4_2");
	E.selectPopulate(divSelect, [V.oData.oInit.oEnum.sSelect[0]]);
	divSelect = A.dg(sID + "-4_3");
	E.selectPopulate(divSelect, [V.oData.oInit.oEnum.sSelect[0]]);
	divSelect = A.dg(sID + "-4_4");
	E.selectPopulate(divSelect, [V.oData.oInit.oEnum.sSelect[0]]);
*/	if (!this.bDataLoaded){
		A.ajax({aURL:["profile_data", "picbox", "_2"], 
			fCallback:this.dataA,});
	}
	
};



ENTER_PROFILE.prototype.saveA = function(oData){
	console.log(oData);
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
};



ENTER_PROFILE.prototype.save = function(iPageNo){
	var iI = 0;
	var sID = "";
	var aInputs = ["text", "select-one", "password", "textarea", 
		"weekhours", "checkbox", "upload", "email", "address"];

	var oForm = V.oData.oInit["oForm_RegisterStep" + iPageNo];
	var iNumEles = oForm.length - 2;
	var sIDPre = "workpage_5-" + iPageNo + "_";
	var sErrors = "";
	var aValues = [];
	for (iI = 0; iI < iNumEles; iI++){
		var oEle = oForm[iI];
		sID = sIDPre + iI;
		var divSave = A.dg(sID);
		if (divSave){
			var sType = divSave.type;
			if ((aInputs.indexOf(sType) > -1) && (!oEle.bNoProfile)){
				var sValue = A.valueGet(sIDPre + iI);
				
				var sTestValue = sValue;
				if (oEle.sType == "select"){
					if (sValue == -1){
						sTestValue = "";
					} else {
						sTestValue = "x";
					}
				}
				if ((oEle.bURL) && (sValue)){
					if ((sValue.substr(0, 7) != V.oData.oInit.oBanners.oGeneral.sHTTP) &&
						(sValue.substr(0, 8) != V.oData.oInit.oBanners.oGeneral.sHTTPs)){
						sValue = V.oData.oInit.oBanners.oGeneral.sHTTP + sValue;
						A.valueSet(sID, sValue);
					}
				}
				if ((oEle.bRequired) && (!sTestValue)){
					sErrors += V.oData.oInit.oBanners.oGeneral.sRequired + 
						oEle.sText + ", ";
				}
				if ((oEle.bEmail) && (!A.validEmailAddress(sValue))){
					sErrors += V.oData.oInit.oBanners.oGeneral.sInvalid + 
						oEle.sText + ", ";
				}
				aValues.push({sID:sID, sValue:sValue});
			}
		}
	}
	
	if (iPageNo == 2){
		aValues.push({sID:"sCompanyLogo", sValue:this.sCompanyLogo});
	}
	
	if (iPageNo == 4){
		var aAddresses = A.getJSON(aValues[0].sValue, 1);
		if (aAddresses.length > 0){
			sErrors = 0;
		}
	}
	
	if (sErrors){
		var sError = V.oData.oError.oRequiredFields.sMessage;
		sError += "<br><br>" + sErrors.slice(0, -2) + ".";
		E.popup({sTitle:V.oData.oError.oRequiredFields.sTitle,
			sMessage:sError});
	} else {
		var iNumValues = aValues.length;
		var aURL = ["profile_savepage", "pageno", iPageNo, "sidpre", "workpage_5"];
		for (iI = 0; iI < iNumValues; iI++){
			aURL.push(aValues[iI].sID);
			aURL.push(aValues[iI].sValue);
		}
		if (iPageNo == 4){
		}
		A.ajax({aURL:aURL, fCallback:this.saveA});
	}
};



ENTER_PROFILE.prototype.uploadedLogo = function(oData){
	var iIndex = oData.sFilename.indexOf("-R");
	if (iIndex == -1){
		V.aAA[this.iObjLogo].showControls(1, 1);
	} else {
		V.aAA[this.iObjLogo].showControls(1, 0);
		V.oDiv.crop.removeChild(V.oDiv.crop.lefttop);
		V.oDiv.crop.removeChild(V.oDiv.crop.rightbottom);
		V.oDiv.crop.removeChild(V.oDiv.crop.leftcut);
		V.oDiv.crop.removeChild(V.oDiv.crop.rightcut);
		V.oDiv.crop.removeChild(V.oDiv.crop.topcut);
		V.oDiv.crop.removeChild(V.oDiv.crop.bottomcut);
	}
};



		ENTER_PROFILE.bInited = 1;
	}
}
//== END OF PROFILE CLASS ===================================================



//== USERS CLASS ===============================================================
function ENTER_USERS(){
	this.sName = "users";
	this.aGridNos = [0];
	this.aGridRecord = [];
	
	if (typeof ENTER_USERS.bInited == "undefined"){


ENTER_USERS.prototype.action = function(sAction, aParams){
	var that = A.actionA(this, sAction, this.aParams);
	var aAction = that.sAction.split("_");
	var iI = 0;
	var iGridNo = 0;
	console.log(that.sAction);
	switch (aAction[0]){
		case "parent":
			iGridNo = parseInt(aAction[2]);
			var iRowNo = parseInt(aAction[3]);
			that.that.aGridRecord = V.aAA[iGridNo].findRecordIndex(iGridNo, iRowNo);
			if (that.that.aGridRecord){
				switch (aAction[1]){
					case "editgrid":
						if (aAction[2] == that.that.aGridNos[1]){
							that.that.edit();
						}
					break;
				}
			}
		break;

		case "useradd":
			that.that.add();
		break;
		
		case "add":
			switch (aAction[1]){
				case "save":
					that.that.addSave();
				break;
			}
		break;
		
		case "edit":
			switch (aAction[1]){
				case "save":
					that.that.editSave();
				break;
			}
		break;
		
		case "datagrid":
		break;
	}
};
		
		

ENTER_USERS.prototype.addSaveA = function(oData){
	E.action("editclose");
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
	if (oData.sError){
		E.popup(V.oData.oError[oData.sError]);
	}
	var that = A.findObject("users");
	var oGrid = V.aAA[that.aGridNos[1]];
	oGrid.aData = oData.aData.aData;
	oGrid.aDataFiltered = 0;
	that.action("datagrid_" + that.aGridNos[1]);
	oGrid.populate();
};



ENTER_USERS.prototype.addSave = function(){
	var aInputs = ["text", "select-one", "password", "textarea", "weekhours",
		"email"];
	var sIDPre = "usersadduser-";
	var iI = 0;
	var aValues = [{sID:"id",sValue:this.aGridRecord[0]}];
	var sErrors = "";
	var sID = "";
	var oForm = V.oData.oInit.oForm_UsersAdd;
	while (A.dg(sIDPre + iI)){
		sID = sIDPre + iI;
		var divSave = A.dg(sID);
		var sType = divSave.type;
		if (aInputs.indexOf(sType) > -1){
			var sValue = A.valueGet(sID);
			var oEle = oForm[iI];
			var sTestValue = sValue;
			if (oEle.sType == "select"){
				if (sValue == -1){
					sTestValue = "";
				} else {
					sTestValue = "x";
				}
			}
			if ((oEle.bRequired) && (!sTestValue)){
				sErrors += V.oData.oInit.oBanners.oGeneral.sRequired + 
					oEle.sText + ", ";
			}
			if ((oEle.bEmail) && (!A.validEmailAddress(sValue))){
				sErrors += V.oData.oInit.oBanners.oGeneral.sInvalid + 
					oEle.sText + ", ";
			}
			aValues.push({sID:sID, sValue:sValue});
		}
		iI++;
	}
	if (sErrors){
		var sError = V.oData.oError.oRequiredFields.sMessage;
		sError += "<br><br>" + sErrors.slice(0, -2) + ".";
		E.popup({sTitle:V.oData.oError.oRequiredFields.sTitle,
			sMessage:sError});
	} else {
		var iNumValues = aValues.length;
		var aURL = ["users_addsave"];
		for (iI = 0; iI < iNumValues; iI++){
			aURL.push(aValues[iI].sID);
			aURL.push(aValues[iI].sValue);
		}
		A.ajax({aURL:aURL, fCallback:this.addSaveA});
	}
};



ENTER_USERS.prototype.add = function(){
	V.oDiv.work1.style.display = "none";
	V.oDiv.work2.style.display = "block";
	V.oDiv.work2.innerHTML = "";
	var aBreadcrumbs = [V.oGen.aBreadcrumbs[0], V.oGen.aBreadcrumbs[1], "Add"];
	E.breadcrumbs(aBreadcrumbs);
	var sForm = "oForm_UsersAdd";
	var sIDPre = "usersadduser-";
	var oForm = V.oData.oInit[sForm];
	var that = A.findObject("users");
	var divBox = V.oDiv.work2;
	var sIDBox = "workpage_0";
	var iNumEles = oForm.length;
	for (var iJ = 0; iJ < iNumEles; iJ++){
		oRec = oForm[iJ];
		oRec.that = that;
		if (oRec.iTabNo){
			oRec.divParent = A.dg(sIDBox + "_" + oRec.iTabNo);
		} else {
			oRec.divParent = divBox;
		}
		oRec.sID = sIDPre + iJ;
		var divEle = E[oRec.sType](oRec);
	}
	var divIcon = E.div(A.dg("workpage_0"), "iconeditclose");
	divIcon.innerHTML = "&#xE06E;";
	A.action.call(divIcon, "click", E.action, [E, "editclose"]);
};



ENTER_USERS.prototype.editSaveA = function(oData){
	if (oData.sMessage){
		V.sMessageTimed = V.oData.oError[oData.sMessage].sMessage;
	}
	if (oData.sError){
		E.popup({sTitle:V.oData.oError.oDefault.sTitle, sMessage:oData.sError});
	}
	var that = A.findObject("users");
	var oGrid = V.aAA[that.aGridNos[1]];
	oGrid.aData = oData.aData.aData;
	oGrid.aDataFiltered = 0;
	that.action("datagrid_" + that.aGridNos[1]);
	oGrid.populate();
	E.action("editclose");
};



ENTER_USERS.prototype.editSave = function(){
	var aInputs = ["text", "select-one", "password", "textarea", 
		"weekhours", "email"];
	var sIDPre = "usersedituser-";
	var iI = 0;
	var aValues = [{sID:"id",sValue:this.aGridRecord[0]}];
	var sErrors = "";
	var sID = "";
	var oForm = V.oData.oInit.oForm_UsersEdit;
	while (A.dg(sIDPre + iI)){
		sID = sIDPre + iI;
		var divSave = A.dg(sID);
		var sType = divSave.type;
		if (aInputs.indexOf(sType) > -1){
			var sValue = A.valueGet(sID);
			var oEle = oForm[iI];
			var sTestValue = sValue;
			if (oEle.sType == "select"){
				if (sValue == -1){
					sTestValue = "";
				} else {
					sTestValue = "x";
				}
			}
			if ((oEle.bRequired) && (!sTestValue)){
				sErrors += V.oData.oInit.oBanners.oGeneral.sRequired + 
					oEle.sText + ", ";
			}
			if ((oEle.bEmail) && (!A.validEmailAddress(sValue))){
				sErrors += V.oData.oInit.oBanners.oGeneral.sInvalid + 
					oEle.sText + ", ";
			}
			aValues.push({sID:sID, sValue:sValue});
		}
		iI++;
	}
	console.log(aValues);
	if (sErrors){
		var sError = V.oData.oError.oRequiredFields.sMessage;
		sError += "<br><br>" + sErrors.slice(0, -2) + ".";
		E.popup({sTitle:V.oData.oError.oRequiredFields.sTitle,
			sMessage:sError});
	} else {
		var iNumValues = aValues.length;
		var aURL = ["users_editsave"];
		for (iI = 0; iI < iNumValues; iI++){
			aURL.push(aValues[iI].sID);
			aURL.push(aValues[iI].sValue);
		}
		A.ajax({aURL:aURL, fCallback:this.editSaveA});
	}
};



ENTER_USERS.prototype.edit = function(){
	V.oDiv.work1.style.display = "none";
	V.oDiv.work2.style.display = "block";
	V.oDiv.work2.innerHTML = "";
	var aBreadcrumbs = [V.oGen.aBreadcrumbs[0], V.oGen.aBreadcrumbs[1], "Edit"];
	E.breadcrumbs(aBreadcrumbs);
	var oGrid = V.aAA[this.aGridNos[1]];
	var sForm = "oForm_UsersEdit";
	var sIDPre = "usersedituser-";
	var oForm = V.oData.oInit[sForm];
	var that = A.findObject("users");
	var divBox = V.oDiv.work2;
	var sIDBox = "workpage_0";
	var iNumEles = oForm.length;
	for (var iJ = 0; iJ < iNumEles; iJ++){
		oRec = oForm[iJ];
		oRec.that = that;
		if (oRec.iTabNo){
			oRec.divParent = A.dg(sIDBox + "_" + oRec.iTabNo);
		} else {
			oRec.divParent = divBox;
		}
		oRec.sID = sIDPre + iJ;
		var divEle = E[oRec.sType](oRec);
	}
	var divIcon = E.div(A.dg("workpage_0"), "iconeditclose");
	divIcon.innerHTML = "&#xE06E;";
	A.action.call
		(divIcon, "click", E.action, [E, "editclose"]);
	A.valueSet(sIDPre + "0", this.aGridRecord[1]);
	A.valueSet(sIDPre + "1", this.aGridRecord[2]);
	A.valueSet(sIDPre + "2", this.aGridRecord[3]);
	A.valueSet(sIDPre + "3", this.aGridRecord[4], 1);
};



ENTER_USERS.prototype.initialize = function(){
	var sIDBox = "workpage_4";
	var divBox = A.dg(sIDBox);
	divBox.innerHTML = "";
	var sForm = "oForm_Users";
	var oForm = V.oData.oInit[sForm];
	var iNumEles = oForm.length;
	for (var iJ = 0; iJ < iNumEles; iJ++){
		var oRec = oForm[iJ];
		oRec.that = this;
		if (oRec.iTabNo){
			oRec.divParent = A.dg(sIDBox + "_" + oRec.iTabNo);
		} else {
			oRec.divParent = divBox;
		}

		oRec.sID = "users-" + iJ;
		var divEle = E[oRec.sType](oRec);
		if (oRec.sType == "grid"){
			this.aGridNos.push(parseInt(divEle.id.split("_")[1]));
		}
	}
	A.dg(sIDBox + "_1").style.display = "block";
};



		ENTER_USERS.bInited = 1;
	}
}
//== END OF USERS CLASS ========================================================



//== GRID CLASS ================================================================
function ENTER_GRID(){
	this.aData = [];
	this.aDataFiltered = 0;
	this.oDiv = {};
	this.oEle = {};
	this.iHeight = 0;
	this.sName = "grid";
	this.iNumRecords = 0;
	this.iNumRecordsFiltered = 0;
	this.iPagesMax = 1;
	this.iPageNumber = 1;
	
	if (typeof ENTER_GRID.bInited == "undefined"){


ENTER_GRID.prototype.action = function(sAction, aParams){
	var that = A.actionA(this, sAction, this.aParams);
	var iI = 0;
	var iColumn = 0;
	var aAction = that.sAction.split("_");
console.log(that.sAction);
	switch (aAction[0]){
		case "export":
			that.that.exporter();
		break;
		case "parent":
			var oParent = that.that.oEle.that;
			oParent.action(that.sAction);
		break;
		case "searchclear":
			that.that.oDiv.search.value = V.oData.oInit.oBanners.oGeneral.sSearch;
			that.that.oDiv.search.style.color = "#CBCDCF";
			that.that.aDataFiltered = 0;
			that.that.iNumRecordsFiltered = 0;
			that.that.iPageNumber = 1;
//			that.that.oDiv.search.focus();
			that.that.oDiv.searchclear.style.display = "none";
			that.that.populate();
		break;
		
		case "searchtyping":
			var iNumCols = that.that.oEle.aColumns.length;
			iColumn = parseInt(aAction[1]);
			that.that.oEle.oSort.iDirection *= -1;
			that.that.aDataFiltered = [];
			for (iI = 0; iI < that.that.iNumRecords; iI++){
				var aRow = that.that.aData[iI];
				for (var iJ = 1; iJ < iNumCols; iJ++){
					var sValue = ("" + aRow[iJ]).toLowerCase();
					var sSearch = that.that.oDiv.search.value.toLowerCase();
					if (sSearch.length > 0){
						that.that.oDiv.searchclear.style.display = "block";
					} else {
						that.that.oDiv.searchclear.style.display = "none";
					}
					if (sValue.indexOf(sSearch) != -1){
						that.that.aDataFiltered.push(aRow);
						break;
					}
				}
			}
			that.that.iNumRecordsFiltered = that.that.aDataFiltered.length;
			that.that.iPageNumber = 1;
			that.that.populate();
		break;
		
		case "sort":
			that.that.sort(parseInt(aAction[2]));
		break;		
	
	}
};
		
		

ENTER_GRID.prototype.clear = function(){
/*	var iNumRows = this.iNumRecords - 1;
	if (this.aDataFiltered){
		iNumRows = this.iNumRecordsFiltered -1;
	}*/
//	var iFirstRecordShow = ((this.iPageNumber - 1) * iNumRows);
	this.oDiv.cellsbox.innerHTML = "";
	var iNumCols = this.oEle.aColumns.length;
	var iLeft = 0;
	var iI = 0;
	var divCol = 0;
	for (iI = 0; iI < iNumCols; iI++){
		var oRec = this.oEle.aColumns[iI];
		divCol = E.div(this.oDiv.cellsbox, "gridcolumn",
			"gridcol_" + this.oEle.iGridNo + "_" + iI);
		divCol.style.left = iLeft + "%";
		if (iI < iNumCols - 1){
			divCol.style.width = oRec.iWidth + "%";
			iLeft += oRec.iWidth;
		} else {
			if (oRec.iWidth){
				divCol.style.width = oRec.iWidth + "%";
			} else {
				divCol.style.width = (100 - iLeft) + "%";
			}
		}
	}	
};



ENTER_GRID.prototype.row = function
	(iRowNo){
	var iNumCols = this.oEle.aColumns.length;
	for (var iJ = 0; iJ < iNumCols; iJ++){
		divCol = A.dg("gridcol_" + this.oEle.iGridNo + "_" + iJ);
		var sClassName = "gridcell";
		//+ (iRowNo % 2);
		if (!iJ){
			sClassName = "gridcellrowno";
		}
		var divCell = E.div(divCol, sClassName,
			"gridcell_" + this.oEle.iGridNo + "_" + iRowNo + "_" + iJ);
		if ((!iJ) && (iRowNo < this.iNumRecords) &&
			(iRowNo < this.iNumRecords)){
			var divDesc = E.div(divCell, "gridcellrownodesc");
			divDesc.textContent = (iRowNo + 1);
		}
	}
	var divRowOver = E.div(this.oDiv.cellsbox, "gridrowover");
	if (!this.oEle.bNoEdit){
		A.action.call(divRowOver, "click", 
			this.action, [this, "parent_edit" + this.sName + "_" + iRowNo]);
	}
	if (this.oEle.iRowOverWidth){
		divRowOver.style.width = this.oEle.iRowOverWidth + "%";
	}
};



ENTER_GRID.prototype.dataA = function(oData){
	console.log(oData);
	var that = V.aAA[oData.iGridNo];
	that.aData = oData.aData;
	if (that.aData){
		that.iNumRecords = that.aData.length;
		that.iPageNumber = 1;
		that.oEle.that.action("data" + that.sName);
		that.populate();
	}
};



ENTER_GRID.prototype.data = function(){
	A.ajax({aURL:["grid_data", "name", this.oEle.sName, "no", 
		this.sName.split("_")[1]], 
		fCallback:this.dataA});
};



ENTER_GRID.prototype.exporter = function(){
	var iTime = A.timeUnix();
	V.oGen.oSession.iTimeClient = iTime;
	var sURL = btoa("?a=gridexport" + 
		"&time=" + iTime + 
		"&session=" + V.oGen.oSession.sID + 
		"&form=" + this.oEle.sFormName +
		"&tab=" + this.oEle.iTabNo
	);
	window.open(V.sSrvURL + "?link=" + sURL, "_parent");
};



ENTER_GRID.prototype.findRecordIndex = function(iGridNo, iRowNo){
	var divRowNo = A.dg("gridcell_" + iGridNo + "_" + iRowNo + "_0");
	var iRecordID = divRowNo.iRecordID;
	var oGrid = V.aAA[iGridNo];
	var iNumRecords = oGrid.iNumRecords;
	var aData = oGrid.aData;
	if (oGrid.aDataFiltered){
		iNumRecords = oGrid.iNumRecordsFiltered;
		aData = oGrid.aDataFiltered;
	}
	var iFound = 0;
	var iI = 0;
	while ((!iFound) && (iI < iNumRecords)){
		if (aData[iI][0] == iRecordID){
			iFound = (iI + 1);
		} else {
			iI++;
		}
	}
	var aReturn = 0;
	if (iFound){
		aReturn = aData[iFound - 1];
	} 
	return aReturn;
};



ENTER_GRID.prototype.initialize = function(oEle){
	this.sName = "grid_" + oEle.iGridNo;
	this.oDiv.box = E.div(oEle.divParent, "gridbox", "grid_" + oEle.iGridNo);
	this.oDiv.box.style.top = (oEle.aPos[1] - 10) + "px";
//	this.oDiv.box.style.height = (1358 - oEle.aPos[1]) + "px";
	this.toolbar();
	this.oDiv.columnsheading = E.div(this.oDiv.box, "gridcolumnsheading");
	E.div(this.oDiv.box, "gridcolumnsheadinghidescroll");
	A.selectNone(this.oDiv.columnsheading);
	this.oDiv.cellsbox = E.div(this.oDiv.box, "gridcellsbox");
	oEle.aColumns.unshift({sName:"", iWidth:0});
	var iLeft = 0;
	var iNumCols = oEle.aColumns.length;
	for (var iI = 0; iI < iNumCols; iI++){
		var oRec = oEle.aColumns[iI];
		var divCol = E.div(this.oDiv.columnsheading, "gridcolumn");
		divCol.style.left = iLeft + "%";
		if (iI < iNumCols - 1){
			divCol.style.width = (oRec.iWidth) + "%";
			iLeft += oRec.iWidth;
		} else {
			divCol.style.width = (100 - iLeft) + "%";
		}
		var divHeading = E.div(divCol, "gridcolumnheading");
		if (!iI){
			divHeading.style.border = "none";
		}
		var divDesc = E.div(divHeading, "gridcolumnheadingdesc");
		if ((iI) && (!oRec.bNoSort)){
			A.action.call(divHeading, "click", this.action, 
				[this, "sort_" + oEle.iGridNo + "_" + iI]);
			var divIcon = E.div(divHeading, "icongridsort", 
				"gridsort_" + oEle.iGridNo + "_"+ iI);
			divIcon.innerHTML = "&#xE05E;";
			if (iI == oEle.oSort.iColumn){
				divIcon.style.display = "none";
				if (oEle.oSort.iDirection == 1){
					divIcon.innerHTML = "&#xE05F;";					
				}
			}
		}
		divDesc.textContent = oRec.sName;
	}
//	this.resize();
	if (!this.oEle.bNoPopulate){
		this.data();
	}
	return this.oDiv.box;
};



ENTER_GRID.prototype.populate = function(){
	var iI = 0;
	this.clear();
	this.iNumRecords = this.aData.length;
	var iNumRows = this.iNumRecords;
	var iFirstRecordShow = ((this.iPageNumber - 1) * iNumRows);
	if (iFirstRecordShow < 0){
		iFirstRecordShow = 0;
	}
	var iNumCols = this.oEle.aColumns.length;
	var iGridNo = this.oEle.iGridNo;
	var aData = this.aData;
	var divCell = 0;
	var oEle = {};
	if (this.aDataFiltered){
		aData = this.aDataFiltered;
		this.iNumRecordsFiltered = aData.length;
		iNumRows = this.iNumRecordsFiltered;
	}
	for (iI = 0; iI <= this.iNumRecords - 1; iI++){
		this.row(iI);
	}

	for (iI = 0; iI < iNumRows; iI++){
		var iRowIndex = iI + iFirstRecordShow;
		var aRow = aData[iRowIndex];
		if (aRow){
			divCell = A.dg("gridcell_" + iGridNo + "_" + iI + "_0");
			if (!divCell){
				this.row();
			}
			for (var iJ = 1; iJ < iNumCols; iJ++){
				divCell = A.dg("gridcell_" + iGridNo + "_" + iI + "_" + iJ);
				if (this.oEle.aColumns[iJ].oEle){
					oEle = {};
					oEle.sText = this.oEle.aColumns[iJ].oEle.sText;
					oEle.sType = this.oEle.aColumns[iJ].oEle.sType;
					oEle.sAction = this.oEle.aColumns[iJ].oEle.sAction;
					oEle.divParent = divCell;
					oEle.that = this;
					oEle.sClassName = "grid";
					oEle.bNoPos = 1;
					oEle.sAction = "parent_" + oEle.sAction + this.sName + "_" + 
						iRowIndex;
					E[oEle.sType](oEle);
				} else {
					var divDesc = E.div(divCell, "gridcelldesc");
					divDesc.textContent = aRow[iJ];
					if (this.oEle.aColumns[iJ].sAction){
						var sAction = "parent_" + this.oEle.aColumns[iJ].sAction + 
							this.sName + "_" + iRowIndex;
						A.action.call(divCell, "click", this.action, 
							[this, sAction]);

					}
				}
			}		
			divCell = A.dg("gridcell_" + iGridNo + "_" + iI + "_0");
			if (divCell){
				divCell.iRecordID =  parseInt(aRow[0]);
			}
		}
	}
	A.resizeBodyA();
};



ENTER_GRID.prototype.resize = function(){
	var iHeight = V.oDiv.body.clientHeight - this.oEle.aPos[1] - 120;
	var divParent = this.oDiv.box.parentNode;
	this.oDiv.box.style.height = (iHeight) + "px";
	this.oDiv.cellsbox.style.height = (iHeight - 80) + "px";
	var iHRecs = this.iNumRecords;
	if (this.iNumRecordsFiltered){
		iHRecs = this.iNumRecordsFiltered;
	}
	iHRecs *= 46;
	if (iHRecs < (iHeight - 80)){
		this.oDiv.cellsbox.style.height = (iHRecs) + "px";
	}
};



ENTER_GRID.prototype.sort = function(iColumn){
	if (!iColumn){
		iColumn = this.oEle.oSort.iColumn;
		this.oEle.oSort.iDirection *= -1;
	}
	iNumCols = this.oEle.aColumns.length;
	this.oEle.oSort.iDirection *= -1;
	for (iI = 1; iI < iNumCols; iI++){
		var divIcon = A.dg("gridsort_" + this.oEle.iGridNo + "_" + iI);
		if (divIcon){
			if (iI == iColumn){
				divIcon.style.display = "none";
				var bNumerical = this.oEle.aColumns[iI].bNumerical;
				var bReverse = 0;
				if (this.oEle.oSort.iDirection == 1){
					bReverse = 1;
					divIcon.innerHTML = "&#xE05F;";
				} else {
					divIcon.innerHTML = "&#xE05E;";
				}
				var aData = this.aData;
				if (this.aDataFiltered){
					aData = this.aDataFiltered;
				}
				aData = A.sort(aData, iColumn, bNumerical, bReverse);
				if (this.aDataFiltered){
					this.aDataFiltered = aData;
				} else {
					this.aData = aData;
				}
				this.populate();
			} else {
				divIcon.style.display = "none";
			}
		}
	}
};



ENTER_GRID.prototype.toolbar = function(){
	var divIcon = 0;
	var divDesc = 0;
	this.oDiv.toolbar = E.div(this.oDiv.box, "gridtoolbar");
	A.selectNone(this.oDiv.toolbar);
	this.oDiv.search = E.input({divParent:this.oDiv.toolbar, 
		sText:"", sClassName:"ele-inputgridsearch", aPos:[0, -0.7],
		sID:"gridsearch_" + this.oEle.iGridNo});
	this.oDiv.search.parentNode.parentNode.style.left = "0";
	this.oDiv.search.parentNode.parentNode.style.right = "10px";
	this.oDiv.search.parentNode.parentNode.style.position = "absolute";
	this.oDiv.search.value = V.oData.oInit.oBanners.oGeneral.sSearch;
	this.oDiv.search.style.color = "#CBCDCF";
	this.oDiv.search.onfocus = function(){
		var that = V.aAA[document.activeElement.id.split("_")[1]];
		if (!that){
			return;
		}
		var sValue = that.oDiv.search.value;
		if (sValue == V.oData.oInit.oBanners.oGeneral.sSearch){
			that.oDiv.search.value = "";
			that.oDiv.search.style.color = "black";
		}
	};
	A.action.call
		(this.oDiv.search, "keypress", this.action, [this, "searchtyping"]);
	divIcon = E.div
		(this.oDiv.search.parentNode.parentNode, "icongridsearchclear");
	divIcon.innerHTML = "&#xE06E;";
	divIcon.style.display = "none";
	this.oDiv.searchclear = divIcon;
	A.action.call(divIcon, "click", this.action, [this, "searchclear"]);
	/*
	divIcon = E.div(this.oDiv.toolbar, "icongridexport");
	divIcon.innerHTML = "&#xE062;";
	A.action.call(divIcon, "click", this.action, [this, "export"]);
	*/
};



		ENTER_GRID.bInited = 1;
	}
}
//== END OF GRID CLASS =========================================================






V.oGen.bHouseLoaded = 1;
