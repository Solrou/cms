//VARIABLES  ===================================================================
var V ={
	sSrvURL: "h2srv/",
	aAA:[],
	oDiv:{},
	oDB:{
		oWords:{sLoginTitlebar:"Login", sButtonCancel:"CANCEL", 
			oDate:{sDays:"Su|Mo|Tu|We|Th|Fr|Sa",sMonths:"January|February|March|April|May|June|July|August|September|October|November|December",sDaysLong:"Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday"},
			sButtonOK:"OK", sButtonNextStep:"NEXT STEP"},
		oError:{
			oInput:{
				empty:"Required field is empty.",
				email:"Invalid email address.",
				password:"Invalid password.",
			},
		},
		oEnum:{
			aSelect:[{iID:-1,sValue:"--- Select ---"}],
			sUserType:[{iID:1,sValue:"Contributor"},{iID:2,sValue:"Administrator"}],
			sOrganisationType:[{iID:0,sValue:"NGO"},{iID:1,sValue:"CBO"},{iID:2,sValue:"FBO"},{iID:3,sValue:"Section 21"},{iID:4,sValue:"Trust"},{iID:5,sValue:"Other"}],
			sNeedType:[{iID:0,sValue:"Time"},{iID:1,sValue:"Funds"},{iID:2,sValue:"Items"}],
			sPostPriority:[{iID:0,sValue:"Normal"},{iID:1,sValue:"High"}],
			sPostStatus:[{iID:0,sValue:"Inactive"},{iID:1,sValue:"Active"},{iID:2,sValue:"Deleted"},{iID:3,sValue:"Archived"}],
			sOrganisationCategory:[{iID:0,sValue:"Animal Welfare"},{iID:1,sValue:"Education"},{iID:2,sValue:"Environment"},{iID:3,sValue:"Health"},{iID:4,sValue:"Safety"},{iID:5,sValue:"Social Services"}],
			sReadStatus:[{iID:0,sValue:"Unread"},{iID:1,sValue:"Read"}],
			sInboxType:[{iID:0,sValue:"Email"},{iID:1,sValue:"Push"}],
			sPushType:[{iID:0,sValue:"None"},{iID:1,sValue:"Post"},{iID:2,sValue:"Success Sory"},{iID:3,sValue:"Message"}],
			sRegionType:[{iID:0,sValue:"Country"},{iID:1,sValue:"Province"},{iID:2,sValue:"Suburb"},{iID:3,sValue:"Town"}]
		},
		oHouseText:{
			sIconLogo:"&#xE05B;",
			sText1:"Helping Out is a mobile platform that enables non-profit organisations to receive help by advertising their needs free of charge to individuals who are willing to volunteer their time or donate to a good cause.",
			aSocials:[{sIcon:"&#xE073",sLink:"http://fb"},{sIcon:"&#xE07C",sLink:"http://fb"},{sIcon:"&#xE083",sLink:"mailto:helpouting@helpingout.com"}],
			sIconStep:"&#xE080",
			sText21:"Are you an organisation that",
			sText22:"NEEDS HELP?",
			aIcons2:[
			{sIcon:"&#xE081",sText1:"REGISTER",sText2:"Register your organisation online"},
			{sIcon:"&#xE082",sText1:"LIST NEEDS",sText2:"List things your organisation needs help with on the website"},
			{sIcon:"&#xE083",sText1:"GET HELP",sText2:"Receive commitments from individuals via your organisation's mail box"}
			],
			sButtonRegister:"REGISTER NOW",
			sText31:"Are you an individual looking to",
			sText32:"HELP OUT?",
			aIcons3:[
			{sIcon:"&#xE084",sText1:"DOWNLOAD",sText2:"Download the app on your phone"},
			{sIcon:"&#xE085",sText1:"FIND NEEDS",sText2:"Find and commit to needs close to your heart and connect with organisations"},
			{sIcon:"&#xE086",sText1:"HELP OUT",sText2:"Help out with needs you found and make the world a better place"}
			],
			aAppLinks:["https://play.google.com","https://www.apple.com"],
			aSocials:[
			{sIcon:"&#xE073",sName:"socialfacebook",sURL:"https://www.facebook.com/helpingoutza/"},
			{sIcon:"&#xE07C",sName:"socialtwitter",sURL:"https://twitter.com/@Helpingout_ZA"},
			{sIcon:"&#xE083",sName:"socialemail",sURL:"mailto:helpout@4imobile.co.za"}
			],
			sDocs:"View our |Terms & Conditions| and/or |Privacy Policy",
			aMenusLeft:["HOME", "CREATE POST", "MY POSTS", "USERS", "PROFILE"],
			aMenusTop:["Login", "Register"],
			aTabsRegister:["USER LOGIN", "BASIC INFO", "CONTACT INFO", "ADDRESS", "LINKS"],
			aTabsMyPosts:["ACTIVE", "DRAFTS", "HISTORY"],
			aFooterButtons1:["CANCEL", "NEXT STEP"],
			oUserInfo:{sCompany:"SOLO Dogs", sName:"Sol Rousseau",
				sLogo:"data/0delmeavatar.jpg"},
				
		},
		oForm:{
			aLogin:[
				{sType:"email", sText:"Email", sClassName:"popup"},
				{sType:"password", sText:"Password", sClassName:"popup"},
				{sType:"button", sText:"LOGIN", sClassName:"centered act", bDefault:1,
					sID:"login", bCanDisable:1},
				{sType:"href", sText:"Forgot Password", sClassName:"centered act"},
				{sType:"memo", sText:"Not registered yet?", sClassName:"notregisteredyet"},
				{sType:"href", sText:"Register Now", sClassName:"registernow act"},
			],
			aLoginForgotPassword:[
				{sType:"memo", sText:"Use this form to reset your password. You will receive a password reset link via email when you continue.", sClassName:"popup"},
				{sType:"email", sText:"Email", sClassName:"popup"},
				{sType:"button", sText:"SEND RESET LINK", sClassName:"centered act",
					bDefault:1, sID:"sendresetlink"},
			],
			aUserBox:[
				{sType:"button", sText:"PROFILE", sClassName:"menuuserbox act"},
				{sType:"button", sText:"CHANGE PASSWORD", sClassName:"menuuserbox act"},
				{sType:"button", sText:"LOGOUT", sClassName:"menuuserbox act"},				
			],	
			aUserBoxSmall:[
				{sType:"button", sText:"CREATE POST", sClassName:"menuuserbox act"},
				{sType:"button", sText:"MY POSTS", sClassName:"menuuserbox act"},
				{sType:"button", sText:"USERS", sClassName:"menuuserbox act"},				
				{sType:"button", sText:"PROFILE", sClassName:"menuuserbox act"},
				{sType:"button", sText:"CHANGE PASSWORD", sClassName:"menuuserbox act"},
				{sType:"button", sText:"LOGOUT", sClassName:"menuuserbox act"},				
			],	
			aReg1:[
				{sType:"line", sClassName:"registerpage"},
				{sType:"input", sText:"Name", bRequired:1},
				{sType:"input", sText:"Surname", bRequired:1},
				{sType:"email", sText:"Email", bRequired:1},
				{sType:"password", sText:"Password", bRequired:1},
				{sType:"password", sText:"Confirm Password", bRequired:1},
				{sType:"line", sClassName:"registerpage"},
			],
			
			aReg2:[
				{sType:"line", sClassName:"registerpage"},
				{sType:"input", sText:"Organisation Name", bRequired:1},
				{sType:"upload", sText:"Upload Company Logo", bRequired:1},
				{sType:"imagebox", sText:"Company Logo"},
				{sType:"select", sText:"Organisation Category", 
					sEnum:"sOrganisationCategory", bRequired:1},
				{sType:"select", sText:"Organisation Type", sEnum:"sOrganisationType"},
				{sType:"inputarea", sText:"Description / Statement"},
				{sType:"line", sClassName:"registerpage"},
			],
		
			aReg3:[
				{sType:"line", sClassName:"registerpage"},
				{sType:"input", sText:"Contact Person", bRequired:1},
				{sType:"email", sText:"Email", bRequired:1},
				{sType:"telephone", sText:"Contact Number", bRequired:1},
				{sType:"weekhours", sText:"Business Hours"},
				{sType:"line", sClassName:"registerpage"},
			],
		
			aReg4:[
				{sType:"line", sClassName:"registerpage"},
				{sType:"address"},
				{sType:"line", sClassName:"registerpage"},
			],
			
			aReg5:[
				{sType:"line", sClassName:"registerpage"},
				{sType:"line", sClassName:"registerpage"},
				{sType:"line", sClassName:"registerpage"},
				{sType:"line", sClassName:"registerpage"},
			],
			aMyPosts1:[
				{sType:"grid",iTabNo:1,sName:"mypostsactive",sFormName:"MyPosts",aPos:[0,10,0,0],oSort:{iColumn:4,iDirection:1},iRowOverWidth:70,
					aColumns:[{sName:"Title",iWidth:25},{sName:"Responses",iWidth:12,bNumerical:1},{sName:"Type",iWidth:13},{sName:"Date",iWidth:20},{sName:"",bNoSort:1,
						oEle:{sType:"button",sText:"DUPLICATE TO DRAFTS",
							sAction:"activeduplicatetodrafts"}
					}]},
				{sType:"line", sClassName:"registerpage"},
			],
			aMyPosts2:[
				{sType:"input", sText:"Bla"},
			],
			aMyPosts3:[
				{sType:"address"},
			],
			
		},
		
	},
	oGen:{
		iLastKey:0,
		divActive:0,
		iMenuNo:0,
		oMouse:{},
		bResizing:0,
		iScrollTop:0,
		iWidthScrollbar:0,
		oZIndex:{iIndex:5, divLast:0},
		sIDButtonDefault:"",
		sIDButtonDisabled:"",
		bUserBoxShow:0,
		iWidthWindow:0,
		iAAStart:1, iAAMax:1,
		bJSLoaded:0,
		sRows:"",/*				"1<br>2<br>3<br>4<br>5<br>6<br>7<br>8<br>9<br>0<br>" +
		"1<br>2<br>3<br>4<br>5<br>6<br>7<br>8<br>9<br>0<br>" +
		"1<br>2<br>3<br>4<br>5<br>6<br>7<br>8<br>9<br>0<br>" ,*/

		iDisplayScreenSize:0,
		aDisplayScreenSizes:[
			{//0
				iFontSize:10,
				iHeightFooter:20,
				iHeightHeader:48,
				iHeightMainMin:200,
				iHeightMenuLeft:150,
				iHeightTabs:75,
				iWidthLogo:60,
				iWidthLeft:0,
				iWidthMain:160,
			},
		
			{//1
				iFontSize:10,
				iHeightFooter:30,
				iHeightHeader:48,
				iHeightMainMin:300,
				iHeightTabs:75,
				iWidthLogo:80,
				iWidthLeft:0,
				iWidthMain:360,
			},
			{//2
				iFontSize:11,
				iHeightFooter:30,
				iHeightHeader:48,
				iHeightLogo:100,
				iHeightMainMin:500,
				iHeightTabs:75,
				iWidthLeft:120,
				iWidthLogo:120,
				iWidthMain:640,
			},
			{//3
				iFontSize:12,
				iHeightFooter:30,
				iHeightHeader:50,
				iHeightLogo:130,
				iHeightMainMin:600,
				iHeightTabs:97,
				iWidthLeft:160,
				iWidthLogo:160,
				iWidthMain:1040,
			},
			{//4
				iFontSize:13,
				iHeightFooter:50,
				iHeightHeader:50,
				iHeightLogo:150,
				iHeightMainMin:600,
				iHeightTabs:110,
				iWidthLeft:200,
				iWidthLogo:200,
				iWidthMain:1440,
			},
			{//5
				iWidthMain:10000,
			}
		],
		
	}
};



//APP CLASS  ===================================================================
var A = {
		
		
		
action:function(oObj, sAction){
	console.log(oObj.sName + " initaction: " + sAction);
	return function(){
		console.log(oObj.sName + " action: " + sAction);
		oObj.action(sAction);
	}
},



ajaxA:function(oAjax){
	oAjax.oXHR = new XMLHttpRequest();
	if ("withCredentials" in oAjax.oXHR){
		oAjax.oXHR.open("POST", V.sSrvURL, true);
	} else if (typeof XDomainRequest != "undefined"){
		oAjax.oXHR = new XDomainRequest();
		oAjax.oXHR.open("POST", V.sSrvURL);
	} else {
		return;
	}
	oAjax.oXHR.setRequestHeader("Content-type", 
		"application/x-www-form-urlencoded; charset=utf-8");
	oAjax.oXHR.onload = function(){
		var oData = A.json(atob(oAjax.oXHR.responseText));
//		var oData = A.json(oData);
		if (oAjax.fCallback){
			oAjax.fCallback(oData);
		}
		V.aAA[oAjax.iAjaxNo] = 0;
	};
	var sData = A.json(1, {sAction:oAjax.sAction, aValues:oAjax.aValues});
	sData = encodeURIComponent(sData);
	oAjax.oXHR.send("data=" + btoa(sData));	
},



ajax:function(sAction, aValues, fCallback){
	var iObjNo = A.objectnew();
	V.aAA[iObjNo] = {sAction:sAction, aValues:aValues, fCallback:fCallback};
	V.aAA[iObjNo].sType =  "ajax";
	V.aAA[iObjNo].iStatus =  0;
	V.aAA[iObjNo].iObjNo =  iObjNo;
},



dg:function(sID){
	return document.getElementById(sID);
},



gcn:function(sClassName, bAll){
	var iI = 0;
	var aReturn = document.getElementsByClassName(sClassName);
	if (!aReturn.length){
		aReturn = [V.oDiv.dummy];
		console.log("V.oDiv.dummy = "+sClassName)
	}
	if (bAll){
		var iSize = aReturn.length;
		var aRet1 = [];
		for (iI = 0; iI < iSize; iI++){
			var oRec = aReturn[iI];
			if (oRec.className.indexOf(sClassName) != -1){
				aRet1.push(oRec);
			}
		}
		return aRet1;
	} else {
		return aReturn[0];
	}
},


history:function(sHash){
	var sHashNow = window.location.href.split("#")[1];
	var sTitle = document.title.split(" - ")[0];
	if (sHashNow != sHash){
		window.history.pushState({"html":"", "pageTitle":""}, 
			"", ".#" + sHash);
	}
},



json:function(sKey, sValue){
	if (sValue){
		return JSON.stringify(sValue);
	} else {
		return JSON.parse(sKey);
	}
},



objectfind:function(sName){
	var iI = V.oGen.iAAStart;
	var iFound = 0;
	while ((!iFound) && (iI <= V.oGen.iAAMax)){
		var oRec = V.aAA[iI]; 
		if ((oRec.sName) && (oRec.sName == sName)){
			iFound = iI;
		} else {
			iI++;
		}
	}
	if (iFound){
		return V.aAA[iFound];
	}
},



objectnew:function(){
	var iI = V.oGen.iAAStart;
	while ((V.aAA[iI] !== undefined) && (V.aAA[iI] !== 0)){
		iI++;
	}
	if (iI > V.oGen.iAAMax){
		V.oGen.iAAMax = iI;
	}
	V.aAA[iI] = {};
	return iI;
},



session:function(sKey, sValue){
	var sAppName = document.title.replace(/ /g, "");
	var oSession = sessionStorage.getItem(sAppName);
	if (oSession){
		oSession = A.json(oSession);
	} else {
		//create
		oSession = {iTimeClient:0, iTimeServer:0, sURLHash:""};
		sessionStorage.setItem(sAppName, A.json(0, oSession));
	}
	if ((sValue) || (sValue === 0)){
		oSession = A.json(sessionStorage.getItem(sAppName));
		oSession[sKey] = sValue;
		sessionStorage.setItem(sAppName, A.json(0, oSession));
		return sValue;
	} else {
		return oSession[sKey];
	}
},



valid:function(oEle){
	var bValid = 1;
	switch (oEle.sType){
		case "email":
			var sReg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if ((oEle.sValue) && (!sReg.test(oEle.sValue))){
				bValid = 0;
			}
		break;
		
		case "password":
			if (oEle.sValue.length < 6){
				bValid = 0;
			}
		break;
			
	}
	return bValid;
},



value:function(sID, sValue){
	var oReturn = {};
	var divX = 0;
	var iIndex = (parseInt(sValue) * 1);
	divX = A.dg(sID);
	if (!divX){
		return;
	}
	if ((!sValue) && (!iIndex)){
		//get
		switch (divX.type){
			case "email":
				oReturn = {sValue:divX.value, sType:divX.type, sID:sID};
			break;
			
			case "password":
				oReturn = {sValue:divX.value, sType:divX.type, sID:sID};
			break;
	
		}
	} else {
		//set
		switch (divX.type){
			case "select-one":
				divX.innerHTML = "";
				var iIndex = 0;
				var divOption = E.div(divX, "eleselectoption", "", "option");
				divOption.value = -1;
				divOption.text = V.oDB.oEnum.aSelect[0].sValue;
				sValue.forEach(function(oRec){
					var divOption = E.div(divX, "eleselectoption", "", "option");
					divOption.value = iIndex;
					divOption.text = oRec.sValue;
				});
			break;
			
		}
	}
	return oReturn;
},



zindex:function(divX){
	if (divX){
		if (V.oGen.oZIndex.divLast != divX){
			divX.style.zIndex = V.oGen.oZIndex.iIndex;
			V.oGen.oZIndex.iIndex++;	
		}
		V.oGen.oZIndex.divLast = divX;
/*		if (divX != V.oDiv.header){
			A.zindex(V.oDiv.header);
		}*/
	}
}
};



//BODY CLASS  ==================================================================
var B = {
	sName:"BODY",
	oOb:{iLogin:0, iRegister:0, iHome:0, iCreatePost:0, iMyPosts:0,
		iUsers:0, iProfile:0},
	
	
actions:function(){
	var aDivAct = A.gcn("act", 1);
	var iI = 0;
	aDivAct.forEach(function(oRec){
		var sClassName  = oRec.className;
		if (sClassName == "maindummy"){
			delete oRec;
		} else {
			var sClass = "";
			var aClassName = sClassName.split(" ");
			aClassName.forEach(function(sName){
				if (sName != "act"){
					sClass += sName + " ";
				}
			});
			sClass = sClass.slice(0, -1);
			var sType = oRec.type;
			var sText = "";
			switch (sType){
				case "menu":
					sText = oRec.childNodes[0].textContent;
				break;
				
				case "icon":
					sText = oRec.name;
				break;
				
				case "href":
					sText = oRec.childNodes[0].textContent;
				break;
				
				case "image":
					sText = oRec.name;
				break;
				
				case "span":
					sText = oRec.name;
				break;
				
				case "button":
					sText = oRec.childNodes[0].textContent;
				break;
				
			}
			sText = sText.replace(/ /g, "").toLowerCase().substr(0, 15);
			oRec.className = sClass;
			sClass = sClass.replace(/ /g, ".");
			var sID = oRec.id;
			if (!sID){
				sID = "";
			}
			oRec.onclick = A.action(B, sClass + "-" + sText + "-" + sID);
		}
		iI++;
	});
},



action:function(sAction){
	var aAction = sAction.split("-");
	var sElement = aAction[0].split(".")[0];
	switch (sElement){
		case "login":
			V.aAA[this.oOb.iLogin].action(sAction);
		break;
		
		case "mainlogo1":
			V.oGen.iMenuNo = 0;
			B.reset()
		break;
			
		case "menuleft":
			var iIndex = parseInt(aAction[2].split("_")[1]);
			var divX = A.gcn("menuleft active");
			if (divX.className != "maindummy"){
				divX.className = "menuleft";
			}
			var aDivMenus = A.gcn("menuleft", 1);
			aDivMenus.forEach(function(divX){
				var aID = divX.id.split("_");
				if (aID[1] == iIndex){
					divX.className = "menuleft active";
				} else {
					divX.className = "menuleft";
				}
			});
			switch (iIndex){
				case 1:
					if (!this.oOb.iCreatePost){
						this.oOb.iCreatePost = A.objectnew();
						V.aAA[this.oOb.iCreatePost] = new HO_CREATEPOST();
						V.aAA[this.oOb.iCreatePost].iObjNo = this.oOb.iCreatePost;
					}
					V.aAA[this.oOb.iCreatePost].init();		
				break;
				
				case 2:
					if (!this.oOb.iMyPosts){
						this.oOb.iMyPosts = A.objectnew();
						V.aAA[this.oOb.iMyPosts] = new HO_MYPOSTS();
						V.aAA[this.oOb.iMyPosts].iObjNo = this.oOb.iMyPosts;
					}
					V.aAA[this.oOb.iMyPosts].init();		
				break;
				
				case 3:
					if (!this.oOb.iUsers){
						this.oOb.iUsers = A.objectnew();
						V.aAA[this.oOb.iUsers] = new HO_USERS();
						V.aAA[this.oOb.iUsers].iObjNo = this.oOb.iUsers;
					}
					V.aAA[this.oOb.iUsers].init();		
				break;
				
				case 4:
					if (!this.oOb.iProfile){
						this.oOb.iProfile = A.objectnew();
						V.aAA[this.oOb.iProfile] = new HO_PROFILE();
						V.aAA[this.oOb.iProfile].iObjNo = this.oOb.iProfile;
					}
					V.aAA[this.oOb.iProfile].init();		
				break;
			}
			
		break;

		case "menutop":
			switch (aAction[1]){
				case "login":
					if (!this.oOb.iLogin){
						this.oOb.iLogin = A.objectnew();
						V.aAA[this.oOb.iLogin] = new HO_LOGIN();
						V.aAA[this.oOb.iLogin].iObjNo = this.oOb.iLogin;
					}
					V.aAA[this.oOb.iLogin].init();		
				break;
				
				case "register":
					if (!this.oOb.iRegister){
						this.oOb.iRegister = A.objectnew();
						V.aAA[this.oOb.iRegister] = new HO_REGISTER();
						V.aAA[this.oOb.iRegister].iObjNo = this.oOb.iRegister;
					}
					V.aAA[this.oOb.iRegister].init();
				break;
				
			}
		break;
		
		case "menuuserboxover":
			if (B.oOb.iHome){
				V.aAA[B.oOb.iHome].userbox();
			}
		break;
			
		case "elehref":
			switch (aAction[1]){
				case "forgotpassword":
					V.aAA[this.oOb.iLogin].passwordforgot();
				break;
				case "registernow":
					E.action("popupclose");
					B.action("menutop-register");
				break;
			}			
		break;

		case "eleiconover":
			switch (aAction[1]){
				case "popupclosex":
					E.action("popupclosex");
				break;
				case "logo":
					var iUser = A.session("iUser");
					if (iUser){
						V.oGen.iMenuNo = 1;
						B.reset();						
					} else {
						V.oGen.iMenuNo = 0;
						B.reset();
					}
				break;
				case "socialfacebook":
					
				break;
				case "socialtwitter":
					
				break;
				case "socialemail":
					
					break;
			}			
		break;

		case "eleimage":
			switch (aAction[1]){
				case "android":
					
				break;
				case "apple":
					
				break;
			}			
		break;
		
		case "elebutton":
			switch (aAction[1]){
			
				case "changepassword":
					V.aAA[this.oOb.iHome].changepassword();
				break;
				
				case "login":
					V.aAA[this.oOb.iLogin].go();
				break;
				
				case "logout":
					V.aAA[this.oOb.iHome].logout();
				break;
				
				case "ok":
					if (aAction[2] == "okpopup"){
						E.action("popupclose");
					}
				break;
				
				case "profile":
					V.aAA[this.oOb.iHome].profile();
				break;
				
				case "createpost":
					V.aAA[this.oOb.iHome].userbox();
					B.action("menuleft--_1");
				break;
				
				case "myposts":
					V.aAA[this.oOb.iHome].userbox();
					B.action("menuleft--_2");
				break;
				
				case "users":
					V.aAA[this.oOb.iHome].userbox();
					B.action("menuleft--_3");
				break;
				
				case "registernow":
					E.action("popupclose");
					B.reset(2);
				break;
				
			}
			if (!aAction[2]){
				return;
			}
			aAction = aAction[2].split("_");
			switch (aAction[0]){
			
				case "regbutton":
					V.aAA[this.oOb.iRegister].step(aAction);
				break;
				
				case "tabbutton":
					V.aAA[this.oOb.iMyPosts].step(aAction);
				break;
				
				
				case "footerbutton":
					V.aAA[this.oOb.iRegister].footer(sAction);
				break;
				
			}
			
		break;
	
		case "footerdocslinks":
			switch (aAction[1]){
				case "terms":
					
				break;
				case "privacy":
					
				break;
			}
		break;
	


	}
},



height:function(){
	var divBody = document.body;
	var divHTML = document.documentElement;
	var iH = Math.max(divBody.scrollHeight,divBody.offsetHeight, 
		divHTML.clientHeight, divHTML.scrollHeight, divHTML.offsetHeight);
	return iH;
},



initScrollbar:function(){
	var divOuter = document.createElement("div");
  divOuter.style.visibility = "hidden";
  divOuter.style.width = "100px";
  divOuter.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps
  document.body.appendChild(divOuter);
  var iWidthNoScroll = divOuter.offsetWidth;
  divOuter.style.overflow = "scroll";
  var divInner = document.createElement("div");
  divInner.style.width = "100%";
  divOuter.appendChild(divInner);        
  var iWidthWithScroll = divInner.offsetWidth;
  divOuter.parentNode.removeChild(divOuter);
  return iWidthNoScroll - iWidthWithScroll;
},



initEvents:function(){
	screen.orientation.addEventListener("change", function(){
		B.resizeA();
	});
	window.onresize = B.resizeA;
	V.oDiv.body.onkeyup = B.onKeyPress;
	V.oDiv.body.onmousemove = B.onMouseMove;
	window.onhashchange = B.onHashChange;
	window.onbeforeunload = B.onBeforeUnload;
},



init:function(){
	V.oDiv.body = document.getElementsByTagName("body")[0];
	V.oGen.iWidthScrollbar = B.initScrollbar();
	this.initEvents();
	var iUser = A.session("iUser");
	var iMenuNo = V.oGen.iMenuNo;
	var sURLHash = A.session("sURLHash");
	if (!sURLHash){
		A.history("home");
		A.session("sURLHash", "home");
	}
	if (sURLHash){
		switch (sURLHash){
			case "home":
				iMenuNo = 0;
			break;
			
			case "register":
				iMenuNo = 2;
			break;
			
		}		
	}
	if (iUser){
		iMenuNo = 1;
		V.oGen.iMenuNo = 1;
		this.oOb.iHome = A.objectnew();
		V.aAA[this.oOb.iHome] = new HO_HOME();
		V.aAA[this.oOb.iHome].iObjNo = this.oOb.iHome;
	}
	/*
	var iMenuNo = A.session("iMenuNo");
	if (!iMenuNo){
		iMenuNo = V.oGen.iMenuNo;
	}
	if (iUser){
		iMenuNo = 1;
		V.oGen.iMenuNo = 1;
		this.oOb.iHome = A.objectnew();
		V.aAA[this.oOb.iHome] = new HO_HOME();
		V.aAA[this.oOb.iHome].iObjNo = this.oOb.iHome;
	}
	var aHash = window.location.href.split("#");
	if (aHash[1]){
		switch (aHash[1]){
			case "home":
				iMenuNo = 0;
			break;
			
			case "register":
				iMenuNo = 2;
			break;
		}
	} else {
		A.history("home");
	}
	A.session("iMenuNo", iMenuNo);
	*/
	V.oGen.iMenuNo = iMenuNo;
	this.reset();
	window.setInterval(B.loopMain, 500);
},



loopMain: function(){
	//ajax
	var iI = V.oGen.iAAStart;
	var iFound = 0;
	while ((!iFound) && (iI <= V.oGen.iAAMax)){
		var oRecord = V.aAA[iI];
		if ((oRecord) && (oRecord.sType)){
			if (oRecord.iStatus == 1){
				V.aAA[iI] = 0;
			} else {
				if (!oRecord.iStatus){
					iFound = iI;
					V.aAA[iI].iStatus = 2;
				}
			}
		}
		iI++;
	}
	if (iFound){
		V.aAA[iFound].iAjaxNo = iFound;
		A.ajaxA(V.aAA[iFound]);
	}
	//timed messaged
	if (V.sMessageTimed){
		V.oDiv.messagetimed.style.padding = "0 3em 0 3em";
		V.oDiv.messagetimed.innerHTML = V.sMessageTimed;
		V.sMessageTimed = "";
		window.setTimeout(function(){
			V.oDiv.messagetimed.style.padding = "0";
			V.oDiv.messagetimed.innerHTML = "";
		}, 1500);
	}
},



onBeforeUnload: function(oEvent){
	var aHash = window.location.href.split("#");
	if (aHash[1]){
		A.session("sURLHash", aHash[1]);
	}
	
},



onHashChange:function(oEvent){
	var iMenuNo = V.oGen.iMenuNo;
	var sURLHash = location.hash.slice(1);
	switch (sURLHash){
		case "home":
			iMenuNo = 0;
		break;
		
		case "register":
			iMenuNo = 2;
		break;
		
	}
	V.oGen.iMenuNo = iMenuNo;
	B.reset();
},



onKeyPress:function(oEvent){
	V.oGen.iLastKey = oEvent.keyCode || oEvent.which;
	V.oGen.divActive = document.activeElement;
	if ((V.oGen.sIDButtonDefault) && (V.oGen.iLastKey == 13)){
		var divButton = A.dg(V.oGen.sIDButtonDefault);
		divButton.click();
	}
},



onMouseMove:function(oEvent){
	V.oGen.oMouse.divParent = oEvent.target;
	V.oGen.oMouse.sClassName = oEvent.target.className;
	V.oGen.oMouse.sID = oEvent.target.id;
	V.oGen.oMouse.aPos = 
		[oEvent.screenX, oEvent.screenY, oEvent.offsetX, oEvent.offsetY];
	if (V.oGen.bUserBoxShow){
		if (V.oGen.oMouse.sClassName){
			if (V.oGen.oMouse.sClassName.indexOf("menuuser") == -1){
				if (B.oOb.iHome){
					V.aAA[B.oOb.iHome].userbox();
				}
			}
		}
	}
},



reset2:function(){
	var iI = 0;
	var oEle = 0;
	var oAll = V.oDB.oHouseText;
	var divParent = E.div(V.oDiv.body, "mainheaderfull");
	var divHeader = E.div(divParent, "mainheader");
	var divXmainlogo = E.div(divHeader, "mainlogo1");
	V.oDiv.logo = E.icon({divParent:divXmainlogo, sClassName:"logo1",
		bAction:1, sName:"logo", sText:oAll.sIconLogo});
	A.zindex(V.oDiv.logo);
	divParent = E.div(divHeader, "menutopbox1");
	
	divParent = E.div(divParent, "menutopbox1");
	for (iI = 0; iI < oAll.aMenusTop.length; iI++){
		var divMenu = E.menu({divParent:divParent, sClassName:"top", 
			iIndex:iI, aMenus:oAll.aMenusTop});
		if (iI == 1){
			divMenu.className = "menutop active";
		}
	}
	divParent = E.div(divHeader, "mainheadertabs1");
	E.div(divParent, "mainlefttabs1");
	divParent = E.div(divParent, "tabbuttonsbox");
	divParent = E.div(divParent, "tabbuttonsboxin");
	var iWidth = 0;
	for (iI = 0; iI < oAll.aTabsRegister.length; iI++){
		var sClassName = "regbutton";
		if (iI == oAll.aTabsRegister.length - 1){
			sClassName = "regbuttonlast";
		}
		var divButton = E.button({divParent:divParent, 
			sClassName:sClassName + " act", 
			sID:"regbutton_" + (iI + 1), sText:oAll.aTabsRegister[iI]});
		iWidth += (divButton.clientWidth + 45);
	}
	divParent.style.width = (iWidth) + "px";
	divParent = E.div(V.oDiv.body, "mainpage1");
	V.oDiv.mainpage = divParent;	
	E.div(divParent, "mainleft1");
	V.oDiv.work = E.div(divParent, "mainwork1");
	V.oDiv.footer = E.div(divParent, "mainworkfooter1");

	var divTabButtonBox = E.div(V.oDiv.footer, "mainheadertabs1");
	E.div(divTabButtonBox, "mainfootertabs1");
	var divTabButtonsBox = E.div(divTabButtonBox, "tabbuttonsbox");
	divParent = E.div(divTabButtonsBox, "tabbuttonsboxin");
	
	var aButtons = [V.oDB.oWords.sButtonCancel,V.oDB.oWords.sButtonNextStep];
	for (iI = 0; iI < aButtons.length; iI++){
		var sClassName = "footerbutton";
		var divButton = E.button({divParent:divParent, 
			sClassName:sClassName + " act", 
			sID:"footerbutton_" + (iI + 1), sText:aButtons[iI]});
		iWidth += (divButton.clientWidth + 45);
	}
	divParent.style.width = "100%";
	
	return [];
},



reset1:function(){
	var iI = 0;
	var oEle = 0;
	var oAll = V.oDB.oHouseText;
	var divParent = E.div(V.oDiv.body, "mainheaderfull");
	var divHeader = E.div(divParent, "mainheader");
	var divXmainlogo = E.div(divHeader, "mainlogo1");
	V.oDiv.logo = E.icon({divParent:divXmainlogo, sClassName:"logo1",
		bAction:1, sName:"logo", sText:oAll.sIconLogo});
	A.zindex(V.oDiv.logo);
	
	divParent = E.div(divHeader, "menutopbox1");

	divParent = E.div(divParent, "menuuserbox");
	E.div(divParent, "menuuserboxdropdown");
	var divChild = E.div(divParent, "menuusertext");
	divChild.textContent = oAll.oUserInfo.sCompany;
	divChild = E.div(divParent, "menuuserlogo");
	divChild = E.div(divChild, "img100", "", "img");
	divChild.src = oAll.oUserInfo.sLogo;
	E.div(divParent, "menuuserspacer");
	var divChild = E.div(divParent, "menuusertext");
	divChild.textContent = oAll.oUserInfo.sName;	
	E.div(divParent, "menuuserboxover act");

	divParent = E.div(divHeader, "mainheadertabs1");
	E.div(divParent, "mainlefttabs1");

	var divMenu = E.div(divHeader, "mainheadertabs2");
	divMenu = E.div(divMenu, "mainlefttabs2");
	var divMenu = E.div(divMenu, "menuleftbox");
	E.div(divMenu,"menuleftspacer");
	iI = 0;
	oAll.aMenusLeft.forEach(function(oRec){
		if ((!iI) || ((iI + 1) == oAll.aMenusLeft.length)){
			
		} else {
			divParent = E.div(divMenu, "menuleft act", "menuleft_" + iI);
			divChild = E.div(divParent, "menulefttext");
			divChild.textContent = oAll.aMenusLeft[iI];
		}
		iI++;
	});

	divParent = E.div(V.oDiv.body, "mainpage1");
	V.oDiv.mainpage = divParent;	
	E.div(divParent, "mainleft1");
	V.oDiv.work = E.div(divParent, "mainwork1");
	V.oDiv.footer = E.div(divParent, "mainworkfooter1");

	var divTabButtonBox = E.div(V.oDiv.footer, "mainheadertabs1");
	E.div(divTabButtonBox, "mainfootertabs1");
	var divTabButtonsBox = E.div(divTabButtonBox, "tabbuttonsbox");
	divParent = E.div(divTabButtonsBox, "tabbuttonsboxin");
	
	
	return [];
},



reset0:function(){
	var iI = 0;
	var oEle = {};
	var oAll = V.oDB.oHouseText;
	var divParent = E.div(V.oDiv.body, "mainheaderfull");
	divParent = E.div(divParent, "mainheader");
	divParent = E.div(divParent, "menutopbox1");
	for (iI = 0; iI < oAll.aMenusTop.length; iI++){
		E.menu({divParent:divParent, sClassName:"top", 
			iIndex:iI, aMenus:oAll.aMenusTop});
	}
	V.oDiv.mainpage = E.div(V.oDiv.body, "mainpage0");
	var divHouse = E.div(V.oDiv.mainpage, "house1");
	E.icon({divParent:divHouse, sText:oAll.sIconLogo, sClassName:"house1",
		sName:"logo"});
	var divCenter = E.div(divHouse, "house1center");
	var divBox = E.div(divCenter, "house1box");
	var divLine = E.div(divBox, "house1boxline");
	var divImg = E.div({divParent:divLine, sClassName:"img100", sType:"img"});
	divImg.src = "data/houseline.png"
	var divText = E.div(divBox, "house1boxtext");
	divText.innerHTML = oAll.sText1;
	var divLine = E.div(divBox, "house1boxline");
	var divImg = E.div({divParent:divLine, sClassName:"img100", sType:"img"});
	divImg.src = "data/houseline.png"	
	var divTable = E.div({divParent:divCenter, sType:"table",
		sClassName:"house1centericons"});
	var divTR = E.div({divParent:divTable, sType:"tr"});
	var iSize = oAll.aSocials.length;
	var iI = 0;
	for (iI = 0; iI < iSize; iI++){
		var divTD = E.div({divParent:divTR, sType:"td"}); 
		E.icon({divParent:divTD, sText:oAll.aSocials[iI].sIcon, 
			sClassName:"house1social", bAction:1, sName:oAll.aSocials[iI].sName});
	}

	divHouse = E.div(V.oDiv.mainpage, "house2");
	var divCenter = E.div(divHouse, "house2centerphones");
	var divPhones = E.div(divCenter, "house2phones");
	var divImg = E.div({divParent:divPhones, sClassName:"img100", sType:"img"});
	divImg.src = "data/housephones.png"		
		
	A.zindex(divTable)	
		
	divHouse = E.div(V.oDiv.mainpage, "house3");
	var divText = E.div(divHouse, "house3text1");
	divText.textContent = oAll.sText21;
	var divText = E.div(divHouse, "house3text2");
	divText.textContent = oAll.sText22;
	E.button({divParent:divHouse, sClassName:"whitebutton act", 
		sText:oAll.sButtonRegister});
	divHouse = E.div(V.oDiv.mainpage, "house4");
	var divCenter = E.div(divHouse, "house4center");
	for (iI = 0; iI < 3; iI++){
		var oRec = oAll.aIcons2[iI];
		var divBox = E.div(divHouse, "house4iconsbox")
		E.icon({divParent:divBox, sClassName:"house41", sText:oAll.sIconStep});
		var divCount = E.div(divBox, "house4count");
		divCount.textContent = (iI + 1);
		E.icon({divParent:divBox, sClassName:"house42", sText:oRec.sIcon});
		var divText = E.div(divBox, "house4text1");
		divText.textContent = oRec.sText1;
		var divText = E.div(divBox, "house4text2");
		divText.textContent = oRec.sText2;
	}
	divHouse = E.div(V.oDiv.mainpage, "house5");
	var divText = E.div(divHouse, "house3text1");
	divText.textContent = oAll.sText31;
	var divText = E.div(divHouse, "house3text2");
	divText.textContent = oAll.sText32;
	var divBox = E.div(divHouse, "house5center");
	E.image({divParent:divBox, sName:"android", 
		sClassName:"eleimage house5 android act", sSrc:"data/houseandroid.png"});
	E.image({divParent:divBox, sName:"apple", 
		sClassName:"eleimage house5 apple act", sSrc:"data/houseapple.png"});
	divHouse = E.div(V.oDiv.mainpage, "house6");
	var divCenter = E.div(divHouse, "house4center");
	for (iI = 0; iI < 3; iI++){
		var oRec = oAll.aIcons3[iI];
		var divBox = E.div(divHouse, "house4iconsbox")
		E.icon({divParent:divBox, sClassName:"house41", sText:oAll.sIconStep});
		var divCount = E.div(divBox, "house4count");
		divCount.textContent = (iI + 1);
		E.icon({divParent:divBox, sClassName:"house62", sText:oRec.sIcon});
		var divText = E.div(divBox, "house4text1");
		divText.textContent = oRec.sText1;
		var divText = E.div(divBox, "house4text2");
		divText.textContent = oRec.sText2;
	}
	V.oDiv.footer = E.div(V.oDiv.mainpage, "mainworkfooter1");
	var divDocs = E.div(V.oDiv.footer, "footerdocs");
	var aText = oAll.sDocs.split("|");
	E.text({divParent:divDocs, sText:aText[0]});
	E.span({divParent:divDocs, sClassName:"footerdocslinks act", sName:"terms",
		sText:aText[1]});
	E.text({divParent:divDocs, sText:aText[2]});
	E.span({divParent:divDocs, sClassName:"footerdocslinks act", sName:"privacy",
		sText:aText[3]});
//	V.oDiv.bottom = E.div(V.oDiv.mainpage, "bottom");
	return [];
},



reset:function(){
	var iI = 0;
	var aTabs = [];
	V.oDiv.body.innerHTML = "";
	V.oDiv.cloak = E.div(V.oDiv.body, "maincloak");
	V.oDiv.dummy = E.div(V.oDiv.cloak, "maindummy");
	switch (V.oGen.iMenuNo){
		case 0:
			aTabs = B.reset0();
		break;
		
		case 1:
			aTabs = B.reset1();
		break;
	
		case 2:
			aTabs = B.reset2();
		break;
		
	}
	B.actions();
	B.resizeA();
},



resizeA:function(){
	if (V.oGen.bResizing){
		return;
	}
	window.setTimeout(function(){
		B.resize();
	}, 50);
	V.oGen.bResizing = 1;
},



resize:function(){
	var iI = 0;
	var iW = window.innerWidth - V.oGen.iWidthScrollbar;
	var bGrowing = 1;
	if (iW < V.oGen.iWidthWindow){
		bGrowing = 0;
	}
	V.oGen.iWidthWindow = iW;
	var iMode = 0;
	if (bGrowing){
		iI = 0;
		V.oGen.aDisplayScreenSizes.forEach(function(oRec){
			if ((iW + 30) >= oRec.iWidthMain){
				iMode = iI;
			}
			iI++;
		});
	} else {
		iI = 0
		V.oGen.aDisplayScreenSizes.forEach(function(oRec){
			if ((iW - 30) >= oRec.iWidthMain){
				iMode = iI;
			}
			iI++;
		});
	}
	if (V.oDiv.work){
		V.oDiv.work.innerHTML +=
			("iMode = " + iMode+ ",pg=" + V.oGen.iMenuNo+
			",iw="+iW+",gr="+bGrowing+", ")		
	}

	var oSize = V.oGen.aDisplayScreenSizes[iMode];
	V.oDiv.body.style.fontSize = oSize.iFontSize + "px";
	V.oGen.iDisplayScreenSize = iMode;
	switch (V.oGen.iMenuNo){
		case 1:
			oSize.iWidthMainNow = (iW);
			V.oDiv.mainpage.style.width = (oSize.iWidthMainNow) + "px";
			oSize.iWidthWork = (oSize.iWidthMainNow - oSize.iWidthLeft);
			if (V.oDiv.work){
				V.oDiv.work.style.width = (oSize.iWidthWork) + "px";
				V.oDiv.mainpage.style.height = (V.oDiv.work.clientHeight) + "px";
			}
			A.gcn("mainheaderfull").style.height = oSize.iHeightHeader + "px";
			A.gcn("mainheadertabs1").style.height = oSize.iHeightTabs + "px";
/*			var aBoxes = A.gcn("mainheadertabs1", 1);
			aBoxes.forEach(function(divBox){
				divBox.style.width = (oSize.iWidthWork - 25) + "px";
			});
	*/		var aBoxes = A.gcn("mainlefttabs1", 1);
			aBoxes.forEach(function(divBox){
				divBox.style.width = oSize.iWidthLeft + "px";
			});
			var aBoxes = A.gcn("tabbuttonsbox", 1);
			aBoxes.forEach(function(divBox){
				divBox.style.width = (oSize.iWidthWork - 20) + "px";
			});
			var aBoxes = A.gcn("gridbox", 1);
			aBoxes.forEach(function(divBox){
				divBox.style.width = (oSize.iWidthWork - 20) + "px";
			});
			A.gcn("menutopbox1").style.height = oSize.iHeightHeader + "px";
			A.gcn("mainleft1").style.width = oSize.iWidthLeft + "px";
//			A.gcn("mainlefttabs1").style.width = oSize.iWidthLeft + "px";
			A.gcn("mainlefttabs2").style.width = oSize.iWidthLeft + "px";
			A.gcn("mainfootertabs1").style.width = oSize.iWidthLeft + "px";
			A.gcn("gridcolumnsbox").style.width = (oSize.iWidthWork - 20) + "px";
			A.gcn("mainlogo1").style.width = oSize.iWidthLogo + "px";
			var iFontSize = ((oSize.iWidthLogo / 22) - iMode / 5);
			if (iFontSize > 7){
				iFontSize = 7;
			}
			if (V.oDiv.logo){
				V.oDiv.logo.style.fontSize = iFontSize + "em";
			}
		break;

		case 2:
			oSize.iWidthMainNow = (iW);
			V.oDiv.mainpage.style.width = (oSize.iWidthMainNow) + "px";
			oSize.iWidthWork = (oSize.iWidthMainNow - oSize.iWidthLeft);
			if (V.oDiv.work){
				V.oDiv.work.style.width = (oSize.iWidthWork) + "px";
				V.oDiv.mainpage.style.height = (V.oDiv.work.clientHeight) + "px";
			}
			A.gcn("mainheaderfull").style.height = oSize.iHeightHeader + "px";
			A.gcn("mainheadertabs1").style.height = oSize.iHeightTabs + "px";
			A.gcn("menutopbox1").style.height = oSize.iHeightHeader + "px";
			A.gcn("mainleft1").style.width = oSize.iWidthLeft + "px";
			A.gcn("mainlefttabs1").style.width = oSize.iWidthLeft + "px";
			A.gcn("mainfootertabs1").style.width = oSize.iWidthLeft + "px";
			var aBoxes = A.gcn("tabbuttonsbox", 1);
			aBoxes.forEach(function(divBox){
				divBox.style.width = (oSize.iWidthWork - 25) + "px";
			});
			A.gcn("mainlogo1").style.width = oSize.iWidthLogo + "px";
			A.gcn("mainlogo1").style.height = oSize.iHeightLogo + "px";
			if (V.oDiv.logo){
				if (oSize.iHeightLogo){
					V.oDiv.logo.parentNode.style.height = (oSize.iHeightLogo) + "px";
				} else {
					V.oDiv.logo.style.height = (oSize.iHeightHeader) + "px";
				}
				A.zindex(V.oDiv.logo);
			}
		break;
		
		case 0:
			oSize.iWidthWork = oSize.iWidthMain - oSize.iWidthLeft;
			V.oDiv.mainpage.style.width = oSize.iWidthMain + "px";
			V.oDiv.footer.style.height = oSize.iHeightFooter + "px";
			A.gcn("mainheaderfull").style.height = oSize.iHeightHeader + "px";
			var divXheader = A.gcn("mainheader");
			divXheader.style.height = 
				(oSize.iHeightHeader + oSize.iHeightTabs) + "px";
			A.gcn("menutopbox1").style.height = oSize.iHeightHeader + "px";
			V.oDiv.mainpage.style.minHeight = oSize.iHeightMainMin + "px";
		break;
		
	}
	
	if (V.oDiv.work){
		V.oDiv.work.innerHTML += iMode + ", " + oSize.iWidthMain+","+iW+"<br>"; 
	}
	
	V.oGen.bResizing = 0;	
},



scrolltop:function(){
	var divDoc = document.documentElement;
	var iTop = (window.pageYOffset || 
		divDoc.scrollTop)  - (divDoc.clientTop || 0);	
	return iTop;
},



};



// ELEMENTS CLASS ==============================================================
var E = {
	sName:"ELES",

action:function(sAction){
	var divParent = 0;
	var divX = 0;
	var aAction = sAction.split("_");
	switch (aAction[0]){
		case "buttondisable":
			var divButton = A.dg(V.oGen.sIDButtonDisabled);
			divButton.parentNode.childNodes[1].style.display = "block";
		break;
		
		case "buttonenable":
			var divButton = A.dg(V.oGen.sIDButtonDisabled);
			divButton.parentNode.childNodes[1].style.display = "none";
		break;

		case "popupclose":
			divX = A.gcn("elepopupover");
			if ((divX) && (divX.className != "maindummy")){
				divParent =	divX.parentNode;
				divParent.removeChild(divX)
			} else {
				V.oDiv.cloak.innerHTML = "";
				V.oDiv.cloak.style.display = "none";
				V.oDiv.popup = 0;
				window.scrollTo(0, V.oGen.iScrollTop);
			}
		break;
		
		case "popupclosex":
			V.oDiv.cloak.innerHTML = "";
			V.oDiv.cloak.style.display = "none";
			V.oDiv.popup = 0;
			window.scrollTo(0, V.oGen.iScrollTop);
		break;
		
		case "popupclosebutton":
			V.oDiv.cloak.innerHTML = "";
			V.oDiv.cloak.style.display = "none";
			V.oDiv.popup = 0;
			window.scrollTo(0, V.oGen.iScrollTop);
		break;
		
		case "tabbutton":
			var aDivWorkTab = A.gcn("tabwork", 1);
			aDivWorkTab.forEach(function(oRec){
				var aID = oRec.id.split("_");
				if (aID[1] == aAction[1]){
					oRec.style.display = "block";
				} else {
					oRec.style.display = "none";
				}
			});
		break;
	}
},



address:function(oEle){
	var sText = oEle.sText;
	if (!sText){
		sText = "";
	}
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox " + sClassName);
	var divX = E.div(divBox, "eleinputinput", oEle.sID, "input");
	return divX;
},



button:function(oEle){
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var sID = oEle.sID
	if (!sID){
		sID = ""; 
	}
	var divParent = E.div(oEle.divParent, "elebuttonbox");
	var divX = E.div(divParent, "elebutton " + sClassName, sID);
	divX.innerHTML = oEle.sText;
	sClassName = sClassName.replace(/ act/g, "");
	if (oEle.bCanDisable){
		var divOver = E.div(divParent, "elebutton disabled " + sClassName);
	}
	if (oEle.bDefault){
		V.oGen.sIDButtonDefault = oEle.sID;
	}
	divX.type = "button";
	return divX;
},



div:function(oEle, sClassName, sID, sType){
	if (sClassName){
		if (!sID){
			sID = "";
		}
		if (!sType){
			sType = "";
		}
		oEle = {divParent:oEle, sClassName:sClassName, sID:sID, sType:sType};
	}	
	var sNewType = "div";
	if (oEle.sType){
		sNewType = oEle.sType;
	}
	var divAType = document.createElement(sNewType);
	if (oEle.sClassName){
		divAType.className = oEle.sClassName;
	}
	if (oEle.sID){
		divAType.setAttribute("id", oEle.sID);
	}
	if (oEle.divParent){
		oEle.divParent.appendChild(divAType);
	}
	return divAType;
},



email:function(oEle){
	var divX = E.input(oEle);
	divX.type = "email";
	return divX;
},



error:function(oEle){
	var sError = "";
	var iI = 0;
	if (oEle.aInput){
		oEle.aInput.forEach(function(oRec){
			sError += V.oDB.oError.oInput[oRec] + "<br>";
			iI++;
		});
	}
	if (iI){
		oEle.sMessage = sError;
		E.popup(oEle);
	}
},



form:function(oForm){
	var iI = 0;
	if (!oForm.iIndex){
		oForm.iIndex = "";
	}
	var aForm = V.oDB.oForm[oForm.sFormName + oForm.iIndex];
	if (!aForm){
		return;
	}
	var iObjNo = 0;
	if (!oForm.bNooObject){
		iObjNo = A.objectnew();
		V.aAA[iObjNo] = new HO_FORM();
		V.aAA[iObjNo].oForm = oForm;
	}
	aForm.forEach(function(oRec){
		oRec.divParent = oForm.divParent;
		var sIDPost = "";
		if (oForm.sIDPost){
			sIDPost = "-" + oForm.sIDPost;
		}
		if (!oRec.iIndex){
			oRec.iIndex = 0;
		}
		oRec.sID = oForm.sIDPre + "-" + iObjNo + "_" + 
			oForm.iIndex+ "_"+ iI + sIDPost;
		oRec.that = oForm.that;
		var divX = E[(oRec.sType)](oRec);
		iI++;
	});
	B.actions();
	return iObjNo;
},



grid:function(oEle){
	console.log(oEle)
	var divHeader = A.gcn("mainheader");
	divHeader = E.div(divHeader, "mainheadertabs1");
	E.div(divHeader, "mainlefttabs1");
	divHeader = E.div(divHeader, "gridcolumnsbox");
	var divBox = E.div(oEle.divParent, "gridbox");
	var iObjNo = A.objectnew();
	V.aAA[iObjNo] = new HO_GRID();
	V.aAA[iObjNo].iObjNo = iObjNo;
	V.aAA[iObjNo].oDiv.header = divHeader;
	V.aAA[iObjNo].oDiv.box = divBox;
	V.aAA[iObjNo].oEle = oEle;
	V.aAA[iObjNo].init();		
},



href:function(oEle){
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "elehref " + sClassName, oEle.sID);
	divX.textContent = oEle.sText;
	divX.type = "href";
	if (oEle.sName){
		divX.name = oEle.sName;
	}
	return divX;
},



icon:function(oEle){
	var sID = "";
	if (oEle.sID){
		sID = oEle.sID;
	}
	var divX = E.div(oEle.divParent, "eleicon " + oEle.sClassName);
	divX.innerHTML = oEle.sText;
	var sClassName = "eleiconover";
	if (oEle.bAction){
		sClassName += " act";
	}
	divX = E.div(divX, sClassName, sID);
	if (oEle.sName){
		divX.name = oEle.sName;
	} else {
		divX.name = "";
	}
	divX.type = "icon";
	return divX;
},



imagebox:function(oEle){
	var sText = oEle.sText;
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleimagebox " + sClassName);
	if (!oEle.aSize){
		oEle.aSize = [300, 300];
	}
	divBox.style.width = oEle.aSize[0] + "px";
	divBox.style.height = oEle.aSize[1] + "px";
	return divX;
},



image:function(oEle){
	var divX = E.div(oEle.divParent, oEle.sClassName);
	var divImg = E.div({divParent:divX, sClassName:"img100", sType:"img"});
	divImg.src = oEle.sSrc;
	divX.type = "image";
	if (oEle.sName){
		divX.name = oEle.sName;
	} else {
		divX.name = "";
	}
},



inputarea:function(oEle){
	var sText = oEle.sText;
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox " + sClassName);
	var divX = E.div(divBox, "eleinputinput", oEle.sID, "input");
	return divX;
},



input:function(oEle){
	var sText = oEle.sText;
	if (!sText){
		sText = "";
	}
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox " + sClassName);
	if (oEle.bBoxOnly){
		divBox.id = oEle.sID;
		return divBox;
	} else { 
		var divX = E.div(divBox, "eleinputinput", oEle.sID, "input");
	}
	return divX;
},



line:function(oEle){
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleline" + sClassName, oEle.sID);
	return divX;
},



memo:function(oEle){
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "elememo " + sClassName, oEle.sID);
	divX.textContent = oEle.sText;
	return divX;
},



menu:function(oEle){
	var sClassName = "menu" + oEle.sClassName;
	var divParent = E.div(oEle.divParent, sClassName + " act");
	var divX = E.div(divParent, sClassName + "text");
	divX.textContent = oEle.aMenus[oEle.iIndex];
	if (oEle.iIndex < oEle.aMenus.length - 1){
		E.div(oEle.divParent, sClassName +"spacer");
	}
	divParent.type = "menu";
	return divParent;
},



page:function(oEle){
	if (!(V.oDiv.work)){
		return;
	}
	var divX = E.div(V.oDiv.work, "tabwork", "tabwork-" + oEle.iIndex);
	var iObjNo = E.form({sFormName:oEle.sFormName, iIndex:oEle.iIndex, 
		divParent:divX, sIDPre:"reg", sIDPost:"", that:oEle.that});
	return iObjNo;
},



		
password:function(oEle){
	var divX = E.input(oEle);
	divX.type = "password";
	return divX;
},



popup:function(oEle){
	var iI = 0;
	var divTitle = 0;
	var divChild;
	var iReturn = 0;
	var iObjNo = 0;
	var divWork = 0;
	var divParent = 0;
	var divOver = 0;
	var divTitle = 0;
	if (!V.oDiv.popup){
		V.oGen.iScrollTop = B.scrolltop();
		V.oDiv.cloak.style.display = "block";
		V.oDiv.cloak.style.height = B.height() + "px";
		A.zindex(V.oDiv.cloak);
		var divParent = E.div(V.oDiv.cloak, "maincloaktop");
		V.oDiv.cloak.style.display = "block";
		divParent = E.div(divParent, "elepopup");
		var iTop = 0;
		if (V.oGen.iDisplayScreenSize > 2){
			iTop = 70;
		}
		divParent.style.marginTop = (V.oGen.iScrollTop + iTop) + "px";
		V.oDiv.popup = divParent;
		divTitle = E.div(divParent, "elepopuptitlebar");
		divChild = E.div(divTitle, "elepopuptitlebartext");
		divChild.textContent = oEle.sTitle;
		E.icon({divParent:divTitle, sText:"&#xE06E", 
			sClassName:"popupclose", sName:"popupclosex", bAction:1});	
		var divWork = E.div(divParent,"elepopupwork");
		if (oEle.sMessage){
			divWork.innerHTML = oEle.sMessage;
		}
		iReturn = divParent;
		iObjNo = 0;
	} else {
		//over
		divTitle = V.oDiv.popup.childNodes[0].childNodes[0];
		divTitle.textContent = oEle.sTitle;
		divWork = V.oDiv.popup.childNodes[1];
		divParent = divWork.parentNode;
		divOver = E.div(divWork, "elepopupover");
		divOver.style.width = (divWork.clientWidth - 30) + "px";
		if (oEle.sMessage){
			divOver.innerHTML = oEle.sMessage;
		}
		divParent = divOver;
		divWork = divOver;
	}
	if (oEle.sFormName){
		iObjNo = E.form({divParent:divWork, that:oEle.that,
			sFormName:oEle.sFormName, sIDPre:oEle.sIDPre});
		iReturn = iObjNo;
	} else {
		var divButtons = E.div(divParent,"elepopupbuttons");
		if (!oEle.aButtons){
			E.button({divParent:divButtons, sText:"OK",
				sName:"popupok", sClassName:"centered act", sID:"okpopup"});
			B.actions();
		}
	}
	return iReturn;
},



select:function(oEle){
	var sText = oEle.sText;
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox " + sClassName);
	var divX = E.div(divBox, "eleinputselect", oEle.sID, "select");
	if (oEle.aValues){
		A.value(oEle.sID, oEle.aValues);
	}
	if (oEle.sEnum){
		A.value(oEle.sID, V.oDB.oEnum[oEle.sEnum]);
	}
	return divX;
},



span:function(oEle){
	var sID = "";
	if (oEle.sID){
		sID = oEle.sID;
	}
	var divX = E.div(oEle.divParent, oEle.sClassName, sID, "span");
	divX.innerHTML = oEle.sText;
	divX.type = "span";
	if (oEle.sName){
		divX.name = oEle.sName;
	}
	return divX;
},



telephone:function(oEle){
	var sText = oEle.sText;
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox " + sClassName);
	var divX = E.div(divBox, "eleinputinput", oEle.sID, "input");
	divX.type = "telephone";
	return divX;
},



text:function(oEle){
	var divX = E.div({divParent:oEle.divParent, sType:"span"});
	divX.innerHTML = oEle.sText;
	return oEle.divParent;
},



upload:function(oEle){
	var sText = oEle.sText;
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	var divBox = E.div(divX, "eleinputbox upload");
	E.div(divBox, "eleuploadfilename");
	var divBrowse = E.div(divBox, "eleuploadbrowse");
	divBrowse.textContent = "BROWSE";
	var divStart = E.div(divBox, "eleuploadstart"); 
	divStart.textContent = "UPLOAD";
	var divProgress = E.div(divBox, "eleuploadprogress", 
		oEle.sID + "_progress", "progress");
	
	return divX;
},



		
weekhours:function(oEle){
	var iObjNo = A.objectnew();
	V.aAA[iObjNo] = new HO_WEEKHOURS();
	V.aAA[iObjNo].iObjNo = iObjNo;
	V.aAA[iObjNo].oEle = oEle;
	V.aAA[iObjNo].init();

	/*
	var iI = 0;
	var divBox = 0;
	var divX = 0;
	var sText = oEle.sText;
	if (oEle.bRequired){
		sText += " *";
	}
	var sClassName = oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var aDaysLong = V.oDB.oWords.oDate.sDaysLong.split("|");
	var aOptions = [];
	aDaysLong.forEach(function(oRec){
		aOptions.push({iID:(iI + 1), sValue:aDaysLong[iI]});
		iI++;
	});
	divX = E.div(oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	divBox = E.div(divX, "eleinputbox weekhours" + sClassName, oEle.sID);
	var divTable = E.div(divBox,"eletable", "", "table");
	var divTR = E.div(divTable, "eletablerow", "", "tr");
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divTD.textContent = "Days From ";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divX = E.div(divTD, "eleselectweekhours", oEle.sID + "_dayfrom", "select");
	A.value(oEle.sID + "_dayfrom", aOptions);
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divTD.textContent = " To ";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divX = E.div(divTD, "eleselectweekhours", oEle.sID + "_dayto", "select");
	A.value(oEle.sID + "_dayto", aOptions);
	var divTR = E.div(divTable, "eletablerow", "", "tr");
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divTD.textContent = "Time From ";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divX = E.div(divTD, "eletime", oEle.sID + "_timefrom", "input");
	divX.type = "time";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divTD.textContent = " To ";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divX = E.div(divTD, "eletime", oEle.sID + "_timefrom", "input");
	divX.type = "time";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	E.icon({divParent:divTD, sText:"&#xE06B;", sClassName:"eleweekhoursicon"})
	var divTD = E.div(divTR, "eletablecol", "", "td");
	E.icon({divParent:divTD, sText:"&#xE06C;", sClassName:"eleweekhoursicon"});
	*/
	
},



};



//== CREATEPOST CLASS ===============================================================
function HO_CREATEPOST(){
	this.sName = "CREATEPOST";
	this.iObjNo = 0;
	
	if (typeof HO_CREATEPOST.bInited == "undefined"){

HO_CREATEPOST.prototype.init = function(){
	console.log("createpost")
};



		HO_CREATEPOST.bInited = 1;
	}
};




//== FORM CLASS ===============================================================
function HO_FORM(){
	this.sName = "FORM",
	this.oForm = {};
	
	if (typeof HO_FORM.bInited == "undefined"){

HO_FORM.prototype.action = function(sElement){
};



HO_FORM.prototype.values = function(oParams){
	var aValues = [];
	var oValue = {};
	var aInclude = "email,password".split(",")
	var aForm = V.oDB.oForm[this.oForm.sFormName + this.oForm.iIndex];
	if (!aForm){
		return;
	}
	aForm.forEach(function(oRec){
		var sType = oRec.sType;
		if (aInclude.indexOf(oRec.sType) != -1){
			oValue = A.value(oRec.sID);
			aValues.push(oValue);
		}
	});
	return aValues;
};



		HO_FORM.bInited = 1;
	}
}



//== WEEKHOURS CLASS ===========================================================
function HO_WEEKHOURS(){
	this.sName = "WEEKHOURS";
	this.iObjNo = 0;
	this.oEle = {};
	
	if (typeof HO_WEEKHOURS.bInited == "undefined"){

HO_WEEKHOURS.prototype.action = function(sAction){
};



HO_WEEKHOURS.prototype.draw = function(divBox){
	var iI = 0;
	var divX = 0;
	var aDaysLong = V.oDB.oWords.oDate.sDaysLong.split("|");
	var aOptions = [];
	aDaysLong.forEach(function(oRec){
		aOptions.push({iID:(iI + 1), sValue:aDaysLong[iI]});
		iI++;
	});
	var that = this;
	iI = 0;
	this.oEle.that.aWeekHours.forEach(function(iObjNo){
		var iObjNo = that.oEle.that.aWeekHours[iI];
		var sIDPre = that.oEle.sID + "_" + iObjNo + "-";
		var divTable = E.div(divBox,"eletable", "", "table");
		var divTR = E.div(divTable, "eletablerow", "", "tr");
		var divTD = E.div(divTR, "eletablecol", "", "td");
		divTD.textContent = "Days From ";
		var divTD = E.div(divTR, "eletablecol", "", "td");
		divX = E.div(divTD, "eleselectweekhours", 
			sIDPre + "dayfrom", "select");
		A.value(sIDPre + "dayfrom", aOptions);
		var divTD = E.div(divTR, "eletablecol", "", "td");
		divTD.textContent = " To ";
		var divTD = E.div(divTR, "eletablecol", "", "td");
		divX = E.div(divTD, "eleselectweekhours", 
			sIDPre + "dayto", "select");
		A.value(sIDPre + "dayto", aOptions);
		var divTR = E.div(divTable, "eletablerow", "", "tr");
		var divTD = E.div(divTR, "eletablecol", "", "td");
		divTD.textContent = "Time From ";
		var divTD = E.div(divTR, "eletablecol", "", "td");
		divX = E.div(divTD, "eletime", 
			sIDPre + "timefrom", "input");
		divX.type = "time";
		var divTD = E.div(divTR, "eletablecol", "", "td");
		divTD.textContent = " To ";
		var divTD = E.div(divTR, "eletablecol", "", "td");
		divX = E.div(divTD, "eletime", 
				sIDPre + "timeto", "input");
		divX.type = "time";
		var divTD = E.div(divTR, "eletablecol", "", "td");
		E.icon({divParent:divTD, sText:"&#xE06B;", 
			sClassName:"eleweekhoursicon", sID:sIDPre + "plus"});
		var divTD = E.div(divTR, "eletablecol", "", "td");
		E.icon({divParent:divTD, sText:"&#xE06C;", 
			sClassName:"eleweekhoursicon", sID:sIDPre + "minus"});
		iI++;
	});
};



HO_WEEKHOURS.prototype.init = function(){
	this.oEle.that.aWeekHours.push(this.iObjNo);
	this.oEle.bBoxOnly = 1;
	this.oEle.sClassName = "weekhours";
	var divBox = E.input(this.oEle);
	divBox.type = "weekhours";
	this.draw(divBox);
	/*
	var iI = 0;
	var divBox = 0;
	var divX = 0;
	var sText = this.oEle.sText;
	
	if (this.oEle.bRequired){
		sText += " *";
	}
	var sClassName = this.oEle.sClassName;
	if (!sClassName){
		sClassName = "";
	}
	var aDaysLong = V.oDB.oWords.oDate.sDaysLong.split("|");
	var aOptions = [];
	aDaysLong.forEach(function(oRec){
		aOptions.push({iID:(iI + 1), sValue:aDaysLong[iI]});
		iI++;
	});
	divX = E.div(this.oEle.divParent, "eleinput");
	divX.innerHTML = sText;
	divBox = E.div(divX, "eleinputbox weekhours" + sClassName, this.oEle.sID);
	var divTable = E.div(divBox,"eletable", "", "table");
	var divTR = E.div(divTable, "eletablerow", "", "tr");
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divTD.textContent = "Days From ";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divX = E.div(divTD, "eleselectweekhours", this.oEle.sID + "_dayfrom", "select");
	A.value(this.oEle.sID + "_dayfrom", aOptions);
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divTD.textContent = " To ";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divX = E.div(divTD, "eleselectweekhours", this.oEle.sID + "_dayto", "select");
	A.value(this.oEle.sID + "_dayto", aOptions);
	var divTR = E.div(divTable, "eletablerow", "", "tr");
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divTD.textContent = "Time From ";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divX = E.div(divTD, "eletime", this.oEle.sID + "_timefrom", "input");
	divX.type = "time";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divTD.textContent = " To ";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	divX = E.div(divTD, "eletime", this.oEle.sID + "_timefrom", "input");
	divX.type = "time";
	var divTD = E.div(divTR, "eletablecol", "", "td");
	E.icon({divParent:divTD, sText:"&#xE06B;", sClassName:"eleweekhoursicon"})
	var divTD = E.div(divTR, "eletablecol", "", "td");
	E.icon({divParent:divTD, sText:"&#xE06C;", sClassName:"eleweekhoursicon"})
	*/
};



		HO_WEEKHOURS.bInited = 1;
	}
}



//== LOGIN CLASS ===============================================================
function HO_LOGIN(){
	this.sName = "LOGIN";
	this.sEmailAddress = "";
	this.iForm = 0;
	this.iObjNo = 0;
	
	if (typeof HO_LOGIN.bInited == "undefined"){

HO_LOGIN.prototype.action = function(sAction){
	var oForm = {};
	var aAction = sAction.split("-");
	this[aAction[1]]();
};



HO_LOGIN.prototype.goA = function(oData){
	console.log(oData);
	E.action("buttonenable");
	if (oData.aErrors.length){
		alert(1)
	} else {
		//success
		V.oGen.iMenuNo = 1;
		A.session("iUser", 1);
		E.action("popupclosex");
		B.init();
	}
};



HO_LOGIN.prototype.go = function(){
	var aErrors = [];
	oForm = V.aAA[this.iForm];
	var aValues = oForm.values();
	/*
	if (!aValues[0].sValue){
		aErrors.push("empty");
	}
	if (!A.valid(aValues[0])){
		aErrors.push(aValues[0].sType);
	}
	if (!A.valid(aValues[1])){
		aErrors.push(aValues[1].sType);
	}
	*/
	if (aErrors.length > 0){
		E.error({sTitle:"Login", aInput:aErrors});
	} else {
		//log in
		V.oGen.sIDButtonDisabled = "login-" + this.iForm + "__2";
		E.action("buttondisable");
		A.ajax("login", aValues, this.goA);
	}
};



HO_LOGIN.prototype.init = function(){
	this.iForm = E.popup({sTitle:V.oDB.oWords.sLoginTitlebar, 
		that:this, sIDPre:"login", sFormName:"aLogin"});
	var sID = "login-" + this.iForm + "__0"; 
	A.dg(sID).focus();
};



HO_LOGIN.prototype.passwordforgot = function(){
	var sID = "login-" + this.iForm + "__0"; 
	var sEmail = A.value(sID);
	E.popup({sTitle:"Forgot Password", sFormName:"aLoginForgotPassword"});
};


HO_LOGIN.prototype.registernow = function(){
	V.oGen.iMenuNo = 2;
	E.action("popupclose");
	V.aAA[this.iObjNo] = 0;
	B.reset();
};



		HO_LOGIN.bInited = 1;
	}
};




//== REGISTER CLASS ============================================================
function HO_REGISTER(){
	this.sName = "REGISTER",
	this.iObjNo = 0;
	this.iStepNow = 1;
	this.iStepsMax = 5;
	this.aForms = [];
	this.aWeekHours = [];
	this.aAddresses = [];
	
	if (typeof HO_REGISTER.bInited == "undefined"){

HO_REGISTER.prototype.action = function(sAction){
};



HO_REGISTER.prototype.footer = function(sAction){
	var aAction =sAction.split("-");
	switch (aAction[1]){
		case "nextstep":
			this.iStepNow++;
			if (this.iStepNow > this.iStepsMax){
				this.iStepNow = this.iStepsMax;
				var divX = A.dg(aAction[2]);
				divX.childNodes[0].textContent = "SAVE & CREATE";
			}
			B.action("elebutton.regbutton-x-regbutton_" + this.iStepNow);
		break;
		case "cancel":
			
		break;
	}
	console.log(sAction)
	
};



HO_REGISTER.prototype.init = function(){
	A.history("register");
	var iI = 0;
	V.oGen.iMenuNo = 2;
	B.reset();
	var aTabs = V.oDB.oHouseText.aTabsRegister;
	this.iStepsMax = aTabs.length;
	for (iI = 1; iI <= this.iStepsMax; iI++){
		E.page({iIndex:iI, sFormName:"aReg", that:this});
		var divButton = A.dg("regbutton_" + iI);
		E.icon({divParent:divButton, sClassName:"regbutton", sText:"&#xE00B;"});
		E.span({divParent:divButton, sClassName:"regbuttonnum", sText:iI});
	}
	B.action("elebutton.regbutton-userlogin-regbutton_1");
};



HO_REGISTER.prototype.step = function(aAction){
	var aDivWorkTab = A.gcn("tabwork", 1);
	aDivWorkTab.forEach(function(oRec){
		var sClassName = "regbutton";
		var aID = oRec.id.split("-");
		var divButton = A.dg("regbutton_" + aID[1]);
		var divIcon = divButton.childNodes[1];
		if (parseInt(aID[1]) == 5){
			sClassName = "regbuttonlast";
		}
		if (aID[1] == aAction[1]){
			oRec.style.display = "block";
			divButton.className = "elebutton " + sClassName + " active";
			divIcon.className = "eleicon " + sClassName + " active";
		} else {
			oRec.style.display = "none";
			divButton.className = "elebutton " + sClassName;
			divIcon.className = "eleicon " + sClassName;
		}
	});
};



		HO_REGISTER.bInited = 1;
	}
};




//== HOME CLASS ===============================================================
function HO_HOME(){
	this.sName = "HOME";
	this.iObjNo = 0;
	
	if (typeof HO_HOME.bInited == "undefined"){

HO_HOME.prototype.changepassword = function(){
	this.userbox();
};



HO_HOME.prototype.logout = function(){
	A.session("iUser", 0);
	V.oGen.iMenuNo = 0;
	V.aAA[B.oOb.iHome] = 0;
	B.oOb.iHome = 0;
	B.reset();
};



HO_HOME.prototype.profile = function(){
	this.userbox();
	B.action("menuleft--_4")
};



HO_HOME.prototype.userbox = function(){
	var divBox = A.gcn("menuuserboxdropdown");
	V.oGen.bUserBoxShow = 0;
	if (!divBox){
		return;
	}
	if (divBox.style.display == "block"){
		divBox.style.display = "none";
	} else {
		V.oGen.bUserBoxShow = 1;
		divBox.style.display = "block";
		A.zindex(divBox);
		divBox.innerHTML = "";
		if (V.oGen.iDisplayScreenSize < 2){
			E.form({divParent:divBox, sFormName:"aUserBoxSmall", bNooObject:1});
		} else {
			E.form({divParent:divBox, sFormName:"aUserBox", bNooObject:1});
		}
	}
};



		HO_HOME.bInited = 1;
	}
};



//== MYPOSTS CLASS ===============================================================
function HO_MYPOSTS(){
	this.sName = "MYPOSTS";
	this.iObjNo = 0;
	
	if (typeof HO_MYPOSTS.bInited == "undefined"){

HO_MYPOSTS.prototype.init = function(){
	var oAll = V.oDB.oHouseText;
	var divParent = A.gcn("mainheadertabs1");
//	E.div(divParent, "mainlefttabs1");
	divParent = E.div(divParent, "tabbuttonsbox");
	divParent = E.div(divParent, "tabbuttonsboxin");
	var iWidth = 0;
	for (iI = 0; iI < oAll.aTabsMyPosts.length; iI++){
		var sClassName = "tabbutton";
		var divButton = E.button({divParent:divParent, 
			sClassName:sClassName + " act", 
			sID:"tabbutton_" + (iI + 1), sText:oAll.aTabsMyPosts[iI]});
		iWidth += (divButton.clientWidth + 45);
	}
	divParent.style.width = (iWidth) + "px";
	
	
	
	var iI = 0;
	var aTabs = V.oDB.oHouseText.aTabsMyPosts;
	this.iStepsMax = aTabs.length;
	for (iI = 1; iI <= this.iStepsMax; iI++){
		E.page({iIndex:iI, sFormName:"aMyPosts", that:this});
	}
	B.action("elebutton.tabbutton-active-tabbutton_1");
/*	
	var divParent = A.dg("")
	E.grid({divParent:divParent})
	*/
	B.resizeA();
	B.actions();
	
};



HO_MYPOSTS.prototype.step = function(aAction){
	var aDivWorkTab = A.gcn("tabwork", 1);
	aDivWorkTab.forEach(function(oRec){
		var sClassName = "tabbutton";
		var aID = oRec.id.split("-");
		var divButton = A.dg("tabbutton_" + aID[1]);
		if (aID[1] == aAction[1]){
			oRec.style.display = "block";
			divButton.className = "elebutton " + sClassName + " active";
		} else {
			oRec.style.display = "none";
			divButton.className = "elebutton " + sClassName;
		}
	});
};



		HO_MYPOSTS.bInited = 1;
	}
};




//== PROFILE CLASS ===============================================================
function HO_PROFILE(){
	this.sName = "PROFILE";
	this.iObjNo = 0;
	
	if (typeof HO_PROFILE.bInited == "undefined"){

HO_PROFILE.prototype.init = function(){
	console.log("profile")
};



		HO_PROFILE.bInited = 1;
	}
};




//== USERS CLASS ===============================================================
function HO_USERS(){
	this.sName = "USERS";
	this.iObjNo = 0;
	
	if (typeof HO_USERS.bInited == "undefined"){

HO_USERS.prototype.init = function(){
	console.log("users")
};



		HO_USERS.bInited = 1;
	}
};



//== GRID CLASS ================================================================
function HO_GRID(){
	this.aData = [];
	this.aDataFiltered = 0;
	this.oDiv = {};
	this.oEle = {};
	this.iHeight = 0;
	this.sName = "grid";
	this.iNumRecords = 0;
	this.iNumRecordsFiltered = 0;
	this.iPagesMax = 1;
	this.iPageNumber = 1;
	
	if (typeof HO_GRID.bInited == "undefined"){


HO_GRID.prototype.action = function(sAction, aParams){
};



HO_GRID.prototype.header = function(){
	var that = this;
	this.oDiv.toolbar = E.div(this.oDiv.header, "gridtoolbar");
	this.oDiv.search = E.input({divParent:this.oDiv.toolbar, 
		sText:"", sClassName:"gridsearch",
		sID:"gridsearch_" + this.oEle.iGridNo});
	V.oDiv.columns = [];
	var iW = 100; 
	this.oEle.aColumns.forEach(function(oRec){
		var divX = E.div(that.oDiv.header, "gridcolumnheading");
		var divY = E.div(that.oDiv.box, "gridcolumn");
		V.oDiv.columns.push(divY);
		if (oRec.iWidth){
			divX.style.width = (oRec.iWidth) + "%";
			divY.style.width = (oRec.iWidth) + "%";
			iW -= oRec.iWidth;
		} else {
			divX.style.width = iW + "%";
			divY.style.width = iW + "%";
		}
		E.text({divParent:divX, sText:oRec.sName});
	});
	
};



HO_GRID.prototype.init = function(){
	this.header();
	for (var iI = 0; iI < 10; iI++){
		for (var iJ = 0; iJ < 5; iJ++){
			var divX = V.oDiv.columns[iJ];
			var divCell = E.div(divX, "gridcell");
			E.text({divParent:divCell, sText:(iI + " " + iJ)});
		}
	}
};


		HO_GRID.bInited = 1;
	}
};








V.oGen.bJSLoaded = 1;
